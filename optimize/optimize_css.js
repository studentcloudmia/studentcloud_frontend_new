var cleanCSS = require('clean-css');
var fs = require('fs');

var CSSsource = "";

//get css file
// Make sure we got a filename on the command line.
if (process.argv.length < 3) {
    console.log('Usage: node ' + process.argv[1] + ' FILENAME');
    process.exit(1);
}
// Read the file and print its contents.
var filename = process.argv[2];

fs.readFile(filename, 'utf8',
function(err, data) {
    if (err) throw err;
    console.log('OK: ' + filename);
    console.log('Start minizing...');
	//minize css
    var minimized = cleanCSS.process(data);
	var minFileName = filename.substring(0, filename.length - 4) + '-minified.css';
	console.log('Create minimized file: ', minFileName);
	//save minized css in _minized file
    fs.writeFile(minFileName, minimized,
    function(err) {
        if (err) return console.log('Error at save: ', err);
    });
});



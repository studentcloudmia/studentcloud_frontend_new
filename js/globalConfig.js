// Filename: globalConfig.js
define([
    'jquery',
    'storageApi'
],
        function($) {

            var GC = {
                initialize: function() {
                    //any application init code goes here
                    if (window.innerWidth > 700) {
                        this.tablet = true;
                    }
                    else{
                        this.tablet = false;
                    }
                    // alert('Am I a tablet: ' + this.tablet);
                },
                tablet: false,
                loggedIn: false,
                router: {},
                api: {
                    //Dev site
                    //host: "https://us.studentcloud.com",
                    //port: "443",
                    host: "http://54.235.203.86",
                    port: 8082,
                    apiPreceder: "/api/"

                            //local dev
                            // host: "http://ec2-54-221-220-169.compute-1.amazonaws.com",
                            // port: "8081",
                            // apiPreceder: "/api/"

                            //local dev 2
                            // host: "http://ec2-23-21-121-17.compute-1.amazonaws.com",
                            // port: "8081",
                            // apiPreceder: "/api/"

                            //Dev site 2
                            // host: "https://dev.studentcloud.com",
                            // port: "443",
                            // apiPreceder: "/api/"

                            //Test site
                            //host: "https://test.studentcloud.com",
                            //port: "443",
                            //apiPreceder: "/api/"

                            //Alex machine
                            // host: "http://192.168.10.199",
                            // port: "8000",
                            // apiPreceder: "/api/"

                            //Alex2 Machine
                            // host: "http://192.168.10.172",
                            // port: "8000",
                            // apiPreceder: "/api/"
                },
                timeout: 50000, //50 seconds

                //storage
                localStorage: $.initNamespaceStorage('SC_default').localStorage,
                sessionStorage: $.initNamespaceStorage('SC_default').sessionStorage,
                pickadateDefaults: {
                    min: -365 * 3,
                    max: 365 * 3,
                    format: 'mm/dd/yyyy',
                    selectYears: true,
                    selectMonths: true,
                    container: '#fullPage'
                }

            };

            /*generate api url*/
            GC.api.url = GC.api.host + ":" + GC.api.port + GC.api.apiPreceder;

            return GC;

        });

define(function(require) {
    "use strict";
    return {
        weather				: require('tpl!templates/widgets/weather.tmpl'),
        notification		: require('tpl!templates/widgets/notification.tmpl'),
        classSchedule		: require('tpl!templates/widgets/classSchedule.tmpl'),
        majorMap			: require('tpl!templates/widgets/majorMap.tmpl'),
        faPending			: require('tpl!templates/widgets/faPending.tmpl')
    };
});


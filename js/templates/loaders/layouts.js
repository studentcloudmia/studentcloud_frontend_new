//layout template loader
define(function(require){
  "use strict";
  return {
    appLayoutOne        	: require('tpl!templates/layouts/appLayoutOne.tmpl'),
    sidebarLayout        	: require('tpl!templates/layouts/sidebarLayout.tmpl'),
	dashboardLayoutOne		: require('tpl!templates/layouts/dashboardLayoutOne.tmpl')
  };
});
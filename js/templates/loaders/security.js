
define(function(require){
  "use strict";
  return {
    login        		: require('tpl!templates/security/login.tmpl'),
	sidebarMain			: require('tpl!templates/security/sidebarMain.tmpl'),
	sidebarHandle		: require('tpl!templates/security/sidebarHandle.tmpl')
  };
});


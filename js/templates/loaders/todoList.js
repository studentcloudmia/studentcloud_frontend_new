
define(function(require){
  "use strict";
  return {
    todoItemView        : require('tpl!templates/todoList/todoItemView.tmpl'),
    todosCompositeView  : require('tpl!templates/todoList/todoListCompositeView.tmpl'),
    footer              : require('tpl!templates/todoList/footer.tmpl'),
    header              : require('tpl!templates/todoList/header.tmpl')
  };
});


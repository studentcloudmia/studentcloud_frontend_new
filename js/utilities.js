// Filename: utilities.js
define([
    'jquery'
],
        function($) {

            var Utilities = {
                checkBoxConvert: function(val) {
                    if (val)
                        return 1;
                    else
                        return 0;
                },
                
                isHttps: function(){
                    return location.protocol == 'https:';
                },
				isTouchDevice: function() {
                    return ('ontouchstart' in document && !('callPhantom' in window));
                }

            };

            return Utilities;

        });

// major map widget sub module
define(['marionette',
    'app',
    'vent',
    'modules/academics/schedule/widget/views/WidgetLayoutView',
    //controllers
    'modules/academics/schedule/widget/controllers/NotificationCountController',
    'modules/academics/schedule/widget/controllers/NotificationPopupController',
    'modules/academics/schedule/widget/controllers/FilterController',
    'modules/academics/schedule/widget/controllers/FooterController',
    'modules/academics/schedule/widget/controllers/WeekViewController',
    'modules/academics/schedule/widget/controllers/DayViewController',
    'modules/academics/schedule/widget/controllers/NoteController',
    //entities
    'entities/collections/schedule/ScheduleCollection',
    'entities/models/schedule/NoteModel',
    'entities/models/notifications/WeatherModel',
    'entities/collections/schedule/NotesCollection',
    'entities/collections/BaseCollection'
    ],
function(Marionette, App, vent, WidgetLayoutView, 
    //controllers
    NotificationCountController, NotificationPopupController, FilterController, FooterController, WeekViewController, DayViewController, NoteController, 
    //entities
    ScheduleCollection, NoteModel, WeatherModel, NotesCollection, BaseCollection) {

    var ScheduleModule = App.module('Academics.Schedule.Widget');

    ScheduleModule.Controller = Marionette.Controller.extend({

        currentView: '',
        
        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
			this.layout = new WidgetLayoutView();
            
            this.collection = new ScheduleCollection();
            this.newClassColl = new BaseCollection();
            this.notesCollection = new NotesCollection();
            this.weatherModel = new WeatherModel({forecast: true});
            this.newClassArr_holder = [];
            this.filterArr = ['teaching', 'notes', 'attending', 'acad'];
            this.daysArr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            
            //close controller when layout closes
            this.listenTo(this.layout, 'close', function(){
                self.close();
            }, this);    

            this.listenTo(this.layout, 'show', function(){
                this.showFilters();
                this.showNotificationCount();
                this.showFooter();
                
            }, this);
            
            this.listenTo(this.weatherModel, 'change', this.UpdateWeather);
            this.listenTo(this.collection, 'reset', this.collectionReset);
            this.listenTo(this.notesCollection, 'reset', this.notesCollectionReset);
            
            this.region.show(this.layout);
            
            //fetch weather to start it all
            this.weatherModel.getWeather();
        },
        
        fetchAll: function(){
            this.newClassArr_holder = [];
            this.collection.fetch({reset: true, loader: false});
            this.notesCollection.fetch({reset: true, loader: false});
        },
        
        UpdateWeather: function(){
            //set flag
            this.weatherIn = true;
            this.fetchAll();
        },
        
        getIcon: function(dateIn){
            var reading = this.weatherModel.get(dateIn);
            if(reading){
                return {icon_url: reading.icon_url, conditions: reading.conditions};
            }else{
                return false;
            }
            
        },
        
        collectionReset: function(){
            this.prepareCollection();
            this.filterAll();
            this.showWeekView();
        },
        
        notesCollectionReset: function(){
            this.prepareNotesCollection();
            this.filterAll();
            this.showWeekView();
        },
        
        showFilters: function(){
            this.filterCont = new FilterController({region: this.layout.widgFilters});
            this.activateFilterListeners();
        },
        
        showNotificationCount: function(){
            this.notifCountController = new NotificationCountController({region: this.layout.notificationBadge});
            this.listenTo(this.notifCountController, 'selected', this.notifCountSelected);
        },
        
        showFooter: function(){
            this.footerCont = new FooterController({region: this.layout.schedFooter});
            this.activateFooterListeners();
        },
        
        activateFilterListeners: function(){
            this.listenTo(this.filterCont, 'filter', this.filterAction);
        },
        
        activateFooterListeners: function(){
            this.listenTo(this.footerCont, 'action', this.footerAction);
        },
        
        filterAction: function(action){
            switch (action) {
                case 'all':
                    this.filterAll();
                    break;
                default:
                    this.updateFilterArr(action);
            }
        },
        
        footerAction: function(action){
            switch (action) {
            case 'week':
                this.showWeekView();
                this.filterItems();
                break;
            case 'day':
                this.showDayView();
                break;
            case 'new':
                this.newNote();
                break;
            case 'save':
                this.saveNote();
                break;
            case 'del':
                this.delNote();
                break;
            case 'cancel':
                this.cancelNote();
                break;
            default:
                console.warn('unknown action from footer: ', action);
            }
        },
        
        ActionSelected: function(options){
            switch (options.act) {
            case 'dtls':
                vent.trigger('Academics:Enroll:ClassDtls', options.sclass);
                break;
            case 'roster':
                vent.trigger('Academics:roster', options.sclass);
                break;
            default:
                console.warn('This action has not been captured: ', options);
            }
        },
        
        showNextView: function(){
            switch (this.currentView) {
            case 'week':
                this.showDayView();
                break;
            case 'day':
                this.showWeekView();
                break;
            default:
                this.showDayView();
            }
        },
        
        showSameView: function(){
            switch (this.currentView) {
            case 'week':
                this.currentView = '';
                this.showWeekView();
                break;
            case 'day':
                this.currentView = '';
                this.showDayView();
                break;
            default:
                this.currentView = '';
                this.showWeekView();
            }
        },
        
        showWeekView: function(){
            if(this.currentView == '' || this.currentView != 'week'){
                this.viewController = new WeekViewController({region: this.layout.schedView, collection: this.newClassColl});
                this.layout.afterShow();
                this.filterCont.updateFilters(this.filterArr);
                this.currentView = 'week';
                this.listenTo(this.viewController, 'NoteSelected', this.NoteSelected);
                this.listenTo(this.viewController, 'ActionSelected', this.ActionSelected);
            }            
        },
        
        showDayView: function(){
            if(this.currentView == '' || this.currentView != 'day'){
                this.viewController = new DayViewController({region: this.layout.schedView, collection: this.newClassColl});
                this.layout.afterShow();
                this.filterCont.updateFilters(this.filterArr);
                this.currentView = 'day';
                this.listenTo(this.viewController, 'NoteSelected', this.NoteSelected);
            }            
        },
        
        NoteSelected: function(noteId){
            //get note model form collection
            this.noteModel = new NoteModel(this.notesCollection.get(noteId).toJSON());
            //bring in new note controller
            this.noteController = new NoteController({region: this.layout.schedView, model: this.noteModel});
            //hide filters
            this.filterCont.showDtlNote();
            //hide day, week and new buttons. Show Save, Cancel buttons
            this.footerCont.showDtlNote();
        },
        
        newNote: function(){
            this.noteModel = new NoteModel();
            //bring in new note controller
            this.noteController = new NoteController({region: this.layout.schedView, model: this.noteModel});
            //hide filters
            this.filterCont.showNewNote();
            //hide day, week and new buttons. Show Save, Cancel buttons
            this.footerCont.showNewNote();
        },
        
        cancelNote: function(){
            //show filters
            this.filterCont.showFilters();
            //show day, week and new buttons. hide Save, Cancel buttons
            this.footerCont.showSched();
            //show same view
            this.showSameView();
        },
        
        saveNote: function(){
            var canSave = this.noteController.attemptSave();
            if(canSave){
                this.noteModel.save();
                this.listenToOnce(this.noteModel, 'created', function(e){
                    this.fetchAll();
                    this.cancelNote();
                }, this);
                this.listenToOnce(this.noteModel, 'updated', function(e){
                    this.fetchAll();
                    this.cancelNote();
                }, this);
            }else{
                console.warn('we cannot save');
            }
        },
        
        delNote: function(){
            vent.trigger('Components:Alert:Confirm', {
                heading: 'Delete',
                message: 'Are you sure you want to delete this note?',
                cancelText: 'Cancel',
                confirmText: 'Delete',
                callee: this,
                callback: this.delNoteConfirm
            });
        },
        
        delNoteConfirm: function(){
            this.noteModel.destroy();
            this.listenToOnce(this.noteModel, 'sync:stop', function(e){
                this.fetchAll();
                this.cancelNote();
            }, this);
        },
        
        prepareCollection: function(){
            var self = this;
            var CollJSON = this.collection.toJSON();
            
            //split all classes by the days in their schedule
            var todayName = moment().format('dddd');
            var todayIndex = _.indexOf(this.daysArr, todayName);
            var todayWeek = moment().week();
            
            _.each(CollJSON, function(cls){
                _.each(cls.schedule, function(sched){
                    var currDate = moment().add('d', _.indexOf(self.daysArr, sched.day) - todayIndex).format('MM/DD/YYYY');
                    var start_time = moment(sched.start_time, 'hh:mm:ss').format('hh:mma');
                    var end_time = moment(sched.end_time, 'hh:mm:ss').format('hh:mma');
                    if(todayWeek == moment(currDate, 'MM/DD/YYYY').week()){
                        self.newClassArr_holder.push({cls: cls, sched: sched, day: sched.day, extra: moment(currDate, 'MM/DD/YYYY').format('MMM Do'), date: currDate, start_time: start_time, end_time: end_time, weather: false, type: cls.type});
                    }
                });
            });
            
            this.newClassArr_holder = _.sortBy(self.newClassArr_holder, function(cls) { return _.indexOf(self.daysArr, cls.day); });
            
            //remove the repeated days
            var lastDay = '';            
            _.each(self.newClassArr_holder, function(cls){
                
                if(self.weatherIn){
                    //get weather icon
                    cls.weather = self.getIcon(moment(cls.date, 'MM/DD/YYYY').format('YYYYMMDD'));
                }
                
                if(cls.day == lastDay){
                    cls.day = '';
                }else{
                    lastDay = cls.day;
                }
            });
        },
        
        prepareNotesCollection: function(){
            var self = this;
            var CollJSON = this.notesCollection.toJSON();
            
            //split all classes by the days in their schedule
            var todayName = moment().format('dddd');
            var todayIndex = _.indexOf(this.daysArr, todayName);
            var thisDay = '';
            var todayWeek = moment().week();
            
            _.each(CollJSON, function(note){
                //only add note if id is present
                if(note.id){
                    thisDay = moment(note.udate, 'MM/DD/YYYY').format('dddd');
                    var currDate = moment().add('d', _.indexOf(self.daysArr, thisDay) - todayIndex).format('MM/DD/YYYY');
                    // adding sched.day so it works with Day view
                    if(todayWeek == moment(note.udate, 'MM/DD/YYYY').week()){
                        self.newClassArr_holder.push({note: note, day: thisDay, extra: moment(currDate, 'MM/DD/YYYY').format('MMM Do'), sched: {day: thisDay}, date: currDate, weather: false, type: 'notes'});
                    }
                }
            });
            
            /*Add temporary events*/
            var newNote = {
                title: 'Last day to Add/Drop',
                udate: '10/08/2013',
                utime: '11:59 pm'
            };
            var currDate = moment().add('d', _.indexOf(self.daysArr, 'Tuesday') - todayIndex).format('MM/DD/YYYY');
            var thisDay = 'Tuesday';
            //only push if its part of this week
            if(todayWeek == moment(newNote.udate, 'MM/DD/YYYY').week()){
                self.newClassArr_holder.push({note: newNote, day: thisDay, extra: moment(currDate, 'MM/DD/YYYY').format('MMM Do'), sched: {day: thisDay}, date: currDate, weather: false, type: 'acad'});
            }

            var newNote = {
                title: 'Last day to Add/Drop',
                udate: '10/15/2013',
                utime: '11:59 pm'
            };
            var currDate = moment().add('d', _.indexOf(self.daysArr, 'Tuesday') - todayIndex).format('MM/DD/YYYY');
            var thisDay = 'Tuesday';
            //only push if its part of this week
            if(todayWeek == moment(newNote.udate, 'MM/DD/YYYY').week()){
                self.newClassArr_holder.push({note: newNote, day: thisDay, extra: moment(currDate, 'MM/DD/YYYY').format('MMM Do'), sched: {day: thisDay}, date: currDate, weather: false, type: 'acad'});
            }
            var newNote = {
                title: 'Acc Game: SC vs UM',
                udate: '10/12/2013',
                utime: '4:00 pm'
            };
            var currDate = moment().add('d', _.indexOf(self.daysArr, 'Saturday') - todayIndex).format('MM/DD/YYYY');
            var thisDay = 'Saturday';
            //only push if its part of this week
            if(todayWeek == moment(newNote.udate, 'MM/DD/YYYY').week()){
                self.newClassArr_holder.push({note: newNote, day: thisDay, extra: moment(currDate, 'MM/DD/YYYY').format('MMM Do'), sched: {day: thisDay}, date: currDate, weather: false, type: 'acad'});
            }
            
            this.newClassArr_holder = _.sortBy(self.newClassArr_holder, function(cls) { return _.indexOf(self.daysArr, cls.day); });
            
            console.log('added new note: ', self.newClassArr_holder);
            
            //remove the repeated days
            var lastDay = '';
            
            _.each(self.newClassArr_holder, function(cls){
                
                if(self.weatherIn){
                    //get weather icon
                    cls.weather = self.getIcon(moment(cls.date, 'MM/DD/YYYY').format('YYYYMMDD'));
                }
                
                if(cls.day == lastDay){
                    cls.day = '';
                }else{
                    lastDay = cls.day;
                }
            });
        },
        
        filterAll: function(){
            this.filterArr = ['teaching', 'notes', 'attending', 'acad'];
            this.filterItems();
        },
        
        updateFilterArr: function(filterName){
            if(_.contains(this.filterArr, filterName)){
                this.filterArr = _.without(this.filterArr, filterName);
            }else{
                this.filterArr.push(filterName);
            }
            this.filterItems();
        },
        
        filterItems: function(){
            var self = this;
            //loop through array and only show items filtered
            var filteredItems = _.compact(_.map(self.newClassArr_holder, function(item){
                if(_.contains(self.filterArr, item.type))
                    return item;
            }));
            this.filterCont.updateFilters(this.filterArr);
            this.newClassColl.reset(filteredItems);
            this.layout.afterShow();
        },
        
        showClassDtls: function(sclass){
            vent.trigger('Academics:Enroll:ClassDtls', sclass);
        },
        
        notifCountSelected: function(){
            if(this.notisPopupController){
                this.notisPopupController.view.close();
                this.notisPopupController = false;
            }else{
                //show popup with notifications
                this.notisPopupController = new NotificationPopupController({region: this.layout.notificationPopup});
            }            
        }

    });

    return ScheduleModule;
});
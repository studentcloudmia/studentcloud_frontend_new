// notificationCount view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/notificationCount.tmpl'
],function(_, Marionette, vent, App, template){
	
	var NotificationCountView = App.Views.ItemView.extend({
		template: template,
        tagName: 'span',
        className: 'badge badge-important circle-badge',
        
        triggers: {
            'click': 'selected'
        }

	});
	
	return NotificationCountView;
	
});
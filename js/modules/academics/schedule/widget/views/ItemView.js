// item view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/_class.tmpl',
	'tpl!modules/academics/schedule/widget/templates/_note.tmpl',
	'tpl!modules/academics/schedule/widget/templates/_acad.tmpl'
],function(_, Marionette, vent, App, classTemp, noteTemp, acadTemp){
	
	var WeekView = App.Views.ItemView.extend({
		template: classTemp,
        className: 'daysCollCont',
        
        initialize: function(){                
            switch (this.model.get('type')) {
            case 'notes':
                this.template = noteTemp;
                break;
            case 'acad':
                this.template = acadTemp;
                break;
            default:
                this.template = classTemp;
            }
        },
        
        events: {
            'click .menuIcon': 'actionSel'
        },
        
        onRender: function(){
            var self = this;
            if(this.model.get('type') != 'notes'){
                
                var radius = (this.model.get('type') != 'teaching') ? 110 : 90;
                
                this.$el.find('.circle_menu').circleMenu({
                    item_diameter: 35,
                    pointer_diameter: 40,
                    circle_radius: radius,
                    trigger: 'click',
                    angle: {start: -20, end: 40}
                });
            
                this.$el.find('.schedItemCont').on('click', function(){
                    self.$el.find('.showBubble').trigger('click');
                });
                
            }else{
                this.$el.find('.schedItemCont').on('click', function(){
                    self.trigger('NoteSelected');
                });
            }         
        },
        
        actionSel: function(e){
            var $sel = $(e.currentTarget);
            this.act = $sel.data('action');
            if(this.model.get('type') != 'notes'){
                this.sclass = this.model.get('cls').sclass;
            }
            this.trigger('action');
        },
        
        onClose: function(){
            this.$el.find('.schedItemCont').off('click');
        }

	});
	
	return WeekView;
	
});
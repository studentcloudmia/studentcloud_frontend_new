// day view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/academics/schedule/widget/views/ItemView',
	'tpl!modules/academics/schedule/widget/templates/day.tmpl',
	'tpl!modules/academics/schedule/widget/templates/empty.tmpl'
],function(_, Marionette, vent, App, ItemView, template, emptyTemplate){
	
	var DayView = App.Views.CompositeView.extend({
		template: template,
        className: 'iScrollWrapper animated fadeInRight',
        itemView: ItemView,
        itemViewContainer: '.dayCont',
        
        initialize: function(){
            this.listenTo(this, 'childview:NoteSelected', this.NoteSelected);
            this.listenTo(this, 'childview:action', this.ActionSelected);  
        },
        
        ActionSelected: function(v){
            var sclass = v.sclass || '';
            var options = {act: v.act, sclass: sclass};
            this.trigger('ActionSelected', options);
        },
        
        NoteSelected: function(v){
            this.trigger('NoteSelected',v.model.get('note').id);
        },
        
        showEmpty: function(dayName){
            this.$el.html(emptyTemplate({day: dayName}));
        }

	});
	
	return DayView;
	
});
// filter view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/footerSched.tmpl',
	'tpl!modules/academics/schedule/widget/templates/footerNote.tmpl'
],function(_, Marionette, vent, App, templateSched, templateNote){
	
	var FooterView = App.Views.ItemView.extend({
		template: templateSched,
		templateNote: templateNote,
        
        className: 'bottomMenu navbar navbar-fixed-bottom',
        
        events: {
            'click span': 'actionSel'
        },
        
        showNewNote: function(){
            this.$el.html(this.templateNote());
            //hide delete button
            this.$el.find('.btnDelete').addClass('nonvisible');
        },
        
        showDtlNote: function(){
            this.$el.html(this.templateNote());
        },
        
        showSched: function(){
            this.$el.html(this.template());          
        },
        
        actionSel: function(e){
            var $sel = $(e.currentTarget);
            this.trigger('action', $sel.data('action'));
        }

	});
	
	return FooterView;
	
});
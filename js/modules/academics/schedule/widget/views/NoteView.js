// week view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/academics/schedule/widget/templates/note.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var NoteView = App.Views.ItemView.extend({
		template: template,
        tagName: 'form',
        className: 'animated fadeIn',
        
        ui: {
            validationBtn: '.schedValidationBtn'
        },
        
        onRender: function(){
            this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            this.$el.find('.mobiTime').mobiscroll().time({
                theme: 'ios',
                display: 'bubble',
                mode: 'scroller'
            });
            this.$el.find("input,select").not("[type=submit] , .datepicker , .mobiTime").jqBootstrapValidation();
        },
        
        attemptSave: function(){
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        }

	});
	
	return NoteView;
	
});
// NotificationPopup view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/notificationPopup.tmpl'
],function(_, Marionette, vent, App, template){
	
	var NotificationPopupView = App.Views.CompositeView.extend({
		template: template,
        tagName: 'div',
        className: 'notificationPopup',
        
        render: function(){
            var compiledTemplate = this.template();
			this.popElem = $('.notificationPopup');
            //create popover
            this.popElem.popover2({
                id: 'notificationPopover',
                position: 'right',
                trigger: 'none',
                content: compiledTemplate,
                verticalOffset: 30
            });
            
            //create iScroll
            var options = {
                // click: true,
                scrollY: true,
                momentum: true,
                bounceEasing: 'elastic',
                bounceTime: 1200,
                //scrollbars
                scrollbars: false,
                mouseWheel: true,
                interactiveScrollbars: false,
                onBeforeScrollStart: function(e) {
                    e.stopPropagation();
                }
            }            
            
            this.popElem.popover2('show');
            
            var myScroll = new IScroll('.notificationPopoveriScrollWrapper', options);
            
            //increase width now
            $(' #notificationPopover .wrap .content2').css('max-height', '300px');            
        },
		
		onBeforeClose: function(){
			this.popElem.popover2('destroy');
			this.popElem = null;
			this.shown = false;
		}

	});
	
	return NotificationPopupView;
	
});
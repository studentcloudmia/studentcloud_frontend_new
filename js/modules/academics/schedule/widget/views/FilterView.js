// filter view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/filter.tmpl'
],function(_, Marionette, vent, App, template){
	
	var FilterView = App.Views.ItemView.extend({
		template: template,
        tagName: 'ul',
        className: 'nav nav-pills',
        
        ui: {
            'filAll': '.filAll',
            'teaching': '.filTeach',
            'learning': '.filLearn',
            'note': '.filNotes'
        },
        
        events: {
            'click .filAll': 'filAll',
            'click .fil': 'filSel'
        },
        
        filAll: function(e){
            this.trigger('filter', {action: 'all'});
        },
        
        filSel: function(e){
            var $sel = $(e.currentTarget);
            this.trigger('filter', {action: $sel.data('action')});
        },
        
        updateFilters: function(filArr){
            var self = this;
            this.clearSelected();
            _.each(filArr, function(filter){
                switch (filter) {
                case 'teaching':
                    self.ui.teaching.addClass('active');
                    break;
                case 'attending':
                    self.ui.learning.addClass('active');
                    break;
                case 'notes':
                    self.ui.note.addClass('active');
                    break;
                default:
                    
                }
            });
        },
        
        showNewNote: function(){
            this.$el.find('li').hide();
            this.$el.find('.note > h3').html('New Item');
            this.$el.find('.note').show();
        },
        
        showDtlNote: function(){
            this.$el.find('li').hide();
            this.$el.find('.note > h3').html('Item Details');
            this.$el.find('.note').show();       
        },
        
        showFilters: function(){
            this.$el.find('li').show();
            this.$el.find('.note').hide();
        },
        
        clearSelected: function(){
            this.$el.find('li').removeClass('active');
        }

	});
	
	return FilterView;
	
});
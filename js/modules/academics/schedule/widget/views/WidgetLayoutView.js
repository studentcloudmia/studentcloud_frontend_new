// major map widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/schedule/widget/templates/widgetLayout.tmpl'
],function(_, Marionette, vent, App, template){
	
	var WidgetView = App.Views.Layout.extend({
		template: template,
        className: 'scheduleWidget',
        
        regions: {
            widgFilters: '.widgFilters',
            notificationBadge: '.notificationBadge',
            notificationPopup: '.notificationPopup',
            schedView: '.schedView',
            notesView: '.notesView',
            schedFooter: '.schedFooter'
        },
        
        flipContainer: function(){
            //find out which face is active
            this.$el.find('.schedContainer').toggleClass('flipped');
        }

	});
	
	return WidgetView;
	
});
// week view for schedule widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/academics/schedule/widget/views/ItemView',
	'tpl!modules/academics/schedule/widget/templates/week.tmpl'
],function(_, Marionette, vent, App, ItemView, template){
	
	var WeekView = App.Views.CompositeView.extend({
		template: template,
        className: 'iScrollWrapper animated fadeInLeft',
        itemView: ItemView,
        itemViewContainer: '.dayCont',
        
        initialize: function(){
            this.listenTo(this, 'childview:NoteSelected', this.NoteSelected);
            this.listenTo(this, 'childview:action', this.ActionSelected);           
        },
        
        ActionSelected: function(v){
            var sclass = v.sclass || '';
            var options = {act: v.act, sclass: sclass};
            this.trigger('ActionSelected', options);
        },
        
        NoteSelected: function(v){
            this.trigger('NoteSelected',v.model.get('note').id);
        }

	});
	
	return WeekView;
	
});
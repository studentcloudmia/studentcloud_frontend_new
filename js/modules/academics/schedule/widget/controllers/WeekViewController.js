// schedule widg week view controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/controllers/SchedViewController',
    'modules/academics/schedule/widget/views/WeekView'
    ],
function(Marionette, App, vent, SchedViewController, WeekView) {

    WeekViewController = SchedViewController.extend({

        initialize: function(options) {
            var self = this;
            this.region = options.region;
            this.collection = options.collection;
            
			this.view = new WeekView({collection: this.collection});
            
            this.listenTo(this.view, 'NoteSelected', this.NoteSelected);
            this.listenTo(this.view, 'ActionSelected', this.ActionSelected);            
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.region.show(this.view);
        }

    });

    return WeekViewController;
});
// schedule widg notification popup controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/views/NotificationPopupView'
    ],
function(Marionette, App, vent, NotificationPopupView) {

    NotificationPopupController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
			this.view = new NotificationPopupView();
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.view, 'selected', this.sel);
            
            this.region.show(this.view);
        },
        
        sel: function(){
            this.trigger('selected');
        }

    });

    return NotificationPopupController;
});
// schedule widg filters controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/views/FilterView'
    ],
function(Marionette, App, vent, FilterView) {

    FilterController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
			this.view = new FilterView();
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.view, 'filter', this.filSelected);
            
            this.region.show(this.view);
        },
        
        filSelected: function(options){
            var action = options.action;
            this.trigger('filter', action);
        },
        
        updateFilters: function(filArr){
            this.view.updateFilters(filArr);
        },
        
        showNewNote: function(){
            this.view.showNewNote();
        },
        
        showDtlNote: function(){
            this.view.showDtlNote();
        },
        
        showFilters: function(){
            this.view.showFilters();
        }

    });

    return FilterController;
});
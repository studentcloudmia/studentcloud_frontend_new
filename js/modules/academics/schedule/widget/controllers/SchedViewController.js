// schedule widg views controller
define(['marionette',
    'app',
    'vent'
    ],
function(Marionette, App, vent) {

    ViewController = Marionette.Controller.extend({
        
        NoteSelected: function(noteId){
            this.trigger('NoteSelected', noteId);
        },
        
        ActionSelected: function(options){
            this.trigger('ActionSelected', options);
        },
        
        onClose: function(){
            
        }

    });

    return ViewController;
});
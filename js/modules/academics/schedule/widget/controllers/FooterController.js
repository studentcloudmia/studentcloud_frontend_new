// schedule widg footer controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/views/FooterView'
    ],
function(Marionette, App, vent, FooterView) {

    FooterController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
			this.view = new FooterView();
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.view, 'action', this.Action);
            
            this.region.show(this.view);
        },
        
        Action: function(action){
            this.trigger('action', action);
        },
        
        showNewNote: function(){
            this.view.showNewNote();
        },
        
        showDtlNote: function(){
            this.view.showDtlNote()
        },
        
        showSched: function(){
            this.view.showSched();
        }

    });

    return FooterController;
});
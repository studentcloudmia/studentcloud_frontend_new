// schedule widg day view controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/controllers/SchedViewController',
    'modules/academics/schedule/widget/views/DayView'
    ],
function(Marionette, App, vent, SchedViewController, DayView) {

    DayViewController = SchedViewController.extend({

        initialize: function(options) {
            var self = this;
            this.region = options.region;
            this.collection = options.collection;
            
			this.view = new DayView({collection: this.collection});
            this.dayName = moment().format('dddd');
            // this.dayName = 'Monday';
            this.filterDay();
            
            this.listenTo(this.view, 'NoteSelected', this.NoteSelected);
            this.listenTo(this.view, 'ActionSelected', this.ActionSelected); 
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.collection, 'reset', this.filterDay);
            
            this.region.show(this.view);
        },
        
        filterDay: function(){
            var self = this;
            var itemsArr = _.reject(self.collection.toJSON(), function(item){                
                return item.sched.day != self.dayName;                    
            });
            this.collection.set(itemsArr, {silent: true});

            if(itemsArr.length == 0){
                this.view.showEmpty(this.dayName);
            }else{
                this.view.render();
            }
            
        }

    });

    return DayViewController;
});
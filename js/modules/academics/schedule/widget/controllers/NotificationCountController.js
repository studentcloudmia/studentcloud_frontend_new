// schedule widg notification count controller
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/schedule/widget/views/NotificationCountView'
    ],
function(Marionette, App, vent, NotificationCountView) {

    NotificationCountController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
			this.view = new NotificationCountView();
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.view, 'selected', this.sel);
            
            this.region.show(this.view);
        },
        
        sel: function(){
            this.trigger('selected');
        }

    });

    return NotificationCountController;
});
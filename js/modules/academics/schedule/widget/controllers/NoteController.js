// schedule notes view controller
define(['marionette',
    'app',
    'vent',
    'modules/academics/schedule/widget/views/NoteView'
    ],
function(Marionette, App, vent, NoteView) {

    WeekViewController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            this.region = options.region;
            this.model = options.model;
            
			this.view = new NoteView({model: this.model});
            
            //close controller when view closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.region.show(this.view);
        },
        
        attemptSave: function(){
            return this.view.attemptSave();
        }

    });

    return WeekViewController;
});
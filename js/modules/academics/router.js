define([
    'marionette',
    'app',
],
        function(Marionette, app) {

            var Router = Marionette.AppRouter.extend({
                appRoutes: {
                    'enroll': 'enroll',
                    'roster(/:id)': 'roster'
                }
            });

            return Router;

        });

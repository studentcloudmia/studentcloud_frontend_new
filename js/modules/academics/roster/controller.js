// roster sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/roster/controllers/MainController'
],
        function(Marionette, App, vent, MainController) {

            var RosterModule = App.module('Academics.Roster');

            RosterModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.fetchId = options.fetchId;
                    this.layout = App.request("Academics:getLayout");

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHeader({
                            region: this.layout.header,
                            heading: 'Roster',
                            links: ""
                        });
                        this.showController();
                    }, this);

                    //show layout
                    this.region.show(this.layout);
                    //Listen to components click
                    this.listenTo(vent, "Components:Builders:Header:triggerInfoClick", function() {
                        self.mainController.showClassDetails(self.classDetails);
                    });
                },
                showHeader: function(options) {
                    vent.trigger('Components:Builders:Header:on', options);
                },
                updateHeader: function(options) {
                    vent.trigger('Components:Builders:Header:changeDetailsManual', options);
                },
                showController: function() {
                    this.mainController = new MainController({region: this.layout.mainContent, layout: this.layout, fetchId: this.fetchId});
                    this.controllerListeners();
                },
                controllerListeners: function() {
                    var self = this;
                    this.listenTo(this.mainController, 'updateHeader', function(topInfo) {
                        this.classDetails = topInfo;
                        var info = new Array;
                        info.push('<a class="triggerClassInfo noLink">' + topInfo.get('course_catalog_id') + '</a> - ');
                        info.push(topInfo.get('term'));
                        info.push('[ Enrolled: ' + topInfo.get('number_enrolled') + ' ]');
                        info.push('<span class=" badge badge-info largeInside topInfoBadge rosterStatusBadge" style="background-color:transparent!important;">' + topInfo.get('roster_status') + '</span>')
                        var trigger = '.triggerClassInfo';
                        self.updateHeader({html: info, trigger: trigger});

                    });
                },
                onClose: function() {
                    this.layout = null;
                }

            });

            return RosterModule;
        });
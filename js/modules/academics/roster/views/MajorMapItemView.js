// Major Map Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/academics/roster/templates/_majorMap.tmpl',
], function(Marionette, App, vent, template) {

    var MajorMapItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'span',
        className: 'label span12'
    });
    return MajorMapItemView;
});
// Class Details View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/academics/roster/templates/classDetail.tmpl'
], function(Marionette, App, vent, template) {

    var ClassDetailView = App.Views.ItemView.extend({
        template: template,
        events: {
            'click .closeModal': 'modalClose'
        },
        modalClose: function() {
            this.close();
        }
    });
    return ClassDetailView;
});
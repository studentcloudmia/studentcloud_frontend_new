// Major Map main view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/academics/roster/views/MajorMapItemView',
    'tpl!modules/academics/roster/templates/majorMap.tmpl'
], function(_, Marionette, vent, App, ItemView, template) {

    var MajorMapView = App.Views.CompositeView.extend({
        itemView: ItemView,
        itemViewContainer: '.majorMapInner',
        template: template,
        events: {
            'click .closeModal': 'modalClose'
        },
        modalClose: function() {
            this.close();
        }
    });

    return MajorMapView;

});
// Roster Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/academics/roster/templates/_roster.tmpl',
    'tpl!modules/academics/roster/templates/_grades.tmpl'
], function(Marionette, App, vent, template, gradesTemplate) {

    var RosterItemView = App.Views.ItemView.extend({
        template: template,
        gradesTemplate: gradesTemplate,
        className: 'well',
        ui: {
            circleMenu: '.circleMenu'
        },
        onShow: function() {
            var self = this;
            this.$el.attr("id", self.model.get('uid'));
            this.$el.find('.circleMenu_' + this.model.get('uid')).circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 70,
                trigger: 'click',
                angle: {start: 50, end: -50}
            });
            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                self.$el.find('.showBubble').trigger('click');
            });
            //selecting item
            this.$el.find('.selectOne').on('click', function() {
                if (self.$el.hasClass('selected')) {
                    self.$el.find('.checkedIcon').removeClass('icomoon-checked').addClass('icomoon-unchecked');
                    self.$el.find('.selectColor').removeClass('badge-success').addClass('badge-primary');
                    self.$el.removeClass('selected');
                }
                else {
                    self.$el.find('.checkedIcon').removeClass('icomoon-unchecked').addClass('icomoon-checked');
                    self.$el.find('.selectColor').removeClass('badge-primary').addClass('badge-success');
                    self.$el.addClass('selected');
                }
                self.trigger('multipleSelected');
            });
            //Append Major Maps
            var countbadge = '';
            if (self.model.get('major_map').length > 1)
                countbadge = ' <span class="badge showMoreMajors badge-inverse">' + self.model.get('major_map').length + '</span>';
            _.each(this.model.get('major_map'), function(item) {
                self.$el.find('.majorMap').html(item.description + countbadge);
            });
            this.$el.find('.showMoreMajors').on('click', function() {
                self.trigger("MajormapModalShow");
            });

            //Only show grade section if object Grade is not empty
            if (this.model.get('assigned_grade') != 'flase') {
                this.$el.find('.gradesSection').html(this.gradesTemplate(this.model.attributes));
                var grades = new Array();
                grades = ['- Select Grade', 'A', 'B', 'C', 'D', 'F', 'I'];
                _.each(grades, function(item) {
                    var sel
                    if (self.model.get('assigned_grade') == item)
                        sel = 'selected';
                    self.$el.find('.rosterGrade').append('<option ' + sel + '>' + item + '</option>');
                });

                var incomplete = new Array();
                incomplete = [{id: 0, val: '- Select Attendance'}, {id: 69, val: 'Never Attended'}, {id: 70, val: 'Stopped Prior to 60% Date'}, {id: 71, val: 'Stopped/Completed After 60% Date'}, {id: 72, val: 'Began and Cannot Determine'}];
                _.each(incomplete, function(item) {
                    var sel
                    if (self.model.get('attendance_option') == item.id)
                        sel = 'selected';
                    else
                        sel = '';
                    self.$el.find('.attnOpt').append('<option ' + sel + ' value=' + item.id + '>' + item.val + '</option>');
                });

                //incomplete form
                this.$el.find('.incompleteForm').on('click', function() {
                    self.trigger('showIncompleteForm');
                });
                //when grade is I
                this.$el.find('.rosterGrade').on('change', function() {
                    if (self.$el.find('.rosterGrade')[0].value == 'I') {
                        self.$el.find('.incompleteFormWrapper').removeClass('hidden');
                    }
                    if (self.$el.find('.rosterGrade')[0].value != 'I') {
                        self.$el.find('.incompleteFormWrapper').addClass('hidden');
                    }
                });
                //when grade is F
                this.$el.find('.rosterGrade').on('change', function() {
                    if (self.$el.find('.rosterGrade')[0].value == 'F') {
                        self.$el.find('.attnsOpt').removeClass('displayNone');
                    }
                    if (self.$el.find('.rosterGrade')[0].value != 'F') {
                        self.$el.find('.attnsOpt').addClass('displayNone');
                    }
                });
                //check on load

                if (self.$el.find('.rosterGrade')[0].value == 'F') {
                    self.$el.find('.attnsOpt').removeClass('displayNone');
                }
                if (self.$el.find('.rosterGrade')[0].value != 'F') {
                    self.$el.find('.attnsOpt').addClass('displayNone');
                }
                if (self.$el.find('.rosterGrade')[0].value == 'I') {
                    self.$el.find('.incompleteFormWrapper').removeClass('hidden');
                }
                if (self.$el.find('.rosterGrade')[0].value != 'I') {
                    self.$el.find('.incompleteFormWrapper').addClass('hidden');
                }
                if (this.model.get('roster_status') == "Post") {
                    this.$el.find('.gradesInput').remove();
                    this.$el.find('.gradesInputWrapper').append('<span class=" badge badge-info largeInside">' + this.model.get('official_grade') + '</span>');
                    var attnOpt;
                    if (this.model.get('attendance_option') == 69) {
                        attnOpt = 'Never Attended';
                    }
                    else if (this.model.get('attendance_option') == 70) {
                        attnOpt = 'Stopped Prior to 60% Date';
                    }
                    else if (this.model.get('attendance_option') == 71) {
                        attnOpt = 'Stopped/Completed After 60% Date';
                    }
                    else if (this.model.get('attendance_option') == 72) {
                        attnOpt = 'Began and Cannot Determine';
                    }

                    this.$el.find('.attnInput').remove();
                    this.$el.find('.attnWrapper').append('<span class=" badge badge-info largeInside">' + attnOpt + '</span>');

                    this.$el.find('.incompleteFormWrapper').html('<span class=" badge badge-info largeInside">INCOMPLETE</span>');
                }
            }
        },
        onClose: function() {
            this.$el.find('.userImg').off('click');
            this.$el.off('click');
        }
    });
    return RosterItemView;
});
// SideView
define([
    'marionette',
    'app',
    'modules/academics/roster/views/SidebarItemView',
    'tpl!modules/academics/roster/templates/sidebar.tmpl'
], function(Marionette, App, ItemView, template) {

    var SideView = App.Views.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '#classes',
        triggers: {
            'change #searchField': 'search',
            'submit #searchForm': 'search'
        },
        initialize: function(options) {
            this.colorObj = {color: 'rgb(81, 173, 219)', border: 'rgb(0, 58, 87)'};
            this.listenTo(this, 'childview:render', this.itemAdded);
        },
        ui: {
            search: '#searchField'
        },
        setTerms: function(terms) {
            var self = this;
            _.each(terms, function(term) {
                self.$el.find('#searchField').append('<option value=' + term.id + ' >' + term.description + '</option>');
            });
        },
        itemAdded: function(view) {
            //view.$el.css('background-color', this.color);
            view.$el.css('background-color', this.colorObj.color).css('border-color', this.colorObj.border);
            view.$el.find('.fc-event-title').css('color', this.colorObj.border);
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.blur();
            return val;
        }
    });

    return SideView;

});
// Incomplete Form View
define([
    'marionette',
    'app',
    'vent',
    'globalConfig',
    'tpl!modules/academics/roster/templates/incompleteForm.tmpl'
], function(Marionette, App, vent, GC, template) {

    var IncompleteFormView = App.Views.ItemView.extend({
        template: template,
        required: 0,
        events: {
            'click .closeModal': 'modalClose',
            'click .saveIncomplete': 'save'
        },
        modalClose: function() {
            this.close();
        },
        save: function() {
            var data = Backbone.Syphon.serialize(this);
            this.model.set(data);
            this.model.set('missing_assig_perc', parseInt(this.model.get('missing_assig_perc')));
            vent.trigger('Roster:Incomplete:Save');
            this.model.save();
            this.trigger('saved');
            this.close();
        },
        onShow: function() {
            var self = this;
            this.$el.find('#selectGradeEarned').html('');
            var grades = ['A', 'B', 'C', 'D', 'F'];
            var sel = '';
            _.each(grades, function(grade) {
                if (self.model.get('grade_earned_date') == grade) {
                    sel = 'selected';
                }
                else {
                    sel = '';
                }
                self.$el.find('#selectGradeEarned').append('<option ' + sel + ' value=' + grade + '>' + grade + '</option>');
            });
            self.$el.find('.required').on('keyup change', function() {
                if (self.$el.find('.required')[0].value == '' || self.$el.find('.required')[1].value == '' || self.$el.find('.required')[2].value == '' || self.$el.find('.required')[3].value == '') {
                    self.$el.find('.saveIncomplete').addClass('displayNone');
                }
                else {
                    self.$el.find('.saveIncomplete').removeClass('displayNone');
                }
            });
            _.each(self.$el.find('.required'), function(item) {
                if ($(item)[0].value == '') {
                    self.$el.find('.saveIncomplete').addClass('displayNone');
                }
            });
        },
        onRender: function() {
            //start date picker
            this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
        }
    });
    return IncompleteFormView;
});
// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/academics/roster/templates/_sidebar.tmpl'
], function(Marionette, App, vent, template) {

    var SideItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'fc-event fc-event-vert fc-event-start fc-event-end classDraggable animated flipInX',
        events: {
            'click': 'triggerSelected'
        },
        onRender: function() {
            var self = this;
            //add class id to html elem
            this.$el.attr('data-id', this.model.get('sclass'));
            this.$el.draggable({
                revert: "invalid",
                handle: "div.fc-event-inner",
                helper: "clone",
                appendTo: 'body',
                zIndex: 31, //1 greater than sidebar
                start: function() {
                    //close popover if open
                    if (self.pop) {
                        self.pop.popover('destroy');
                    }
                }
            });

        },
        triggerSelected: function(e) {
            vent.trigger('Academics:Roster:Selected:Model', this.model.get('sclass'));
        }
    });

    return SideItemView;

});
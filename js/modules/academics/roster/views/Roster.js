// roster main view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/academics/roster/views/RosterItemView',
    'tpl!modules/academics/roster/templates/roster.tmpl'
], function(_, Marionette, vent, App, ItemView, template) {

    var RosterView = App.Views.CompositeView.extend({
        itemView: ItemView,
        itemViewContainer: '.rosterInner',
        template: template,
        id: 'rosterWrapper',
        className: 'row-fluid',
        initialize: function() {
            var self = this;
            this.listenTo(this, 'childview:MajormapModalShow', this.majorMapModal);
            this.listenTo(this, 'childview:showIncompleteForm', this.incompleteForm);
            this.listenTo(vent, 'Academics:Roster:Post:Show', function() {
                self.$el.find('#postRoster').removeClass('hidden');
            });
            this.listenTo(vent, 'Academics:Roster:Post:Hide', function() {
                self.$el.find('#postRoster').addClass('hidden');
            });
            this.listenTo(vent, 'Academics:Roster:Print:Show', function() {
                self.$el.find('#printRoster').removeClass('hidden');
                self.$el.find('#selectAll').removeClass('hidden');
            });
            this.listenTo(vent, 'Academics:Roster:Print:Hide', function() {
                self.$el.find('#printRoster').addClass('hidden');
                self.$el.find('#selectAll').addClass('hidden');
            });
            this.listenTo(this, 'childview:multipleSelected', this.multiples);

        },
        onShow: function() {
            //Post Click
            var self = this;
            this.$el.find('#postRoster').on('click', function() {
                self.postGrades();
            });
            this.$el.find('#printRoster').on('click', function() {
                self.print();
            });
            this.$el.find('#selectAll').on('click', function() {
                self.selectAll();
            });

        },
        selectAll: function() {
            this.children.each(function(view) {
                view.$el.addClass('selected');
                view.$el.find('.selectColor').removeClass('badge-primary').addClass('badge-success');
                view.$el.find('.checkedIcon').removeClass('icomoon-unchecked').addClass('icomoon-checked');
            });
            this.multiples();
        },
        majorMapModal: function(view) {
            this.trigger('MajormapModalShow', view.model.get('major_map'));
        },
        incompleteForm: function(view) {
            this.trigger('showIncompleteForm', view.model.get('enrollment_id'));
        },
        //POST GRADES
        postGrades: function() {
            var grade = 0;
            var attendance = 0;
            this.children.each(function(view) {
                if (view.$el.find('.rosterGrade')[0].value == '- Select Grade') {
                    view.$el.find('.gradesInput').addClass('animated shake');
                    setTimeout(function() {
                        view.$el.find('.gradesInput').removeClass('animated shake');
                    }, 1000);
                    grade++;
                }
                if (view.$el.find('.rosterGrade')[0].value == 'F' && view.$el.find('.attnOpt')[0].value == '0') {
                    view.$el.find('.attnInput').addClass('animated shake');
                    setTimeout(function() {
                        view.$el.find('.attnInput').removeClass('animated shake');
                    }, 1000);
                    attendance++;
                }
            });
            if (grade == 0 && attendance == 0)
                this.trigger('save', {models: this.collection.models, action: 'post'});
        },
        //SAVE GRADES
        save: function() {
            var attendance = 0;
            this.children.each(function(view) {
                if (view.$el.find('.rosterGrade')[0].value == 'F' && view.$el.find('.attnOpt')[0].value == '0') {
                    view.$el.find('.attnInput').addClass('animated shake');
                    setTimeout(function() {
                        view.$el.find('.attnInput').removeClass('animated shake');
                    }, 1000);
                    attendance++;
                }
            });
            if (attendance == 0)
                this.trigger('save', {models: this.collection.models, action: 'save'});
        },
        print: function() {
            window.print();
        },
        serialize: function() {
            var data = new Array();
            this.children.each(function(view) {
                data.push(Backbone.Syphon.serialize(view));
            });
            return data;
        },
        multiples: function() {
            var self = this;
            this.emailArr = new Array();
            var itemsSelected = 0;
            this.children.each(function(view) {
                if (view.$el.hasClass('selected')) {
                    itemsSelected++;
                    self.emailArr.push('social@studentcloud.com');
                }
            });
            //saving all
            if (itemsSelected > 1) {
                this.groupActions('show');
            }
            else {
                this.groupActions('hide');
            }
        }
        ,
        groupActions: function(action) {
            if (action == 'hide')
                this.$el.find('.groupAction').addClass('hidden');
            else {
                this.$el.find('.groupAction').removeClass('hidden');
                var href = 'mailto:' + _.flatten(this.emailArr);
                this.$el.find('.multipleMail').attr('href', href);
            }


        }
    });

    return RosterView;

});
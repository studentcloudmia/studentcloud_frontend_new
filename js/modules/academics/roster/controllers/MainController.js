//roster main controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/academics/roster/views/Roster',
    'modules/academics/roster/views/MajorMaps',
    'modules/academics/roster/views/ClassDetail',
    //models
    'entities/models/roster/RosterModel',
    'entities/models/oBuilder/OBuilderModelList',
    //collections
    'entities/collections/roster/RosterCollection',
    'entities/collections/roster/ClassRosterSearchCollection',
    'entities/collections/BaseCollection',
    //cotrollers
    'modules/academics/roster/controllers/SideController',
    'modules/academics/roster/controllers/IncompleteFormController'

],
        function(Marionette, App, vent, RosterView, MajorMapView, ClassDetail, RosterModel, OBuilderModel,
                RosterCollection, SearchCollection, BaseCollection, SideController, IncompleteFormController) {

            var MainController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.fetchId = options.fetchId;
                    this.topInfo = new RosterModel();
                    this.collection = new RosterCollection();
                    this.searchCollection = new SearchCollection();
                    this.view = new RosterView({collection: this.collection});
                    //show view
                    this.region.show(this.view);
                    this.viewListeners();
                    //Drop or Select Search Result
                    this.listenTo(vent, 'Academics:Roster:Selected:Model', this.modelSelected, this);
                    this.activateDroppable();
                    //ask for sidebar
                    this.sidebarController = new SideController({collection: this.searchCollection});
                    vent.trigger('Sidebar:On', {controller: this.sidebarController});
                    //Update header info
                    this.listenTo(this.topInfo, 'change', function() {
                        this.trigger('updateHeader', self.topInfo);
                    });
                    this.oBuilderModel = new OBuilderModel();
                    this.listenTo(this.oBuilderModel, 'sync:stop', function() {
                        //Show modal for class details
                        self.topInfoModel.set('institution', self.oBuilderModel.get(0).InstID);
                        this.classDetail = new ClassDetail({model: self.topInfoModel});
                        App.modals.show(this.classDetail);
                    });
                    vent.trigger('Sidebar:Open');
                    this.bindFooterEvents();
                    this.listenTo(this.collection, 'reset', function() {
                        if (this.collection.length > 0) {
                            //update footer buttons
                            if (this.collection.at(0).get('roster_status') == "Pending Post") {
                                self.updateFooter({save: true});
                                vent.trigger('Academics:Roster:Post:Show');
                            }
                            else {
                                self.updateFooter({save: false});
                                vent.trigger('Academics:Roster:Post:Hide');
                            }
                            vent.trigger('Academics:Roster:Print:Show');
                        }
                        else {
                            //update footer buttons
                            self.updateFooter({save: flase});
                            vent.trigger('Academics:Roster:Post:Hide');
                            vent.trigger('Academics:Roster:Print:Show');
                        }
                    });
                    this.listenTo(vent, 'Roster:Incomplete:Save', this.save);

                    if (this.fetchId != 'none') {
                        this.modelSelected(this.fetchId);
                    }
                },
                showClassDetails: function(topInfo) {
                    this.topInfoModel = topInfo;
                    this.oBuilderModel.fetch({}, {1: '1', 2: '2', key: topInfo.get('subject')}, {}, false);
                },
                modelSelected: function(modelId) {
                    this.fetchRoster(modelId);
                    vent.trigger('Sidebar:Close');
                },
                fetchRoster: function(modelId) {
                    this.collection.fetch({reset: true}, {id: modelId}, {}, false);
                    this.topInfo.fetch({}, {topinfo: 'topinfo', id: modelId}, {}, false);
                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'composite:collection:rendered', function() {
                        this.layout.refreshIScroll();
                    }, this);
                    this.listenTo(this.view, 'save', this.readySave, this);
                    //Show modal for major map
                    this.listenTo(this.view, 'MajormapModalShow', function(mmap) {
                        self.majorMapCollection = new BaseCollection();
                        _.each(mmap, function(item) {
                            self.majorMapCollection.add(item);
                        });
                        self.majorMapView = new MajorMapView({region: App.modals, collection: self.majorMapCollection});
                        App.modals.show(self.majorMapView);
                    });
                    this.listenTo(this.view, 'showIncompleteForm', function(modelId) {
                        self.IncompleteFormController = new IncompleteFormController({modelId: modelId, status: self.collection.at(0).get('roster_status')});
                    });
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.mainContent.el).droppable({
                        accept: ".classDraggable",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.modelSelected(ui.draggable.attr("data-id"));
                        }
                    });
                },
                updateFooter: function(options) {
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                bindFooterEvents: function() {
                    var self = this;
                    self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
                },
                save: function() {
                    this.view.save();
                },
                readySave: function(options) {
                    var arr = options.models;
                    var self = this;
                    _.each(arr, function(model) {
                        model.set('action', options.action);
                    });
                    this.saveModel = new RosterModel();
                    this.listenTo(this.saveModel, 'updated', function(e) {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Success',
                            message: 'Roster Saved'
                        });
                        this.fetchRoster(self.saveModel.id);
                    });
                    this.saveModel.clear();
                    this.saveModel.id = this.topInfo.get('sclass');
                    var serializedItmes = self.view.serialize();
                    _.each(serializedItmes, function(sItem) {
                        _.each(arr, function(item) {
                            if (item.get('uid') == sItem.uid) {
                                if (sItem.assigned_grade != "- Select Grade") {
                                    item.set('assigned_grade', sItem.assigned_grade);
                                }
                                else {
                                    item.set('assigned_grade', '');
                                }
                                if (sItem.attendance_option != "- Select Attendance") {
                                    item.set('attendance_option', sItem.attendance_option);
                                }
                            }
                        });
                    });
                    this.saveModel.set('roster', arr);
                    this.saveModel.save();
                }
            });
            return MainController;
        });
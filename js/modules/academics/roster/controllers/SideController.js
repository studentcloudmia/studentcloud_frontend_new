// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/academics/roster/views/SidebarView',
    //collection
    'entities/collections/terms/ClassTermCollection'
],
        function(Marionette, App, vent, SidebarView, TermsCollectionList) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.collection = options.collection;
                    this.view = new SidebarView({collection: this.collection});
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    //search from sidebar or from widget:
                    this.listenTo(this.view, 'search', this.search, this);
                    this.termsCollectionList = new TermsCollectionList();
                    this.listenTo(self.termsCollectionList, 'sync:stop', function() {
                        this.view.setTerms(self.termsCollectionList.toJSON());
                    });
                    this.termsCollectionList.fetch();
                },
                show: function(options) {
                    this.region = options.region;
                    this.region.show(this.view);
                },
                search: function() {
                    var sText = this.view.getSearchText();
                    this.collection.fetch({reset: true}, {search: sText}, {search: 1}, false);
                }

            });

            return SideController;
        });
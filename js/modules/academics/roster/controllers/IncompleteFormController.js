// incomplete form controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/academics/roster/views/IncompleteFormView',
    //model
    'entities/models/roster/IncompleteModel'
],
        function(Marionette, App, vent, IncompleteFormView, IncompleteModel) {

            var IncompleteFormController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.modelId = options.modelId;
                    this.model = new IncompleteModel();
                    this.model.id = this.modelId;
                    this.model.set('roster_status', options.status);
                    this.model.fetch();
                    this.listenTo(this.model, 'sync:stop', this.show);
                },
                show: function() {
                    this.view = new IncompleteFormView({model: this.model});
                    App.modals.show(this.view);
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                }
            });

            return IncompleteFormController;
        });
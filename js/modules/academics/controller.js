define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/academics/views/layout',
    //controllers
    'modules/academics/enroll/controller',
    'modules/academics/roster/controller',
    'modules/academics/schedule/widget/controller',
    'modules/academics/enroll/controllers/ClassDtlsController'
],
        function(Marionette, App, vent, LayoutView, EnrollModule, RosterModule, ScheduleWidget, ClassDtlsController) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;

                    self.layout = new LayoutView();

                    App.reqres.setHandler("Academics:getLayout", function() {
                        return self.layout;
                    });

                    App.reqres.setHandler("Academics:getHeading", function() {
                        return 'Academics';
                    });
                    
                    this.listenTo(vent, "Academics:Enroll:ClassDtls", function(sclass) {
                        //bring in class detail controller
                        new ClassDtlsController({region: false, sclass: sclass});
                    });

                    this.listenTo(vent, 'Academics:enroll', function() {
                        App.navigate('academics/enroll');
                        self.enroll();
                    }, this);

                    this.listenTo(vent, 'Academics:roster', function(id) {
                        App.navigate('roster/' + id);
                        self.roster(id);
                    }, this);

                    //TODO: move this to schedule module when it is created
                    this.listenTo(vent, 'Academics:Schedule:widget', function(opts) {
                        self.scheduleWidget(opts);
                    }, this);

                },
                enroll: function() {
                    //start enroll
                    new EnrollModule.Controller({region: App.main});
                },
                roster: function(urlId) {
                    console.log('urlId: ', urlId);
                    urlId = urlId || 'none';
                    //start roster
                    new RosterModule.Controller({region: App.main, fetchId: urlId});
                    //show footer
                    var optionsFooter = {
                        region: this.layout.footer
                    };
                    vent.trigger('Components:Builders:Footer:on', optionsFooter);
                },
                scheduleWidget: function(options) {
                    //start enroll
                    new ScheduleWidget.Controller({region: options.region});
                },
                onClose: function() {
                    App.reqres.removeHandler("Academics:getLayout");
                    App.reqres.removeHandler("Academics:getHeading");
                    this.layout = null;
                }

            });

            return Controller;

        });
// Academics module
define([
    'marionette',
    'app',
    'vent',
    'modules/academics/router',
    'modules/academics/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var AcademicsModule = App.module('Academics');

        //bind to module finalizer event
        AcademicsModule.addFinalizer(function() {
	
        });

        AcademicsModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return AcademicsModule;
    });
define([
'marionette',
'app',
'tpl!modules/academics/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: "#dashHeader",
            mainContent: ".mainContent",
            footer: "#bottomMenu"
        }

    });
    return LayoutView;
});

// enroll sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/academics/enroll/controllers/MainController'
],
        function(Marionette, App, vent, MainController) {

            var EnrollModule = App.module('Academics.Enroll');

            EnrollModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;

                    /*set requests first*/
                    App.reqres.setHandler("Academics:Enroll:Color", function(status) {
                        return self.returnColors(status);
                    });

                    //save region where we can show
                    this.region = options.region;

                    this.layout = App.request("Academics:getLayout");

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHeader();
                        this.showController();
                    }, this);

                    //show layout
                    this.region.show(this.layout);
                },
                showHeader: function() {
                    //start header controller
                    var options = {
                        region: this.layout.header
                    };

                    vent.trigger('Components:Student:Header:on', options);
                },
                showController: function() {
                    //hide sidebar
                    //vent.trigger('Sidebar:Close');

                    this.mainController = new MainController({region: this.layout.mainContent});
                },
                returnColors: function(status) {
                    var color = '';
                    switch (status) {
                        case 'Save':
                            color = 'rgb(75, 75, 75)'; 
                            border = 'rgb(75, 75, 75)';
                            titleColor = '#000000';
                            textColor = '#ffffff';
                            break;
                        case 'Standby':
                            color = '#ffa500'; 
                            border = '#AD7000';
                            titleColor = '#000000';
                            textColor = '#000000';
                            break;
                        case 'Favorite':
                            color = '#ffffff'; 
                            border = '#ffffff';
                            titleColor = '#000000';
                            textColor = '#000000';
                            break;
                        case 'Suggested':
                            color = '#ffffff'; 
                            border = '#ffffff';
                            titleColor = '#000000';
                            textColor = '#000000';
                            break;
                        case 'Search':
                            color = '#ffffff'; 
                            border = '#ffffff';
                            titleColor = '#000000';
                            textColor = '#000000';
                            break;
                        case 'New':
                            color = '#ffffff';
                            border = '#ffffff';
                            titleColor = '#000000';
                            textColor = '#000000';
                            break;
                        case 'Enroll':
                            color = '#9C9FA3';
                            border = '#9C9FA3';
                            titleColor = '#000000';
                            textColor = '#ffffff';
                            break;
                        default:
                            color = '#000000';
                            border = '#ffffff';
                            titleColor = '#000000';
                            textColor = '#ffffff';
                    }
                    return {color: color, border: border, titleColor: titleColor, textColor: textColor};
                },
                onClose: function() {
                    App.reqres.removeHandler("Academics:Enroll:Color");
                    this.layout = null;
                }

            });

            return EnrollModule;
        });
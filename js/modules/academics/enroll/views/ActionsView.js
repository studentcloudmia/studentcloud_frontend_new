// enrollment header
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/enroll/templates/actions.tmpl'
],function(_, Marionette, vent, App, template){
	
	var ActionView = App.Views.ItemView.extend({
		template: template,
        tagName: 'form',
        className: 'navbar-fixed-bottom',
        
        triggers:{
            'click .btnVal': 'Validate',
            'click .btnEnrl': 'Enroll',
            'click .btnSave': 'Save'
        },
        
        events: {
            'click #btnFav': 'favorites',
            'click #btnSugg': 'suggested',
            'click #btnAuto': 'auto'
        },
        
        onRender: function(){
            this.$el.find('.btn').hide();
        },
        
        favorites: function(e){
            e.preventDefault();
            e.stopPropagation();
            this.trigger('Favorites', this.getElemCoords('btnFav'));
        },
        
        suggested: function(e){
            e.preventDefault();
            //e.stopPropagation();
            this.trigger('Suggested', this.getElemCoords('btnSugg'));
        },
        
        auto: function(e){
            e.preventDefault();
            //e.stopPropagation();
            this.trigger('Auto', this.getElemCoords('btnAuto'));
        },
        
        getElemCoords: function(id) {
            return {
                top: $('#'+id).offset().top,
                left: $('#'+id).offset().left
            };
        },
        
        activateBtns: function(){
            this.$el.find('.btn').show();
        }


	});
	
	return ActionView;
	
});
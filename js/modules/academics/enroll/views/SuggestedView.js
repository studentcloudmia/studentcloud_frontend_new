// enrollment Suggested Classes
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/academics/enroll/views/EnrlWidgClass',
	'tpl!modules/academics/enroll/templates/suggested.tmpl'
],function(_, Marionette, vent, App, EnrlWidgClass, template){
	
	var SuggestedView = EnrlWidgClass.extend({
		template: template,
        negaviteLeft: 20

	});
	
	return SuggestedView;
	
});
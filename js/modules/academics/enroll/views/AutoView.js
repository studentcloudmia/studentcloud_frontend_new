// auto enrollment
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/academics/enroll/views/EnrlWidgClass',
    'tpl!modules/academics/enroll/templates/auto.tmpl'
], function(_, Marionette, vent, App, EnrlWidgClass, template) {

    var AutoView = EnrlWidgClass.extend({
        template: template,
        tagName: 'form',
        className: 'form-inline',
        negaviteLeft: 270,
        initialize: function(options) {
            this.timesArr = options.timesArr;
        },
        ui: {
            timeFrom: '#timeFrom',
            timeTo: '#timeTo',
            credits: '#credits',
            daysPicker: '#daysPicker2'
        },
        events: {
            'submit': 'formSubmitted',
            'change #timeFrom': 'updateTimeTo'
        },
        updateDays: function(days) {
            var dayOpts = '';
            _.each(days, function(day) {
                dayOpts = dayOpts + '<option value="' + day.id + '">' + day.DAY + '</option>';
            });
            this.ui.daysPicker.html(dayOpts);
        },
        onRender: function() {
            //add times to time from
            var timeOpts = '<option value="">Select Time From</option>';
            _.each(this.timesArr, function(item, index) {
                timeOpts = timeOpts + '<option value="' + index + '">' + item + '</option>';
            });
            this.ui.timeFrom.html(timeOpts);

            var creditOpts;
            for (var i = 3; i < 31; i++) {
                creditOpts = creditOpts + '<option value="' + i + '">' + i + '</option>';
            }
            this.ui.credits.html(creditOpts);
        },
        updateTimeTo: function() {
            var foundInd = parseInt(this.ui.timeFrom.val());
            //add times to time from
            var timeOpts = '';
            var timeToArr = _.rest(this.timesArr, foundInd + 1);

            _.each(timeToArr, function(item, index) {
                timeOpts = timeOpts + '<option value="' + index + '">' + item + '</option>';
            });

            this.ui.timeTo.html(timeOpts);
        },
        formSubmitted: function(e) {
            e.preventDefault();
            e.stopPropagation();
            this.trigger('FormSubmitted', Backbone.Syphon.serialize(this));
        }

    });

    return AutoView;

});
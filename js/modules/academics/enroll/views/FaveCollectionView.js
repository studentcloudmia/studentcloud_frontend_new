// favorite classes
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/academics/enroll/views/ClassView',
],function(_, Marionette, vent, App, ClassView){
	
	var FaveCollectionView = App.Views.CollectionView.extend({
        tagName: 'div',
        className: 'scroller',
        negaviteLeft: 130,
        animateOpen: 'bounceInDown',
        animateClose: 'bounceOutUp',
        itemView: ClassView,
        
        initialize: function(){
            this.listenTo(this, 'childview:Selected', this.selectedClass);
            this.listenTo(this, 'childview:ClassAction', this.classAction);            
            
            this.colorObj = App.request('Academics:Enroll:Color', 'Favorite');
        },
        
        onRender: function(){
            var self = this;
            //color classes accordingly
            this.$el.find('> .fc-event').css('background-color', this.colorObj.color).css('border-color', this.colorObj.border);
            this.$el.find('> .fc-event .fc-event-title').css('color', this.colorObj.titleColor);
            this.$el.find('> .fc-event .fc-event-time').css('color', this.colorObj.textColor);
            //activate droppable
            this.$el.parent().droppable({
                greedy: true,
                accept: ".classDraggable",
                hoverClass: "ui-state-highlight",
                drop: function(event, ui) {
                    //call selected model with dragged model
                    self.classAdded(ui.draggable.attr("data-id"));
                }
            });            
        },
        
        classAdded: function(classId){
            // alert('class dropped: ' + classId);
            this.trigger('ClassAdded', classId);
        },
        
        classAction: function(view, args){
            this.trigger('ClassAction', args);
        },
        
        selectedClass: function(view){
            this.trigger('ClassSelected', view.model.get('sclass'));
        },
        
        toggle: function(options){
            //update positioning
            this.updatePos(options);
            var $P = this.$el.parent();
            if($P.hasClass(this.animateOpen)){
                //opened so close me
                this.closeWidg();
            }else{
                //closed so open me
                this.openWidg();
            }
        },
        
        openWidg: function(){
            var $P = this.$el.parent();
            //closed so open me
            $P.removeClass('hidden ' + this.animateClose).addClass(this.animateOpen);
            //refresh iScroll
            this.refreshIScroll();
        },
        
        closeWidg: function(){
            var $P = this.$el.parent();
            $P.removeClass(this.animateOpen).addClass(this.animateClose);
        },
        
        updatePos: function(options){
            _.defaults(options, {
                top: 0,
                left: 0
            });
            // var settings = { bottom: 60, left: options.left - this.negaviteLeft };
            var settings = { top: options.top + 40, left: options.left - this.negaviteLeft };
            this.$el.parent().css( settings );
        }

	});
	
	return FaveCollectionView;
	
});
// single class view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/academics/enroll/templates/_class.tmpl'
], function(_, Marionette, vent, App, template) {

    var ClassView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'fc-event fc-event-vert fc-event-start fc-event-end classDraggable animated flipInX',
        initialize: function() {
            //create unique class for action buttons
            this.btnclass = _.uniqueId('enrlActBtn_');
        },
        events: {
            'click': 'classSelected'
        },
        onRender: function() {
            var self = this;
            //add class id to html elem
            this.$el.attr('data-id', this.model.get('sclass'));
            this.$el.attr('data-model', JSON.stringify(this.model.toJSON()));
            // this.$el.draggable({ revert: "invalid",helper: "clone" });
            this.$el.draggable({
                revert: "invalid",
                handle: "div.fc-event-title",
                helper: "clone",
                appendTo: 'body',
                zIndex: 31, //1 greater than sidebar
                start: function() {
                    //close popover if open
                    if (self.pop) {
                        self.pop.popover('destroy');
                    }
                }
            });

        },
        classSelected: function(e) {
            e.preventDefault();
            var self = this;

            //action buttons
            var dropAction = '<button data-action="drop" data-sclass="' + this.model.get('sclass') + '" class="' + this.btnclass + ' btn btn-info" aria-hidden="true"><h5 class="icomoon-close"></h5><p>Remove</p></button>';
            var dtlsAction = '<button data-action="details" data-sclass="' + this.model.get('sclass') + '" class="' + this.btnclass + ' btn btn-info" aria-hidden="true"><h5 class="icomoon-stack"></h5><p>Details</p></button>';
            var shareAction = '<button data-action="share" data-sclass="' + this.model.get('sclass') + '" class="' + this.btnclass + ' btn btn-info" aria-hidden="true"><h5 class="icomoon-upload"></h5><p>Share</p></button>';
            var contextString = dtlsAction + ' ' + shareAction;

            var url = _.result(this.model, 'url');

            //show remove button for favorite classes
            if (url.indexOf("/FavoriteClass/") != -1) {
                contextString = dropAction + ' ' + contextString;
            }

            self.pop = this.$el;
            self.pop.popover({
                html: true,
                placement: 'top',
                content: contextString,
                container: 'body'
            }).popover('show');

            //hide when click outside
            App.main.$el.on('mouseup touchend', function(e) {
                var pop = self.$el.find('.popover');
                if (pop.has(e.target)
                        .length === 0) {
                    self.pop.popover('destroy');
                }
            });


            //TODO: need to listen like this since popover is outside view. Try to find better way
            this.listenToActionButtons();
        },
        listenToActionButtons: function() {
            var self = this;
            //TODO: I really don't like this... Need to find better way...
            $('.' + this.btnclass).on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                //stop listening
                $('.' + this.btnclass).off();
                var selBtn = $(e.currentTarget);
                var selAct = selBtn.data('action');
                var selClass = selBtn.data('sclass');
                //destroy popover
                self.pop.popover('destroy');
                //take care of dtls here
                if (selAct == 'details') {
                    vent.trigger('Academics:Enroll:ClassDtls', selClass);
                } else {
                    self.trigger('ClassAction', {action: selAct, selClass: selClass});
                }
            });
        },
        onClose: function() {
            //stop listening to act button
            $('.' + this.btnclass).off();

        }

    });

    return ClassView;

});
// enrollment term
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/enroll/templates/_term.tmpl'
],function(_, Marionette, vent, App, template){
	
	var TermView = App.Views.ItemView.extend({
		template: template,
        tagName: 'li',
        
        events:{
            'click': 'termSelected'
        },
        
        termSelected: function(e){
            e.preventDefault();
            if(!this.$el.hasClass('active')){
                this.trigger('Selected', this);
            }            
        }

	});
	
	return TermView;
	
});
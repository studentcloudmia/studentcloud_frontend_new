// enrollment Widgets
define([
	'underscore',
	'marionette',
	'app'
],function(_, Marionette, App){
	
	var EnrlWidgClass = App.Views.ItemView.extend({
        tagName: 'div',
        className: 'scroller',
        negaviteLeft: 10,
        animateOpen: 'bounceInDown',
        animateClose: 'bounceOutUp',
        
        toggle: function(options){
            //update positioning
            this.updatePos(options);
            var $P = this.$el.parent();
            if($P.hasClass(this.animateOpen)){
                //opened so close me
                this.closeWidg();
            }else{
                //closed so open me
                this.openWidg();
            }
        },
        
        openWidg: function(){
            var $P = this.$el.parent();
            //closed so open me
            $P.removeClass('hidden ' + this.animateClose).addClass(this.animateOpen);
            //refresh iScroll
            this.refreshIScroll();
        },
        
        closeWidg: function(){
            var $P = this.$el.parent();
            $P.removeClass(this.animateOpen).addClass(this.animateClose);
        },
        
        updatePos: function(options){
            _.defaults(options, {
                top: 0,
                left: 0
            });
            // var settings = { bottom: 60, left: options.left - this.negaviteLeft };
            var settings = { top: options.top + 40, left: options.left - this.negaviteLeft };
            this.$el.parent().css( settings );
        }

	});
	
	return EnrlWidgClass;
	
});

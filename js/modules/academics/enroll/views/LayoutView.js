// enrollment layout
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/enroll/templates/layout.tmpl'
],function(_, Marionette, vent, App, template){
	
	var LayoutView = App.Views.Layout.extend({
		template: template,
        
        regions: {
            termBtns: '.termBtns',
            classBtns: '.classBtns',
            actions: '.enrollIcons',
            enrCal: '.enrCal',
            suggClass: '.suggClass',
            faveClass: '.faveClass',
            auto: '.autoClass',
            classOverlay: Marionette.Region.Overlay.extend({ el: ".classOverlay"})
        }

	});
	
	return LayoutView;
	
});
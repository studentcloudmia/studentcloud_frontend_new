// SideView
define([
    'marionette', 
    'app',
	'modules/academics/enroll/views/ClassView',
    'tpl!modules/academics/enroll/templates/sidebar.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var SideView = App.Views.CompositeView.extend({
            template: template,
			itemView: ItemView,
			itemViewContainer: '#classes',
			
            initialize: function(options) {
                this.timesArr = options.timesArr;
                this.colorObj = App.request('Academics:Enroll:Color', 'Search');
                this.listenTo(this, 'childview:render', this.itemAdded);
                this.listenTo(this.collection, 'reset', this.showResults);
            },
            
			events:{
				'submit #searchFields': 'search',
                'change #timeFrom': 'updateTimeTo',
                'submit #searchResults': 'showSearch'
			},
			
			ui:{
                timeFrom: '#timeFrom',
                timeTo: '#timeTo',
                credits: '#credits',
                daysPicker: '#daysPicker2',
                searchResults: '#searchResults',
                searchFields: '#searchFields',
                tMsg: '.tMsg'
			},
            
            onRender: function(){
                this.ui.tMsg.show();
                this.ui.searchResults.hide();
                this.ui.searchFields.hide();
                
                //add times to time from
                var timeOpts = '<option value="">Select Time From</option>';
                _.each(this.timesArr, function(item, index){
                    timeOpts = timeOpts + '<option value="'+index+'">'+item+'</option>';
                });
                this.ui.timeFrom.html(timeOpts);
            
                timeOpts = '<option value="">Select Credits</option>';
                for(var i = 3; i < 31; i++){
                    timeOpts = timeOpts + '<option value="'+i+'">'+i+'</option>';
                }
                this.ui.credits.html(timeOpts);
                
            },
            
            itemAdded: function(view){
                //view.$el.css('background-color', this.color);
                console.log('search color: ', this.colorObj);
                view.$el.css('background-color', this.colorObj.color).css('border-color', this.colorObj.border);
                view.$el.find('.fc-event-title').css('color', this.colorObj.border);
            },
			
			getSearchText: function(){
				var val = this.ui.search.val();
				this.ui.search.val('');
				this.ui.search.blur();
				return val;
			},
            
            showSearch: function(e){
                if(e){
                    e.preventDefault();
                }
                this.ui.tMsg.hide();
                this.ui.searchResults.hide();
                this.ui.searchFields.show();
            },
            
            showResults: function(){
                this.ui.tMsg.hide();
                this.ui.searchFields.hide();
                this.ui.searchResults.show();
            },
        
            updateDays: function(days){
                var dayOpts = '';
                _.each(days, function(day){
                    dayOpts = dayOpts + '<option value="'+day.id+'">'+day.DAY+'</option>';
                });
                this.ui.daysPicker.html(dayOpts);
            },
        
            updateTimeTo: function(){
                var foundInd = parseInt(this.ui.timeFrom.val());
                //add times to time from
                var timeOpts = '';
                var timeToArr = _.rest(this.timesArr, foundInd+1);
            
                _.each(timeToArr, function(item, index){
                    timeOpts = timeOpts + '<option value="'+index+'">'+item+'</option>';
                });
            
                this.ui.timeTo.html(timeOpts);            
            },
            
            search: function(e){
                e.preventDefault();
                e.stopPropagation();
                this.trigger('formSubmitted', Backbone.Syphon.serialize(this) );
            }
        });
	
        return SideView;
	
    });
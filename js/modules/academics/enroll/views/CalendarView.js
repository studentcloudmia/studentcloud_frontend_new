// enrollment calendar
define(['underscore', 'marionette', 'vent', 'app', 'tpl!modules/academics/enroll/templates/calendar.tmpl',
//fullcalendar plugin
    'fullcalendar'], function(_, Marionette, vent, App, template) {

    var CalendarView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: '',
        ui: {
            cal: '#enrollCalendar'
        },
        events: {
            'click .enrlActBtn': 'selectedActionBtn'
        },
        onShow: function() {
            var self = this;
            //build calendar view
            var height = parseInt(this.ui.cal.css('height')
                    .replace('px', ''));

            var allDaySlot = false;
            this.ui.cal.fullCalendar({
                height: height,
                weekends: true,
                hiddenDays: [0],
                defaultView: 'agendaWeek',
                allDaySlot: allDaySlot,
                allDayText: 'Online',
                slotMinutes: 15,
                minTime: 6,
                maxTime: 22,
                header: {
                    left: '',
                    center: '',
                    right: ''
                },
                columnFormat: {
                    week: 'ddd',
                },
                editable: false,
                disableDragging: true,
                eventRender: function(event, element) {
                    element.html(self.createEventHtml(event));
                },
                eventClick: function(calEvent, jsEvent, view) {
                    jsEvent.stopImmediatePropagation();
                    //action buttons
                    var dropAction = '<button data-action="drop"  data-sclass="' + calEvent.id + '" class="enrlActBtn btn btn-info" aria-hidden="true"><h5 class="icomoon-close"></h5><p>Drop</p></button>';
                    var dtlsAction = '<button data-action="details" data-sclass="' + calEvent.id + '" class="enrlActBtn btn btn-info" aria-hidden="true"><h5 class="icomoon-stack"></h5><p>Details</p></button>';
                    var shareAction = '<button data-action="share" data-sclass="' + calEvent.id + '" class="enrlActBtn btn btn-info" aria-hidden="true"><h5 class="icomoon-upload"></h5><p>Share</p></button>';
                    var contextString = dropAction + ' ' + dtlsAction + ' ' + shareAction;

                    self.pop = $(this);
                    self.pop.popover({
                        html: true,
                        placement: 'top',
                        content: contextString,
                        container: self.el
                    }).popover('show');
                    //hide when click outside
                    App.main.$el.on('mouseup touchend', function(e) {
                        var pop = self.$el.find('.popover');
                        if (pop.has(e.target)
                                .length === 0) {
                            self.pop.popover('destroy');
                        }
                    });
                }
            });
            //add scroller class to calendar div and scroller class to following div
            var selector = (allDaySlot) ? '.fc-view > div > div:nth-child(4)' : '.fc-view > div > div';

            this.$el.find(selector)
                    .addClass('iScrollWrapper enrollCalScroll')
                    .attr('data-scroll-bounceeasing', 'elastic')
                    .find('> div')
                    .addClass('scroller');

            //remove today highlight
            this.$el.find('.fc-today')
                    .removeClass('fc-today fc-state-highlight');

            this.activateDroppable();
        },
        activateDroppable: function() {
            var self = this;
            //activate droppable
            this.$el.droppable({
                greedy: true,
                accept: ".classDraggable",
                hoverClass: "ui-state-highlight",
                drop: function(event, ui) {
                    //call selected model with dragged model
                    //model json
                    var modelJSON = $.parseJSON(ui.draggable.attr("data-model"));
                    self.trigger('ClassDropped', modelJSON);
                }
            });
        },
        createEventHtml: function(event) {
            var timeDiff = moment.duration(event.end - event.start)
                    .asMinutes();
            var course_catalog_id = event.course_catalog_id + '<br/>';
            var courseStatus = event.status + '<br/>';
            var time = moment(event.start)
                    .format('h:mm') + ' - ' + moment(event.end)
                    .format('h:mm') + '<br/>';

            var instructor = 'TBD<br/>';
            if (event.instructor) {
                instructor = event.instructor.first_name + ' ' + event.instructor.last_name + '<br/>';
            }

            var location = event.location + '<br/>';
            //what icon are we using?
            var icon = '';
            switch (event.status) {
                case 'Enroll':
                    icon = 'checkmark';
                    break;
                case 'Standby':
                    icon = 'clock-4';
                    break;
                case 'Save':
                    icon = 'disk-2';
                    break;
                default:
                    icon = '';

            }

            var html = '<div class="fc-event iscrollPassThrough classElem' + event.id + '"><div class="fc-event-inner"><div class="fc-event-title" style="color: ' + event.titleColor + '">';
            html = html + '<span class="statusIcon pull-left icomoon-' + icon + '"></span> ' + event.title;
            html = html + '</div>';
            html = html + '<div class="fc-event-time" style="color: ' + event.textColor + '">';
            // html = html + courseStatus;
            if (timeDiff > 30) {
                html = html + course_catalog_id + time;
            }
            if (timeDiff > 75) {
                html = html + instructor + location;
            }
            html = html + '</div></div></div>';
            return html;
        },
        clearCal: function() {
            this.ui.cal.fullCalendar('removeEvents')
        },
        updateCal: function(eventArr) {
            var self = this;
            var lastEvent = false;
            _.each(eventArr, function(event) {
                lastEvent = event;
                self.ui.cal.fullCalendar('renderEvent', event);
            });

            if (lastEvent) {
                var scroller = this.getScroller('enrollCalScroll');
                if (scroller) {
                    scroller.scrollToElement(document.querySelector('.scroller .classElem' + lastEvent.id), null, null, true);
                }
            }

            this.fixIscroll();
        },
        removeEvent: function(sclass) {
            this.ui.cal.fullCalendar('removeEvents', sclass);
        },
        selectedActionBtn: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var selBtn = $(e.currentTarget);
            var selAct = selBtn.data('action');
            var selClass = selBtn.data('sclass');
            //destroy popover
            this.pop.popover('destroy');

            this.trigger('ClassAction', {action: selAct, selClass: selClass});

        }
    });

    return CalendarView;

});

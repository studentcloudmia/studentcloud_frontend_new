// enrollment term
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/academics/enroll/views/TermView',
],function(_, Marionette, vent, App, TermView){
	
	var TermCollectionView = App.Views.CollectionView.extend({
        tagName: 'ul',
        itemView: TermView,
        className: 'nav nav-pills',
        
        initialize: function(){
            this.listenTo(this, 'childview:Selected', this.selectedTerm);
        },
        
        onRender: function(){
            //select the first term
            // retrieve a view by model
            if(this.collection.length > 0){
                var v = this.children.findByModel(this.collection.at(0));
                this.selectedTerm(v);
            }
        },
        
        selectedTerm: function(view){
            //activate term selected only
            this.$el.find('li').removeClass('active');
            view.$el.addClass('active');
            this.trigger('TermSelected', view.model.get('term_type'));
        }

	});
	
	return TermCollectionView;
	
});
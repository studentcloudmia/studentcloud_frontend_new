// single class detailes view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/enroll/templates/classDtls.tmpl'
],function(_, Marionette, vent, App, template){
	
	var ClassDtlsView = App.Views.ItemView.extend({
		template: template,
        tagName: 'div',
        className: 'classDetails',
        
        render: function(){
            vent.trigger('Components:Alert:Alert', {
                heading: this.model.get('title'),
                message: this.template(this.model.toJSON()),
                icon: 'info'
            });
        }

	});
	
	return ClassDtlsView;
	
});
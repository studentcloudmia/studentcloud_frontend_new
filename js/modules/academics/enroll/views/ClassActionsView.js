// enrollment header
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/academics/enroll/templates/classActions.tmpl'
],function(_, Marionette, vent, App, template){
	
	var ClassActionsView = App.Views.ItemView.extend({
		template: template,
        className: 'pull-right',
        
        events: {
            'click #btnFav': 'favorites',
            'click #btnSugg': 'suggested',
            'click #btnAuto': 'auto'
        },
        
        favorites: function(e){
            e.preventDefault();
            e.stopPropagation();
            this.trigger('Favorites', this.getElemCoords('btnFav'));
        },
        
        suggested: function(e){
            e.preventDefault();
            //e.stopPropagation();
            this.trigger('Suggested', this.getElemCoords('btnSugg'));
        },
        
        auto: function(e){
            e.preventDefault();
            //e.stopPropagation();
            this.trigger('Auto', this.getElemCoords('btnAuto'));
        },
        
        getElemCoords: function(id) {
            return {
                top: $('#'+id).offset().top,
                left: $('#'+id).offset().left
            };
        }


	});
	
	return ClassActionsView;
	
});
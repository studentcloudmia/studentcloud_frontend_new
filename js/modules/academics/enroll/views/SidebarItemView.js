// SideView
define([
    'marionette', 
    'app',
	'vent',
    'tpl!modules/academics/enroll/templates/_sidebar.tmpl'
    ],function(Marionette, App, vent, template){

        var SideItemView = App.Views.ItemView.extend({
            template: template,
            tagName: 'tr',
            className: 'searchRow',

			events:{
				'click': 'triggerSelected'
			},
			
			onRender: function() {
			    var self = this;
			    //add data-id attribute to item
			    this.$el.attr('data-id', this.model.id);
			    this.$el.attr('data-serverid', this.model.getId());
			    //make the item draggable
			    this.$el.draggable({
			        revert: "invalid",
			        helper: "clone",
			        appendTo: 'body',
			        zIndex: 9999,
			        start: function() {
			            console.log('started drag');
			            $('.iScrollWrapper')
			                .css({
			                overflow: 'visible',
			            });
			        },
			        stop: function() {
			            console.log('stopped drag');
			            $('.iScrollWrapper')
			                .css({
			                overflow: 'hidden',
			            });
			        }
			    });


			},

			startedDrag: function() {
                console.log('started drag');
			    $('.iScrollWrapper')
			        .css({
			        overflow: 'visible',
			    });
			},

			stoppedDrag: function() {
                console.log('stopped drag');
			    $('.iScrollWrapper')
			        .css({
			        overflow: 'scroll',
			    });
			},
			


			
			triggerSelected: function(){
				vent.trigger('Academics:Enroll:Selected:Model', this.model.id);
			}
        });

        return SideItemView;

    });
//enrollment controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/TermCollectionView',
//entities
'entities/collections/terms/ClassTermCollection'
],
function(Marionette, App, vent, TermCollectionView, ClassTermCollection) {

    var ActionsController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			this.collection = new ClassTermCollection();
			this.view = new TermCollectionView({collection : this.collection});
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
			this.listenTo(this.view, 'TermSelected', function(termId){
				this.trigger('TermSelected', termId);
			}, this);
            
			//show view when collection resets
            this.listenTo(this.collection, 'sync:stop', function(){
                this.region.show(this.view);
            });
            
			//fetch collection
            this.collection.fetch({},{},{},false);
        }

    });

    return ActionsController;
});
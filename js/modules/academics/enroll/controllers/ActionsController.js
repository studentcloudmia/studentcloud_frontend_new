//enrollment controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/ActionsView'
],
function(Marionette, App, vent, ActionsView) {

    var ActionsController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			
			this.view = new ActionsView();
			this.activateListeners();
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        },
        
        activateListeners: function(){
            this.listenTo(this.view, 'Validate', function(){
                this.trigger('Validate');
            });
            this.listenTo(this.view, 'Enroll', function(){
                this.trigger('Enroll');
            });
            this.listenTo(this.view, 'Save', function(){
                this.trigger('Save');
            });
            /*following are sent with options*/
            this.listenTo(this.view, 'Favorites', function(options){
                this.trigger('Favorites', options);
            });
            this.listenTo(this.view, 'Suggested', function(options){
                this.trigger('Suggested', options);
            });
            this.listenTo(this.view, 'Auto', function(options){
                this.trigger('Auto', options);
            });
        },
        
        updateTerm: function(termId){
            this.view.activateBtns();
        }

    });

    return ActionsController;
});
//enrollment suggested classes
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/SuggCollectionView',
//entities
'entities/collections/class/SuggCollection'
],
function(Marionette, App, vent, SuggCollectionView, SuggCollection) {

    var SuggestedController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.collection = new SuggCollection();
			this.view = new SuggCollectionView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        },
        
        toggle: function(options){
            this.view.toggle(options);
        },
        
        openWidg: function(){
            this.view.openWidg();
            
            this.refreshIScroll();
        },

        closeWidg: function(){
            this.view.closeWidg();
        },
        
        updateTerm: function(termId){
            this.termId = termId;
            this.collection.fetch({reset:true},{termId: termId},{},false);
        }

    });

    return SuggestedController;
});
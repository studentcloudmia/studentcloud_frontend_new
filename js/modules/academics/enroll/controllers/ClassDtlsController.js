//Class Details controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/ClassDtlsView',
//entities
'entities/models/class/ClassModel'
],
function(Marionette, App, vent, ClassDtlsView, ClassModel) {

    var ClassDtlsController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.sclass = options.sclass;
            
			this.model = new ClassModel();
			this.view = new ClassDtlsView({model: this.model});
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
            this.model.fetch({},{id: this.sclass},{},false);
            
			this.listenTo(this.model, 'change', function(){
                if(this.region){
        			//show view
        			this.region.show(this.view);
                }else{
                    this.view.render();
                }
    			
			}, this);            
			
        }

    });

    return ClassDtlsController;
});
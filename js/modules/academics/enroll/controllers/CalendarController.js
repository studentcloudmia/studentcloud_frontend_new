//enrollment calendar
define(['marionette', 'app', 'vent',
//views
    'modules/academics/enroll/views/CalendarView',
//entities
    'entities/collections/class/EnrollCollection',
    'entities/models/class/EnrollModel',
    'entities/models/class/SaveModel'
],
        function(Marionette, App, vent, CalendarView, EnrollCollection, EnrollModel, SaveModel) {

            var CalendarController = Marionette.Controller.extend({
                classesArr: [],
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new EnrollCollection();
                    this.model = new EnrollModel();
                    this.saveModel = new SaveModel();

                    this.view = new CalendarView();

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.view, 'ClassDropped', this.classDropped);
                    this.listenTo(this.view, 'ClassAction', this.classAction);

                    this.listenTo(this.model, 'change', this.modelChanged);
                    this.listenTo(this.saveModel, 'destroy', this.saveModelChanged);

                    this.listenTo(this.collection, 'reset', this.collReset);
                    this.listenTo(this.collection, 'add', this.collAdd);
                    this.listenTo(this.collection, 'remove', this.collRemove);

                    //show view
                    this.region.show(this.view);
                    App.reqres.setHandler("Academics:Enroll:Term", function() {
                        return self.termId;
                    });
                },
                updateTerm: function(termId) {
                    vent.trigger('Academics:Enroll:TermUpdate', termId);
                    this.termId = termId;
                    this.view.clearCal();

                    this.collection.fetch({reset: true}, {term: termId}, {}, false);
                },
                collRemove: function(model) {
                    this.view.removeEvent(model.get('sclass'));
                },
                collReset: function() {
                    this.classesArr = [];
                    var self = this;
                    _.each(this.collection.models, function(model) {
                        self.createClass(model);
                    });
                    //update cal
                    this.view.updateCal(this.classesArr);
                },
                addClasses: function(classes) {
                    var self = this;
                    _.each(classes, function(model) {
                        model.status = 'New';
                        self.collection.add(model, {silent: true});
                    });

                    this.view.clearCal();
                    this.collection.trigger('reset');
                },
                collAdd: function(model) {
                    this.classesArr = [];
                    this.createClass(model);
                    //update cal
                    this.view.updateCal(this.classesArr);
                },
                createClass: function(model) {
                    var self = this;
                    _.each(model.get('schedule'), function(schedule) {
                        var classEvent = {};
                        classEvent.allDay = false;

                        classEvent.status = model.get('status');
                        //get event color
                        var colorObj = App.request('Academics:Enroll:Color', classEvent.status);
                        console.log('colorObj: ', colorObj);
                        classEvent.backgroundColor = colorObj.color;
                        classEvent.borderColor = colorObj.border;
                        classEvent.titleColor = colorObj.titleColor;
                        classEvent.textColor = colorObj.textColor;
                        
                        //update event details
                        classEvent.title = model.get('title');
                        classEvent.id = model.get('sclass');
                        classEvent.course_catalog_id = model.get('course_catalog_id');
                        classEvent.instructor = model.get('instructors')[0];
                        classEvent.location = schedule.building_description + ' ' + schedule.room_description;
                        //update event time
                        var date = moment().day(schedule.day).format('MM/DD/YYYY');
                        classEvent.start = moment(date + ' ' + schedule.start_time, "MM/DD/YYYY HH:mm:ss").toDate();
                        classEvent.end = moment(date + ' ' + schedule.end_time, "MM/DD/YYYY HH:mm:ss").toDate();
                        self.classesArr.push(classEvent);
                    });
                },
                classDropped: function(classJSON) {
                    //a class has been added to the calendar
                    var foundM = this.collection.where({sclass: classJSON.sclass});

                    if (!foundM.length > 0) {
                        //add class to collection
                        classJSON.status = 'New';
                        this.collection.addClass(classJSON);
                    } else {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Warning',
                            message: classJSON.title + ' is already in your schedule'
                        });
                    }
                },
                //class actions
                classAction: function(options) {
                    //find class in collection
                    var classModel = this.collection.where({sclass: options.selClass})[0];
                    switch (options.action) {
                        case 'drop':
                            this.actionDrop(classModel);
                            break;
                        case 'details':
                            vent.trigger('Academics:Enroll:ClassDtls', options.selClass);
                            break;
                        default:
                            alert('unknown action: ', options.action);
                    }
                },
                actionDrop: function(classModel) {
                    this.classToDrop = classModel;
                    if (classModel.get('status') == 'New') {
                        //model not in server. Simply remove from collection
                        this.collection.remove(classModel);
                    } else {
                        var act = (classModel.get('status') == 'Enroll') ? 'Drop' : 'Remove';
                        vent.trigger('Components:Alert:Confirm', {
                            heading: act,
                            message: 'Are you sure you would like to ' + act + ' ' + classModel.get('title'),
                            cancelText: 'Cancel',
                            confirmText: act,
                            callee: this,
                            callback: this.dropConfirmed
                        });
                    }
                },
                dropConfirmed: function() {
                    //do drop for enrolled and standby classes
                    var classStatus = this.classToDrop.get('status');
                    if (classStatus == "Enroll" || classStatus == "Standby") {
                        this.classToDrop.set({status: 'Drop'});
                        var newJson = [];
                        newJson.push(this.classToDrop.toJSON());
                        this.classToDrop.clear({silent: true});
                        this.classToDrop.set({classes: newJson}, {silent: true});
                        this.classToDrop.url = _.result(this.model, 'url') + this.termId + '/';
                        this.listenToOnce(this.classToDrop, 'change', this.classDropReturn);
                        this.classToDrop.save();
                    } else if (classStatus == 'Save') {
                        //do delete for saved
                        this.saveModel.clear({silent: true});
                        this.saveModel.id = this.termId + '/' + this.classToDrop.get('sclass');
                        this.saveModel.destroy();
                        //return model id to correct id
                        this.saveModel.id = this.classToDrop.get('sclass');
                    }
                },
                //enroll and save functions
                enroll: function() {
                    var html = this.getListOfClassesInTable('Enroll');
                    html = 'Are you sure you would like to enroll in' + html;
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Enroll',
                        message: html,
                        cancelText: 'Cancel',
                        confirmText: 'Enroll',
                        callee: this,
                        callback: this.enrollConfirmed
                    });

                },
                save: function() {
                    var html = this.getListOfClassesInTable('Enroll');
                    html = 'Are you sure you would like to save the following' + html;
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Save',
                        message: html,
                        cancelText: 'Cancel',
                        confirmText: 'Save',
                        callee: this,
                        callback: this.saveConfirmed
                    });
                },
                enrollConfirmed: function() {
                    this.changeStatsAndSave('Enroll');
                },
                saveConfirmed: function() {
                    this.changeStatsAndSave('Save');
                },
                changeStatsAndSave: function(newStat) {
                    this.CalActn = newStat;
                    //get all NEW classes
                    var classes = this.getClasses(newStat);

                    //ensure we have new classes to act on
                    if (newStat == 'Save' && classes.length < 1) {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Warning',
                            message: 'There are no \'New\' classes to ' + newStat
                        });
                    } else {
                        _.each(classes, function(model) {
                            model.set('status', newStat, {silent: true});
                        });
                        this.saveEnroll(classes);
                    }
                },
                getClasses: function(forWhat) {
                    //get all NEW classes
                    var classes = this.collection.where({status: 'New'});

                    //add Save if we are enrolling
                    if (forWhat == 'Enroll') {
                        classes = _.union(this.collection.where({status: 'Save'}), classes);
                    }
                    return classes;
                },
                getListOfClassesInTable: function(forWhat) {
                    var classes = this.getClasses(forWhat);
                    var html = '<table class="table table-striped"><tbody>';
                    _.each(classes, function(cls) {
                        html = html + '<tr><td>' + cls.get('title') + '</td></tr>';
                    });
                    html = html + '</tbody></table>';
                    return html;
                },
                saveEnroll: function(classes) {
                    //build JSON to save
                    var json = [];
                    //loop through coll to add classes
                    _.each(classes, function(model) {
                        json.push({sclass: model.get('sclass'), status: model.get('status')});
                    });
                    this.model.clear({silent: true});
                    this.model.set({classes: json}, {silent: true});
                    var oldUrl = _.result(this.model, 'url');
                    this.model.url = oldUrl + this.termId + '/';
                    this.model.save();
                    this.model.url = oldUrl;
                },
                classDropReturn: function() {
                    var title = this.classToDrop.toJSON()[0].title;
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Drop Results',
                        message: 'Successfully dropped ' + title
                    });
                    this.updateTerm(this.termId);
                },
                modelChanged: function() {
                    var html = '<table class="table table-striped"><tbody>';
                    _.each(this.model.toJSON(), function(cls, index) {
                        if (index != 'classes') {
                            html = html + '<tr><td>' + cls.title + '</td><td>' + cls.message + '</td></tr>';
                        }
                    });
                    html = html + '</tbody></table>';

                    vent.trigger('Components:Alert:Alert', {
                        heading: this.CalActn + ' Results',
                        message: html
                    });
                    this.updateTerm(this.termId);
                },
                saveModelChanged: function() {
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Class removed successfully'
                    });
                    this.updateTerm(this.termId);
                }

            });

            return CalendarController;
        });

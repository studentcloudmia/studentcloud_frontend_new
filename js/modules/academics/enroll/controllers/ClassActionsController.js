//enrollment controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/ClassActionsView'
],
function(Marionette, App, vent, ClassActionsView) {

    var ClassActionsController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			
			this.view = new ClassActionsView();
			this.activateListeners();
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        },
        
        activateListeners: function(){
            /*following are sent with options*/
            this.listenTo(this.view, 'Favorites', function(options){
                this.trigger('Favorites', options);
            });
            this.listenTo(this.view, 'Suggested', function(options){
                this.trigger('Suggested', options);
            });
            this.listenTo(this.view, 'Auto', function(options){
                this.trigger('Auto', options);
            });
        },
        
        updateTerm: function(termId){
            this.view.activateBtns();
        }

    });

    return ClassActionsController;
});
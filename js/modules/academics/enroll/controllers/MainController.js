//enrollment main controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/academics/enroll/views/LayoutView',
//controllers
    'modules/academics/enroll/controllers/TermsController',
    'modules/academics/enroll/controllers/ActionsController',
    'modules/academics/enroll/controllers/ClassActionsController',
    'modules/academics/enroll/controllers/CalendarController',
    'modules/academics/enroll/controllers/SuggestedController',
    'modules/academics/enroll/controllers/AutoController',
    'modules/academics/enroll/controllers/FaveController',
    'modules/academics/enroll/controllers/SideController',
    'modules/academics/enroll/controllers/ClassDtlsController'

],
        function(Marionette, App, vent, LayoutView,
                //controllers
                TermsController, ActionsController, ClassActionsController, CalendarController, SuggestedController, AutoController, FaveController, SideController, ClassDtlsController) {

            var MainController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;

                    this.layout = new LayoutView();

                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.layout, 'show', function() {
                        this.startControllers();
                    }, this);

                    //show view
                    this.region.show(this.layout);
                },
                startControllers: function() {
                    //ask for sidebar
                    this.sidebarController = new SideController({/*collection: this.collection*/});
                    vent.trigger('Sidebar:On', {controller: this.sidebarController});

                    this.termController = new TermsController({region: this.layout.termBtns});
                    this.listenTo(this.termController, 'TermSelected', this.termSelected);
                    this.actionsCont = new ActionsController({region: this.layout.actions});
                    this.activateActions();
                    
                    this.classActionsCont = new ClassActionsController({region: this.layout.classBtns});
                    this.activateClassActions();                    

                    this.calController = new CalendarController({region: this.layout.enrCal});
                    this.suggController = new SuggestedController({region: this.layout.suggClass});
                    this.faveController = new FaveController({region: this.layout.faveClass});

                    this.autoController = new AutoController({region: this.layout.auto});
                    this.activateAuto();

                    //listen to requests for class details
                    this.listenTo(vent, 'Academics:Enroll:ClassDtls', function(sclass) {
                        //new ClassDtlsController({region: App.overlays, sclass: sclass});
                        new ClassDtlsController({region: false, sclass: sclass});
                    });

                },
                activateAuto: function() {
                    var self = this;
                    this.listenTo(this.autoController, 'AutoClassesToAdd', function(classes) {
                        //new ClassDtlsController({region: App.overlays, sclass: sclass});
                        this.calController.addClasses(classes);
                    });
                },
                activateActions: function() {
                    this.listenTo(this.actionsCont, 'Enroll', function() {
                        this.calController.enroll();
                    });
                    this.listenTo(this.actionsCont, 'Save', function() {
                        this.calController.save();
                    });
                    // this.listenTo(this.actionsCont, 'Favorites', this.toggleFavorite);
                    // this.listenTo(this.actionsCont, 'Suggested', this.toggleSuggested);
                    // this.listenTo(this.actionsCont, 'Auto', this.toggleAuto);
                },
                
                activateClassActions: function(){
                    this.listenTo(this.classActionsCont, 'Favorites', this.toggleFavorite);
                    this.listenTo(this.classActionsCont, 'Suggested', this.toggleSuggested);
                    this.listenTo(this.classActionsCont, 'Auto', this.toggleAuto);
                },
                
                termSelected: function(termId) {
                    this.termId = termId;

                    this.sidebarController.updateTerm(this.termId);

                    this.actionsCont.updateTerm(this.termId);

                    this.calController.updateTerm(this.termId);
                    this.suggController.updateTerm(this.termId);
                    this.faveController.updateTerm(this.termId);
                    this.autoController.updateTerm(this.termId);
                },
                toggleFavorite: function(options) {
                    console.log('fave: ', options);
                    this.autoController.closeWidg();
                    this.faveController.toggle(options);
                },
                toggleSuggested: function(options) {
                    this.autoController.closeWidg();
                    this.suggController.toggle(options);
                },
                toggleAuto: function(options) {
                    this.faveController.closeWidg();
                    this.suggController.closeWidg();
                    this.autoController.toggle(options);
                },
                onClose: function() {
                    //turn off mouseup and touch end in curent region only
                    App.main.$el.off('mouseup touchend');
                }

            });

            return MainController;
        });
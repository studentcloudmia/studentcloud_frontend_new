// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/SidebarView',
//entities
'entities/collections/class/SearchCollection',
'entities/collections/dictionary/DictionaryCollection'
],
function(Marionette, App, vent, SidebarView, SearchCollection, DictionaryCollection) {

    var SideController = Marionette.Controller.extend({
        
        timesArr: ['6am','7am','8am','9am','10am','11am','12pm','1pm','2pm','3pm','4pm','5pm','6pm','7pm','8pm','9pm','10pm'],
        
        initialize: function(options) {
            
            this.collection = new SearchCollection();
            this.daysColl = new DictionaryCollection();
			
			this.view = new SidebarView({timesArr: this.timesArr, collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.view, 'formSubmitted', this.formSubmitted, this);

			this.listenToOnce(this.daysColl, 'reset', function(){
				this.view.updateDays(this.daysColl.toJSON());
			}, this);
            
            //get days
            this.daysColl.getDays();
            
			// this.listenTo(this.collection, 'reset', this.updateResults, this);
        },

		show: function(options){
            //save region where we can show
            this.region = options.region;
			//show view
			this.region.show(this.view);
		},
                
        searchTriggered: function(){
			//get search text
			this.sText = this.view.getSearchText();
            this.search();
        },

		updateTerm: function(termId){
            this.termId = termId;
            this.view.showSearch();
		},
        
        formSubmitted: function(vars){
            
            //format days
            if(vars.days){
                vars.days = vars.days.join(",");
            }else{
                vars.days = '';
            }                
            
            //format time
            if(vars.start_time.length > 0){
                var start_time_i = parseInt(vars.start_time);
                vars.start_time = moment(this.timesArr[start_time_i], 'ha').format('HH:mm:ss');
                if(vars.end_time.length > 0){
                    var end_time_i = parseInt(vars.end_time) + start_time_i + 1;
                    vars.end_time = moment(this.timesArr[end_time_i], 'ha').format('HH:mm:ss');
                }
            }
            
            //remove empty vars
            _.each(vars, function(val, i){
                if(!val.length > 0){
                    delete vars[i];
                }
            });
            
            if(_.size(vars) > 0){
                //do fetch
                this.collection.fetch({ reset: true }, { term: this.termId }, vars, false);
            }       
        }

    });

    return SideController;
});
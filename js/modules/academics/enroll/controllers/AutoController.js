//enrollment controller
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/AutoView',
//entities
'entities/collections/dictionary/DictionaryCollection',
'entities/collections/class/AutoEnrollCollection'
],
function(Marionette, App, vent, AutoView, DictionaryCollection, AutoEnrollCollection) {

    var AutoController = Marionette.Controller.extend({

        timesArr: ['6am','7am','8am','9am','10am','11am','12pm','1pm','2pm','3pm','4pm','5pm','6pm','7pm','8pm','9pm','10pm'],
        
        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            
            this.daysColl = new DictionaryCollection();
			this.autoCollection = new AutoEnrollCollection();
            
			this.view = new AutoView({timesArr: this.timesArr});
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
			this.listenToOnce(this.daysColl, 'reset', function(){
				this.view.updateDays(this.daysColl.toJSON());
			}, this);
            
			this.listenTo(this.view, 'FormSubmitted', this.formSubmitted);
            
			this.listenTo(this.autoCollection, 'reset', this.collectionReturned);
            
			//show view
			this.region.show(this.view);
            
            //get days
            this.daysColl.getDays();
        },
        
        toggle: function(options){
            this.view.toggle(options);
        },
        
        openWidg: function(){
            this.view.openWidg();
        },

        closeWidg: function(){
            this.view.closeWidg();
        },
        
        updateTerm: function(termId){
            this.termId = termId;
        },
        
        formSubmitted: function(vars){
            //format days
            if(vars.days)
                vars.days = vars.days.join(",");
            //format time
            var start_time_i = parseInt(vars.start_time);
            var end_time_i = parseInt(vars.end_time) + start_time_i + 1;
            vars.start_time = moment(this.timesArr[start_time_i], 'ha').format('HH:mm:ss');
            vars.end_time = moment(this.timesArr[end_time_i], 'ha').format('HH:mm:ss');

            //do fetch
            this.autoCollection.fetch({ reset: true }, { term: this.termId }, vars, false);
        },
        
        collectionReturned: function(){
            this.closeWidg();
            this.trigger('AutoClassesToAdd', this.autoCollection.toJSON());
        }

    });

    return AutoController;
});
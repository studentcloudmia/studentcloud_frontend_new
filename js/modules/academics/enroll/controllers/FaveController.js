//enrollment favorite classes
define([
'marionette',
'app',
'vent',
//views
'modules/academics/enroll/views/FaveCollectionView',
//entities
'entities/collections/class/FaveCollection',
'entities/models/class/FaveModel'
],
function(Marionette, App, vent, FaveCollectionView, FaveCollection, FaveModel) {

    var FaveController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.collection = new FaveCollection();
            this.model = new FaveModel();
            
			this.view = new FaveCollectionView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
			this.listenTo(this.view, 'ClassAction', this.ClassAction);
			this.listenTo(this.view, 'ClassAdded', this.ClassAdded);
			this.listenTo(this.model, 'sync:stop', this.modelChanged);
            
			this.region.show(this.view);
        },
        
        ClassAction: function(args){
            //{action: "drop", selClass: 2} 
            if(args.action == 'drop'){
                //remove class from favorites
                this.model.id = this.termId+'/'+args.selClass
                this.model.destroy();
                this.model.id = args.selClass
                
            }
        },
        
        ClassAdded: function(sclass){
            this.model.clear({silent: true});
            //this.model.url = oldUrl + this.termId + '/';
            this.model.set({sclass: sclass}, {silent: true});
            this.model.id = this.termId + '/' + sclass;
            this.model.save();
            this.model.id = sclass;
        },
        
        modelChanged: function(){
            this.updateTerm(this.termId);
        },
        
        toggle: function(options){
            this.view.toggle(options);
        },
        
        openWidg: function(){
            this.view.openWidg();
            
            this.refreshIScroll();
        },

        closeWidg: function(){
            this.view.closeWidg();
        },
        
        updateTerm: function(termId){
            this.termId = termId;
            this.collection.fetch({reset:true},{termId: termId},{},false);
        }

    });

    return FaveController;
});
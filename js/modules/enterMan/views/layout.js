define([
'marionette',
'app',
'tpl!modules/enterMan/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: "#appHeader",
            leftSide: '.side',
            rightSide: '.values',
            footer: "#bottomMenu"
        }

    });
    return LayoutView;
});
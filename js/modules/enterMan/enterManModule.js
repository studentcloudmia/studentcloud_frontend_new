// EnterMan module
define([
    'marionette',
    'app',
    'vent',
    'modules/enterMan/router',
    'modules/enterMan/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var EnterManModule = App.module('EnterMan');

        //bind to module finalizer event
        EnterManModule.addFinalizer(function() {
	
        });

        EnterManModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return EnterManModule;
    });
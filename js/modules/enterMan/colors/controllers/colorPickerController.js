//color picker controller
define([
'marionette',
'app',
'vent',
//views
'modules/enterMan/colors/views/ColorPickerView'
],
function(Marionette, App, vent, ColorPickerView) {

    var ColorPickerController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			
			this.view = new ColorPickerView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
			this.activateListeners();
        },

		activateListeners: function(){
			//listen to reset in countries
			var self = this;						
		}

    });

    return ColorPickerController;
});
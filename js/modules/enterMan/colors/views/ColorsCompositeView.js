// colors composite view
define([
    'marionette', 
    'app',
    'modules/enterMan/colors/views/ColorsItemView',
    'tpl!modules/enterMan/colors/templates/colors.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var ColorsCompositeView = App.Views.CompositeView.extend({
            itemView: ItemView,
            template: template,
            itemViewContainer: 'form',
            
            onCompositeRendered : function(){
                console.log('All colors rendered');
            }
            
        });

        return ColorsCompositeView;
	
    });
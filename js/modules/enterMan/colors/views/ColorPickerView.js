// ColorPickerView
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/enterMan/colors/templates/colorPicker.tmpl',
    //plugin
    'colorpicker'
],function(_, Marionette, vent, App, template){
	
	var ColorPickerView = App.Views.ItemView.extend({
		template: template,
		tagName: 'div',
		className: 'colorpicker1',
        
        ui: {
            color: '.sp-input',
            hexValue: '.hexvalue',
            flat: '#flat'
        },
        
        events: {
            'click .hexsetvalue': 'setColor'
        },
		
		onRender: function(){
            //init color picker
			this.ui.flat.spectrum({
			    flat: true,
			    showInput: true,
				preferredFormat: "hex"
			});
            //rebind ui elements
            this.bindUIElements();
            //make draggable
			this.ui.hexValue.draggable({ revert: "invalid",helper: "clone" });
		},
        
        setColor: function(e) {
            e.preventDefault();
            var value = this.ui.color.val();
            console.log('selected color: ', value);
            this.ui.hexValue.attr('data-id', value);
            this.ui.hexValue.css("background-color", value);
        }

	});
	
	return ColorPickerView;
	
});
// ColorView
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/enterMan/colors/templates/_color.tmpl'
],function(_, Marionette, vent, App, template){
	
	var ColorView = App.Views.ItemView.extend({
		template: template,
		tagName: 'div',
		className: 'control-group',
		
		onRender: function(){
			console.log('rendered color: ', this.model.id);
		}
	});
	
	return ColorView;
	
});
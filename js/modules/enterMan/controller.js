define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/enterMan/views/layout',
    //controllers
    'modules/enterMan/colors/controller'
],
        function(Marionette, App, vent, LayoutView, EnterManColorsModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
	                var self = this;
                    
					self.layout = new LayoutView();
					
                    App.reqres.setHandler("EnterMan:getLayout", function() {
                        return self.layout;
                    });
					
                    App.reqres.setHandler("EnterMan:getLinks", function() {
                        //TODO: These should be generated from menu collection
                        return self.getLinks();
                    });

                    this.listenTo(vent, 'EnterMan:builder', function() {
                        App.navigate('enterman/colors');
                        self.showColors();
                    }, this);

                    this.listenTo(vent, 'EnterMan:hierarchy', function() {
                        App.navigate('obuilder/images');
                        self.showImages();
                    }, this);

                },

				getLinks: function(){
					return [{
					    name: 'Colors',
					    link: 'enterman/colors'
					},
					{
					    name: 'Images',
					    link: 'enterman/images'
					}];
				},

                showColors: function() {
					//show layout
                    console.log('show colors');
                    new EnterManColorsModule.Controller({region: App.main});
                },

                showImages: function() {
					//show layout
                    console.log('show images');
                },
                        
                onClose: function() {
                    App.reqres.removeHandler("EnterMan:getLinks");
                    App.reqres.removeHandler("EnterMan:getLayout");		
					this.layout = null;
                }

            });

            return Controller;

        });
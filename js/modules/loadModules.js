// include all modules
define([
    //start admin modules
    'modules/sidebar/sidebarModule',
    'modules/fbuilder/fbuilderModule',
    'modules/sbuilder/sbuilderModule',
    'modules/cbuilder/cbuilderModule',
    'modules/csbuilder/csbuilderModule',
    'modules/obuilder/obuilderModule',
    'modules/dashboard/dashboardModule',
    'modules/adminDashboard/adminDashboardModule',
    'modules/adminStudentView/adminStudentViewModule',
    'modules/enterMan/enterManModule',
    'modules/securityMan/securityManModule',
    'modules/profileData/profileModule',
    'modules/application/applicationModule',
    'modules/majorMap/majorMapModule',
    'modules/communication/communicationModule',
    //start student modules
    'modules/academics/academicsModule',
    'modules/admissions/admissionsModule',
    //start helper modules
    'modules/components/componentsModule',
    //***********************************************
    //load access last since it has the default route
    //***********************************************
    'modules/access/accessModule'
],
        function() {
            //include path to all main modules
        });
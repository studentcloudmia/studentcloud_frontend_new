// sbuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/sbuilder/router',
    'modules/sbuilder/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var SbuilderModule = App.module('Sbuilder');

        //bind to module finalizer event
        SbuilderModule.addFinalizer(function() {
	
        });

        SbuilderModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return SbuilderModule;
    });
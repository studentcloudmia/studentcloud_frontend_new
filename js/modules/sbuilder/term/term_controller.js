// sbuilder term
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/sbuilder/term/controllers/mainContent_controller',
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
    'modules/sbuilder/builders_controller',
    //models
    'entities/collections/terms/TermsCollection',
    'entities/collections/BaseCollection',
    'entities/models/terms/TermModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, TermCollection, BaseCollection, TermModel) {

            var SbuilderTermModule = App.module('Sbuilder.Term');

            SbuilderTermModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Sbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new TermCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new TermCollection();
                    this.termModel = new TermModel();
                    this.coll = new TermCollection();
                    this.model = new TermModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Terms'});


                    opts = {
                        entityHeading: 'Term Offered',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        termModel: this.termModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new TermModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return SbuilderTermModule;
        });
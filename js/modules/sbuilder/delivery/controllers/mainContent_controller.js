// sbuilder delivery mainContent controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/sbuilder/delivery/views/MainContentView'
],
        function(Marionette, App, vent, MainContentView) {

            var MainContentController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.model = options.model;
                    this.modelOldeff_date = this.model.get('eff_date');

                    this.view = new MainContentView({model: this.model});

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    //show view
                    this.region.show(this.view);
                    this.activateListeners();
                },
                activateListeners: function() {
                    var self = this;
                    //listen to model create
                    this.listenTo(this.model, 'created', function() {
                        this.modelOldeff_date = this.model.get('eff_date');
                        self.trigger('created');
                    }, this);
                    //listen to model destroy
                    this.listenTo(this.model, 'destroyed', function() {
                        self.trigger('destroyed');
                    }, this);
                    //listen to model update
                    this.listenTo(this.model, 'updated', function() {
                        self.trigger('updated');
                    }, this);

                    this.listenTo(this.view, 'HistoricalPrev', function() {
                        self.trigger('HistoricalPrev');
                    }, this);

                    this.listenTo(this.view, 'HistoricalNext', function() {
                        self.trigger('HistoricalNext');
                    }, this);

                },
                save: function() {
                    //update model
                    if (this.view.updateModel()) {
                        //verify eff_dt has been changed
                        if (this.modelOldeff_date == this.model.get('eff_date')) {
                            vent.trigger('Components:Alert:Confirm', {
                                heading: 'Effective Date Not Changed',
                                message: 'You have not changed the effective date for ' + this.model.get('model_id'),
                                cancelText: 'Edit',
                                confirmText: 'Save',
                                callee: this,
                                callback: this.confirmedSave
                            });
                        } else {
                            this.confirmedSave();
                        }
                    }
                },
                confirmedSave: function() {
                    //TODO: find a way to validate eff date automatically in the form, maybe new form validator?
                    if (this.view.ui.eff_date.val() == '') {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Invalid Effective Date',
                            message: 'Effective date cannot be empty'
                        });
                    } else
                    {
                        //TODO: Change model URL to include eff_date
                        this.model.save();
                    }

                },
                del: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Delete?',
                        message: 'Are you sure you would like to delete ' + this.model.get('model_id'),
                        callee: this,
                        callback: this.confirmedDel
                    });
                },
                confirmedDel: function() {
                    this.model.destroy();
                }

            });

            return MainContentController;
        });
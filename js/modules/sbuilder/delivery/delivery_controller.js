// sbuilder delivery
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/sbuilder/delivery/controllers/mainContent_controller',
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
    'modules/sbuilder/builders_controller',
    //models
    'entities/collections/delivery/DeliveriesCollection',
    'entities/collections/BaseCollection',
    'entities/models/delivery/DeliveryModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, DeliveryCollection, BaseCollection, DeliveryModel) {

            var SbuilderDeliveryModule = App.module('Sbuilder.Delivery');

            SbuilderDeliveryModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Sbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new DeliveryCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new DeliveryCollection();
                    this.deliveryModel = new DeliveryModel();
                    this.coll = new DeliveryCollection();
                    this.model = new DeliveryModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Deliveries'});

                    opts = {
                        entityHeading: 'Delivery Method',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        deliveryModel: this.deliveryModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new DeliveryModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return SbuilderDeliveryModule;
        });
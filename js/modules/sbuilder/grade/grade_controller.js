// sbuilder grade
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/sbuilder/grade/controllers/mainContent_controller',
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
    'modules/sbuilder/builders_controller',
    //models
    'entities/collections/grades/GradesCollection',
    'entities/collections/BaseCollection',
    'entities/models/grades/GradeModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, GradeCollection, BaseCollection, GradeModel) {

            var SbuilderGradeModule = App.module('Sbuilder.Grade');

            SbuilderGradeModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Sbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new GradeCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new GradeCollection();
                    this.gradeModel = new GradeModel();
                    this.coll = new GradeCollection();
                    this.model = new GradeModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Grade Types'});


                    opts = {
                        entityHeading: 'Grade Type',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        gradeModel: this.gradeModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new GradeModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return SbuilderGradeModule;
        });
define(['marionette', 'app', 'vent',
//views
    'modules/sbuilder/views/layout',
//controllers
    'modules/sbuilder/grade/grade_controller', 'modules/sbuilder/term/term_controller', 'modules/sbuilder/component/component_controller', 'modules/sbuilder/subject/subject_controller', 'modules/sbuilder/delivery/delivery_controller',
//sidebar controller
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
//sidebar collection
    'entities/collections/subject/SubjectsCollection'],
        function(Marionette, App, vent, LayoutView,
//sub modules
                SbuilderGradeModule, SbuilderTermModule, SbuilderComponentModule, SbuilderSubjectModule, SbuilderDeliveryModule,
//sidebar controller
                SubjSideController,
//sidebar collection
                SubjectsCollection) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;

                    App.reqres.setHandler("Sbuilder:getLayout", function() {
                        var layout = new LayoutView();
                        return layout;
                    });

                    App.reqres.setHandler("Sbuilder:getHeading", function() {
                        return 'Structure Builder';
                    });

                    App.reqres.setHandler("Sbuilder:getLinks", function() {
                        //TODO: These should be generated from menu collection
                        return [{
                                name: 'Grade Type',
                                link: 'sbuilder/grades'
                            }, {
                                name: 'Terms Offered',
                                link: 'sbuilder/terms'
                            }, {
                                name: 'Delivery Method',
                                link: 'sbuilder/delivery'
                            }, {
                                name: 'Component',
                                link: 'sbuilder/component'
                            }, {
                                name: 'Subject',
                                link: 'sbuilder/subjects'
                            }];
                    });

                    this.listenTo(vent, 'Sbuilder:showGrade', function() {
                        App.navigate('sbuilder/grade');
                        this.showGrade();
                    }, this);

                    this.listenTo(vent, 'Sbuilder:showTerm', function() {
                        App.navigate('sbuilder/terms');
                        this.showTerms();
                    }, this);

                    this.listenTo(vent, 'Sbuilder:showComponent', function() {
                        App.navigate('sbuilder/component');
                        this.showComponent();
                    }, this);

                    this.listenTo(vent, 'Sbuilder:showSubject', function() {
                        App.navigate('sbuilder/subjects');
                        this.showSubject();
                    }, this);

                    this.listenTo(vent, 'Sbuilder:showDelivery', function() {
                        App.navigate('sbuilder/delivery');
                        this.showDelivery();
                    }, this);

                    /*allow others to call for your sidebar controllers*/
                    App.reqres.setHandler("Sbuilder:Subject:SideController", function() {
                        return new SubjSideController({
                            collection: new SubjectsCollection(), searchString: 'Subjects'
                        });
                    }); /*allow others to call for your main content controllers*/
                    App.reqres.setHandler("Sbuilder:Subject:MainController", function(options) {
                        self.showSubject(options);
                    });
                },
                showGrade: function() {
                    //transfer control to show submodule
                    new SbuilderGradeModule.Controller({
                        region: App.main
                    });
                },
                showTerm: function() {
                    //transfer control to show submodule
                    new SbuilderTermModule.Controller({
                        region: App.main
                    });
                },
                showComponent: function() {
                    //transfer control to show submodule
                    new SbuilderComponentModule.Controller({
                        region: App.main
                    });
                },
                showSubject: function(options) {
                    options = options || {};
                    _.defaults(options, {
                        region: App.main,
                        widg: false,
                        modelId: ''
                    });

                    //transfer control to show submodule
                    new SbuilderSubjectModule.Controller(options);
                },
                showDelivery: function() {
                    //transfer control to show submodule
                    new SbuilderDeliveryModule.Controller({
                        region: App.main
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Sbuilder:getLayout");
                    App.reqres.removeHandler("Sbuilder:getHeading");
                    App.reqres.removeHandler("Sbuilder:getLinks");
                    App.reqres.removeHandler("Sbuilder:Subject:SideController");
                }

            });

            return Controller;

        });

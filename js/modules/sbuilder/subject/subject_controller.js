// sbuilder subject
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/sbuilder/subject/controllers/mainContent_controller',
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
    'modules/sbuilder/builders_controller',
    //models
    'entities/collections/subject/SubjectsCollection',
    'entities/collections/BaseCollection',
    'entities/models/subject/SubjectModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, SubjectCollection, BaseCollection, SubjectModel) {

            var SbuilderSubjectModule = App.module('Sbuilder.Subject');

            SbuilderSubjectModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Sbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new SubjectCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new SubjectCollection();
                    this.subjectModel = new SubjectModel();
                    this.coll = new SubjectCollection();
                    this.model = new SubjectModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Subjects'});

                    opts = {
                        entityHeading: 'Subject',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        subjectModel: this.subjectModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout,
                        widg: options.widg,
                        modelId: options.modelId
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new SubjectModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return SbuilderSubjectModule;
        });
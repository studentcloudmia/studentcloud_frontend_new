// sbuilder component mainContent view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/sbuilder/subject/templates/mainContent.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MainContentView = App.Views.ItemView.extend({
		template: template,
		tagName: 'form',
		id: 'Subject',
        className: 'formHeigh',
		
        ui: {
            idInput: '#id',
            validationBtn: '.validationBtn',
            eff_date: '.eff_date'
		},
        
        triggers: {
            'click #historicalNext': 'HistoricalNext',
            'click #historicalPrev': 'HistoricalPrev'
        },
        
		onRender: function(){
			//start jquery validate
			var self = this;
			self.$effDate = self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker, .picker__select--month, .picker__select--year" ).jqBootstrapValidation();
            });
            //readonly id if model is not new
            if(!this.model.isNew()){
                this.ui.idInput.attr("readonly", true);
            }
		},
		
		updateModel: function(){
			//ensure the form is valid			
			var self = this;
			if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
			}else {
			    var data = Backbone.Syphon.serialize(this);
  			    self.model.set(data, {silent:true});
                return true;
			}
		}
	});
	
	return MainContentView;
	
});
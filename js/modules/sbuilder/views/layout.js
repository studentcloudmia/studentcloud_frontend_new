define([
'marionette',
'app',
'tpl!modules/sbuilder/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: "#appHeader",
            mainContent: "#dynamicAppContent",
            footer: "#bottomMenu"
        },
        
        ui: {
            histRight: '#historicRight',
            histLeft: '#historicLeft',
            histTextRight: '#histTextRight',
            histTextLeft: '#histTextLeft',
            histLeftBadge: '.histLeftBadge',
            histRightBadge: '.histRightBadge'
        },
        
        events: {
            'click #histTextRight': 'historicalNav',
            'click #histTextLeft': 'historicalNav'
        },
        
        showRight: function(opt){
            this.ui.histRight.removeClass('hidden');
            this.ui.histTextRight.data('eff_date', opt.eff_date);
            this.ui.histTextRight.html(opt.eff_date + " - " + opt.status);
            //update badge counts
            this.ui.histRightBadge.html(opt.counter);
        },
        
        hideRight: function(){
            this.ui.histRight.addClass('hidden');
        },
        
        showLeft: function(opt){
            this.ui.histLeft.removeClass('hidden');
            this.ui.histTextLeft.data('eff_date', opt.eff_date);
            this.ui.histTextLeft.html(opt.eff_date+ " - " + opt.status);
            //update badge counts
            this.ui.histLeftBadge.html(opt.counter);
        },
                
        hideLeft: function(){
            this.ui.histLeft.addClass('hidden');
        },        
        
        historicalNav: function(e){
            var self = this;
            if(e.currentTarget.id == 'histTextRight')
                {
                     self.ui.histRight.addClass('ui-state-highlight animated pulse');
                     self.ui.histRight.addClass('ui-state-highlight animated pulse');
                    
                    //remove highlight
                    setTimeout(function() {
                         self.ui.histRight.removeClass('ui-state-highlight animated pulse');
                    }, 250);
                }
            else if(e.currentTarget.id == 'histTextLeft')
                {
                     self.ui.histLeft.addClass('ui-state-highlight animated pulse');
                     self.ui.histLeft.addClass('ui-state-highlight animated pulse');
                    
                    //remove highlight
                    setTimeout(function() {
                         self.ui.histLeft.removeClass('ui-state-highlight animated pulse');
                    }, 250);
                }
            this.trigger('Historic:Nav', $(e.target).data('eff_date'));
        }

    });
    return LayoutView;
});
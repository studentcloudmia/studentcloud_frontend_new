// sbuilder component
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/sbuilder/component/controllers/mainContent_controller',
    'modules/sbuilder/sidebar/controllers/Sidebar_controller',
    'modules/sbuilder/builders_controller',
    //models
    'entities/collections/component/ComponentsCollection',
    'entities/collections/BaseCollection',
    'entities/models/component/ComponentModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, ComponentCollection, BaseCollection, ComponentModel) {

            var SbuilderComponentModule = App.module('Sbuilder.Component');

            SbuilderComponentModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Sbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new ComponentCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new ComponentCollection();
                    this.componentModel = new ComponentModel();
                    this.coll = new ComponentCollection();
                    this.model = new ComponentModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Components'});

                    opts = {
                        entityHeading: 'Component',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        componentModel: this.componentModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new ComponentModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return SbuilderComponentModule;
        });
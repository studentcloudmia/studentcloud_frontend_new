define([
'marionette',
'app',
],
function(Marionette, app) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'sbuilder/grades': 'showGrade',
            'sbuilder/terms': 'showTerm',
            'sbuilder/delivery': 'showDelivery',
            'sbuilder/component': 'showComponent',
            'sbuilder/subjects': 'showSubject'
        }
    });

    return Router;

});
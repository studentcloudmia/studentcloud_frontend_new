// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/sbuilder/sidebar/views/SidebarView'
],
        function(Marionette, App, vent, SidebarView) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.collection;
                    this.searchString = options.searchString;
                    this.view = new SidebarView({collection: this.collection, searchString: this.searchString});

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.view, 'search', this.searchTriggered, this);

                    // this.listenTo(this.collection, 'reset', this.updateResults, this);
                },
                show: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    //show view
                    this.region.show(this.view);
                },
                searchTriggered: function() {
                    //get search text
                    this.sText = this.view.getSearchText();
                    this.search();
                },
                search: function() {
                    this.collection.fetch({reset: true}, {}, {search: this.sText, all: 'True'}, false);
                }

            });

            return SideController;
        });
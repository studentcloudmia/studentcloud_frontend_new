// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/sbuilder/sidebar/templates/_sidebar.tmpl'
], function(Marionette, App, vent, template) {

    var SideItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'searchRow',
        events: {
            'click': 'triggerSelected'
        },
        onRender: function() {
            //add data-id attribute to item
            this.$el.attr('data-id', this.model.id);
            this.$el.attr('data-elemid', this.model.get('model_id'));
            //make the item draggable
            this.$el.draggable({revert: "invalid", helper: "clone"});
        },
        triggerSelected: function() {
            vent.trigger('Sbuilder:Selected:Model', this.model.id);
        }
    });

    return SideItemView;

});
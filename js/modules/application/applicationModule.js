// cbuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/application/router',
    'modules/application/controller'
],
        function(Marionette, App, vent, Router, Controller) {

            var ApplicationModule = App.module('Application');

            //bind to module finalizer event
            ApplicationModule.addFinalizer(function() {

            });

            ApplicationModule.addInitializer(function(options) {

                new Router({
                    controller: new Controller()
                });

            });

            return ApplicationModule;
        });
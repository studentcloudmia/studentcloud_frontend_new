// cbuilder application
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/application/app/controllers/controller'
],
        function(Marionette, App, vent, Controller) {

            var ApplicationModule = App.module('Application');
            ApplicationModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.layout = App.request("Application:getLayout");
                    this.region = options.region;
                    this.showApp();
                },
                showApp: function() {
                    new Controller({region: this.region, layout: this.layout});
                }

            });
            return ApplicationModule;
        });
// SideView
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/application/app/templates/ContactContent.tmpl'
], function(_, Marionette, App, vent, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid maxHeigh',
        initialize: function() {
            this.globalCount = 0;
            this.listenTo(vent, 'Application:SubmitApp:update', this.save);
        },
        ui: {
            states1: '.stateSelector1',
            states2: '.stateSelector2',
            countries1: '.phoneCountrySelector1',
            countries2: '.phoneCountrySelector2',
            form: '#personalForm',
            zip: '.zipFormat',
            phone: '.phoneFormat',
            pasrt1: '.part1'
        },
        events: {
            'click .gotoAddress': 'scrollAddress',
            'click .gotoPhone': 'scrollPhone',
            'change .required': 'requiredChanged'
        },
        requiredChanged: function(e) {
            var self = this;
            var count;
            count = (self.model.get('sec1') + self.model.get('sec3'));
            $(":input.required").each(function(i, val) {
                console.log("HERE");
                if ($(val).val().length > 0 && !(($(val).val().indexOf("AK") >= 0)) && !(($(val).val().indexOf("Afghanistan") >= 0))) {
                    console.log("VAL ", val);
                    count++;
                }

            });
            vent.trigger('Admissions:Application:updateProgress', count);
        },
        onRender: function() {
            var self = this;
            //mask inputs
            self.ui.zip.mask('99999-9999');
            self.ui.phone.mask('999-999-9999');
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker , .picker__select--month, .picker__select--year").jqBootstrapValidation();
            });

        },
        onShow: function() {

        },
        onBeforeRender: function() {
            /// this.originalWidth = (100 * parseFloat($('#main').css('width')) / parseFloat(($('#main').parent().css('width')))) + '%';
            // $('#main').css('width', '100%');
        },
        save: function() {

            var self = this;
            $(":input.required").each(function(i, val) {
                if ($(val).val().length > 0 && !(($(val).val().indexOf("AK -") >= 0)) && !(($(val).val().indexOf("Afghanistan") >= 0))) {
                    self.globalCount++;
                }

            });
            this.model.set({sec2: self.globalCount});

            var data = Backbone.Syphon.serialize(this);
            if (data.preferred_phone) {
                data.preferred_phone = 1;
            }
            else {
                data.preferred_phone = 0;
            }

            if (data.preferred_phone_2) {
                data.preferred_phone_2 = 1;
            }
            else {
                data.preferred_phone_2 = 0;
            }
            this.model.set(
                    {
                        Addresses:
                                [
                                    {
                                        Address_Part1: data.Address_Part1,
                                        Address_Part2: data.Address_Part2,
                                        Address_Part3: data.Address_Part3,
                                        Address_Part4: data.Address_Part4,
                                        State: data.State,
                                        City: data.City,
                                        ZipCode: data.ZipCode,
                                        address_type: data.address_type
                                    },
                                    {
                                        Address_Part1: data.Address_Part1_2,
                                        Address_Part2: data.Address_Part2_2,
                                        Address_Part3: data.Address_Part3_2,
                                        Address_Part4: data.Address_Part4_2,
                                        State: data.State_2,
                                        City: data.City_2,
                                        ZipCode: data.ZipCode_2,
                                        address_type: data.address_type_2
                                    }
                                ],
                        Phones:
                                [
                                    {
                                        phone_number: data.phone_number,
                                        phone_type: data.phone_type,
                                        phone_ext: data.phone_ext,
                                        phone_country_code: data.phone_country_code,
                                        enable: data.enable,
                                        preferred_phone: data.preferred_phone
                                    },
                                    {
                                        phone_number: data.phone_number_2,
                                        phone_type: data.phone_type_2,
                                        phone_ext: data.phone_ext_2,
                                        phone_country_code: data.phone_country_code_2,
                                        enable: data.enable_2,
                                        preferred_phone: data.preferred_phone_2
                                    }
                                ]
                    }
            );
        },
        onBeforeClose: function() {
            this.save();
        },
        onClose: function() {
            $('#main').css('width', this.originalWidth);
        },
        setStates: function(states) {
            var self = this;
            //inject options to countries select
            if (states.length > 0) {
                this.ui.states1.html('');
                this.ui.states2.html('');
                _.each(states, function(state) {
//mark as selected if state matches model state
                    var sel = "";
                    var sel2 = "";
                    if (typeof self.model.get('user_personal_info').Addresses != 'undefined') {
                        sel = (state.id == self.model.get('user_personal_info').Addresses[0].State) ? 'selected' : '';
                        sel2 = (state.id == self.model.get('user_personal_info').Addresses[1].State) ? 'selected' : '';
                    }

                    var statesVal = state.Description_Short + ' - ' + state.Description_Long
                    self.ui.states1.append('<option value="' + state.id + '" ' + sel + '>' + statesVal + '</option>');
                    self.ui.states2.append('<option value="' + state.id + '" ' + sel2 + '>' + statesVal + '</option>');
                });
            }
            else {
                this.ui.states1.html('<option selected>No States</option>');
                this.ui.states2.html('<option selected>No States</option>');
            }
        },
        setCountries: function(countries) {
//inject options to countries select
            var self = this;
            this.ui.countries1.html('');
            this.ui.countries2.html('');
            _.each(countries, function(country) {
//mark as selected if country matches model country
                var sel = "";
                var sel2 = "";
                if (typeof self.model.get('user_personal_info').Phones != 'undefined') {
                    sel = (country.id == self.model.get('user_personal_info').Phones[0].phone_country_code) ? 'selected' : '';
                    sel2 = (country.id == self.model.get('user_personal_info').Phones[1].phone_country_code) ? 'selected' : '';
                }

                self.ui.countries1.append('<option value="' + country.id + '" ' + sel + '>' + country.Description_Long + '</option>');
                self.ui.countries2.append('<option value="' + country.id + '" ' + sel2 + '>' + country.Description_Long + '</option>');
            });
            this.requiredChanged();

        },
        scrollAddress: function(e) {
            e.preventDefault();
            $('.addressTypes > li').removeClass('active');
            $(e.target.parentElement).addClass('active');
            var scroll_y = e.currentTarget.id.replace('address_', '');
            this.addressScroller = this.getScroller('admissionsApplication_address');
            this.addressScroller.scrollToElement('.eachAddres_' + scroll_y, 200);
        },
        scrollPhone: function(e) {
            e.preventDefault();
            $('.phoneTypes > li').removeClass('active');
            $(e.target.parentElement).addClass('active');
            var scroll_y = e.currentTarget.id.replace('phone_', '');
            this.phoneScroller = this.getScroller('admissionsApplication_phone');
            this.phoneScroller.scrollToElement('.eachPhone_' + scroll_y, 200);
        }
    });
    return MainContentView;
});
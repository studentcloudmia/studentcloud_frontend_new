// SideView
define([
    'underscore',
    'marionette',
    'app',
    'globalConfig',
    'vent',
    'tpl!modules/application/app/templates/PersonalContent.tmpl'
], function(_, Marionette, App, GC, vent, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid maxHeigh',
        initialize: function() {
            this.globalCount = 0;
            this.listenTo(vent, 'Application:SubmitApp:update', this.save);
        },
        ui: {
            dob: '.dob',
            ssn: '.ssn',
            req: '.required'
        },
        events: {
            'change .required': 'requiredChanged'
        },
        requiredChanged: function(e) {
            var self = this;
            var count;
            count = (self.model.get('sec2') + self.model.get('sec3'));
            $(":input.required").each(function(i, val) {
                if ($(val).val().length > 0 && !($(val).val().indexOf("- Select") >= 0)) {
                    count++;
                }

            });
            vent.trigger('Admissions:Application:updateProgress', count);
        },
        onShow: function() {
            this.requiredChanged();
        },
        onBeforeRender: function() {
            //this.originalWidth = (100 * parseFloat($('#main').css('width')) / parseFloat(($('#main').parent().css('width')))) + '%';
            //$('#main').css('width', '100%');
        },
        onRender: function() {
            var self = this;
            //mask inputs
            self.ui.ssn.mask('999-99-9999');
            self.$dob = self.$el.find('.dob').pickadate(GC.pickadateDefaults);
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker , .picker__select--month, .picker__select--year").jqBootstrapValidation();
            });
        },
        save: function() {

            var self = this;
            $(":input.required").each(function(i, val) {
                if ($(val).val().length > 0 && !($(val).val().indexOf("- Select") >= 0)) {
                    self.globalCount++;
                }

            });

            this.model.set({sec1: self.globalCount});
            var data = Backbone.Syphon.serialize(this);
            if (typeof self.model.get('user_personal_info') != 'undefined') {

                data.citizenship_country = self.model.get('user_personal_info').citizenship_country;
                data.citizenship_status = self.model.get('user_personal_info').citizenship_status;
                data.passport_country = self.model.get('user_personal_info').passport_country;
                data.passport_number = self.model.get('user_personal_info').passport_number;
                data.visa_type = self.model.get('user_personal_info').visa_type;
                data.visa_number = self.model.get('user_personal_info').visa_number;
                data.visa_status = self.model.get('user_personal_info').visa_status;
                data.ethnic_group = self.model.get('user_personal_info').ethnic_group;
                data.military_veteran_status = self.model.get('user_personal_info').military_veteran_status;

            }

            this.model.set({
                is_active: '1',
                username: data.username,
                password: data.password,
                usergroups: [
                    {
                        uid: data.username,
                        group_name: 'prospects'
                    }

                ],
                user_internal_info: {
                    'uid': data.username,
                    'user_info': user_info = {
                        'username': data.username
                    },
                    'eff_date': '09/01/2012'
                },
                user_personal_info: {
                    url: "http://www.google.com",
                    uid: data.username,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    gender: data.gender,
                    DOB: data.DOB,
                    prefix: data.prefix,
                    ssn: data.ssn,
                    suffix: data.suffix,
                    marital_status: data.marital_status,
                    citizenship_country: data.citizenship_country,
                    citizenship_status: data.citizenship_status,
                    passport_country: data.passport_country,
                    passport_number: data.passport_number,
                    visa_type: data.visa_type,
                    visa_number: data.visa_number,
                    visa_status: data.visa_status,
                    ethnic_group: data.ethnic_group,
                    military_veteran_status: data.military_veteran_status
                }
            });
        },
        onBeforeClose: function() {
            this.save();
        },
        onClose: function() {
            $('#main').css('width', this.originalWidth);
        }
    });
    return MainContentView;
});
// SideView
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/application/app/templates/IdentificationContent.tmpl'
], function(_, Marionette, App, vent, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid maxHeigh',
        initialize: function() {
            this.globalCount = 0;
            this.listenTo(vent, 'Application:SubmitApp:update', this.save);
        },
        ui: {
            citizenshipCountries: '#citizenship_country',
            passportCountries: '#passport_country'
        },
        events: {
            'change .required': 'requiredChanged'
        },
        requiredChanged: function(e) {
            var self = this;
            var count;
            count = (self.model.get('sec1') + self.model.get('sec2'));
            $(":input.required").each(function(i, val) {
                if ($(val).val().length > 0 && !(($(val).val().indexOf("- Select") >= 0)) && !(($(val).val().indexOf("Afghanistan") >= 0))) {
                    count++;
                }

            });
            vent.trigger('Admissions:Application:updateProgress', count);
        },
        onShow: function() {
            this.requiredChanged();
        },
        onBeforeRender: function() {
            // this.originalWidth = (100 * parseFloat($('#main').css('width')) / parseFloat(($('#main').parent().css('width')))) + '%';
            // $('#main').css('width', '100%');
        },
        onClose: function() {
            $('#main').css('width', this.originalWidth);
        },
        setCountries: function(countries) {
//inject options to countries select
            var self = this;
            this.ui.citizenshipCountries.html('');
            this.ui.passportCountries.html('');
            _.each(countries, function(country) {
//mark as selected if country matches model country
                var sel = "";
                if (typeof self.model.get('user_personal_info') != 'undefined') {
                    sel = (country.id == self.model.get('user_personal_info').citizenship_country) ? 'selected' : '';
                }

                self.ui.citizenshipCountries.append('<option value="' + country.id + '" ' + sel + '>' + country.Description_Long + '</option>');
                self.ui.passportCountries.append('<option value="' + country.id + '" ' + sel + '>' + country.Description_Long + '</option>');
            });
            //trigger select on countries
            this.ui.citizenshipCountries.change();
            this.ui.passportCountries.change();
        },
        save: function() {
            var self = this;
            $(":input.required").each(function(i, val) {
                if ($(val).val().length > 0 && !(($(val).val().indexOf("- Select") >= 0)) && !(($(val).val().indexOf("Afghanistan") >= 0))) {
                    self.globalCount++;
                }

            });

            this.model.set({sec3: self.globalCount});
            var self = this;
            var data = Backbone.Syphon.serialize(this);
            data.first_name = self.model.get('user_personal_info').first_name;
            data.last_name = self.model.get('user_personal_info').last_name;
            data.gender = self.model.get('user_personal_info').gender;
            data.DOB = self.model.get('user_personal_info').DOB;
            data.prefix = self.model.get('user_personal_info').prefix;
            data.ssn = self.model.get('user_personal_info').ssn;
            data.suffix = self.model.get('user_personal_info').suffix;
            data.marital_status = self.model.get('user_personal_info').marital_status;
            data.username = self.model.get('user_personal_info').uid;

            this.model.set(
                    {
                        user_personal_info: {
                            citizenship_country: data.citizenship_country,
                            citizenship_status: data.citizenship_status,
                            passport_country: data.passport_country,
                            passport_number: data.passport_number,
                            visa_type: data.visa_type,
                            visa_number: data.visa_number,
                            visa_status: data.visa_status,
                            ethnic_group: data.ethnic_group,
                            military_veteran_status: data.military_veteran_status,
                            uid: data.username,
                            url: "http://www.google.com",
                            first_name: data.first_name,
                            last_name: data.last_name,
                            gender: data.gender,
                            DOB: data.DOB,
                            prefix: data.prefix,
                            ssn: data.ssn,
                            suffix: data.suffix,
                            marital_status: data.marital_status

                        }
                    }
            );
        },
        onBeforeClose: function() {
            this.save();
        }
    });
    return MainContentView;
});
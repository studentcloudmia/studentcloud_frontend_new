// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/application/app/views/PersonalContentView',
    'modules/application/app/views/ContactContentView',
    'modules/application/app/views/IdentificationContentView',
    //models
    'entities/models/user/UserModelAdd',
    //collections
    'entities/collections/address/StateCollection',
    'entities/collections/address/CountryCollection'

],
        function(Marionette, App, vent, PersonalContentView, ContactContentView, IdentificationContentView, UserModel, StateCollection, CountryCollection) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.layout = options.layout;
                    this.opts = options.opts;
                    this.model = new UserModel();
                    this.model.set({sec1: 0, sec2: 0, sec3: 0}, {silent: true});
                    this.entitylisteners();
                    this.view = new PersonalContentView({opts: this.opt, model: this.model});
                    this.region.show(this.view);
                    this.states = new StateCollection();
                    this.countries = new CountryCollection();
                    this.listenTo(this.states, 'sync', function() {
                        this.view.setStates(this.states.toJSON());
                    }, this);
                    this.listenTo(this.countries, 'sync', function() {
                        this.view.setCountries(this.countries.toJSON());
                    }, this);
                    this.listenTo(vent, 'Admissions:Application:updateProgress', this.updateProgress, this);
                    this.listenTo(vent, 'Application:showSection', this.showSection);
                    this.listenTo(vent, 'Application:SubmitApp:save', this.save, this);

                    $(this.layout.ui.progress).progressbar({
                        transition_delay: 300,
                        refresh_speed: 50,
                        display_text: 2,
                        use_percentage: true
                    });
                },
                onClose: function() {
                    this.view.close();
                },
                updateProgress: function(count) {
                    $(this.layout.ui.progress).css('width', '100%');
                    $(this.layout.ui.progress).css('width', (100 * (count / 26)).toFixed(0) + '%');
                    $(this.layout.ui.progress).html((100 * (count / 26)).toFixed(0) + '%');
                },
                showSection: function(e) {
                    if (e.currentTarget.id == 'personal') {
                        this.view = new PersonalContentView({opts: this.opt, model: this.model});
                        this.region.show(this.view);
                        $('.appNav > li').removeClass('active');
                        $(e.target.parentElement).addClass('active');
                    }
                    else if (e.currentTarget.id == 'contact') {
                        this.view = new ContactContentView({opts: this.opt, model: this.model});
                        this.region.show(this.view);
                        this.states.fetch({}, {id: '218'}, {}, false);
                        this.countries.fetch({}, {}, {}, false);
                        $('.appNav > li').removeClass('active');
                        $(e.target.parentElement).addClass('active');
                    }
                    else if (e.currentTarget.id == 'identification') {
                        this.view = new IdentificationContentView({opts: this.opt, model: this.model});
                        this.region.show(this.view);
                        this.countries.fetch({}, {}, {}, false);
                        $('.appNav > li').removeClass('active');
                        $(e.target.parentElement).addClass('active');
                    }

                },
                entitylisteners: function() {
                    var self = this;
                    //listen to model create
                    this.listenTo(this.model, 'created', function() {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Success',
                            message: 'Application Sucessfully Submited'
                        });
                    }, this);
                },
                save: function() {
                    var ssn2 = (this.model.get('user_personal_info').ssn.replace('-', '')).replace('-', '');
                    this.model.get('user_personal_info').ssn = ssn2;
                    //checks empty addresses are not sent to the server
                    var addressArr = [];
                    if (typeof this.model.get('Addresses') != 'undefined') {
                        if (this.model.get('Addresses')[0].Address_Part1 != '') {
                            addressArr.push(this.model.get('Addresses')[0]);
                            this.model.get('user_personal_info').Addresses = addressArr;
                        }
                        if (this.model.get('Addresses')[1].Address_Part1 != '') {
                            addressArr.push(this.model.get('Addresses')[1]);
                            this.model.get('user_personal_info').Addresses = addressArr;
                        }
                    }
                    if (typeof this.model.get('Phones') != 'undefined') {
                        //checks empty phones are not sent to the server
                        var phoneArr = [];
                        if (this.model.get('Phones')[0].phone_number != '') {
                            var phone = this.model.get('Phones')[0].phone_number;
                            this.model.get('Phones')[0].phone_number = (phone.replace('-', '')).replace('-', '');
                            phoneArr.push(this.model.get('Phones')[0]);
                            this.model.get('user_personal_info').Phones = phoneArr;
                        }
                        if (this.model.get('Phones')[1].phone_number != '') {
                            var phone = this.model.get('Phones')[1].phone_number;
                            this.model.get('Phones')[1].phone_number = (phone.replace('-', '')).replace('-', '');
                            phoneArr.push(this.model.get('Phones')[1]);
                            this.model.get('user_personal_info').Phones = phoneArr;
                        }
                    }

                    //this.model.unset('Phones', {silent: true});
                    //this.model.unset('Addresses', {silent: true});
                    console.log('model to save ', this.model);
                    this.model.save();
                }

            });

            return Controller;
        });
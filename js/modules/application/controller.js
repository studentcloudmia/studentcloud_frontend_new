define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/application/views/layout',
    //controllers
    'modules/application/app/app_controller'
],
        function(Marionette, App, vent, LayoutView, ApplicationModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.layout = new LayoutView();
                    App.reqres.setHandler("Application:getLayout", function() {
                        var layout = new LayoutView();
                        return layout;
                    });
                    this.listenTo(this.layout, 'exitApp', function() {
                        console.log('EXIT APP?');
                        vent.trigger('Components:Alert:Confirm', {
                            heading: 'Exit',
                            message: 'Are you sure you want to exit?',
                            cancelText: 'Cancel',
                            confirmText: 'Exit',
                            callee: this,
                            callback: this.confirmedExit
                        });
                    }, this);
                    this.listenTo(vent, 'Application:show', function() {
                        App.navigate('application', {trigger: true});
                        this.show();
                    }, this);


                },
                show: function() {
                    App.main.show(this.layout);
                    new ApplicationModule.Controller({region: this.layout.mainContent});
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Application:getLayout");
                },
                confirmedExit: function() {
                    App.navigate('login', {trigger: true});
                }
            });

            return Controller;

        });
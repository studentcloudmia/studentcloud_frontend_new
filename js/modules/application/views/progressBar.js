define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/application/templates/progressBar.tmpl'
],
        function(Marionette, App, vent, template) {
            "use strict";
            var ProgressBar = App.Views.Layout.extend({
                template: template,
                tagName: 'div',
                id: 'verticalProgressWrapper',
                className: 'verticalProgress'
            });
            return ProgressBar;
        });
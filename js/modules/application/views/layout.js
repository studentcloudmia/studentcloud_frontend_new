define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/application/templates/layout.tmpl'
],
        function(Marionette, App, vent, template) {
            "use strict";
            var LayoutView = App.Views.Layout.extend({
                template: template,
                ui: {
                    progress: '#progress',
                    exit: '.exit'
                },
                regions: {
                    mainContent: '#appSection'
                },
                events: {
                    'click .showSection': 'showSection',
                    'click .submitApp': 'submit',
                    'click .section': 'prevent'

                },
                triggers: {
                    'click .exit': 'exitApp'
                },
                prevent: function(e) {
                    e.preventDefault();
                },
                showSection: function(e) {
                    e.preventDefault();
                    vent.trigger('Application:showSection', e);
                },
                submit: function() {
                    vent.trigger("Application:SubmitApp:update");
                    vent.trigger("Application:SubmitApp:save");
                }
            });
            return LayoutView;
        });
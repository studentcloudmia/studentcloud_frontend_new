define([
    'marionette',
    'app'
],
        function(Marionette, app) {

            var Router = Marionette.AppRouter.extend({
                appRoutes: {
                    'application': 'show'
                }
            });

            return Router;

        });
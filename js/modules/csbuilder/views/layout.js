define([
    'marionette',
    'app',
    'tpl!modules/csbuilder/templates/layout.tmpl'
],
        function(Marionette, App, template) {
            "use strict";

            var LayoutView = App.Views.Layout.extend({
                template: template,
                regions: {
                    header: "#appHeader",
                    //class data
                    classContent: '#classMain',
                    //popovers
                    instructors: '#instructors',
                    instructorsPopover: '#instructorsPopover',
                    textbooks: '#textbooks',
                    textbooksPopover: '#textbooksPopover',
                    sections: '#sections',
                    sectionsPopover: '#sectionsPopover',
                    addNewSection: '#addNewSection',
                    footer: "#bottomMenu"
                }

            });
            return LayoutView;
        });
// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/csbuilder/sidebar/views/SidebarView'
],
        function(Marionette, App, vent, SidebarView) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.collection;
                    this.searchString = options.searchString;
                    this.row = options.row;
                    this.view = new SidebarView({collection: this.collection, searchString: this.searchString, row: this.row});
                    this.listenTo(this.view, 'selectedModel', this.selectedModel, this);
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'search', this.searchTriggered, this);
                },
                show: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    //show view
                    this.region.show(this.view);
                },
                selectedModel: function(modelId) {
                    this.trigger('selectedModel', modelId);
                },
                searchTriggered: function() {
                    //get search text
                    this.sText = this.view.getSearchText();
                    this.search();
                },
                search: function() {
                    if (this.row == 'instructorsSearchRow')
                        this.collection.fetch({reset: true}, {}, {uid: this.sText}, false);
                    else
                        this.collection.fetch({reset: true}, {}, {search: this.sText}, false);
                }

            });
            return SideController;
        });
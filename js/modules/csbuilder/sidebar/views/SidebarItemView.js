// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/csbuilder/sidebar/templates/_sidebarInstructor.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/_sidebarClass.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/_sidebarCourse.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/_sidebarTextbook.tmpl'
], function(Marionette, App, vent, instructorTemplate, classTemplate, courseTemplate, textbookTemplate) {

    var SideItemView = App.Views.ItemView.extend({
        tagName: 'tr',
        template: classTemplate,
        initialize: function(options) {
            this.row = options.row;
            if (this.row == 'searchRow')
                this.template = classTemplate;
            else if (this.row == 'instructorsSearchRow')
                this.template = instructorTemplate;
            else if (this.row == 'coursesSearchRow')
                this.template = courseTemplate;
            else if (this.row == 'textbooksSearchRow')
                this.template = textbookTemplate;
        },
        triggers: {
            'click': 'selectedModel'
        },
        onRender: function() {
            //add data-id attribute to item
            this.$el.attr('data-id', this.model.getId());
            this.$el.attr('data-serverid', this.model.getId());
            //make the item draggable
            this.$el.draggable({revert: "invalid", helper: "clone"});
            this.$el.addClass(this.row);
        }
    });

    return SideItemView;

});
// SideView
define([
    'marionette',
    'app',
    'modules/csbuilder/sidebar/views/SidebarItemView',
    'tpl!modules/csbuilder/sidebar/templates/sidebarInstructor.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/sidebarTextbook.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/sidebarClass.tmpl',
    'tpl!modules/csbuilder/sidebar/templates/sidebarCourse.tmpl'
], function(Marionette, App, ItemView, instructorTemplate, textbookTemplate, classTemplate, courseTemplate) {

    var SideView = App.Views.CompositeView.extend({
        itemView: ItemView,
        itemViewContainer: 'tbody',
        itemViewOptions: {},
        initialize: function(options) {
            this.setSearchString = options.searchString;
            this.row = options.row;
            if (this.row == 'searchRow')
                this.template = classTemplate;
            else if (this.row == 'instructorsSearchRow')
                this.template = instructorTemplate;
            else if (this.row == 'coursesSearchRow')
                this.template = courseTemplate;
            else if (this.row == 'textbooksSearchRow')
                this.template = textbookTemplate;
            this.listenTo(this, 'childview:selectedModel', this.selectedModel);
            this.itemViewOptions.row = this.row;
        },
        triggers: {
            'change #searchField': 'search'
        },
        ui: {
            search: '#searchField'
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.val('');
            this.ui.search.blur();
            return val;
        },
        selectedModel: function(view) {
            this.trigger('selectedModel', view.model.getId());
        },
        onShow: function() {
            this.$el.find('#searchField').attr('placeholder', 'Search ' + this.setSearchString);
        }
    });

    return SideView;

});
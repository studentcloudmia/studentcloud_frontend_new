define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/csbuilder/views/layout',
    //controllers
    'modules/csbuilder/class/class_controller'
],
        function(Marionette, App, vent, LayoutView, CsbuilderClassModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function() {

                    App.reqres.setHandler("Csbuilder:getLayout", function() {
                        var layout = new LayoutView();
                        return layout;
                    });

                    this.listenTo(vent, 'Csbuilder:showClass', function() {
                        App.navigate('csbuilder/class');
                        this.showClass();
                    }, this);

                },
                showClass: function() {
                    //transfer control to show submodule
                    this.layout = new LayoutView();
                    this.listenTo(this.layout, 'show', this.showAllContent, this);
                    App.main.show(this.layout);
                },
                showAllContent: function() {
                    //show header
                    var optionsHeader = {
                        region: this.layout.header,
                        heading: 'Class Builder',
                        links: ""
                    };
                    vent.trigger('Components:Builders:Header:on', optionsHeader);
                    //show footer
                    var optionsFooter = {
                        region: this.layout.footer
                    };
                    vent.trigger('Components:Builders:Footer:on', optionsFooter);
                    //show main class content
                    new CsbuilderClassModule.Controller({layout: this.layout});


                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Csbuilder:getLayout");
                    App.reqres.removeHandler("Csbuilder:getLinks");
                }

            });

            return Controller;

        });
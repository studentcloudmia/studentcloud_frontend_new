// csbuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/csbuilder/router',
    'modules/csbuilder/controller'
],
        function(Marionette, App, vent, Router, Controller) {

            var CsbuilderModule = App.module('Csbuilder');

            //bind to module finalizer event
            CsbuilderModule.addFinalizer(function() {

            });

            CsbuilderModule.addInitializer(function() {

                new Router({
                    controller: new Controller()
                });

            });

            return CsbuilderModule;
        });
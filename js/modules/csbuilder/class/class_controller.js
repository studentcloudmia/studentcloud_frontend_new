// csbuilder class
define(['marionette',
    'app',
    'vent',
    'utilities',
    //CONTROLLERS
    'modules/csbuilder/sidebar/controllers/Sidebar_controller',
    'modules/csbuilder/class/controllers/Requirements_controller',
    'modules/csbuilder/class/schedule/schedule_controller',
    'modules/csbuilder/class/popovers/popovers_controller',
    //MODELS
    'entities/models/class/ClassScheduleModel',
    'entities/models/class/ClassNoteModel',
    'entities/models/course/CourseModel',
    //COLLECTIONS
    'entities/collections/class/ClassScheduleCollection',
    'entities/collections/course/CourseCollection',
    'entities/collections/schedule/ScheduleCollection',
    'entities/collections/textbook/TextbookCollection',
    'entities/collections/user/UserCollection',
    'entities/collections/terms/ClassTermCollection',
    'entities/collections/lms/LmsProviderCollection',
    'entities/collections/dictionary/DictionaryCollection',
    'entities/collections/location/BuildingCollection',
    'entities/collections/location/RoomCollection',
    'entities/collections/exam/ExamCodeCollection',
    'entities/collections/class/ClassTopics',
    //VIEWS
    'modules/csbuilder/class/views/MainContentView'
],
        function(Marionette, App, vent, utilities,
                //CONTROLLERS
                SideController, Requirements_controller, CsbuilderScheduleModule,
                PopoverController,
                //MODELS
                ClassModel, ClassNoteModel, CourseModel,
                //COLLECTIONS
                ClassCollection, CourseCollection, ScheduleCollection, TextbookCollection,
                InstructorCollection, ClassTermCollection, LmsProviderCollection, DictionaryCollection,
                BuildingCollection, RoomCollection, ExamCodeCollection, TopicCollection,
                //VIEWS
                MainContentView) {
            var CsbuilderClassModule = App.module('Csbuilder.Class');
            CsbuilderClassModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    this.layout = options.layout;
                    this.region = this.layout.classContent;
                    //Class Collections (main collections)
                    this.collection = new ClassCollection();
                    this.countColl = new ClassCollection();
                    //return control of sidebar to me
                    this.listenTo(vent, 'Csbuilder:ClassBuilderController:Sidebar:on', function() {
                        this.sidebarController.close();
                        this.sidebarController = new SideController({collection: this.collection, searchString: 'Class', row: 'searchRow'});
                        //ask for sidebar
                        vent.trigger('Sidebar:On', {controller: this.sidebarController});
                        this.listenTo(this.sidebarController, 'selectedModel', this.selectedModel, this);
                    }, this);
                    //ask for sidebar
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Classes', row: 'searchRow'});
                    vent.trigger('Sidebar:On', {controller: this.sidebarController});
                    //listen to sidebar selecting result
                    this.model = new ClassModel();

                    this.listenTo(this.sidebarController, 'selectedModel', this.selectedModel, this);
                    this.courseModel = new CourseModel();
                    this.listenTo(this.courseModel, 'sync', function() {
                        this.view.setHeadder(this.courseModel.toJSON());
                        vent.trigger('Sidebar:Close');
                    }, this);
                    //class note grab
                    this.classNoteModel = new ClassNoteModel();
                    this.listenTo(this.classNoteModel, 'sync', function() {
                        this.view.resetClassNote(this.classNoteModel.toJSON());
                    }, this);
                    //basic functions:
                    this.bindFooterEvents();
                    this.getCount();
                    this.activateDroppable();
                    this.listenTo(this.layout, 'close', this.close, this);
                    this.modelListeners();
                },
                getCount: function() {
                    this.countColl.fetch({}, {}, {length: 'True'}, false);
                    this.listenTo(this.countColl, 'sync', this.countReturn, this);
                },
                countReturn: function() {
                    var count = this.countColl.at(0).get('RowCount');
                    //delete reference to countColl
                    this.countColl = null;
                    if (count > 0) {
                        //open sidebar to allow search
                        vent.trigger('Sidebar:Open');
                        //update footer buttons
                        this.updateFooter({add: true});
                    } else {
                        //show user empty form to allow them to add
                        this.add();
                    }
                },
                add: function() {
                    //show user empty form to allow them to add
                    this.model = new ClassModel();
                    this.schedule = new ScheduleCollection();
                    this.textbook = new TextbookCollection();
                    this.instructor = new InstructorCollection();
                    this.modelListeners();
                    this.showMain();
                    //update footer buttons
                    this.updateFooter({save: true});
                },
                showMain: function() {
                    var self = this;
                    //hide sidebar
                    vent.trigger('Sidebar:Close');
                    //update header details
                    vent.trigger('Components:Builders:Header:changeDetails', {model: this.model});
                    this.view = new MainContentView({model: this.model});

                    //location collections
                    this.buildings = new BuildingCollection();
                    this.rooms = new RoomCollection();
                    this.buildings.fetch();
                    //exam code
                    this.examCode = new ExamCodeCollection();
                    this.examCode.fetch();
                    //topic
                    this.topics = new TopicCollection();
                    this.topics.fetch();
                    this.entityListeners();
                    this.viewListeners();
                    this.region.show(this.view);
                    //create enetities
                    this.scheduleController = new CsbuilderScheduleModule.Controller({
                        collection: self.schedule,
                        region: this.layout.sections,
                        popOverRegion: this.layout.addNewSection,
                        addNewRegion: this.layout.addNewSection
                    });
                    this.scheduleController.initSchedule();

                    this.textbookControlelr = new PopoverController.Controller({
                        collection: self.textbook,
                        region: this.layout.textbooks,
                        popOverRegion: this.layout.textbooksPopover,
                        popover: {
                            badge: 'Textbooks',
                            type: 'textbook',
                            row: 'textbooksSearchRow',
                            searchString: 'Textbooks',
                            name: 'textbook',
                            one: 'instructor'
                        }
                    });
                    this.instructorController = new PopoverController.Controller({
                        collection: self.instructor,
                        region: this.layout.instructors,
                        popOverRegion: this.layout.instructorsPopover,
                        popover: {
                            badge: 'Instructors',
                            type: 'instructor',
                            row: 'instructorsSearchRow',
                            searchString: 'Instructors',
                            name: 'instructor',
                            one: 'textbook'
                        }
                    });

                    this.textbookControlelr.initPopover();
                    this.instructorController.initPopover();

                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'searchCourse', function() {
                        self.courseCollection = new CourseCollection();
                        self.courseSidebar = new SideController({collection: self.courseCollection, searchString: 'Course', row: 'coursesSearchRow'});
                        vent.trigger('Sidebar:On', {controller: self.courseSidebar});
                        vent.trigger('Sidebar:Open');
                        //selecting search result:
                        this.activateDroppableCourse();
                        this.listenTo(this.courseSidebar, 'selectedModel', this.selectedCourse, this);
                        this.view.showCancelSearch();
                    }, this);
                    this.listenTo(this.view, 'showRequiremnts', function(reqs) {
                        this.requirements_controller = new Requirements_controller({reqs: reqs});
                    }, this);
                    this.listenTo(this.view, 'getNote', function(noteId) {
                        if (noteId != '') {
                            this.classNoteModel.id = noteId;
                            this.classNoteModel.fetch();
                        }
                    }, this);
                    this.listenTo(this.buildings, 'sync', function() {
                        this.view.resetBuildings(this.buildings.toJSON());
                    }, this);
                    this.listenTo(this.rooms, 'sync', function() {
                        this.view.resetRooms(this.rooms.toJSON());
                    }, this);
                    this.listenTo(this.view, 'buildingChange', function(building) {
                        this.rooms.fetch({}, {building: building}, {}, false);
                    }, this);
                    this.listenTo(this.examCode, 'sync', function() {
                        this.view.resetExamCode(this.examCode.toJSON());
                    }, this);
                    this.listenTo(this.topics, 'sync', function() {
                        this.view.resetTopics(this.topics.toJSON());
                    }, this);
                },
                modelListeners: function() {
                    var self = this;
                    this.listenTo(this.model, 'sync', function() {
                        this.modelSelected();
                    }, this);
                    //listen to model create
                    this.listenTo(this.model, 'created', function() {
                        self.created();
                    }, this);
                    //listen to model destroy
                    this.listenTo(this.model, 'destroyed', function() {
                        self.destroyed();
                    }, this);
                    //listen to model update
                    this.listenTo(this.model, 'updated', function() {
                        self.updated();
                    }, this);
                },
                entityListeners: function() {
                    //dropdown selects
                    this.classTermCollection = new ClassTermCollection();
                    this.classTermCollection.fetch();
                    this.listenTo(this.classTermCollection, 'sync', function() {
                        this.view.resetTerms(this.classTermCollection.toJSON());
                    }, this);
                    this.lmsProviderCollection = new LmsProviderCollection()
                    this.lmsProviderCollection.fetch();
                    this.listenTo(this.lmsProviderCollection, 'sync', function() {
                        this.view.resetLMSProvider(this.lmsProviderCollection.toJSON());
                    }, this);
                    this.classStatus = new DictionaryCollection();
                    this.classStatus.getClassStatus();
                    this.listenTo(this.classStatus, 'sync', function() {
                        this.view.resetClassStatus(this.classStatus.toJSON());
                    }, this);


                },
                updateFooter: function(options) {
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                bindFooterEvents: function() {
                    var self = this;
                    self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
                    self.listenTo(vent, 'Components:Builders:Footer:add', self.add, self);
                    self.listenTo(vent, 'Components:Builders:Footer:del', self.del, self);
                    self.listenTo(vent, 'Components:Builders:Footer:prev', self.prev, self);
                    self.listenTo(vent, 'Components:Builders:Footer:next', self.next, self);
                },
                //selected model takes either an eff_date or a model id
                selectedModel: function(modelId) {
                    var self = this;
                    self.navModel = this.collection.get(modelId);
                    //model id is passed
                    self.model.id = modelId;
                    self.model.fetch();
                },
                modelSelected: function() {
                    var self = this;
                    self.model.set({
                        exam_start_time: moment(this.model.get('exam_start_time'), "HH:mm:ss").format("hh:mm a"),
                        exam_end_time: moment(this.model.get('exam_end_time'), "HH:mm:ss").format("hh:mm a")
                    });
                    _.each(self.model.get('scheduleclass_id'), function(schl) {
                        schl.start_time = moment(schl.start_time, "HH:mm:ss").format("hh:mm a");
                        schl.end_time = moment(schl.end_time, "HH:mm:ss").format("hh:mm a");
                    });
                    //create peripheral enetities:
                    this.schedule = new ScheduleCollection();
                    _.each(self.model.get('scheduleclass_id'), function(sch) {
                        self.schedule.add(sch);
                    });
                    this.textbook = new TextbookCollection();
                    _.each(self.model.get('textbookclass_id'), function(txt) {
                        self.textbook.add(txt);
                    });
                    this.instructor = new InstructorCollection();
                    _.each(self.model.get('class_id'), function(instr) {
                        self.instructor.add(instr);
                    });
                    //bring in course info:
                    this.courseModel.id = self.model.get('course');
                    this.courseModel.fetch();

                    this.showMain();
                    //update footer buttons
                    if (this.collection.hasPrev(this.navModel) && this.collection.hasNext(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: true})
                    else if (this.collection.hasNext(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: false})
                    else if (this.collection.hasPrev(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: false, prev: true});
                    else
                        this.updateFooter({save: true, del: true, add: true, collNav: false, next: false, prev: false});


                },
                selectedCourse: function(modelId) {
                    this.courseModel.id = modelId;
                    this.courseModel.fetch();
                    vent.trigger("Csbuilder:ClassBuilderController:Sidebar:on");
                    this.view.hideCancelSearch();
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.classContent.el).droppable({
                        accept: ".searchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.selectedModel(ui.draggable.attr("data-serverid"));
                        }
                    });
                },
                activateDroppableCourse: function() {
                    var self = this;
                    $(this.view.ui.header).droppable({
                        accept: ".coursesSearchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.selectedCourse(ui.draggable.attr("data-id"));

                        }
                    });
                },
                /**** ENTITY SAVE DELETE UPDATE ****/
                save: function() {
                    vent.trigger('Csbuilder:Popover:Hide:Schedule');
                    //update model
                    if (this.view.updateModel()) {
                        this.confirmedSave();
                    }

                },
                confirmedSave: function() {
                    //append schedule to model
                    var scheduleArr = new Array;
                    _.each(this.schedule.models, function(model) {
                        scheduleArr.push(model.toJSON());
                    });
                    this.model.set({scheduleclass_id: scheduleArr});

                    //append instructors to model
                    var instrArr = new Array;
                    _.each(this.instructor.models, function(model) {
                        instrArr.push(model.toJSON());
                    });
                    this.model.set({class_id: instrArr});

                    //append textbooks to model
                    var txtbookArr = new Array;
                    _.each(this.textbook.models, function(model) {
                        txtbookArr.push(model.toJSON());
                    });
                    this.model.set('textbookclass_id', txtbookArr);

                    this.model.set('exam_start_time', moment(this.model.get('exam_start_time'), "hh:mm a").format("HH:mm:ss"));
                    this.model.set('exam_end_time', moment(this.model.get('exam_end_time'), "hh:mm a").format("HH:mm:ss"));
                    this.model.set('total_seats_avail', parseInt(this.model.get('total_seats_avail')));

                    _.each(this.model.get('scheduleclass_id'), function(sch, i) {
                        sch.start_time = moment(sch.start_time, "hh:mm a").format("HH:mm:ss");
                        sch.end_time = moment(sch.end_time, "hh:mm a").format("HH:mm:ss");
                    });

                    //save model
                    this.model.save();
                },
                del: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Delete?',
                        message: 'Are you sure you would like to delete this item',
                        callee: this,
                        callback: this.confirmedDel
                    });
                },
                confirmedDel: function() {
                    this.model.destroy();
                },
                created: function() {
                    this.updateFooter({save: true, add: true, del: true});
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Created'
                    });
                },
                updated: function() {
                    var self = this;
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Updated'
                    });
                    this.model.fetch();
                },
                destroyed: function() {
                    //redo search
                    this.sidebarController.search();
                    this.updateFooter({add: true});
                    this.add();
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Deleted'
                    });
                },
                /**** END ENTITY SAVE DELETE UPDATE ****/

                /**** START SEARCH RESULT NAVIGATION ****/
                prev: function() {
                    this.navModel = this.collection.prevModel(this.navModel);
                    this.selectedModel(this.navModel.id);
                },
                next: function() {
                    this.navModel = this.collection.nextModel(this.navModel);
                    this.selectedModel(this.navModel.id);
                }
                /**** END SEARCH RESULT NAVIGATION ****/
            });
            return CsbuilderClassModule;
        });
// SideView
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/csbuilder/class/popovers/templates/btnInstructor.tmpl',
    'tpl!modules/csbuilder/class/popovers/templates/btnTextbook.tmpl'
], function(_, Marionette, App, vent, isntructorTemplate, textbookTemplate) {

    var BtnView = App.Views.ItemView.extend({
        template: {},
        tagName: 'button',
        className: 'btn btn-inverse btn-mini popoverBtn',
        initialize: function(popover) {
            if (popover.name == "instructor")
                this.template = isntructorTemplate;
            else if (popover.name == "textbook")
                this.template = textbookTemplate;
        },
        ui: {
            badge: '.badge'
        },
        triggers: {
            'click': 'clicked'
        },
        resetCount: function(count) {
            this.ui.badge.html(count);
        },
        getClass: function() {
            return this.$el;
        }
    });

    return BtnView;

});
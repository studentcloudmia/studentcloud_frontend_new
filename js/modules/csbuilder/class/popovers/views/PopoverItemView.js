// Popover Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/csbuilder/class/popovers/templates/_instructor.tmpl',
    'tpl!modules/csbuilder/class/popovers/templates/_textbook.tmpl'
], function(Marionette, App, vent, instructorTemplate, textbookTemplate) {

    var PopoverItemView = App.Views.ItemView.extend({
        template: {},
        triggers: {
            'click .delItem  ': 'delete'
        },
        initialize: function(options) {
            if (options.popover.name == "instructor")
                this.template = instructorTemplate;
            else if (options.popover.name == "textbook")
                this.template = textbookTemplate;
        }
    });

    return PopoverItemView;

});
// csbuilder Instructor PopoverView view
define([
    'jquery',
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/csbuilder/class/popovers/views/PopoverItemView',
    'tpl!modules/csbuilder/class/popovers/templates/popover.tmpl'
], function($, _, Marionette, vent, App, ItemView, Template) {

    var PopoverView = App.Views.CompositeView.extend({
        itemView: ItemView,
        tagName: 'div',
        itemViewContainer: '.popOverContent',
        className: 'popOverWrapper animated bounceOutDown hidden noClick',
        template: Template,
        itemViewOptions: {},
        initialize: function(options) {
            this.popover = options.popover;
            this.itemViewOptions = {popover: options.popover};
            this.listenTo(this, 'childview:delete', this.triggerDelete);
            this.listenTo(vent, 'Csbuilder:Popover:Hide:' + options.popover.name, this.hideMe);
            this.listenTo(vent, 'Csbuilder:Popover:Hide:fromSchedule', this.hideMe);
        },
        triggerDelete: function(view) {
            if (this.popover.name == 'textbook')
                this.trigger('delete', view.model.get('ISBN'));
            else {
                this.trigger('delete', view.model.get('username'));
            }
        },
        hideMe: function() {
            this.$el.removeClass('bounceInUp').addClass('bounceOutDown');
        },
        toggle: function() {
            var self = this;
            if (this.$el.hasClass('bounceOutDown')) {
                this.$el.removeClass('bounceOutDown hidden noClick').addClass('bounceInUp allowClick');
                this.trigger('search');
                vent.trigger('Csbuilder:Popover:Hide:' + self.popover.one);
                vent.trigger('Csbuilder:Popover:Hide:Schedule');
                vent.trigger('Csbuilder:Popover:Hide:fromPopovers');
            }
            else {
                this.$el.removeClass('bounceInUp').addClass('bounceOutDown');
                //return control over the sidebar to class controller on view close
                vent.trigger("Csbuilder:ClassBuilderController:Sidebar:on");

            }
        },
        onClose: function() {
            this.$el.addClass('noClick');
        }
    });

    return PopoverView;

});
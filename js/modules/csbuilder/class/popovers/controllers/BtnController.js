// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/csbuilder/class/popovers/views/BtnView'
],
        function(Marionette, App, vent, BtnView) {

            var BtnController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.popover = options.popover;
                    this.view = new BtnView(options.popover);
                    this.show();
                    this.listenTo(vent, 'Csbuilder:' + self.popover.badge + ':Badge:update', this.resetCount, this);
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.view, 'clicked', this.getButtonElem, this);
                },
                resetCount: function(count) {
                    this.view.resetCount(count);
                },
                show: function() {
                    this.region.show(this.view);
                },
                getButtonElem: function() {
                    this.trigger('clicked', {btn: this.view.getClass()});
                }

            });

            return BtnController;
        });
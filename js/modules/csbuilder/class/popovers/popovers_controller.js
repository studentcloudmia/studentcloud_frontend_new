// csbuilder popovers
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/csbuilder/sidebar/controllers/Sidebar_controller',
    'modules/csbuilder/class/popovers/controllers/BtnController',
    'modules/csbuilder/class/popovers/controllers/PopoverController',
    //models
    'entities/collections/user/UserCollection',
    'entities/models/user/UserModel',
    'entities/collections/textbook/TextbookCollection',
    'entities/models/textbook/TextbookModel'
],
        function(Marionette, App, vent, SideController, BtnController, PopoverController, InstructorCollection, InstructorModel, TextbookCollection, TextbookModel) {

            var CsbuilderPopoversModule = App.module('Csbuilder.Popovers');
            CsbuilderPopoversModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    //option variables
                    this.popover = options.popover;
                    this.region = options.region;
                    this.popOverRegion = options.popOverRegion;
                    this.collection = options.collection;
                    var self = this;
                    //entities, decide which collection to use:
                    if (options.popover.type == 'instructor')
                        this.searchCollection = new InstructorCollection();
                    else
                        this.searchCollection = new TextbookCollection();

                    this.btnController = new BtnController({region: this.region, popover: self.popover});
                    //listeners
                    this.listenTo(this.btnController, 'close', this.close, this);
                    this.listenTo(this.btnController, 'clicked', this.showPopover, this);
                    //display collection length in badge
                    vent.trigger("Csbuilder:" + self.popover.badge + ":Badge:update", this.collection.length);
                    this.listenTo(this.collection, 'add remove', function() {
                        vent.trigger("Csbuilder:" + self.popover.badge + ":Badge:update", this.collection.length);
                    }, this);
                },
                del: function(modelId) {
                    var self = this;
                    if (self.popover.name == "instructor")
                        self.collection.remove(self.collection.where({username: modelId}));
                    else
                        self.collection.remove(self.collection.where({ISBN: modelId}));
                },
                initPopover: function() {
                    var self = this;
                    this.popoverController = new PopoverController({region: this.popOverRegion, popover: self.popover, coll: this.collection});
                    this.listenTo(this.popoverController, 'delete', this.del, this);
                    this.listenTo(this.popoverController, 'search', this.search);
                },
                showPopover: function() {
                    this.popoverController.showPopover();
                },
                search: function() {
                    var self = this;
                    this.sideController = new SideController({collection: this.searchCollection, searchString: self.popover.searchString, row: self.popover.row});
                    vent.trigger('Sidebar:On', {controller: this.sideController});
                    vent.trigger('Sidebar:Open');
                    this.activateDroppable();
                    this.listenTo(this.sideController, 'selectedModel', this.selectedModel, this);
                },
                showItem: function() {
                    //add model to collection
                    if (this.popover.name == 'instructor') {
                        this.model.set({instructor: 'alex', instructor_type: 40})
                        this.collection.add(this.model);
                    }
                    else if (this.popover.name == 'textbook') {
                        this.model.set({textbookclass_id: this.model.id});
                        if (!this.collection.findModelByParam(this.model, 'ISBN')) {
                            this.collection.add(this.model);
                        }
                    }

                },
                selectedModel: function(modelId) {
                    //fetch selected model
                    if (this.popover.type == 'instructor') {
                        this.model = new InstructorModel();
                        this.model.fetch({}, {}, {uid: modelId, basic: 1}, true);
                    }
                    else {
                        this.model = new TextbookModel();
                        this.model.fetch({}, {id: modelId}, {}, true);
                    }
                    this.listenTo(this.model, 'sync:stop', this.showItem, this);
                },
                activateDroppable: function() {
                    var self = this;
                    $('.popOverWrapper').droppable({
                        accept: "." + self.popover.row,
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.selectedModel(ui.draggable.attr("data-id"));
                        }
                    });
                }
            });
            return CsbuilderPopoversModule;
        });
// csbuilder req mainContent view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/csbuilder/class/templates/requirements.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var MainContentView = App.Views.CompositeView.extend({
        template: template,
        render: function() {
            this.listenTo(vent, 'Components:Alert:Closed', this.close);
            vent.trigger('Components:Alert:Alert', {
                icon: 'list-3',
                heading: 'Requirements',
                message: this.template({model: this.model})
            });
            this.afterShow();
        }
    });

    return MainContentView;

});
// csbuilder class mainContent view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/csbuilder/class/templates/mainContent.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        tagName: 'form',
        id: 'Class',
        className: 'formHeighFull',
        ui: {
            validationBtn: '.validationBtn',
            header: '.header',
            course_id: '.course_id',
            course_classTitle: '.classTitle',
            course_ref: '#course_id',
            noteId: '#classNoteId',
            noteContent: '#noteContent',
            terms: '#terms',
            endDate: '#end_date',
            startDate: '#start_date',
            lmsProvider: '#lmsProvider',
            classStatus: '#classStatus',
            buildings: '#buildings',
            rooms: '#rooms',
            codes: '#examCode',
            topic: '#topic',
            cancelSearch: '#cancelSearch'
        },
        events: {
            'click #showRequirements': 'showReqs',
            'click .getNote': 'getNote',
            'keyup #classNoteId': 'getNoteEnter',
            'change #terms': 'updateDates',
            'change #buildings': 'buildingChange',
            'click #cancelSearch': 'cancelSearch',
            'focus #classNoteId': 'setSearchButton'
        },
        triggers: {
            'click .header .search': 'searchCourse'
        },
        onRender: function() {
            var self = this;
            //start jquery validate
            self.$el.find("input,select,textarea").not("[type=submit], .datepicker ").jqBootstrapValidation({
                submitSuccess: function($form, event) {

                    event.preventDefault();
                }

            });
            //start date picker
            self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            self.$el.find('.timepicker').pickatime();
            self.$el.find('.mobiTime').mobiscroll().time({
                theme: 'ios',
                display: 'bubble',
                mode: 'scroller'
            });
            this.listenTo(vent, 'Csbuilder:Popover:Hide:fromSchedule', this.hideCancelSearch);
            this.listenTo(vent, 'Csbuilder:Popover:Hide:fromPopovers', this.hideCancelSearch);
        },
        showCancelSearch: function() {
            vent.trigger('Csbuilder:Popover:Hide:fromSchedule');
            vent.trigger('Csbuilder:Popover:Hide:Schedule');
            this.ui.cancelSearch.removeClass('hidden');
        },
        hideCancelSearch: function() {
            this.ui.cancelSearch.addClass('hidden');
        },
        cancelSearch: function(e) {
            e.preventDefault();
            this.hideCancelSearch();
            vent.trigger("Csbuilder:ClassBuilderController:Sidebar:on");
        },
        setSearchButton: function() {
            console.log('hello')
            var self = this;
            $(this).keypress(function(e) {
                if (e.which == 13) {
                    self.getNoteEnter();
                }
            });
        },
        showReqs: function(e) {
            e.preventDefault();
            this.trigger('showRequiremnts', this.courseModel);
        },
        getNote: function(e) {
            e.preventDefault();
            var self = this;
            this.trigger('getNote', self.ui.noteId.val());
        },
        getNoteEnter: function(e) {
            e.preventDefault();
            var self = this;
            if (e.keyCode === 13)
                this.trigger('getNote', self.ui.noteId.val());
        },
        resetClassNote: function(classNote) {
            this.ui.noteId.val(classNote.note_code);
            this.ui.noteContent.val(classNote.description);
        },
        updateDates: function(isLoad) {
            _.defaults(isLoad, '');
            var self = this;
            if (isLoad != 'load') {
                this.$el.find('.alertDates').addClass('animated shake alertText');
                setTimeout(function() {
                    self.$el.find('.alertDates').removeClass('animated shake alertText');
                }, 1000);
                this.ui.startDate.val(self.ui.terms.find(':selected').data('start'));
                this.ui.endDate.val(self.ui.terms.find(':selected').data('end'));
            }
            if (this.model.isNew()) {
                this.ui.startDate.val(self.ui.terms.find(':selected').data('start'));
                this.ui.endDate.val(self.ui.terms.find(':selected').data('end'));
            }

        },
        resetExamCode: function(codes) {
            //inject options to codes select
            var self = this;
            this.ui.codes.html('');
            _.each(codes, function(code) {
                //mark as selected if code matches model code
                var sel = (code.code == self.model.get('exam_code')) ? 'selected' : '';
                self.ui.codes.append('<option value="' + code.code + '" ' + sel + '>' + code.description + '</option>');
            });
        },
        resetTopics: function(topics) {
            //inject options to topics select
            var self = this;
            this.ui.topic.html('');
            _.each(topics, function(topic) {
                //mark as selected if topic matches model topic
                var sel = (topic.id == self.model.get('topic')) ? 'selected' : '';
                self.ui.topic.append('<option value="' + topic.id + '" ' + sel + '>' + topic.description + '</option>');
            });
        },
        resetTerms: function(terms) {
            //inject options to terms select
            var self = this;
            this.ui.terms.html('');
            _.each(terms, function(term) {
                //mark as selected if term matches model term
                var sel = (term.id == self.model.get('class_term')) ? 'selected' : '';
                self.ui.terms.append('<option data-end="' + term.end_date + '"  data-start="' + term.start_date + '" value="' + term.id + '" ' + sel + '>' + term.description + '</option>');
            });
            //trigger change to update start and end dates
            this.updateDates('load');
        },
        resetLMSProvider: function(providers) {
            //inject options to LMS provider select
            var self = this;
            this.ui.lmsProvider.html('');
            _.each(providers, function(provider) {
                //mark as selected if LMS provider matches model term
                var sel = (provider.id == self.model.get('lms_provider')) ? 'selected' : '';
                self.ui.lmsProvider.append('<option value="' + provider.id + '" ' + sel + '>' + provider.description + '</option>');
            });
        },
        resetClassStatus: function(status) {
            //inject options to class status select
            var self = this;
            this.ui.classStatus.html('');
            _.each(status, function(stat) {
                //mark as selected if class status matches model term
                var sel = (stat.id == self.model.get('class_status')) ? 'selected' : '';
                self.ui.classStatus.append('<option value="' + stat.id + '" ' + sel + '>' + stat.CLASSSTATUS + '</option>');
            });
        },
        resetBuildings: function(buildings) {
            //inject options to buildings select
            var self = this;
            this.ui.buildings.html('');
            _.each(buildings, function(building) {
                //mark as selected if building matches model building
                var sel = (building.id == self.model.get('building_id')) ? 'selected' : '';
                self.ui.buildings.append('<option value="' + building.id + '" ' + sel + '>' + building.description + '</option>');
            });
            //trigger select on buildings
            this.ui.buildings.change();
        },
        buildingChange: function() {
            var building = this.ui.buildings.val();
            this.trigger('buildingChange', building);
        },
        resetRooms: function(rooms) {
            var self = this;
            //inject options to rooms select
            if (rooms.length > 0) {
                this.ui.rooms.html('');
                _.each(rooms, function(room) {
                    //_.each(self.model.get('scheduleclass_id'), function(sec) {
                    //mark as selected if room matches model room
                    var sel = (room.id == self.model.get('exam_location')) ? 'selected' : '';
                    self.ui.rooms.append('<option value="' + room.id + '" ' + sel + '>' + room.description + '</option>');
                    //});
                });
                //trigger select on rooms
                this.ui.rooms.change();
            }
            else {
                this.ui.rooms.html('<option selected>No Rooms</option>');
            }
        },
        setHeadder: function(model) {
            this.courseModel = model;
            $(this.ui.course_id).html(model.course_catalog_id);
            $(this.ui.course_classTitle).html(model.description);
            $(this.ui.course_ref).val(model.id);
            $(this.$el.find('#showRequirements')).removeClass('btn-warning-disabled');
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (!self.ui.course_ref.val()) {
                self.ui.header.addClass('ui-state-highlight');
                var oldHtml = self.ui.header.html();
                self.ui.header.html('<h2 class="headerErr">Click in this area to add course info</h2>');
                $('.headerErr').addClass('animated shake');
                setTimeout(function() {
                    self.ui.header.html(oldHtml);
                    self.bindUIElements();
                    self.ui.header.removeClass('ui-state-highlight');
                }, 3000);
            }
            else if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
            }

            else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        },
        onShow: function() {
            var self = this;
            this.trigger('getNote', self.ui.noteId.val());
        }
    });
    return MainContentView;
});
// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/csbuilder/class/schedule/views/BtnView'
],
        function(Marionette, App, vent, BtnView) {

            var BtnController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.view = new BtnView();
                    this.show();
                    this.listenTo(vent, 'CsBuilder:Section:Badge:update', this.resetCount, this);
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.view, 'clicked', this.getButtonElem, this);
                    this.listenTo(this.view, 'addNew', this.triggerAddNew, this);
                },
                triggerAddNew: function() {
                    this.trigger('addNew');
                },
                resetCount: function(count) {
                    this.view.resetCount(count);
                },
                selected: function() {
                    this.view.selected();
                },
                show: function() {
                    this.region.show(this.view);
                },
                getButtonElem: function() {
                    this.trigger('clicked', {btn: this.view.getClass()});
                }

            });

            return BtnController;
        });
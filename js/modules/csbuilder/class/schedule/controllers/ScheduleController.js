// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/csbuilder/class/schedule/views/ScheduleView',
    //collection
    'entities/collections/dictionary/DictionaryCollection',
    'entities/collections/location/BuildingCollection',
    'entities/collections/location/RoomCollection'

],
        function(Marionette, App, vent, ScheduleView, DictionaryCollection, BuildingCollection, RoomCollection) {

            var PopoverController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.collection = new DictionaryCollection();
                    this.collection.getDays();
                    this.scheduleList = options.collection;
                    this.buildings = new BuildingCollection();
                    this.rooms = new RoomCollection();
                    this.listeners();
                },
                listeners: function() {
                    this.listenTo(this.collection, 'reset', function() {
                        this.showView();
                    }, this);
                },
                viewListeners: function() {
                    //listen to building sync before fetching
                    this.listenTo(this.buildings, 'sync', function() {
                        this.view.resetBuildings(this.buildings.toJSON());
                    }, this);
                    //fetch buildings:
                    this.buildings.fetch();

                    this.listenTo(this.view, 'buildingChange', function(opts) {
                        this.roomToReset = opts.id;
                        this.rooms.fetch({}, {building: opts.building}, {}, false);

                        this.listenToOnce(this.rooms, 'sync', function() {
                            this.view.resetRooms({rooms: this.rooms.toJSON(), room: opts.id});
                        }, this);

                    }, this);
                    this.listenTo(this.view, 'save', function(sec) {
                        this.trigger('saveNewSchedule', sec);
                    }, this);
                    this.listenTo(this.view, 'close', this.close);
                },
                showView: function() {
                    this.view = new ScheduleView({collection: this.collection, schedule: this.scheduleList});
                    this.viewListeners();
                    this.region.show(this.view);
                },
                showPopover: function() {
                    this.view.toggle();
                }
            });

            return PopoverController;
        });
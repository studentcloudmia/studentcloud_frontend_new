// csbuilder schedule
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/csbuilder/class/schedule/controllers/BtnController',
    'modules/csbuilder/class/schedule/controllers/ScheduleController'
],
        function(Marionette, App, vent, BtnController, ScheduleController) {

            var CsbuilderSchedule = App.module('Csbuilder.Schedule');
            CsbuilderSchedule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    //option variables
                    this.region = options.region;
                    this.popOverRegion = options.popOverRegion;
                    this.addNewRegion = options.addNewRegion;
                    this.scheduleList = options.collection;
                    //entities
                    this.btnController = new BtnController({region: this.region});
                    //listeners
                    this.listenTo(this.btnController, 'clicked', this.showPopover, this);
                    this.listenTo(this.btnController, 'close', this.close);
                },
                addNewSchedule: function(schedule) {
                    var self = this;
                    self.scheduleList.reset();
                    if (schedule != 'clearAll') {
                        _.each(schedule, function(sch, i) {
                            sch.start_time = moment(sch.start_time, "hh:mm a").format("HH:mm:ss");
                            sch.end_time = moment(sch.end_time, "hh:mm a").format("HH:mm:ss");
                            self.scheduleList.add(sch);
                        });
                    }
                },
                initSchedule: function() {
                    this.scheduleController = new ScheduleController({collection: this.scheduleList, region: this.popOverRegion});
                    this.listenTo(this.scheduleController, 'saveNewSchedule', function(schedule) {
                        this.addNewSchedule(schedule);
                    }, this);

                },
                showPopover: function() {
                    this.scheduleController.showPopover();
                }
            });
            return CsbuilderSchedule;
        });
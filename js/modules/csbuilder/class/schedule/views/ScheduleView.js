// cbuilder coRequisites PopoverView view
define([
    'jquery',
    'underscore',
    'marionette',
    'vent',
    'app',
    'utilities',
    'tpl!modules/csbuilder/class/schedule/templates/schedule.tmpl'
], function($, _, Marionette, vent, App, utl, Template) {

    var PopoverView = App.Views.ItemView.extend({
        tagName: 'div',
        className: 'addNew popOverWrapper animated bounceOutDown hidden noClick',
        template: Template,
        events: {
            'click .set': 'set',
            'click .clear': 'clear',
            'click .clearAll': 'clearAll',
            'change .building': 'buildingChange',
            'click .nav-tabs li a': 'reloadBuilding'
        },
        ui: {
            addnewWindow: '.addNew',
            buildings: '.building',
            rooms: '.room'
        },
        render: function() {
            vent.trigger('Csbuilder:Popover:Hide:fromSchedule');
            this.listenTo(vent, 'Csbuilder:Popover:Hide:Schedule', this.hideMe);
            $(this.el).html(this.template({days: this.collection.toJSON(), schedule: this.options.schedule.toJSON()}));
            this.onRender();
            this.sections = this.options.schedule.toJSON();
            this.bindUIElements();
            return this;
        },
        clearAll: function() {
            this.sections = 'clearAll';
            this.$el.find('.checked').remove();
        },
        clear: function(e) {
            var self = this;
            var selector = 'link_' + e.currentTarget.parentElement.parentElement.id.replace('tab_', '');
            $('#linkIcon_' + selector.replace('link_', '')).remove();
            //remove object from array
            //self.sections = _.without(self.sections, ({day: e.currentTarget.parentElement.parentElement.id.replace('tab_', '')}));
            self.sections = _.reject(self.sections, function(sec1) {
                return sec1.day == e.currentTarget.parentElement.parentElement.id.replace('tab_', '');
            });
        },
        save: function() {
            this.trigger('save', this.sections);
        },
        hideMe: function() {
            this.save();
            this.$el.removeClass('bounceInUp').addClass('bounceOutDown');
        },
        set: function(e) {
            e.preventDefault();
            var self = this;
            if (!this.$el.find('#dayStartTime_' + e.currentTarget.id.replace('set_', '')).val()) {
                this.$el.find(this.$el.find('#dayStartTime_' + e.currentTarget.id.replace('set_', ''))[0].parentElement).addClass('ui-state-highlight animated shake');
                setTimeout(function() {
                    self.$el.find(self.$el.find('#dayStartTime_' + e.currentTarget.id.replace('set_', ''))[0].parentElement).removeClass('ui-state-highlight animated shake');
                }, 2000);
            }
            else if (!this.$el.find('#dayEndTime_' + e.currentTarget.id.replace('set_', '')).val()) {
                this.$el.find(this.$el.find('#dayEndTime_' + e.currentTarget.id.replace('set_', ''))[0].parentElement).addClass('ui-state-highlight animated shake');
                setTimeout(function() {
                    self.$el.find(self.$el.find('#dayEndTime_' + e.currentTarget.id.replace('set_', ''))[0].parentElement).removeClass('ui-state-highlight animated shake');
                }, 2000);
            }
            else {
                var currID = e.currentTarget.parentElement.parentElement.id;
                var selector = 'link_' + e.currentTarget.parentElement.parentElement.id.replace('tab_', '');
                var selectorIcon = 'linkIcon_' + e.currentTarget.parentElement.parentElement.id.replace('tab_', '');
                var id = e.currentTarget.parentElement.parentElement.id.replace('tab_', '');

                $('#' + selector).html('<i id="linkIcon_' + selector.replace('link_', '') + '" class="icomoon-checkmark-4 checked pull-right"></i>');
                //manual serialize
                var newSection = {};
                $('#' + currID).children().find('input,select').each(function() {
                    if (newSection[this.name] !== undefined) {
                        if (newSection[this.name].push) {
                            newSection[this.name] = [newSection[this.name]];
                        }
                        newSection[this.name].push(this.value || '');
                    } else {
                        newSection[this.name] = this.value || '';
                    }

                });
                //TODO: waiting on scheduleequip_id specs, hardcoding it for now
                var scheduleequip_id = new Array;
                newSection.scheduleequip_id = scheduleequip_id;
                _.each(this.sections, function(sec) {
                    if (sec.day == newSection.day) {
                        self.sections = _.reject(self.sections, function(sec1) {
                            return sec1.day == newSection.day;
                        });
                    }
                });
                this.sections.push(newSection);
            }
        },
        onRender: function() {
            var self = this;
            //time picker
            self.$el.find('.mobiTime').mobiscroll().time({
                theme: 'ios',
                display: 'bubble',
                mode: 'scroller'
            });
        },
        resetBuildings: function(buildings) {
            //inject options to buildings select
            var self = this;
            this.ui.buildings.html('');
            _.each(self.collection.toJSON(), function(day) {
                _.each(buildings, function(building) {
                    self.$el.find('#building_' + day.id).append('<option value="' + building.id + '">' + building.description + '</option>');
                    if (self.sections.length !== 0) {
                        _.each(self.sections, function(sec) {
                            if (sec.day == day.id && sec.building_id == building.id) {
                                $("#building_" + day.id + " option[value='" + building.id + "']").attr("selected", "selected")
                            }
                        });
                    }
                });
            });
            $("#building_32").change();
        },
        reloadBuilding: function(e) {
            if (utl.isTouchDevice())
                $(e.currentTarget).click();
            var building = $('#' + e.target.hash.replace('#tab_', 'building_')).val();
            this.trigger('buildingChange', {building: building, id: e.target.hash.replace('#tab', 'room')});
        },
        buildingChange: function(e) {
            var building = $('#' + e.target.id).val();
            this.trigger('buildingChange', {building: building, id: e.target.id.replace('building', 'room')});
        },
        resetRooms: function(opts) {
            var self = this;
            this.ui.rooms.html('');
            _.each(self.collection.toJSON(), function(day) {
                _.each(opts.rooms, function(room) {
                    self.$el.find('#room_' + day.id).append('<option value="' + room.id + '">' + room.description + '</option>');
                    if (self.sections.length !== 0) {
                        _.each(self.sections, function(sec) {
                            if (sec.day == day.id && sec.room == room.id) {
                                $("#room_" + day.id + " option[value='" + room.id + "']").attr("selected", "selected")
                            }
                        });
                    }
                });
            });
        },
        toggle: function() {
            if (this.$el.hasClass('bounceOutDown')) {
                this.$el.removeClass('bounceOutDown hidden noClick').addClass('bounceInUp allowClick');
                vent.trigger('Csbuilder:Popover:Hide:fromSchedule');
                //return control over the sidebar to class controller on hide
                vent.trigger("Csbuilder:ClassBuilderController:Sidebar:on");
            }
            else {
                this.hideMe();
            }
        }
    });

    return PopoverView;

});
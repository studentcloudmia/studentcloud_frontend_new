// SideView
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/csbuilder/class/schedule/templates/btn.tmpl'
], function(_, Marionette, App, vent, template) {

    var BtnView = App.Views.ItemView.extend({
        template: template,
        tagName: 'button',
        className: 'btn btn-inverse btn-mini',
        ui: {
            badge: '.badge'
        },
        triggers: {
            'click': 'clicked'
        },
        resetCount: function(count) {
            this.ui.badge.html(count);
        },
        getClass: function() {
            return this.$el;
        }
    });

    return BtnView;

});
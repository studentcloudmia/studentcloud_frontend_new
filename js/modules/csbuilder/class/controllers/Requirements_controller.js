// requirement controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/csbuilder/class/views/RequirementsView',
    //MODELS
    'entities/models/course/CourseModel'
],
        function(Marionette, App, vent, RequirementsView, CourseModel) {

            var RequirementsController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.model = new CourseModel();
                    this.model = options.reqs;
                    this.view = new RequirementsView({model: this.model});
                    this.view.render();
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                }
            });

            return RequirementsController;
        });
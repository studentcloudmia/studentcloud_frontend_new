define([
    'marionette',
    'app',
],
        function(Marionette, app) {

            var Router = Marionette.AppRouter.extend({
                appRoutes: {
                    'csbuilder/class': 'showClass'
                }
            });

            return Router;

        });
define([
    'marionette',
    'app',
    'vent',
    'modules/components/loader/loader_view'
],
        function(Marionette, App, vent, LoaderView) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.count = 0;
                    //listen to events
                    this.listenTo(vent, "Components:Loader:Show", this.Show);
                    this.listenTo(vent, "Components:Loader:Hide", this.Hide);
                },
                Show: function(options) {
                    this.count = this.count + 1;
                    this.region.show(new LoaderView());
                },
                Hide: function(options) {
                    this.count = this.count - 1;
                    if (this.count == 0) {
                        this.region.close();
                    }
                }

            });

            return Controller;

        });
// alerts module
define([
'marionette',
'app',
'vent',
'modules/components/loader/controller'
],
function(Marionette, App, vent, Controller) {

    var LoaderModule = App.module('Components.Loader');

    LoaderModule.addInitializer(function() {
		this.controller = new Controller({region: App.loading});
    });

    LoaderModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return LoaderModule;
});
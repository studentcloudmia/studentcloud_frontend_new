// loader view
define(['marionette', 'app', 'bootstrap', 'tpl!modules/components/loader/template/loader.tmpl'], function(Marionette, App, Bootstrap, template) {

    var LoaderView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'spinnerRegion'
    });

    return LoaderView;

});

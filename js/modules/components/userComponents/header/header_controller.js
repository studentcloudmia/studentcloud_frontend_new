// access module
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/components/userComponents/header/views/header_view'
],
        function(Marionette, App, vent, HeaderView) {

            var HeaderShowModule = App.module('Components.UserComponents.Header');

            HeaderShowModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    options.currentUrl = App.getCurrentRoute();
                    this.view = new HeaderView(options);
                    this.listenTo(this.view, 'close', function() {
                        vent.trigger('Components:UserComponents:Closing:header');
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'showMenu', function() {
                        vent.trigger("Profile:Show:Overlay", {
                            region: App.modals,
                            icon: this.view.ui.icon,
                            modelId: options.modelId
                        });
                    }, this);
                    this.region.show(this.view);
                },
                changeDetails: function(model) {
                    this.view.changeDetails(model);
                },
                changeDetailsManual: function(options) {
                    this.view.changeDetailsManual(options);
                }


            });

            return HeaderShowModule;
        });
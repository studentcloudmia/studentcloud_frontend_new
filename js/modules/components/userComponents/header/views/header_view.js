// login view
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/components/userComponents/header/template/header.tmpl'
], function(_, Marionette, App, vent, template) {

    var HeaderView = App.Views.ItemView.extend({
        template: template,
        initialize: function(options) {
            this.data = options;
        },
        events: {
            'click .userImage': 'showProfileMenu'
        },
        ui: {
            details: '#headings',
            icon: '.userImage'
        },
        render: function() {
            this.$el.html(this.template(this.data));
            this.bindUIElements();
        },
        showProfileMenu: function() {
            this.trigger("showMenu");
        },
        changeDetails: function(model) {
            this.model = model;
            var html = '';
            if (!this.model.isNew()) {
                html = this.model.getId() + ' | ' + this.model.get('eff_date') + ' | ' + this.model.get('Status');
            }
            this.ui.details.html(html);
        },
        changeDetailsManual: function(options) {
            var html = '',
                    i = 0;
            _.each(options, function(val) {
                if (i > 0) {
                    html = html + ' | ';
                }
                html = html + val;
                i++;
            });
            this.ui.details.html(html);
        }

    });

    return HeaderView;

});
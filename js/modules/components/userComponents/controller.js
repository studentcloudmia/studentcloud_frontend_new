define([
    'marionette',
    'app',
    'vent',
    'modules/components/userComponents/header/header_controller'
],
        function(Marionette, App, vent, HeaderShowModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //Header listeners
                    this.headerListeners();
                },
                headerListeners: function() {

                    this.listenTo(vent, 'Components:UserComponents:UserHeader:on',
                            function(options) {
                                this.headerController = new HeaderShowModule.Controller(options);
                            },
                            this);

                    this.listenTo(vent, 'Components:UserComponents:UserHeader:changeDetails',
                            function(options) {
                                this.headerController.changeDetails(options.model);
                            },
                            this);
                }

            });

            return Controller;

        });
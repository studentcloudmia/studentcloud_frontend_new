// builders module
define([
'marionette',
'app',
'vent',
'modules/components/userComponents/controller'
],
function(Marionette, App, vent, Controller) {

    var UserComponentsModule = App.module('Components.UserComponents');

    UserComponentsModule.addInitializer(function() {
		this.controller = new Controller();
    });

    UserComponentsModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return UserComponentsModule;
});
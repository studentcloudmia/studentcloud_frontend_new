define([
'marionette',
'app',
'vent',
'modules/components/student/header/header_controller'
],
function(Marionette, App, vent, HeaderShowModule) {

    var Controller = Marionette.Controller.extend({

        initialize: function(options) {
			//Header listeners
			this.headerListeners();
        },

		headerListeners: function(){
            this.listenTo(vent, 'Components:Student:Header:on',
            function(options) {
                this.headerController = new HeaderShowModule.Controller(options);
            },
            this);		
		}

    });

    return Controller;

});
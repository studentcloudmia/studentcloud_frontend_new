// students module
define([
'marionette',
'app',
'vent',
'modules/components/student/controller',
],
function(Marionette, App, vent, Controller) {

    var StudentModule = App.module('Components.Student');

    StudentModule.addInitializer(function() {
		this.controller = new Controller();
    });

    StudentModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return StudentModule;
});
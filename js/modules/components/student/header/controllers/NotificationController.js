define([
'marionette',
'app',
'vent',
'modules/components/student/header/views/NotificationView',
'entities/collections/notifications/NotificationsCollection',
'entities/collections/dictionary/DictionaryCollection'
],
function(Marionette, App, vent, NotificationView, NotificationsCollection, DictionaryCollection) {

    var NotificationController = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
			this.region = options.region;
            this.collection = new NotificationsCollection();
            this.notifTypeColl = new DictionaryCollection();
            this.view = new NotificationView({ collection : this.collection });
            
            this.listenTo(this.view, 'close', function(){
                this.close();
            }, this);
            
            this.listenTo(this.notifTypeColl, 'reset', this.changeHeading);
            this.notifTypeColl.getNotificationType();
            
            this.collection.fetch({loader: false});
            
            this.messageCalls = window.setInterval(function(){
                //fetch for notifications at a specific interval
                self.collection.fetch({loader: false});
            },60000);
            
            //show view
            this.region.show(this.view);
        },
        
        changeHeading: function(){
            this.view.updateHeader(this.notifTypeColl.toJSON());
        },
        
        onClose: function(){
            window.clearInterval(this.messageCalls);
        }

    });

    return NotificationController;

});
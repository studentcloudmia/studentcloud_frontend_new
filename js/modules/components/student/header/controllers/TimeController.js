define([
'marionette',
'app',
'vent',
'modules/components/student/header/views/TimeView',
'entities/models/notifications/WeatherModel'
],
function(Marionette, App, vent, TimeView, WeatherModel) {

    var TimeController = Marionette.Controller.extend({

        initialize: function(options) {
			this.region = options.region;
            this.model = new WeatherModel();
            this.view = new TimeView({model: this.model});
            this.model.getWeather();
            this.listenTo(this.model, 'change', function(){
                //show view
                this.region.show(this.view);
            });            
        }

    });

    return TimeController;

});
// access module
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/components/student/header/views/layout',
    //controllers
    'modules/components/student/header/controllers/TimeController',
    'modules/components/student/header/controllers/NotificationController'
    ],
    function(Marionette, App, vent, LayoutView, TimeController, NotificationController){

        var HeaderShowModule = App.module('Components.Student.Header');

        HeaderShowModule.Controller = Marionette.Controller.extend({

            initialize: function(options) {
                //save region where we can show
                this.region = options.region;
				
                this.layout = new LayoutView();

                this.listenTo(this.layout, 'close', function(){
                    this.close();
                }, this);

                this.listenTo(this.layout, 'show', function(){
                    this.showControllers();
                }, this);

                this.region.show(this.layout);

            },
            
            showControllers: function(){
                new TimeController({region: this.layout.time});
                new NotificationController({region: this.layout.notification});
            }

        });

        return HeaderShowModule;
    });
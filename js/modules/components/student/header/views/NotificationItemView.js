// Notification Item View
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/components/student/header/templates/_notification.tmpl'
],function(_, Marionette, vent, App, template){
	
	var NotificationItemView = App.Views.ItemView.extend({
		template: template,
        tagName: 'span',
        className: 'notif notificationMsg'

	});

	return NotificationItemView;

});
// header layout
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/components/student/header/templates/layout.tmpl'
], function(_, Marionette, App, vent, template) {

    var LayoutView = App.Views.Layout.extend({
        template: template,
        
        regions: {
            time:           '.headerTime',
            notification:   '.headerNotification'
        }

    });

    return LayoutView;

});
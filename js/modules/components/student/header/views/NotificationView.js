// Notification View
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'modules/components/student/header/views/NotificationItemView',
	'tpl!modules/components/student/header/templates/notification.tmpl'
],function(_, Marionette, vent, App, ItemView, template){
	
	var NotificationView = App.Views.CompositeView.extend({
		template: template,
		itemView: ItemView,
		itemViewContainer: '.messages',
        interval: 300,
        count: 0,
        
        ui: {
            title: '.notifTitle'
        },
        
        onAfterItemAdded: function(){
            //this.interval = 10000;
        },
        
        onRender: function(){
            this.startScroller();
        },
        
        updateHeader: function(json){
            var self = this;
            if(json){
                this.headings = json;
            }
            if(this.headings && this.collection.length > 0){
                var title = '';
                var alertIcon = '<span class="icomoon-warning"/> </span>';
                //get first model and use that as heading
                var delivery_method = this.collection.at(0).get('delivery_method');
                _.each(this.headings, function(heading){
                    if(heading.id == delivery_method){
                        title = heading.MESSAGETYPE;
                    }
                });
                
                if(title == 'Alert'){
                    this.ui.title.html(alertIcon + title);
                }else{
                    this.ui.title.html(title);
                }
                this.headerUpdated = true;
            }           
        },
        
        startScroller: function(){
            //get all notifs
            var self = this;
            var notifs = this.$el.find('.notif');
            //this.updateHeader();
            //reset count if reached end
            if(this.count >= notifs.length){
                this.count = 0;
            }
            
            setTimeout(function(){
                notifs.hide();
                self.$el.find(notifs[self.count]).show();
                self.count = self.count + 1;    
                self.startScroller();
            },self.interval);
            
            if(notifs.length > 0){
                self.interval = 10000;
            }
            
        }

	});
	
	return NotificationView;
	
});
// Time View
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/components/student/header/templates/time.tmpl'
],function(_, Marionette, vent, App, template){
	
	var TimeView = App.Views.ItemView.extend({
		template: template,
        
        onBeforeRender: function(){
            //add data to model
            var data = {
                fullDate: moment().format("MMM Do YYYY"), 
                day: moment().format('dddd')
            }
            this.model.set('Date', data, {
                silent:true
            });
        }

	});
	
	return TimeView;
	
});
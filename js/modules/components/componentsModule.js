// components module
define([
'marionette',
'app',
'vent',
//sub modules
'modules/components/alerts/alertsModule',
'modules/components/builders/buildersModule',
'modules/components/student/studentModule',
'modules/components/loader/loaderModule',
'modules/components/userComponents/userComponentsModule'
],
function(Marionette, App, vent) {

    var ComponentsModule = App.module('Components');

    // AccessModule.startWithParent = false;

    //bind to module finalizer event
    ComponentsModule.addInitializer(function() {
		
		
    });
	
    return ComponentsModule;
});
define([
    'marionette',
    'app',
    'vent',
    'modules/components/alerts/alert_view',
    'modules/components/alerts/confirm_view',
    'modules/components/alerts/login_view'
],
        function(Marionette, App, vent, AlertView, ConfirmView, LoginView) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    //listen to events
                    this.listenTo(vent, "Components:Alert:Alert", this.Alert);
                    this.listenTo(vent, "Components:Alert:Confirm", this.Confirm);
                    this.listenTo(vent, "Components:Alert:Login", this.Login);
                },
                Alert: function(options) {
                    this.alertView = new AlertView({data: options});
                    this.listenTo(this.alertView, 'Alerts:Dismiss', this.CloseAlert, this);
                    this.region.show(this.alertView);
                },
                CloseAlert: function() {
                    this.alertView.close();
                    vent.trigger("Components:Alert:Closed");
                },
                Confirm: function(options) {

                    this.confirmView = new ConfirmView({data: options});
                    this.listenTo(this.confirmView, 'Confirm:Dismiss', this.CloseConfirm, this);
                    this.region.show(this.confirmView);
                },
                CloseConfirm: function() {
                    this.confirmView.close();
                    vent.trigger("Components:Confirm:Closed");
                },
                Login: function(options) {

                    this.loginView = new LoginView({data: options});
                    this.listenTo(this.loginView, 'Login:Dismiss', this.CloseLogin, this);
                    this.region.show(this.loginView);
                },
                CloseLogin: function() {
                    this.loginView.close();
                },
            });

            return Controller;

        });
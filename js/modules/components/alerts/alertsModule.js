// alerts module
define([
'marionette',
'app',
'vent',
'modules/components/alerts/controller'
],
function(Marionette, App, vent, Controller) {

    var AlertsModule = App.module('Components.Alerts');

    AlertsModule.addInitializer(function() {
		this.controller = new Controller({region: App.modalAlerts});
    });

    AlertsModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return AlertsModule;
});
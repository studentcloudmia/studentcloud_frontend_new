// login view
define(['marionette', 'app', 'bootstrap', 'tpl!modules/components/alerts/template/login.tmpl'], function(Marionette, App, Bootstrap, template) {

    var LoginView = App.Views.ItemView.extend({
        template: template,

        initialize: function(options) {
            _.defaults(options.data, {
                cancelText: 'Cancel',
                login_type: 'Internal'
            });
            this.data = options.data;
        },

        ui: {
            modalAlert: '#MsgModal',
            username: '.username',
            password: '.password',
            login_type: '.login_type'
        },

        events: {
            'click .closeModal': 'closeModal',
            'click .confirmModal': 'confirmModal',
            'hidden #MsgModal': 'alertClosed'
        },

        render: function() {
            this.$el.html(this.template(this.data));
            this.bindUIElements();
        },

        onShow: function() {
            var self = this;
            //show modal
            this.ui.modalAlert.modal('show');
        },

        alertClosed: function() {
            this.trigger('Login:Dismiss');
            this.close();
        },

        closeModal: function(e) {
            e.preventDefault();
            this.ui.modalAlert.modal('hide');
        },
        
        confirmModal: function(e){
            e.preventDefault();
            this.closeModal(e);
            this.data.callback.apply(this.data.callee, [this.ui.username.val(), this.ui.password.val(), this.ui.login_type.val()]);
        },
        
        onClose: function(){
            //remove lingering backdrop div
            $('.modal-backdrop').remove();
        }

    });

    return LoginView;

});

// login view
define([
	'marionette', 
	'app',
	'bootstrap',
	'tpl!modules/components/alerts/template/alert.tmpl'
],function(Marionette, App, Bootstrap, template){
	
	var AlertView = App.Views.ItemView.extend({
		template: template,
		
		initialize: function(options){
            _.defaults(options.data, {
                icon: 'warning',
                heading: 'Test Heading',
                message: 'Test Message',
            });
			this.data = options.data;
		},
		
		ui:{
			modalAlert: '#MsgModal'
		},
		
		events:{
			'click .closeModal': 'closeModal',
			'hidden #MsgModal': 'alertClosed'
		},
		
		render: function(){
			this.$el.html(this.template(this.data));
			this.bindUIElements();
		},
		
		onShow: function(){	
			var self = this;
			//show modal
            // this.ui.modalAlert.modal({backdrop: false});
			this.ui.modalAlert.modal('show');
		},
		
		alertClosed: function(){
			this.trigger('Alerts:Dismiss');
            this.close();
		},
		
		closeModal: function(e){
            e.preventDefault();
            e.stopPropagation();
			this.ui.modalAlert.modal('hide');		
		},
        
        onClose: function(){
            //remove lingering backdrop div
            $('.modal-backdrop').remove();
        }
		
	});
	
	return AlertView;
	
});
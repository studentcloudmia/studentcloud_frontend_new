// login view
define(['marionette', 'app', 'bootstrap', 'tpl!modules/components/alerts/template/confirm.tmpl'], function(Marionette, App, Bootstrap, template) {

    var ConfirmView = App.Views.ItemView.extend({
        template: template,
        initialize: function(options) {
            _.defaults(options.data, {
                cancelText: 'Cancel',
                confirmText: 'Confirm'
            });
            this.data = options.data;
        },
        ui: {
            modalAlert: '#MsgModal'
        },
        events: {
            'click .closeModal': 'closeModal',
            'click .confirmModal': 'confirmModal',
            'hidden #MsgModal': 'alertClosed'
        },
        render: function() {
            this.$el.html(this.template(this.data));
            this.bindUIElements();
        },
        onShow: function() {
            var self = this;
            //show modal
            this.ui.modalAlert.modal('show');
        },
        alertClosed: function() {
            this.trigger('Confirm:Dismiss');
            this.close();
        },
        closeModal: function(e) {
            e.preventDefault();
            this.ui.modalAlert.modal('hide');
        },
        confirmModal: function(e) {
            e.preventDefault();
            this.closeModal(e);
            this.data.callback.apply(this.data.callee);
        },
        onClose: function() {
            //remove lingering backdrop div
            $('.modal-backdrop').remove();
        }

    });

    return ConfirmView;

});

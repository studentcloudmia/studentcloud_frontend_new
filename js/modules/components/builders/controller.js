define([
    'marionette',
    'app',
    'vent',
    'modules/components/builders/header/header_controller',
    'modules/components/builders/footer/footer_controller'
],
        function(Marionette, App, vent, HeaderShowModule, FooterShowModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //Header listeners
                    this.headerListeners();
                    //Footer listeners
                    this.footerListeners();
                },
                headerListeners: function() {
                    this.listenTo(vent, 'Components:Builders:Header:on',
                            function(options) {
                                this.headerController = new HeaderShowModule.Controller(options);
                            },
                            this);

                    this.listenTo(vent, 'Components:Builders:Header:changeDetails',
                            function(options) {
                                this.headerController.changeDetails(options);
                            },
                            this);
                    this.listenTo(vent, 'Components:Builders:Header:changeDetailsManual',
                            function(options) {
                                this.headerController.changeDetailsManual(options);
                            },
                            this);
                },
                footerListeners: function() {
                    this.listenTo(vent, 'Components:Builders:Footer:on',
                            function(options) {
                                this.footerController = new FooterShowModule.Controller(options);
                                this.fControllerListeners();
                            },
                            this);

                    this.listenTo(vent, 'Components:Builders:Footer:updateButtons',
                            function(options) {
                                this.footerController.updateButtons(options);
                            },
                            this);
                },
                fControllerListeners: function() {

                    this.listenTo(this.footerController, 'save',
                            function() {
                                vent.trigger('Components:Builders:Footer:save');
                            },
                            this);

                    this.listenTo(this.footerController, 'add',
                            function() {
                                vent.trigger('Components:Builders:Footer:add');
                            },
                            this);

                    this.listenTo(this.footerController, 'del',
                            function() {
                                vent.trigger('Components:Builders:Footer:del');
                            },
                            this);

                    this.listenTo(this.footerController, 'next',
                            function() {
                                vent.trigger('Components:Builders:Footer:next');
                            },
                            this);

                    this.listenTo(this.footerController, 'prev',
                            function() {
                                vent.trigger('Components:Builders:Footer:prev');
                            },
                            this);
                }

            });

            return Controller;

        });
// builders module
define([
'marionette',
'app',
'vent',
'modules/components/builders/controller',
],
function(Marionette, App, vent, Controller) {

    var BuilderModule = App.module('Components.Builders');

    BuilderModule.addInitializer(function() {
		this.controller = new Controller();
    });

    BuilderModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return BuilderModule;
});
// footer view
define([
    'underscore',
    'marionette',
    'app',
    'tpl!modules/components/builders/footer/template/footer.tmpl'
], function(_, Marionette, App, template) {

    var FooterView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'row-fluid',
        triggers: {
            'click #addBtn': 'add',
            'click #saveBtn': 'save',
            'click #delBtn': 'del',
            'click #nextBtn': 'next',
            'click #prevBtn': 'prev'
        },
        ui: {
            add: '#addBtn',
            save: '#saveBtn',
            del: '#delBtn',
            collNav: '#collNav',
            next: '#nextBtn',
            prev: '#prevBtn'
        },
        render: function() {
            this.$el.html(this.template({}));
            this.bindUIElements();
        },
        prepOptions: function(options) {
            _.defaults(options, {
                add: false,
                save: false,
                del: false,
                next: false,
                prev: false,
                collNav: false
            });
            this.options = options;
        },
        updateButtons: function(options) {
            this.prepOptions(options);
            this.setupButtons();
        },
        setupButtons: function() {
            var self = this;
            //loop through options to turn buttons on or off
            _.each(this.options, function(val, name) {
                (val) ? self.ui[name].removeClass('nonvisible') : self.ui[name].addClass('nonvisible');
            });
        }

    });

    return FooterView;

});
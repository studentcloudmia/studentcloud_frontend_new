// access module
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/components/builders/footer/footer_view'
    ],
    function(Marionette, App, vent, FooterView){

        var FooterShowModule = App.module('Components.Builders.Footer');

        FooterShowModule.Controller = Marionette.Controller.extend({

            initialize: function(options) {
                //save region where we can show
                this.region = options.region;
				
                this.view = new FooterView(options);

                this.listenTo(this.view, 'close', function(){
                    this.close();
                }, this);

                this.region.show(this.view);
				this.bindListeners();
            },

			updateButtons: function(options){
				this.view.updateButtons(options);
			},
			
			bindListeners: function(){

                this.listenTo(this.view, 'add', function(){
                    this.trigger('add');
                }, this);

                this.listenTo(this.view, 'save', function(){
                    this.trigger('save');
                }, this);

                this.listenTo(this.view, 'del', function(){
                    this.trigger('del');
                }, this);

                this.listenTo(this.view, 'next', function(){
                    this.trigger('next');
                }, this);

                this.listenTo(this.view, 'prev', function(){
                    this.trigger('prev');
                }, this);
			}

        });

        return FooterShowModule;
    });
// login view
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/components/builders/header/template/header.tmpl'
], function(_, Marionette, App, vent, template) {

    var HeaderView = App.Views.ItemView.extend({
        template: template,
        initialize: function(options) {
            this.data = options;
        },
        ui: {
            details: '#headings',
            icon: '.circle'
        },
        render: function() {
            this.$el.html(this.template(this.data));
            this.bindUIElements();
        },
        changeDetails: function(options) {
            //TODO: Remove this code if nothing broke. This comment as of 08/15/2013
            /*this.model = model;
             var self = this;
             if (typeof this.model.get('eff_date') == 'undefined')
             self.effDate = this.model.get('eff_date');
             else
             self.effDate = this.model.get('eff_date');
             var html = '';
             if (!this.model.isNew()) {
             html = this.model.getId() + ' | ' + self.effDate + ' | ' + this.model.get('Status');
             }*/
            var html = options.heading;

            this.ui.details.html(html);
        },
        changeDetailsManual: function(options) {
            var self = this;
            this.triggerInfo = options.trigger;

            var html = '',
                    i = 0;
            _.each(options.html, function(val) {
                if (i > 0) {
                    html = html + ' ';
                }
                html = html + val;
                i++;
            });
            this.ui.details.html(html);
            if (typeof self.triggerInfo != 'undefined') {
                this.$el.find(self.triggerInfo).on('click', function() {
                    self.trigger('triggerInfoClick');
                });

            }
        },
        onClose: function() {
            var self = this;
            this.$el.find(self.triggerInfo).off('click');
        }

    });

    return HeaderView;

});
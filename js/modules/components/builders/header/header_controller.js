// access module
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/components/builders/header/views/header_view'
],
        function(Marionette, App, vent, HeaderView) {

            var HeaderShowModule = App.module('Components.Builders.Header');

            HeaderShowModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    options.currentUrl = App.getCurrentRoute();

                    this.view = new HeaderView(options);

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'showMenu', function() {
                        vent.trigger("Profile:Show:Menu", {region: this.region, icon: this.view.ui.icon});
                    }, this);
                    this.listenTo(this.view, 'triggerInfoClick', function() {
                        vent.trigger("Components:Builders:Header:triggerInfoClick");
                    }, this);


                    this.region.show(this.view);

                },
                changeDetails: function(options) {
                    this.view.changeDetails(options);
                },
                changeDetailsManual: function(options) {
                    this.view.changeDetailsManual(options);
                }


            });

            return HeaderShowModule;
        });
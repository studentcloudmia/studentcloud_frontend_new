// Admissions module
define([
    'marionette',
    'app',
    'vent',
    'modules/admissions/router',
    'modules/admissions/controller'
],
        function(Marionette, App, vent, Router, Controller) {

            var AdmissionsModule = App.module('Admissions');

            //bind to module finalizer event
            AdmissionsModule.addFinalizer(function() {

            });

            AdmissionsModule.addInitializer(
                    function(options) {
                        new Router({
                            controller: new Controller()
                        });

                    });

            return AdmissionsModule;
        });
// new application view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/application/templates/createAccount.tmpl'
], function(_, Marionette, vent, App, template) {

    var ApplicationView = App.Views.ItemView.extend({
        template: template,
        id: 'newApp'
    });

    return ApplicationView;

});
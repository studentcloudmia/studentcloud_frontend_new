// controls view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/application/templates/controls.tmpl'
], function(_, Marionette, vent, App, template) {

    var ControlsView = App.Views.ItemView.extend({
        template: template,
        id: 'appControls'
    });

    return ControlsView;

});
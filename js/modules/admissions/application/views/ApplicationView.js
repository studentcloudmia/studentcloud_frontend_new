// application main view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/application/templates/application.tmpl'
], function(_, Marionette, vent, App, template) {

    var ApplicationView = App.Views.ItemView.extend({
        template: template,
        id: 'applicationBody'
    });

    return ApplicationView;

});
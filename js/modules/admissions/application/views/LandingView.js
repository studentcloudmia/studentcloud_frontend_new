// landing view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/application/templates/landing.tmpl'
], function(_, Marionette, vent, App, template) {

    var LandingView = App.Views.ItemView.extend({
        template: template,
        id: 'landing',
        className: 'animated fadeIn',
        onShow: function() {
            var self = this;
            this.$el.find('.newApp').on('click', function() {
                self.trigger('newApp');
            });
            this.$el.find('.continueApp').on('click', function() {
            });
        }
    });

    return LandingView;

});
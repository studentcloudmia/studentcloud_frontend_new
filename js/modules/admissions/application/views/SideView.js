// side application view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/application/templates/side.tmpl'
], function(_, Marionette, vent, App, template) {

    var SideView = App.Views.ItemView.extend({
        template: template,
        id: 'sideMenu',
        onShow: function() {
            $('a.menuLinks').on('click',
                    function(e) {
                        //update selected link
                        $('a.menuLinks').parent().removeClass('active');
                        $(this).parent().addClass('active');
                    });

            $('a.menuLinks.direct').on('click', function() {
                $('a.accordionMenuBtn').parent().removeClass("opened");
            });

            $('a.accordionMenuBtn').on('click',
                    function(e) {
                        e.preventDefault();
                        $('a.menuLinks').parent().removeClass("active");
                        //update selected link
                        if ($(this).parent().hasClass('opened')) {
                            $(this).parent().removeClass('opened');
                        } else {
                            $('a.accordionMenuBtn').parent().removeClass('opened');
                            $(this).parent().addClass('opened');
                        }
                    });
        }
    });

    return SideView;

});
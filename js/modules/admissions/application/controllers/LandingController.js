//landing controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/application/views/LandingView'
            //cotrollers
],
        function(Marionette, App, vent, LandingView) {

            var LandingController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.view = new LandingView();
                    //show view
                    this.region.show(this.view);
                    this.listenTo(this.view, 'newApp', function() {
                        self.trigger('newApp');
                    });
                }
            });
            return LandingController;
        });
//new app controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/application/views/NewAppView'
            //cotrollers
],
        function(Marionette, App, vent, NewAppView) {

            var NewAppController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.view = new NewAppView();
                    //show view
                    console.log('REGION ', this.region)
                    this.region.show(this.view);
                }
            });
            return NewAppController;
        });
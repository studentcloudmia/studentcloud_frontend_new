//applicatiom main controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/application/views/ApplicationView'
            //cotrollers
],
        function(Marionette, App, vent, ApplicationView) {

            var MainController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.view = new ApplicationView();
                    //show view
                    this.region.show(this.view);
                }
            });
            return MainController;
        });
//side main controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/application/views/SideView'
            //cotrollers
],
        function(Marionette, App, vent, SideView) {

            var MainController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.view = new SideView();
                    //show view
                    this.region.show(this.view);
                }
            });
            return MainController;
        });
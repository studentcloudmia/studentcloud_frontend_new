//app controlls controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/application/views/ControlsView'
            //cotrollers
],
        function(Marionette, App, vent, ControlsView) {

            var LandingController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.view = new ControlsView();
                    //show view
                    this.region.show(this.view);
                }
            });
            return LandingController;
        });
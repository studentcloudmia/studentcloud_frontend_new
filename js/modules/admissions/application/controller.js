// application sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/admissions/application/controllers/LandingController',
    'modules/admissions/application/controllers/MainController',
    'modules/admissions/application/controllers/SideController',
    'modules/admissions/application/controllers/AppControls',
    'modules/admissions/application/controllers/NewAppController'
],
        function(Marionette, App, vent, LandingController, MainController, SideController, AppControls, NewAppController) {

            var ApplicationModule = App.module('Admissions.Application');

            ApplicationModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = App.request("Admissions:getApplicationLayout");
                    this.landingLayout = App.request("Admissions:getLandingLayout");

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);
                    ////listen to show event and start our controllers
                    this.listenTo(this.landingLayout, 'show', function() {
                        this.landingPage();
                    }, this);
                    //show layout
                    this.region.show(this.landingLayout);
                },
                landingPage: function() {
                    var self = this;
                    this.landingController = new LandingController({region: this.landingLayout.landingPage, layout: this.landingLayout});
                    this.listenTo(this.landingController, 'newApp', function() {
                        self.newApplication();
                    });
                },
                newApplication: function() {
                    this.newAppController = new NewAppController({region: this.landingLayout.landingPage, layout: this.layout});
                },
                loadControllers: function() {
                    this.mainController = new MainController({region: this.layout.bodyContent, layout: this.layout});
                    this.sideController = new SideController({region: this.layout.sideContent, layout: this.layout});
                    this.appControls = new AppControls({region: this.layout.controls, layout: this.layout});
                    this.controllerListeners();
                },
                controllerListeners: function() {
                },
                onClose: function() {
                    this.layout = null;
                }

            });

            return ApplicationModule;
        });
// Prospects Footer view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'tpl!modules/admissions/prospects/templates/footer.tmpl'
], function(_, Marionette, vent, App, template) {

    var FooterView = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid',
        onShow: function() {
            var self = this;
            this.$el.find('.list').on('click', function() {
                self.trigger('list');
            });
            this.$el.find('.add').on('click', function() {
                self.trigger('addNew');
            });
            this.$el.find('.delete').on('click', function() {
                self.trigger('deleteProspect');
            });
            this.$el.find('.campaign').on('click', function() {
                self.trigger('campaign');
            });
        },
        ui: {
            list: '.list',
            add: '.add',
            remove: '.delete',
            campaign: '.campaign'
        },
        enableGenerics: function() {
            this.ui.list[0].removeAttribute('disabled');
            this.ui.add[0].removeAttribute('disabled');
        },
        enable: function() {
            this.ui.remove[0].removeAttribute('disabled');
            this.ui.campaign[0].removeAttribute('disabled');
        },
        disable: function() {
            this.$el.find('.delete').attr("disabled", "disabled");
            this.$el.find('.campaign').attr("disabled", "disabled");
        }
    });

    return FooterView;

});
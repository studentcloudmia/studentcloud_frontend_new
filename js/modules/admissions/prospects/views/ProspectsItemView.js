// Prospects Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/admissions/prospects/templates/_prospects.tmpl',
], function(Marionette, App, vent, template) {

    var RosterItemView = App.Views.ItemView.extend({
        template: template,
        className: 'span4 eachProspect animated flipInX',
        ui: {
            circleMenu: '.circleMenu'
        },
        onShow: function() {
            var self = this;
            this.$el.attr("id", self.model.get('uid'));
            this.$el.find('.circleMenu_' + this.model.get('id')).circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 55,
                trigger: 'click',
                angle: {start: 20, end: -20}
            });
            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                self.$el.find('.showBubble').trigger('click');
            });
            //show details
            this.$el.find('.showDetails').on('click', function() {
                self.trigger('showDetails');
            });
            //selecting item
            this.$el.find('.selectOne').on('click', function() {
                if (self.$el.hasClass('selected')) {
                    self.$el.find('.checkedIcon').removeClass('icomoon-checked').addClass('icomoon-unchecked');
                    self.$el.removeClass('selected');
                }
                else {
                    self.$el.find('.checkedIcon').removeClass('icomoon-unchecked').addClass('icomoon-checked');
                    self.$el.addClass('selected');
                }
                self.trigger('multipleSelected');
            });
        },
        onClose: function() {
            this.$el.find('.userImg').off('click');
            this.$el.off('click');
        }
    });
    return RosterItemView;
});
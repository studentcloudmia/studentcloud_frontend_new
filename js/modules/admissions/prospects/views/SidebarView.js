// SideView
define([
    'marionette',
    'app',
    'tpl!modules/admissions/prospects/templates/sidebar.tmpl'
], function(Marionette, App, template) {

    var SideView = App.Views.ItemView.extend({
        template: template,
        triggers: {
            'submit #searchForm': 'search'
        },
        ui: {
            searchCriteria: '#searchCriteria',
            search: '#searchFieldInut'
        },
        onShow: function() {
            var self = this;
            this.$el.keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    self.trigger('search');
                }
            });
        },
        setCriteria: function(criterias) {
            var self = this;
            self.$el.find('#searchCriteria').html('');
            _.each(criterias, function(criteria) {
                self.$el.find('#searchCriteria').append('<option value=' + criteria.id + ' >' + criteria.description + '</option>');
            });
        },
        getSearchField: function() {
            var val = this.ui.searchCriteria.val();
            return val;
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.blur();
            return val;
        }
    });

    return SideView;

});
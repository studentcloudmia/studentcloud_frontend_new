// prospects main view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/admissions/prospects/views/ProspectsItemView',
    'tpl!modules/admissions/prospects/templates/prospects.tmpl'
], function(_, Marionette, vent, App, ItemView, template) {

    var ProspectsView = App.Views.CompositeView.extend({
        itemView: ItemView,
        itemViewContainer: '.prospectsInner',
        template: template,
        id: 'prospectsWrapper',
        className: 'row-fluid',
        initialize: function() {
            this.listenTo(this, 'childview:showDetails', this.triggerDetails, this);
            this.listenTo(this, 'childview:multipleSelected', this.multiples, this);
        },
        // The default implementation:
        appendHtml: function(collectionView, itemView, index) {
            collectionView.$el.find('.prospectsInner').prepend(itemView.el);
        },
        onShow: function() {
            //Post Click
            var self = this;
            this.$el.find('#selectAll').on('click', function() {
                self.selectAll();
            });
            this.listenTo(this.collection, 'add', function() {
                self.$el.find('.scroller').css('height', self.$el.find('.scroller').height() + 28)
                self.refreshIScroll();
            });
        },
        triggerDetails: function(view) {
            this.trigger('showDetails', view.model.id);
        },
        selectAll: function() {
            this.children.each(function(view) {
                view.$el.addClass('selected');
            });
            this.multiples();
        },
        multiples: function() {
            var self = this;
            var itemsSelected = 0;
            this.prospArr = new Array();
            this.children.each(function(view) {
                if (view.$el.hasClass('selected')) {
                    itemsSelected++;
                    self.prospArr.push(view.model.id);
                }
            });
            if (itemsSelected > 0) {
                vent.trigger('Prospects:Enable:Footer');
            }
            else {
                vent.trigger('Prospects:Disable:Footer');
            }
        },
        groupActions: function(action) {
            if (action == 'hide')
                this.$el.find('.groupAction').addClass('hidden');
            else {
                this.$el.find('.groupAction').removeClass('hidden');
                var href = 'mailto:' + _.flatten(this.emailArr);
                this.$el.find('.multipleMail').attr('href', href);
            }


        }
    });

    return ProspectsView;

});
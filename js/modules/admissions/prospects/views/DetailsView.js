// Prospects Details view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/admissions/prospects/templates/details.tmpl',
    'tpl!modules/admissions/prospects/templates/addNew.tmpl'
], function(_, Marionette, vent, App, GC, template, addNewTemplate) {

    var DetailsView = App.Views.ItemView.extend({
        template: template,
        initialize: function() {
            if (this.model.get('first_name') == "") {
                this.template = addNewTemplate;
            }
            this.listenTo(vent, 'BaseRouter:route', function() {
                this.close();
            });
        },
        onShow: function() {
            var self = this;
            this.$el.find('.upload').on('click', function() {
                self.$el.find('.uploader').click();
            });
            this.$el.find('.closeModal').on('click', function() {
                self.modalClose();
            });
            this.$el.find('.uploader').on('change', function() {
                self.upload($(this));
                self.listenTo(self.model, 'change:picture', function() {
                    self.$el.find('.userImg').attr("src", self.model.get('picture'));
                });
            });
            this.$el.find('.saveNewProspect').on('click', function() {
                var data = Backbone.Syphon.serialize(self);
                self.model.set(data, {silent: true});
                self.model.url = _.result(self.model, 'url').replace('update', 'add');
                if (self.validate())
                    self.model.save();
            });
            this.$el.find('.updateImage').on('click', function() {
                self.model.save();
            });
            this.$el.find('.prospectsImg').on('click', function() {
                self.$el.find('.uploader').click();
                self.$el.find('.updateImage').removeClass('hidden');
            });
            self.$effDate = self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);

        },
        validate: function() {
            var self = this;
            var error = 0;
            _.each(self.$el.find('.required'), function(item) {
                if ($(item)[0].value == "") {
                    error++;
                    $(item).addClass('animated shake requiredRed');
                    setTimeout(function() {
                        $(item).removeClass('animated shake requiredRed');
                    }, 500);
                }
            });
            if (error == 0)
                return true
        },
        upload: function(e) {
            self = this;
            var file = e[0].files[0];
            self.model.readFile(file);
        },
        modalClose: function() {
            this.close();
        }
    });

    return DetailsView;

});
// Prospects campaign view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/admissions/prospects/templates/campaign.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var CampaignView = App.Views.ItemView.extend({
        template: template,
        initilize: function() {
            this.listenTo(vent, 'BaseRouter:route', function() {
                this.close();
            });
        },
        onShow: function() {
            var self = this;
            this.$el.find('.closeModal').on('click', function() {
                self.modalClose();
            });
            this.$el.find('.save').on('click', function() {
                vent.trigger('Components:Alert:Alert', {
                    heading: 'Success',
                    message: 'Prosprects Moved to Campaign'
                });
                self.modalClose();
            });
        },
        modalClose: function() {
            this.close();
        }
    });

    return CampaignView;

});
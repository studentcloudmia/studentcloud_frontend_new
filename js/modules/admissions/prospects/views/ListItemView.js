// List Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/admissions/prospects/templates/_list.tmpl'
], function(Marionette, App, vent, template) {

    var RosterItemView = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid eachInList'
    });
    return RosterItemView;
});
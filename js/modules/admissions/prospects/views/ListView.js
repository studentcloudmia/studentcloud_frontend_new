// Prospects Details view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/admissions/prospects/views/ListItemView',
    'tpl!modules/admissions/prospects/templates/list.tmpl'
], function(_, Marionette, vent, App, itemView, template) {

    var ListView = App.Views.CompositeView.extend({
        template: template,
        itemView: itemView,
        itemViewContainer: '.listInner',
        initilize: function() {
            this.listenTo(vent, 'BaseRouter:route', function() {
                this.close();
            });
        },
        onShow: function() {
            var self = this;
            this.$el.find('.closeModal').on('click', function() {
                self.modalClose();
            });
            this.$el.find('.collDetails').html('Total: ' + this.collection.length + ' prospects');
            //refresh iScroll
            this.listenTo(this.collection, 'add', function() {
                this.refreshIScroll();
            });
        },
        modalClose: function() {
            this.close();
        }
    });

    return ListView;

});
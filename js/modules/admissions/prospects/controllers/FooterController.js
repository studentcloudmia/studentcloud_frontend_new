//propspects main controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/FooterView'

],
        function(Marionette, App, vent, FooterView) {

            var FooterController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.view = new FooterView();
                    this.listenTo(vent, 'Prospects:Enable:Footer', function() {
                        self.view.enable();
                    });
                    this.listenTo(vent, 'Prospects:Disable:Footer', function() {
                        self.view.disable();
                    });
                    this.listenTo(vent, 'Prospects:EnableGenerics:Footer', function() {
                        self.view.enableGenerics();
                    });
                    this.viewListeners();
                    //show view
                    this.region.show(this.view);
                },
                viewListeners: function() {
                    this.listenTo(this.view, 'close', this.close);
                    this.listenTo(this.view, 'list', this.list);
                    this.listenTo(this.view, 'addNew', this.add);
                    this.listenTo(this.view, 'deleteProspect', this.remove);
                    this.listenTo(this.view, 'campaign', this.campaign);
                },
                list: function() {
                    this.trigger('Prospects:Show:List');
                },
                add: function() {
                    this.trigger('Prospects:Show:AddNew');
                },
                remove: function() {
                    this.trigger('Prospects:Show:DeleteProspect');
                },
                campaign: function() {
                    this.trigger('Prospects:Show:Campaign');
                }
            });
            return FooterController;
        });
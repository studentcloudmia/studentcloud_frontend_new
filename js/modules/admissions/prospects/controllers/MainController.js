//prospects main controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/ProspectsView',
    //models
    'entities/models/admissions/prospects/ProspectsModel',
    //collections
    'entities/collections/admissions/prospects/ProspectsCollection',
    //cotrollers
    'modules/admissions/prospects/controllers/CampaignController',
    'modules/admissions/prospects/controllers/SideController',
    'modules/admissions/prospects/controllers/ProspectsDetailsController',
    'modules/admissions/prospects/controllers/FooterController',
    'modules/admissions/prospects/controllers/ProspectsListController'
],
        function(Marionette, App, vent, PropsectsView, ProspectsModel, ProspectsCollection, CampaignController,
                SideController, ProspectsDetailsController, FooterController, ProspectsListController) {

            var MainController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = options.layout;
                    this.collection = new ProspectsCollection();
                    this.view = new PropsectsView({collection: this.collection});
                    //show view
                    this.region.show(this.view);
                    //get footer/
                    this.footerController = new FooterController({region: this.layout.footer});
                    //ask for sidebar
                    this.sidebarController = new SideController({collection: this.collection});
                    vent.trigger('Sidebar:On', {controller: this.sidebarController});
                    vent.trigger('Sidebar:Open');
                    this.viewListeners();
                    this.footerEvents();
                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'showDetails', function(modelId) {
                        self.prospectsModel = self.collection.get(modelId);
                        this.listenTo(self.prospectsModel, 'updated', function() {
                            self.prospectsDetailsController.close();
                            self.collection.remove(self.prospectsModel.id);
                            self.collection.add(self.prospectsModel);
                        });
                        self.prospectsDetailsController = new ProspectsDetailsController({model: self.prospectsModel});
                    });
                    this.listenTo(this.view, 'after:item:added', function() {
                        this.layout.refreshIScroll();
                    }, this);
                },
                footerEvents: function() {
                    var self = this;
                    this.listenTo(this.footerController, 'Prospects:Show:AddNew', function() {
                        self.prospectsModelAdd = new ProspectsModel();
                        self.prospectsAddController = new ProspectsDetailsController({model: self.prospectsModelAdd});
                        this.listenTo(self.prospectsModelAdd, 'created', function() {
                            vent.trigger('Components:Alert:Alert', {
                                heading: 'Success',
                                message: 'Prosprect Added'
                            });
                            self.prospectsAddController.close();
                        });
                    });
                    this.listenTo(this.footerController, 'Prospects:Show:List', function() {
                        self.prospectsListController = new ProspectsListController({collection: self.collection});
                    });
                    this.listenTo(this.footerController, 'Prospects:Show:DeleteProspect', function() {
                        vent.trigger('Components:Alert:Confirm', {
                            heading: 'Delete?',
                            message: 'Are you sure you would like to delete the selected prospects',
                            callee: this,
                            callback: this.cofirmedDelete
                        });
                    });
                    this.listenTo(this.footerController, 'Prospects:Show:Campaign', function() {
                        self.campaignController = new CampaignController();
                    });
                },
                cofirmedDelete: function() {
                    var self = this;
                    self.deleteModel = new ProspectsModel();
                    self.deleteModel.clear();
                    var prospId = new Array;
                    _.each(self.view.prospArr, function(item) {
                        prospId.push({id: item});
                        self.collection.remove(item);
                    });
                    self.deleteModel.set('prospect', prospId);
                    self.deleteModel.url = _.result(self.deleteModel, 'url').replace('update', 'delete');
                    self.deleteModel.save();
                }
            });
            return MainController;
        });
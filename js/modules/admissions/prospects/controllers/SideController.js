// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/SidebarView',
    //collection
    'entities/collections/search/SearchCriteria'
],
        function(Marionette, App, vent, SidebarView, SearchCriteriaCollection) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.collection = options.collection;
                    this.OFFSET_CONSTANT = 5;
                    this.listenTo(this.collection, 'sync:stop', function() {
                        if (self.collection.length > 0) {
                            vent.trigger('Prospects:EnableGenerics:Footer');
                        }
                        self.index = this.collection.length;
                        self.offset = self.OFFSET_CONSTANT;
                        self.refetch = setTimeout(function() {
                            self.collection.fetch({remove: false, loader: false}, {}, {offset: self.offset, field: self.searchField, value: self.sText, index: self.index}, false);
                        }, 5000);
                    });
                    this.view = new SidebarView({collection: this.collection});
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    //get search criteria from backend:
                    this.listenTo(this.view, 'search', this.search, this);
                    this.criteria = new SearchCriteriaCollection();
//                    this.listenTo(self.criteria, 'sync:stop', function() {
//                        this.view.setCriteria(self.criteria.toJSON());
//                    });
                    this.criteria.fetch();
                },
                show: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.region.show(this.view);
                    this.view.setCriteria(self.criteria.toJSON());
                },
                search: function() {
                    this.index = 0;
                    this.offset = 1;
                    this.sText = this.view.getSearchText();
                    this.searchField = this.view.getSearchField();
                    clearTimeout(this.refetch);
                    if (this.sText != '') {
                        this.collection.fetch({reset: true, loader: false}, {}, {offset: this.offset, field: this.searchField, value: this.sText, index: this.index}, false);
                        this.updateCriteria();
                        vent.trigger('Sidebar:Close');
                    }
                },
                updateCriteria: function() {
                    this.topInfo = this.searchField + ':' + this.sText;
                    vent.trigger('Admissions:Propspects:UpdateHeader', this.topInfo);
                }

            });
            return SideController;
        });
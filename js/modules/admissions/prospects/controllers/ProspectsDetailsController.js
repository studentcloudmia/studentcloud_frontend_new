// prospects details controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/DetailsView'
],
        function(Marionette, App, vent, DetailsView) {

            var DetailsController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.model = options.model;
                    this.view = new DetailsView({model: this.model});
                    App.modals.show(this.view);

                }

            });

            return DetailsController;
        });
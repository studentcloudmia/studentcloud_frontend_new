// prospects list controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/ListView'
],
        function(Marionette, App, vent, ListView) {

            var ListController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.collection;
                    this.topInfo = options.topInfo;
                    this.view = new ListView({collection: this.collection});
                    App.modals.show(this.view);
                }

            });

            return ListController;
        });
// campaign details controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/prospects/views/CampaignView'
],
        function(Marionette, App, vent, CampaignView) {

            var CampaignController = Marionette.Controller.extend({
                initialize: function() {
                    this.view = new CampaignView();
                    App.modals.show(this.view);
                }

            });

            return CampaignController;
        });
// propspects sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/admissions/prospects/controllers/MainController'
],
        function(Marionette, App, vent, MainController) {

            var PropspectsModule = App.module('Admissions.Propspects');

            PropspectsModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    //save region where we can show
                    this.region = options.region;
                    this.layout = App.request("Admissions:getLayout");

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHeader({
                            region: this.layout.header,
                            heading: 'Prospects',
                            links: ""
                        });
                        this.showController();
                    }, this);
                    //show layout
                    this.region.show(this.layout);
                },
                showHeader: function(options) {
                    vent.trigger('Components:Builders:Header:on', options);
                },
                updateHeader: function(options) {
                    vent.trigger('Components:Builders:Header:changeDetailsManual', options);
                },
                showController: function() {
                    this.mainController = new MainController({region: this.layout.mainContent, layout: this.layout});
                    this.controllerListeners();
                },
                controllerListeners: function() {
                    var self = this;
                    this.listenTo(vent, 'Admissions:Propspects:UpdateHeader', function(topInfo) {
                        var info = new Array;
                        info.push('Filtered By ' + topInfo);
                        self.updateHeader({html: info});
                    });
                },
                onClose: function() {
                    this.layout = null;
                }

            });

            return PropspectsModule;
        });
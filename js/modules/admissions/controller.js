define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/admissions/views/layout',
    'modules/admissions/views/applicationLayout',
    'modules/admissions/views/LandingLayout',
    //controllers
    'modules/admissions/prospects/controller',
    'modules/admissions/application/controller'
],
        function(Marionette, App, vent, LayoutView, ApplicationLayout, LandingLayout, ProspectsModule, ApplicationModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    self.layout = new LayoutView();
                    self.applicationLayout = new ApplicationLayout();
                    self.landingLayout = new LandingLayout();
                    App.reqres.setHandler("Admissions:getLayout", function() {
                        return self.layout;
                    });
                    App.reqres.setHandler("Admissions:getApplicationLayout", function() {
                        return self.applicationLayout;
                    });
                    App.reqres.setHandler("Admissions:getLandingLayout", function() {
                        return self.landingLayout;
                    });
                    App.reqres.setHandler("Admissions:getHeading", function() {
                        return 'Admissions';
                    });
                    this.listenTo(vent, 'Admissions:prospects', function() {
                        App.navigate('prospects');
                        self.prospects();
                    }, this);
                },
                prospects: function() {
                    //start prospects
                    new ProspectsModule.Controller({region: App.main});
                },
                application: function() {
                    //start prospects
                    new ApplicationModule.Controller({region: App.emptyFullPage});
                },
                onClose: function() {
                    App.reqres.removeHandler("Admissions:getLayout");
                    App.reqres.removeHandler("Admissions:getApplicationLayout");
                    App.reqres.removeHandler("Admissions:getLandingLayout");
                    App.reqres.removeHandler("Admissions:getHeading");
                    this.layout = null;
                }

            });

            return Controller;

        });
define([
    'marionette',
    'app'
],
        function(Marionette, app) {

            var Router = Marionette.AppRouter.extend({
                appRoutes: {
                    'admissions/prospects': 'prospects',
                    'admissions/application': 'application'
                }
            });

            return Router;

        });

define([
    'marionette',
    'app',
    'tpl!modules/admissions/templates/landingLayout.tmpl'
],
        function(Marionette, App, template) {
            "use strict";

            var LayoutView = App.Views.Layout.extend({
                template: template,
                id: "landingLayout",
                regions: {
                    landingPage: ".landingPage"
                }

            });
            return LayoutView;
        });

define([
    'marionette',
    'app',
    'tpl!modules/admissions/templates/applicationLayout.tmpl'
],
        function(Marionette, App, template) {
            "use strict";

            var LayoutView = App.Views.Layout.extend({
                template: template,
                id: "admissionsApplication",
                className: 'animated fadeIn',
                regions: {
                    sideContent: ".sideContent",
                    bodyContent: ".bodyContent",
                    controls: '.controls'
                }

            });
            return LayoutView;
        });

// cbuilder catalog
define(['marionette',
    'app',
    'vent',
    'utilities',
    //controllers
    'modules/cbuilder/sidebar/controllers/Sidebar_controller',
    'modules/cbuilder/catalog/popovers/popovers_controller',
    //models
    'entities/models/course/CourseModel',
    'entities/models/oBuilder/OBuilderModelList',
    //Collection
    'entities/collections/course/CourseCollection',
    'entities/collections/college/CollegeCollectionList',
    'entities/collections/department/DepartmentCollectionList',
    'entities/collections/subject/SubjectsCollectionList',
    'entities/collections/classification/ClassificationCollectionList',
    'entities/collections/delivery/DeliveriesCollectionList',
    'entities/collections/grades/GradesCollectionList',
    'entities/collections/terms/TermsCollection',
    //view
    'modules/cbuilder/catalog/views/MainContentView'
],
        function(Marionette, App, vent, utilities,
                //controllers
                SideController, PopoverController,
                //models
                CourseModel, OBuilderModel,
                //collections
                CourseCollection, CollegeCollection, DepartmentCollection, SubjectCollection, ClassificationCollection, DeliveriesCollection, GradesCollection, TermsCollection,
                //views
                MainContentView) {

            var CbuilderCatalogModule = App.module('Cbuilder.Catalog');
            CbuilderCatalogModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    this.layout = options.layout;
                    this.region = this.layout.catalogContent;
                    //Course Collections
                    this.collection = new CourseCollection();
                    this.historicalCol = new CourseCollection();
                    this.countColl = new CourseCollection();
                    this.hasSubject = 1;
                    //dropdown collections
                    this.collegeCollection = new CollegeCollection();
                    this.departmentCollection = new DepartmentCollection();
                    this.subjectCollection = new SubjectCollection();
                    this.classificationCollection = new ClassificationCollection();
                    this.deliveriesCollection = new DeliveriesCollection();
                    this.gradesCollection = new GradesCollection();
                    this.oBuilderModel = new OBuilderModel();

                    //Dropdown collections (college, dept, subject) sync
                    this.listenTo(this.collegeCollection, 'sync', this.resetCollege, this);
                    this.listenTo(this.departmentCollection, 'sync', this.resetDepartment, this);
                    this.listenTo(this.subjectCollection, 'sync', this.resetSubject, this);



                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Course', row: 'searchRow '});
                    //listen to sidebar selecting result
                    this.listenTo(this.sidebarController, 'selectedModel', this.historicalRecords, this);
                    //ask for sidebar
                    vent.trigger('Sidebar:On', {controller: this.sidebarController});
                    
                    //do search if one was sent
                    if(options.searchText){
                        this.sidebarController.doSearch(options.searchText);
                    }
                    
                    //listen to historical col reset
                    this.listenTo(this.historicalCol, 'reset', this.historicReturned, this);
                    //call these:
                    this.bindFooterEvents();
                    this.getCount();
                    this.activateDroppable();
                    //return control of sidebar to me
                    this.listenTo(vent, 'Cbuilder:CatallogBuilderController:Sidebar:on', function() {
                        this.sidebarController.close();
                        this.sidebarController = new SideController({collection: this.collection, searchString: 'Course', row: 'searchRow '});
                        //ask for sidebar
                        vent.trigger('Sidebar:On', {controller: this.sidebarController});
                        this.listenTo(this.sidebarController, 'selectedModel', this.historicalRecords, this);
                    }, this);
                    //listen to layout Historic Nav to to navigate historic dates
                    this.listenTo(this.layout, 'Historic:Nav', this.historicalNav, this);
                    this.listenTo(this.layout, 'close', this.close, this);
                },
                getCount: function() {
                    this.countColl.fetch({}, {}, {length: 'True'}, false);
                    this.listenTo(this.countColl, 'sync', this.countReturn, this);
                },
                countReturn: function() {
                    var count = this.countColl.at(0).get('RowCount');
                    //delete reference to countColl
                    this.countColl = null;
                    if (count > 0) {
                        //open sidebar to allow search
                        vent.trigger('Sidebar:Open');
                        //update footer buttons
                        this.updateFooter({add: true});
                    } else {
                        //show user empty form to allow them to add
                        this.add();
                    }
                },
                add: function() {
                    //empty hist collection and remove future and past buttons
                    if (this.historicalCol) {
                        this.historicalCol.reset({}, {silent: true});
                    }
                    this.layout.bindUIElements();
                    this.layout.hideLeft();
                    this.layout.hideRight();
                    //show user empty form to allow them to add
                    this.model = new CourseModel();
                    //course peripherals
                    this.termsCollection = new TermsCollection();
                    this.preRequisiteCollection = new CourseCollection();
                    this.coRequisiteCollection = new CourseCollection();
                    this.equivalencesCollection = new CourseCollection();
                    this.showMain();
                    //update footer buttons
                    this.updateFooter({save: true});
                },
                historicalNav: function(option) {
                    //call selected model and pass the effective date
                    this.selectedModel({effdt: option});
                },
                showMain: function() {
                    var self = this;
                    //hide sidebar
                    vent.trigger('Sidebar:Close');
                    this.modelOldeff_date = this.model.get('eff_date');
                    //update header details
                    vent.trigger('Components:Builders:Header:changeDetails', {model: this.model});
                    this.view = new MainContentView({model: this.model});
                    this.region.show(this.view);
                    this.activateListeners();
                    this.entitylisteners();
                    this.fetchIndependentSelects();
                    //create entities
                    this.termsOffered = new PopoverController.Controller({
                        collection: self.termsCollection,
                        region: this.layout.termsOffered,
                        popOverRegion: this.layout.termsPopover,
                        popover: {
                            badge: 'Termsoffered',
                            type: 'term',
                            row: 'termsOfferedSearchRow',
                            searchString: 'Terms Offered',
                            name: 'termsOffered',
                            one: 'preRequisites',
                            two: 'coRequisite',
                            three: 'equivalence'
                        }
                    });
                    this.preReqquisites = new PopoverController.Controller({
                        collection: self.preRequisiteCollection,
                        region: this.layout.preRequisites,
                        popOverRegion: this.layout.preRequisitesPopover,
                        popover: {
                            badge: 'PreRequisite',
                            type: 'course',
                            row: 'preRequisitesSearchRow',
                            searchString: 'Pre Requisites',
                            name: 'preRequisites',
                            one: 'termsOffered',
                            two: 'coRequisite',
                            three: 'equivalence'
                        }
                    });
                    this.coReqquisites = new PopoverController.Controller({
                        collection: self.coRequisiteCollection,
                        region: this.layout.coRequisites,
                        popOverRegion: this.layout.coRequisitesPopover,
                        popover: {
                            badge: 'Corequisites',
                            type: 'course',
                            row: 'coRequisitesSearchRow',
                            searchString: 'Co Requisites',
                            name: 'coRequisite',
                            one: 'termsOffered',
                            two: 'preRequisites',
                            three: 'equivalence'
                        }
                    });
                    this.equivalences = new PopoverController.Controller({
                        collection: self.equivalencesCollection,
                        region: this.layout.equivalences,
                        popOverRegion: this.layout.equivalencesPopover,
                        popover: {
                            badge: 'Equivalence',
                            type: 'course',
                            row: 'equivalencesSearchRow',
                            searchString: 'Equivalences',
                            name: 'equivalence',
                            one: 'termsOffered',
                            two: 'preRequisites',
                            three: 'coRequisite'

                        }
                    });
                    //initializes popover (hidden)
                    this.termsOffered.initPopover();
                    this.preReqquisites.initPopover();
                    this.coReqquisites.initPopover();
                    this.equivalences.initPopover();
                },
                resetCollege: function() {
                    var self = this;
                    if (this.model.get('subject') != '') {
                        this.view.resetColleges(this.collegeCollection.toJSON());
                        this.oBuilderModel.fetch({}, {1: '1', 2: '2', key: self.model.get('subject')}, {}, false);
                    }
                    else {
                        this.view.resetColleges(this.collegeCollection.toJSON());
                    }
                },
                resetDepartment: function() {
                    this.view.resetDepartment(this.departmentCollection.toJSON());
                },
                resetSubject: function() {
                    this.view.resetSubject(this.subjectCollection.toJSON());
                },
                activateListeners: function() {
                    var self = this;
                    //College chnaged
                    this.listenTo(this.view, 'collegeChange', function(college) {
                        if (this.model.get('subject') == '' || this.hasSubject > 1) {
                            this.departmentCollection.fetch({}, {}, {list: 'True', inst: 'FIU', parent: college}, false);
                        }
                        else {
                            this.hasSubject++;
                            this.listenTo(this.oBuilderModel, 'sync', function() {

                                if (typeof this.oBuilderModel.get(0) != 'undefined') {
                                    self.model.set('CollID', self.oBuilderModel.get(0).CollegeID);
                                    self.model.set('DepID', self.oBuilderModel.get(0).DepartmentID);
                                    this.view.resetColleges(self.collegeCollection.toJSON());
                                    this.departmentCollection.fetch({}, {}, {list: 'True', inst: 'FIU', parent: self.oBuilderModel.get(0).CollegeID}, false);
                                }
                            }, this);
                        }
                    }, this);
                    //Dept changed
                    this.listenTo(this.view, 'departmentChange', function(dept) {
                        this.subjectCollection.fetch({}, {}, {list: 'True', inst: 'FIU', parent: dept}, false);
                    }, this);

                    /** NO DROPDOWN COLLECTIONS (ListenToOnce) **/
                    //Classifications collection sync
                    this.listenToOnce(this.classificationCollection, 'sync', function() {
                        this.view.resetClassification(this.classificationCollection.toJSON());
                    }, this);
                    //Deliveries collection sync
                    this.listenToOnce(this.deliveriesCollection, 'sync', function() {
                        this.view.resetDelivery(this.deliveriesCollection.toJSON());
                    }, this);
                    //Grades collection sync
                    this.listenToOnce(this.gradesCollection, 'sync', function() {
                        this.view.resetGrade(this.gradesCollection.toJSON());
                    }, this);
                },
                entitylisteners: function() {
                    var self = this;
                    //listen to model create
                    this.listenTo(this.model, 'created', function() {
                        //update modelOldeff_date
                        this.modelOldeff_date = this.model.get('eff_date');
                        self.created();
                    }, this);
                    //listen to model destroy
                    this.listenTo(this.model, 'destroyed', function() {
                        self.destroyed();
                    }, this);
                    //listen to model update
                    this.listenTo(this.model, 'updated', function() {
                        self.updated();
                    }, this);
                },
                fetchIndependentSelects: function() {
                    //TODO: grab institution from another page (ex. FIU)
                    this.collegeCollection.fetch({}, {}, {list: 'True', inst: 'FIU'}, false);
                    this.classificationCollection.fetch({}, {}, {list: 'True'}, false);
                    this.deliveriesCollection.fetch({}, {}, {list: 'True'}, false);
                    this.gradesCollection.fetch({}, {}, {list: 'True'}, false);
                },
                updateFooter: function(options) {
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                bindFooterEvents: function() {
                    var self = this;
                    self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
                    self.listenTo(vent, 'Components:Builders:Footer:add', self.add, self);
                    self.listenTo(vent, 'Components:Builders:Footer:del', self.del, self);
                    self.listenTo(vent, 'Components:Builders:Footer:prev', self.prev, self);
                    self.listenTo(vent, 'Components:Builders:Footer:next', self.next, self);
                },
                setHistoricNavigation: function(model) {
                    this.layout.bindUIElements();
                    var curIndex = this.historicalCol.indexOf(model);
                    //nav thru historical records
                    //sets triggers to layout in vent for show hide navigators and to set the nav value
                    if (this.historicalCol.length > 1) {
                        if (curIndex < this.historicalCol.length - 1) {
                            this.layout.showLeft({eff_date: this.historicalCol.at(curIndex + 1).get('eff_date'),
                                status: this.historicalCol.at(curIndex + 1).get('Status')});
                        }

                        else if (curIndex == this.historicalCol.length - 1) {
                            this.layout.hideLeft();
                        }
                        if (curIndex > 0 && curIndex <= this.historicalCol.length - 1) {
                            this.layout.showRight({eff_date: this.historicalCol.at(curIndex - 1).get('eff_date'),
                                status: this.historicalCol.at(curIndex - 1).get('Status')});
                        }
                        else if (curIndex == 0) {
                            this.layout.hideRight();
                        }


                    }
                    else if (this.historicalCol.length == 1)
                    {
                        this.layout.hideRight();
                        this.layout.hideLeft();
                    }

                },
                //selected model takes either an eff_date or a model id
                selectedModel: function(options) {
                    var self = this;
                    //course peripherals
                    this.termsCollection = new TermsCollection();
                    this.preRequisiteCollection = new CourseCollection();
                    this.coRequisiteCollection = new CourseCollection();
                    this.equivalencesCollection = new CourseCollection();

                    //if model id is passed and eff_date is null get model from collection by ID ( used for regular)
                    if (options.effdt == null && options.modelId != null) {
                        this.showMain();
                        this.navModel = this.collection.get(options.modelId);
                        this.setHistoricNavigation(this.model);
                    }
                    //if eff date is passed get model from coll by eff date ( used for historical )
                    else if (options.effdt != null) {
                        this.model = this.historicalCol.where({eff_date: options.effdt})[0];
                        this.showMain();
                        this.navModel = this.collection.get(this.model.id);
                        this.setHistoricNavigation(this.model);
                    }
                    //get terms offered attribute and place them in collection (to be passed to terms offered controller)
                    _.each(this.model.get('terms'), function(term) {
                        self.termsCollection.add(term);
                    });
                    //get pre requisites attribute and place them in collection (to be passed to pre requisites controller)
                    _.each(this.model.get('prerequisites'), function(prerequisite) {
                        self.preRequisiteCollection.add(prerequisite);
                    });
                    //get co requisites attribute and place them in collection (to be passed to co requisites controller)
                    _.each(this.model.get('corequisites'), function(corequisite) {
                        self.coRequisiteCollection.add(corequisite);
                    });
                    //get equivalence attribute and place them in collection (to be passed to co equivalence controller)
                    _.each(this.model.get('equivalences'), function(equivalence) {
                        self.equivalencesCollection.add(equivalence);
                    });
                    //update footer buttons
                    if (this.collection.hasPrev(this.navModel) && this.collection.hasNext(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: true})
                    else if (this.collection.hasNext(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: false})
                    else if (this.collection.hasPrev(this.navModel))
                        this.updateFooter({save: true, del: true, add: true, collNav: true, next: false, prev: true});
                    else
                        this.updateFooter({save: true, del: true, add: true, collNav: false, next: false, prev: false});

                },
                //fetched collection returns
                historicReturned: function() {
                    this.model = this.historicalCol.where({Status: "CURRENT"})[0];
                    //call selected model with model ID (first time)
                    this.selectedModel({modelId: this.model.id});
                },
                historicalRecords: function(modelId) {
                    //get model from search collection
                    this.model = this.collection.get(modelId);
                    this.historicalCol.fetch({reset: true}, {modelId: modelId}, {historical: 'True'}, false);
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.catalogContent.el).droppable({
                        accept: ".searchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.historicalRecords(ui.draggable.attr("data-id"));
                        }
                    });
                },
                /**** ENTITY SAVE DELETE UPDATE ****/
                save: function() {
                    //update model
                    if (this.view.updateModel()) {
                        //verify eff_dt has been changed
                        if (this.modelOldeff_date == this.model.get('eff_date')) {
                            vent.trigger('Components:Alert:Confirm', {
                                heading: 'Effective Date Not Changed',
                                message: 'You have not changed the effective date for ' + this.model.get('title'),
                                cancelText: 'Edit',
                                confirmText: 'Save',
                                callee: this,
                                callback: this.confirmedSave
                            });
                        } else {
                            this.confirmedSave();
                        }
                    }
                },
                confirmedSave: function() {
                    //TODO: find a way to validate eff date automatically in the form, maybe new form validator?
                    if (this.view.ui.eff_date.val() == '') {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Invalid Effective Date',
                            message: 'Effective date cannot be empty'
                        });
                    } else
                    {
                        /*APPENDING TERMS OFFERED*/
                        var self = this;
                        var modelArray = [];
                        _.each(self.termsCollection.toJSON(), function(model, i) {
                            modelArray[i] = {terms: model.terms};
                        });
                        this.model.set('terms', modelArray);
                        /*APPENDING PRE-REQUISITES*/
                        var self = this;
                        var modelArray = [];
                        _.each(self.preRequisiteCollection.toJSON(), function(model, i) {
                            modelArray[i] = {course_catalog_pre_req: model.course_catalog_pre_req};
                        });
                        this.model.set('prerequisites', modelArray);
                        /*APPENDING CO-REQUISITES*/
                        var self = this;
                        var modelArray = [];
                        _.each(self.coRequisiteCollection.toJSON(), function(model, i) {
                            modelArray[i] = {course_catalog_co_req: model.course_catalog_co_req};
                        });
                        this.model.set('corequisites', modelArray);
                        /*APPENDING EQUIVALENCES*/
                        var self = this;
                        var modelArray = [];
                        _.each(self.equivalencesCollection.toJSON(), function(model, i) {
                            modelArray[i] = {course_catalog_equi: model.course_catalog_equi};
                        });
                        this.model.set('equivalences', modelArray);
                        this.model.set('repeat', utilities.checkBoxConvert(this.model.get('repeat')));
                        this.model.set('allow_mutiple', utilities.checkBoxConvert(this.model.get('allow_mutiple')));
                        //save model
                        this.model.save();
                    }

                },
                del: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Delete?',
                        message: 'Are you sure you would like to delete ' + this.model.get('title'),
                        callee: this,
                        callback: this.confirmedDel
                    });
                },
                confirmedDel: function() {
                    this.model.destroy();
                },
                created: function() {
                    this.updateFooter({save: true, add: true, del: true});
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.model.get('title') + ' Created'
                    });
                    this.refreshCollection();
                },
                updated: function() {
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.model.get('title') + ' Updated'
                    });
                    this.refreshCollection();
                },
                destroyed: function() {
                    //redo search
                    this.sidebarController.search();
                    //if this was the last model then remove from search and show add
                    if (this.historicalCol.length == 0) {
                        this.updateFooter();
                        this.add();
                    } else {
                        this.refreshCollection();
                    }

                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.model.get('title') + ' Deleted'
                    });
                },
                /**** END ENTITY SAVE DELETE UPDATE ****/

                /**** SEARCH RESULT NAVIGATION ****/
                prev: function() {
                    this.navModel = this.collection.prevModel(this.navModel);
                    this.historicalRecords(this.navModel.id);
                },
                next: function() {
                    this.navModel = this.collection.nextModel(this.navModel);
                    this.historicalRecords(this.navModel.id);
                },
                /**** END SEARCH RESULT NAVIGATION ****/
                refreshCollection: function() {
                    var self = this;
                    //refresh histColl
                    //this.historicalCol.url = _.result(this.model, 'url') + this.model.getId() + '/';
                    this.historicalCol.fetch({reset: true}, {modelId: self.model.id}, {historical: 'True'}, false);
                }
            });
            return CbuilderCatalogModule;
        });
// cbuilder popovers
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/cbuilder/sidebar/controllers/Sidebar_controller',
    'modules/cbuilder/catalog/popovers/controllers/BtnController',
    'modules/cbuilder/catalog/popovers/controllers/PopoverController',
    //models
    'entities/collections/course/CourseCollection',
    'entities/models/course/CourseModel',
    'entities/collections/terms/TermsCollection',
    'entities/models/terms/TermModelList'
],
        function(Marionette, App, vent, SideController, BtnController, PopoverController, CourseCollection, CourseModel, TermsOfferedCollection, TermsOfferedModel) {

            var CbuilderPopoversModule = App.module('Cbuilder.Popovers');
            CbuilderPopoversModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    //option variables
                    this.popover = options.popover;
                    this.region = options.region;
                    this.popOverRegion = options.popOverRegion;
                    this.collection = options.collection;
                    var self = this;
                    //entities, decide which collection to use:
                    if (options.popover.type == 'course')
                        this.searchCollection = new CourseCollection();
                    else
                        this.searchCollection = new TermsOfferedCollection();

                    this.btnController = new BtnController({region: this.region, popover: self.popover});
                    //listeners
                    this.listenTo(this.btnController, 'close', this.close, this);
                    this.listenTo(this.btnController, 'clicked', this.showPopover, this);
                    //display collection length in badge
                    vent.trigger("CBuilder:" + self.popover.badge + ":Badge:update", this.collection.length);
                    this.listenTo(this.collection, 'add remove', function() {
                        vent.trigger("CBuilder:" + self.popover.badge + ":Badge:update", this.collection.length);
                    }, this);
                },
                del: function(modelId) {
                    var self = this;
                    self.collection.remove(self.collection.where({id: modelId}));
                },
                initPopover: function() {
                    var self = this;
                    this.popoverController = new PopoverController({region: this.popOverRegion, popover: self.popover, coll: this.collection});
                    this.listenTo(this.popoverController, 'delete', this.del, this);
                    this.listenTo(this.popoverController, 'search', this.search);
                },
                showPopover: function() {
                    this.popoverController.showPopover();
                },
                search: function() {
                    var self = this;
                    this.sideController = new SideController({collection: this.searchCollection, searchString: self.popover.searchString, row: self.popover.row});
                    vent.trigger('Sidebar:On', {controller: this.sideController});
                    vent.trigger('Sidebar:Open');
                    this.activateDroppable();
                    this.listenTo(this.sideController, 'selectedModel', this.selectedModel, this);
                },
                showItem: function() {
                    //add model to collection
                    if (this.popover.name == 'coRequisite') {
                        this.model.set({course_catalog_co_req: this.model.id});
                        if (!this.collection.findModelByParam(this.model, 'course_catalog_co_req')) {
                            this.collection.add(this.model);
                        }
                    }
                    else if (this.popover.name == 'equivalence') {
                        this.model.set({course_catalog_equi: this.model.id});
                        if (!this.collection.findModelByParam(this.model, 'course_catalog_equi')) {
                            this.collection.add(this.model);
                        }
                    }
                    else if (this.popover.name == 'termsOffered') {
                        this.model.set({terms: this.model.id});
                        if (!this.collection.findModelByParam(this.model, 'terms')) {
                            this.collection.add(this.model);
                        }
                    }
                    else if (this.popover.name == 'preRequisites') {
                        this.model.set({course_catalog_pre_req: this.model.id});
                        if (!this.collection.findModelByParam(this.model, 'course_catalog_pre_req')) {
                            this.collection.add(this.model);
                        }
                    }
                },
                selectedModel: function(modelId) {

                    //fetch selected model
                    if (this.popover.type == 'course')
                        this.model = new CourseModel();
                    else
                        this.model = new TermsOfferedModel();

                    this.model.fetch({}, {id: modelId}, {}, true);
                    this.listenTo(this.model, 'sync:stop', this.showItem, this);
                },
                activateDroppable: function() {
                    var self = this;
                    $('.popOverWrapper').droppable({
                        accept: "." + self.popover.row,
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.selectedModel(ui.draggable.attr("data-id"));
                        }
                    });
                }
            });
            return CbuilderPopoversModule;
        });
// SideView
define([
    'underscore',
    'marionette',
    'app',
    'vent',
    'tpl!modules/cbuilder/catalog/popovers/templates/btnCorequisites.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/btnPrerequisites.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/btnEquivalences.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/btnTermsOffered.tmpl'
], function(_, Marionette, App, vent, coReqTemplate, preReqTemplate, equivTemplate, termTemplate) {

    var BtnView = App.Views.ItemView.extend({
        template: {},
        tagName: 'button',
        className: 'btn btn-inverse btn-mini',
        initialize: function(popover) {
            if (popover.name == "coRequisite")
                this.template = coReqTemplate;
            else if (popover.name == "preRequisites")
                this.template = preReqTemplate;
            else if (popover.name == "equivalence")
                this.template = equivTemplate;
            else if (popover.name == "termsOffered")
                this.template = termTemplate;
        },
        ui: {
            badge: '.badge'
        },
        triggers: {
            'click': 'clicked'
        },
        resetCount: function(count) {
            this.ui.badge.html(count);
        },
        getClass: function() {
            return this.$el;
            //return '.' + this.class;
        }


    });

    return BtnView;

});
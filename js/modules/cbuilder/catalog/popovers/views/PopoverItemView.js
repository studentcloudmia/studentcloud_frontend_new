// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/cbuilder/catalog/popovers/templates/_corequisites.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/_prerequisites.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/_equivalences.tmpl',
    'tpl!modules/cbuilder/catalog/popovers/templates/_termsOffered.tmpl'
], function(Marionette, App, vent, coReqTemplate, preReqTemplate, equivTemplate, termTemplate) {

    var PopoverItemView = App.Views.ItemView.extend({
        template: {},
        triggers: {
            'click .delItem ': 'delete'
        },
        initialize: function(options) {
            if (options.popover.name == "coRequisite")
                this.template = coReqTemplate;
            else if (options.popover.name == "preRequisites")
                this.template = preReqTemplate;
            else if (options.popover.name == "equivalence")
                this.template = equivTemplate;
            else if (options.popover.name == "termsOffered")
                this.template = termTemplate;
        }
    });

    return PopoverItemView;

});
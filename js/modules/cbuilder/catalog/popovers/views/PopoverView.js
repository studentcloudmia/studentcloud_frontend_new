// cbuilder popovers PopoverView view
define([
    'jquery',
    'underscore',
    'marionette',
    'vent',
    'app',
    'modules/cbuilder/catalog/popovers/views/PopoverItemView',
    'tpl!modules/cbuilder/catalog/popovers/templates/popover.tmpl'
], function($, _, Marionette, vent, App, ItemView, Template) {

    var PopoverView = App.Views.CompositeView.extend({
        itemView: ItemView,
        tagName: 'div',
        itemViewContainer: '.popOverContent',
        className: 'popOverWrapper animated bounceOutDown hidden noClick',
        template: Template,
        itemViewOptions: {},
        initialize: function(options) {
            this.popover = options.popover;
            this.itemViewOptions = {popover: options.popover};
            this.listenTo(this, 'childview:delete', this.triggerDelete);
            this.listenTo(vent, 'CBuilder:Popover:Hide:' + options.popover.name, this.hideMe);
        },
        triggerDelete: function(view) {
            this.trigger('delete', view.model.get('id'));
        },
        hideMe: function() {
            this.$el.removeClass('bounceInUp').addClass('bounceOutDown');
        },
        toggle: function() {
            var self = this;
            if (this.$el.hasClass('bounceOutDown')) {
                this.$el.removeClass('bounceOutDown hidden noClick').addClass('bounceInUp allowClick');
                this.trigger('search');
                vent.trigger('CBuilder:Popover:Hide:' + self.popover.one);
                vent.trigger('CBuilder:Popover:Hide:' + self.popover.two);
                vent.trigger('CBuilder:Popover:Hide:' + self.popover.three);
            }
            else {
                this.$el.removeClass('bounceInUp').addClass('bounceOutDown');
                //return control over the sidebar to catalog controller on view close
                vent.trigger("Cbuilder:CatallogBuilderController:Sidebar:on");
            }
        },
        onClose: function() {
            this.$el.addClass('noClick');
        }
    });

    return PopoverView;

});
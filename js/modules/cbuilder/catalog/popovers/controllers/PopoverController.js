// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/cbuilder/catalog/popovers/views/PopoverView'
],
        function(Marionette, App, vent, PopoverView) {

            var PopoverController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.coll;
                    this.region = options.region;
                    this.popover = options.popover;
                    this.view = new PopoverView({collection: this.collection, popover: options.popover});
                    this.region.show(this.view);
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.view, 'delete', this.triggerDelete, this);
                    this.listenTo(this.view, 'search', this.search, this);
                },
                search: function() {
                    this.trigger('search');
                },
                showPopover: function() {
                    this.view.toggle();
                },
                triggerDelete: function(modelId) {
                    this.trigger('delete', modelId);
                }

            });

            return PopoverController;
        });
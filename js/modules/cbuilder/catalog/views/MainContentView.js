// cbuilder catalog mainContent view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/cbuilder/catalog/templates/mainContent.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        tagName: 'form',
        id: 'Catalog',
        className: 'formHeigh',
        ui: {
            college: '#collegeSelector',
            department: '#departmentSelector',
            subject: '#subjectSelector',
            classification: '#classifSelector',
            delivery: '#deliverySelector',
            grade: '#gradeSelector',
            validationBtn: '.validationBtn',
            eff_date: '.eff_date'
        },
        events: {
            'change #collegeSelector': 'collegeChange',
            'change #departmentSelector': 'departmentChange'
        },
        onRender: function() {
            //start jquery validate
            var self = this;
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit], .datepicker ").jqBootstrapValidation();
            });
            //start date picker
            self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
        },
        resetColleges: function(colleges) {
            var self = this;
            //inject options to countries select
            if (colleges.length > 0) {
                this.ui.college.html('');
                _.each(colleges, function(college) {
                    //mark as selected if college matches model college
                    var sel = (college.model_id == self.model.get('CollID')) ? 'selected' : '';
                    var val = college.Description_Short;
                    self.ui.college.append('<option value="' + college.model_id + '" ' + sel + '>' + val + '</option>');
                });
            }
            else {
                this.ui.college.html('<option selected>No Colleges</option>');
            }
            //trigger select on colleges
            this.ui.college.change();
        },
        resetDepartment: function(departments) {
            var self = this;
            //inject options to countries select
            if (departments.length > 0) {
                this.ui.department.html('');
                _.each(departments, function(department) {
                    //mark as selected if department matches model department
                    var sel = (departments.model_id == self.model.get('DepID')) ? 'selected' : '';
                    var val = department.Description_Short;
                    self.ui.department.append('<option value="' + department.model_id + '"' + sel + ' >' + val + '</option>');
                });

            }
            else {
                this.ui.department.html('<option selected>No Departments</option>');
            }
            //trigger select on departments
            this.ui.department.change();
        },
        resetSubject: function(subjects) {
            var self = this;
            //inject options to countries select
            if (subjects.length > 0) {
                this.ui.subject.html('');
                _.each(subjects, function(subject) {
                    //mark as selected if subject matches model subject
                    var sel = (subject.model_id == self.model.get('subject')) ? 'selected' : '';
                    var val = subject.Description_Short;
                    self.ui.subject.append('<option value="' + subject.model_id + '" ' + sel + '>' + val + '</option>');
                });
            }
            else {
                this.ui.subject.html('<option selected>No Subject</option>');
            }
            //trigger select on subjects
            this.ui.subject.change();
        },
        resetClassification: function(classifications) {
            var self = this;
            //inject options to classification select
            if (classifications.length > 0) {
                this.ui.classification.html('');
                _.each(classifications, function(classif) {
                    //mark as selected if classification matches model classification
                    var sel = (classif.id == self.model.get('classif')) ? 'selected' : '';
                    var val = classif.Description_Short;
                    self.ui.classification.append('<option value="' + classif.id + '" ' + sel + '>' + val + '</option>');
                });
            }
            else {
                this.ui.classification.html('<option selected>No Classifications</option>');
            }
            //trigger select on classifications
            this.ui.classification.change();
        },
        resetDelivery: function(deliveries) {
            var self = this;
            //inject options to delivery select
            if (deliveries.length > 0) {
                this.ui.delivery.html('');
                _.each(deliveries, function(delivery) {
                    //mark as selected if delivery matches model delivery
                    var sel = (delivery.id == self.model.get('delivery')) ? 'selected' : '';
                    var val = delivery.Description_Short;
                    self.ui.delivery.append('<option value="' + delivery.id + '" ' + sel + '>' + val + '</option>');
                });
            }
            else {
                this.ui.delivery.html('<option selected>No Deliveries</option>');
            }
            //trigger select on deliveries
            this.ui.delivery.change();
        },
        resetGrade: function(grades) {
            var self = this;
            //inject options to grade select
            if (grades.length > 0) {
                this.ui.grade.html('');
                _.each(grades, function(grade) {
                    //mark as selected if grade matches model grade
                    var sel = (grade.id == self.model.get('grade')) ? 'selected' : '';
                    var val = grade.Description_Short;
                    self.ui.grade.append('<option value="' + grade.id + '" ' + sel + '>' + val + '</option>');
                });
            }
            else {
                this.ui.grade.html('<option selected>No Grades</option>');
            }
            //trigger select on grades
            this.ui.grade.change();
        },
        collegeChange: function() {
            var college = this.ui.college.val();
            this.trigger('collegeChange', college);
        },
        departmentChange: function() {
            var dept = this.ui.department.val();
            this.trigger('departmentChange', dept);
        },
        save: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                vent.trigger('Components:Alert:Alert', {
                    heading: 'Missing Data',
                    message: 'Please review the form for incorrect data'
                });
            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.save(data);
                self.trigger('saved');
            }
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        }
    });

    return MainContentView;

});
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/cbuilder/views/layout',
    //controllers
    'modules/cbuilder/catalog/catalog_controller'
],
        function(Marionette, App, vent, LayoutView, CbuilderCatalogModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {

                    App.reqres.setHandler("Cbuilder:getLayout", function() {
                        var layout = new LayoutView();
                        return layout;
                    });

                    this.listenTo(vent, 'Cbuilder:showCatalog', function() {
                        App.navigate('cbuilder/catalog');
                        this.showCatalog();
                    }, this);

                    this.listenTo(vent, 'Cbuilder:searchCatalog', function(text) {
                        App.navigate('cbuilder/catalog');
                        this.searchText = text;
                        this.showCatalog();
                    }, this);


                },
                showCatalog: function() {
                    //transfer control to show submodule
                    this.layout = new LayoutView();
                    this.listenTo(this.layout, 'show', this.showAllContent, this);
                    App.main.show(this.layout);
                },
                showAllContent: function() {
                    //show header
                    var optionsHeader = {
                        region: this.layout.header,
                        heading: 'Course Catalog',
                        links: ""
                    };
                    vent.trigger('Components:Builders:Header:on', optionsHeader);

                    if(!this.searchText){
                        //show main catalog content
                        new CbuilderCatalogModule.Controller({layout: this.layout});
                    }else{
                        new CbuilderCatalogModule.Controller({layout: this.layout, searchText: this.searchText});
                    }                   

                    //show footer
                    var optionsFooter = {
                        region: this.layout.footer
                    };
                    vent.trigger('Components:Builders:Footer:on', optionsFooter);
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Cbuilder:getLayout");
                    App.reqres.removeHandler("Cbuilder:getLinks");
                }

            });

            return Controller;

        });
// cbuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/cbuilder/router',
    'modules/cbuilder/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var CbuilderModule = App.module('Cbuilder');

        //bind to module finalizer event
        CbuilderModule.addFinalizer(function() {
	
        });

        CbuilderModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return CbuilderModule;
    });
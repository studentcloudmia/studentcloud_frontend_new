// SideView
define([
    'marionette',
    'app',
    'modules/cbuilder/sidebar/views/SidebarItemView',
    'tpl!modules/cbuilder/sidebar/templates/sidebar.tmpl'
], function(Marionette, App, ItemView, template) {

    var SideView = App.Views.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: 'tbody',
        itemViewOptions: {},
        initialize: function(options) {
            this.setSearchString = options.searchString;
            this.row = options.row;
            this.listenTo(this, 'childview:selectedModel', this.selectedModel);
            this.itemViewOptions.row = this.row;
        },
        triggers: {
            'change #searchField': 'search'
        },
        ui: {
            search: '#searchField'
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.val('');
            this.ui.search.blur();
            return val;
        },
        selectedModel: function(view) {
            this.trigger('selectedModel', view.model.id);
        },
        onShow: function() {
            this.$el.find('#searchField').attr('placeholder', 'Search ' + this.setSearchString);
        },
        doSearch: function(text){
            this.ui.search.val(text).trigger('change');
        }
    });
    return SideView;
});
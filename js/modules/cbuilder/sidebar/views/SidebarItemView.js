// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/cbuilder/sidebar/templates/_sidebar.tmpl'
], function(Marionette, App, vent, template) {

    var SideItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'searchRowName',
        initialize: function(options) {
            this.row = options.row;
        },
        triggers: {
            'click': 'selectedModel'
        },
        onRender: function() {
            //add data-id attribute to item
            this.$el.attr('data-id', this.model.id);
            this.$el.attr('data-serverid', this.model.id);
            //make the item draggable
            this.$el.draggable({revert: "invalid", helper: "clone"});
            this.$el.addClass(this.row);
        }
    });

    return SideItemView;

});
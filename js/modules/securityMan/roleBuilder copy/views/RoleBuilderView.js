// ColorPickerView
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'iscroll',
	'tpl!modules/securityMan/roleBuilder/templates/roleBuilder.tmpl'
],function(_, Marionette, vent, App, IScroll, template){
	
	var AssignView = App.Views.ItemView.extend({
		template: template
	});
	
	return AssignView;
	
});
//color picker controller
define([
'marionette',
'app',
'vent',
//views
'modules/securityMan/roleBuilder/views/RoleBuilderView'
],
function(Marionette, App, vent, RoleBuilderView) {

    var MainController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			
			this.view = new RoleBuilderView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        }

    });

    return MainController;
});
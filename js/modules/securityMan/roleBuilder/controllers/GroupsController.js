//groups controller
define([
'marionette',
'app',
'vent',
//views
'modules/securityMan/roleBuilder/views/group/GroupsView',
//entities
'entities/collections/security/GroupCollection'
],
function(Marionette, App, vent, GroupsView, GroupCollection) {

    var MainController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			this.collection = new GroupCollection();
			this.view = new GroupsView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenToOnce(this.collection, 'reset', function(){
				this.view.render();
			}, this);
			
			//show view
			this.region.show(this.view);
            this.collection.fetch({},{},{},false);
        }

    });

    return MainController;
});
//color picker controller
define([
'marionette',
'app',
'vent',
//views
'modules/securityMan/roleBuilder/views/layout',
//controllers
'modules/securityMan/roleBuilder/controllers/GroupsController'
],
function(Marionette, App, vent, LayoutView,
    //controllers
    GroupsController) {

    var MainController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			
			this.layout = new LayoutView();
			
			this.listenTo(this.layout, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.layout, 'show', function(){
				this.showControllers();
			}, this);
			
			//show view
			this.region.show(this.layout);
        },
        
        showControllers: function(){
            new GroupsController({region: this.layout.groupHolder});
        }

    });

    return MainController;
});
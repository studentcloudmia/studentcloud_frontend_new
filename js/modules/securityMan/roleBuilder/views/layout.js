define([
'marionette',
'app',
'tpl!modules/securityMan/roleBuilder/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            instHolder: '.instHolder',
            groupHolder: '.groupHolder',
            navHolder: '.navHolder',
            dataHolder: '.dataHolder'
        },
        
        triggers: {
            'click .instBtn': 'AddInst',
            'click .groupBtn': 'AddGroup',
            'click .navBtn': 'AddNav',
            'click .dataBtn': 'AddData'
        }

    });
    return LayoutView;
});
// Group composite view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'modules/securityMan/roleBuilder/views/group/GroupView',
	'tpl!modules/securityMan/roleBuilder/templates/groups.tmpl'
],function(_, Marionette, vent, App, GroupView, template){
	
	var GroupsView = App.Views.CompositeView.extend({
		template: template,
        tagName: 'div',
        className: 'scroller',
        itemView: GroupView
	});
	
	return GroupsView;
	
});
// Group item view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/securityMan/roleBuilder/templates/_group.tmpl'
],function(_, Marionette, vent, App, template){
	
	var GroupView = App.Views.ItemView.extend({
		template: template,
        tagName: 'div',
        className: 'individualElem'
	});
	
	return GroupView;
	
});
// fbuilder institution
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/securityMan/assign/controllers/MainController'
    ],
function(Marionette, App, vent, MainController) {

    var AssignModule = App.module('SecurityMan.Assign');

    AssignModule.Controller = Marionette.Controller.extend({

        Layout: {},

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            
			this.layout = App.request("SecurityMan:getLayout");

            //close controller when layout closes
            this.listenTo(this.layout, 'close', function(){
                this.close();
            }, this);
            
            //listen to show event and start our controllers
            this.listenTo(this.layout, 'show', function(){
                this.showHeader();
                this.showController();
                this.showFooter();
            }, this);
            
            //show layout
            this.region.show(this.layout);
            
			//ask for sidebar
			//vent.trigger('Sidebar:On');
        },
        
        showHeader: function(){
            //start header controller
			var links = App.request("SecurityMan:getLinks");
			var heading = App.request("SecurityMan:getHeading");
			var options = {
				region: this.layout.header,
				heading: heading,
				links: links
			};
			
            vent.trigger('Components:Builders:Header:on', options);
        },
        
        showController: function(){
            //hide sidebar
            //vent.trigger('Sidebar:Close');
            
            this.mainController = new MainController({region: this.layout.mainContent});
			this.setupMainListeners();
        },

        showFooter: function(){
            //start footer controller
			var options = {
				region: this.layout.footer
			};
	
            vent.trigger('Components:Builders:Footer:on', options);
			this.bindFooterEvents();
        },

        updateFooter: function(options){	
            vent.trigger('Components:Builders:Footer:updateButtons', options);
        },
		
		bindFooterEvents: function(){
			var self = this;
			self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
		},
		
		setupMainListeners: function(){
            //listening to created since we always POST in this case
            //normally it should be updated if we PUT
            // this.listenTo(this.colorController, 'created', this.updated, this);
		},
		
		save: function(){
			this.colorController.save();
		},
        
        updated: function(){
			vent.trigger('Components:Alert:Alert', {
			    heading: 'Success',
			    message: 'Institution Updated'
			});
        },
		
		activateDroppable: function(){
			var self = this;
			$(this.layout.rightSide.el).droppable({
			    accept: ".searchRow",
			    hoverClass: "ui-state-highlight",
			    drop: function(event, ui) {
					//call selected model with dragged model
                    console.log('dropped color: ', ui.draggable.attr("data-id"));
					//self.selectedModel(ui.draggable.attr("data-id"));
			    }
			});
		}

    });

    return AssignModule;
});
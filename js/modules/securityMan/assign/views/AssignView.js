// ColorPickerView
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/securityMan/assign/templates/assign.tmpl'
],function(_, Marionette, vent, App, template){
	
	var AssignView = App.Views.ItemView.extend({
		template: template

	});
	
	return AssignView;
	
});
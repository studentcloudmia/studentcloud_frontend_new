define([
'marionette',
'app',
'tpl!modules/securityMan/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: "#appHeader",
            mainContent: ".mainContent",
            footer: "#bottomMenu"
        }

    });
    return LayoutView;
});

define([
'marionette',
'app',
],
function(Marionette, app) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'security/assign': 'assign',
            'security/roleBuilder': 'roleBuilder'
        }
    });

    return Router;

});
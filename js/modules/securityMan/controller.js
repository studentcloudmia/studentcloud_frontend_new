define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/securityMan/views/layout',
    //controllers
    'modules/securityMan/assign/controller',
    'modules/securityMan/roleBuilder/controller'
],
        function(Marionette, App, vent, LayoutView, AssignModule, RoleBuilderModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
	                var self = this;
                    
					self.layout = new LayoutView();
					
                    App.reqres.setHandler("SecurityMan:getLayout", function() {
                        return self.layout;
                    });
					
                    App.reqres.setHandler("SecurityMan:getHeading", function() {
                        return 'Security Manager';
                    });
					
                    App.reqres.setHandler("SecurityMan:getLinks", function() {
                        //TODO: These should be generated from menu collection
                        return self.getLinks();
                    });

                    this.listenTo(vent, 'SecurityMan:assign', function() {
                        App.navigate('security/assign');
                        self.assign();
                    }, this);

                    this.listenTo(vent, 'SecurityMan:roleBuilder', function() {
                        App.navigate('security/roleBuilder');
                        self.roleBuilder();
                    }, this);

                },

				getLinks: function(){
					return [{
					    name: 'Assign',
					    link: 'security/assign'
					},
					{
					    name: 'Role Builder',
					    link: 'security/roleBuilder'
					}];
				},

                assign: function() {
					//show layout
                    new AssignModule.Controller({region: App.main});
                },

                roleBuilder: function() {
					//show layout
                    new RoleBuilderModule.Controller({region: App.main});
                },
                        
                onClose: function() {
                    App.reqres.removeHandler("SecurityMan:getLinks");
                    App.reqres.removeHandler("SecurityMan:getLayout");		
					this.layout = null;
                }

            });

            return Controller;

        });
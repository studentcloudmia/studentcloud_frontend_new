// EnterMan module
define([
    'marionette',
    'app',
    'vent',
    'modules/securityMan/router',
    'modules/securityMan/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var SecurityManModule = App.module('SecurityMan');

        //bind to module finalizer event
        SecurityManModule.addFinalizer(function() {
	
        });

        SecurityManModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return SecurityManModule;
    });
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/majorMap/views/layout',
    //controllers
    'modules/majorMap/builder/controller',
    'modules/majorMap/userMap/controller',
    'modules/majorMap/widget/controller',
    'modules/majorMap/builder/controllers/Sidebar_controller',
    //entities
    'entities/collections/majorMap/MajorMapCollection'
],
        function(Marionette, App, vent, LayoutView, BuilderModule, UserMapModule, WidgetModule, MapSearchController, MajorMapCollection) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
	                var self = this;
                    
					self.layout = new LayoutView();
					
                    App.reqres.setHandler("MajorMap:getLayout", function() {
                        return self.layout;
                    });
					
                    App.reqres.setHandler("MajorMap:getSearch", function() {
                        return new MapSearchController({collection: new MajorMapCollection()});
                    });
					
                    App.reqres.setHandler("MajorMap:getHeading", function() {
                        return 'Major Map';
                    });

                    this.listenTo(vent, 'MajorMap:builder', function() {
                        App.navigate('path/builder');
                        self.builder();
                    }, this);

                    this.listenTo(vent, 'MajorMap:userMap', function() {
                        App.navigate('path/userMap');
                        self.userMap();
                    }, this);

                    this.listenTo(vent, 'MajorMap:widget', function(opts) {
                        self.widget(opts);
                    }, this);

                },

                builder: function(id) {
                    //start enroll
                    new BuilderModule.Controller({region: App.main, mapId: id});
                },
                
                userMap: function(){
                    //start user map
                    new UserMapModule.Controller({region: App.main});
                },
                
                widget: function(options){
                    //need to send at least options.region
                    this.widgetController = new WidgetModule.Controller(options);
                },
                        
                onClose: function() {
                    App.reqres.removeHandler("MajorMap:getLayout");
                    App.reqres.removeHandler("MajorMap:getSearch");
                    App.reqres.removeHandler("MajorMap:getHeading");		
					this.layout = null;
                }

            });

            return Controller;

        });
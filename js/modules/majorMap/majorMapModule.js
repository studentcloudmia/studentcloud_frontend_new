// MajorMap module
define([
    'marionette',
    'app',
    'vent',
    'modules/majorMap/router',
    'modules/majorMap/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var MajorMapModule = App.module('MajorMap');

        //bind to module finalizer event
        MajorMapModule.addFinalizer(function() {
	
        });

        MajorMapModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return MajorMapModule;
    });
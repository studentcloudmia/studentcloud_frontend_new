// major map widget
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/majorMap/widget/templates/widget.tmpl'
],function(_, Marionette, vent, App, template){
	
	var WidgetView = App.Views.ItemView.extend({
		template: template,
        className: 'majorMapWidg',
        
        triggers: {
            'click': 'Clicked'
        }

	});
	
	return WidgetView;
	
});
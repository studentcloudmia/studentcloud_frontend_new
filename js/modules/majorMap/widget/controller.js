// major map widget sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/majorMap/widget/views/WidgetView',
    //entities
    'entities/models/majorMap/UserMapModel',
    ],
function(Marionette, App, vent, WidgetView, UserMapModel) {

    var WidgetModule = App.module('MajorMap.Widget');

    WidgetModule.Controller = Marionette.Controller.extend({

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
            this.model = new UserMapModel();
			this.view = new WidgetView({model: this.model});
            
            //close controller when layout closes
            this.listenTo(this.view, 'close', function(){
                self.close();
            }, this);
            
            this.listenTo(this.view, 'Clicked', this.Clicked);
            
            //show view
            this.listenTo(this.model, 'change', function(){
                self.region.show(self.view);
            }, this);
            
            //fetch model
            this.model.fetch({muteError: true},{},{},false);            
        },
        
        Clicked: function(){
            //navigate to userMap page
            vent.trigger('MajorMap:userMap');
        }

    });

    return WidgetModule;
});
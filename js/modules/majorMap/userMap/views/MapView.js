// user major map
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/map.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapView = App.Views.ItemView.extend({
		template: template,
        tagName: 'div',
        className: '',
        
        ui: {
            yearsDiv: '.yearColumnDiv .scroller',
            courseLink: '.courseLink',
            mapName: '.mapName'
        },
        
        events: {
            'click .courseLink': 'courseSelected'
        },
        
        onRender: function(){
            var columns = _.size(this.model.get('custom_years'));
            
            var width = (columns + 2) * 250;
            this.ui.yearsDiv.css('width', width+'px');
            
            //create iScrolls
            this.afterShow();
        },
        
        courseSelected: function(e){
            e.preventDefault();
            var courseId = this.$el.find(e.currentTarget).data('id');
            var year = this.$el.find(e.currentTarget).closest('.yearColumn').data('id');
            this.trigger('CourseSelected', { year: year, course: courseId });
        },
        
        updateName: function(name){
            this.ui.mapName.html(name);
        }

	});
	
	return MapView;
	
});
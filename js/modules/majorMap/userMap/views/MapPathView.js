// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapPath.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapPathView = App.Views.ItemView.extend({
		template: template,
        className: 'mapPathConn',

        onShow: function() {
            var self = this;
            this.$el.find('#menu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 60,
                trigger: 'click',
                angle: {start: 120, end: 240}
            });
            this.$el.find('#menu_2').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 60,
                trigger: 'click',
                angle: {start: 120, end: 240}
            });
            this.$el.find('#menu_3').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 60,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('#menu_4').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 60,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('#menu_5').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 60,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });

            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                var currID = this.id.replace('img_', '');
                self.$el.find('#showBubbles_' + currID).trigger('click');
            });
        }

	});
	
	return MapPathView;
	
});
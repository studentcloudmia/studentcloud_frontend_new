// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapDetails.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapDtlsView = App.Views.ItemView.extend({
		template: template,
        className: 'mapDtlsModal'

	});
	
	return MapDtlsView;
	
});
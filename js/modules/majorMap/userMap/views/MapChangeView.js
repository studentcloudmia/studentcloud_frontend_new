// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapChange.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapChangeView = App.Views.ItemView.extend({
		template: template,
        className: 'mapChangeModal',
        
        events: {
            'click .btnSubmit': 'submit'
        },
        
        onRender: function(){
            //make droppable
            var self = this;
            this.$el.find('.mapDroppable').droppable({
                accept: ".searchRow",
                hoverClass: "ui-state-highlight",
                drop: function(event, ui) {
                    //call selected model with dragged model
                    self.trigger('MapDropped', ui.draggable.attr("data-description"));
                    // self.showNewMap(ui.draggable.attr("data-description"));
                }
            });
        },
        
        submit: function(){
            this.$el.find('.stepSub').addClass('active');
            this.$el.find('.changeBtns').toggleClass('nonvisible');
        },
        
        showNewMap: function(mapName){
            this.$el.find('.newMapName').html(mapName);
            this.$el.find('.newMap').removeClass('nonvisible');
        }

	});
	
	return MapChangeView;
	
});
// SideView
define([
    'marionette', 
    'app',
    'tpl!modules/majorMap/userMap/templates/sidebar.tmpl'
    ],function(Marionette, App, template){
	
        var SideView = App.Views.ItemView.extend({
            template: template
			
        });
	
        return SideView;
	
    });
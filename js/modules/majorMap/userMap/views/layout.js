define([
'marionette',
'app',
'tpl!modules/majorMap/userMap/templates/layout.tmpl',
'tpl!modules/majorMap/userMap/templates/pathsTemplate.tmpl'
],
function(Marionette, App, template, pathsTemplate) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,
        pathsTemplate: pathsTemplate,
        
        ui: {
            yearsDiv: '.yearColumnDivScroller'
        },
        
        events:{
            'click .headingButtons h3 > span': 'actSelected',
            'click .mapName': 'ShowPaths'
        },

        regions: {
            newMajor: ".newMajor",
            majorMapMain: ".majorMapMain"
        },
        
        onRender: function(){
                        
            var width = (4 + 2) * 250;
            this.ui.yearsDiv.css('width', width+'px');
            
            //create iScrolls
            this.afterShow();
        },
        
        actSelected: function(e){
            e.preventDefault();
            e.stopPropagation();
            var act = $(e.currentTarget).data('action');
            // console.log('action: ', act);
            this.trigger('action', act);
        },
        
        ShowPaths: function(e){
            e.preventDefault();
            e.stopPropagation();
            var self = this;
            if(!this.shown){
                //show all paths in popup
                var compiledTemplate = this.pathsTemplate();
    			this.popElem = $('.mapName');
                //create popover
                this.popElem.popover2({
                    id: 'pathsPopover',
                    position: 'right',
                    trigger: 'none',
                    content: compiledTemplate,
                    verticalOffset: 70
                });
            
                $('#fullPage').on('click', function(){
                    console.log('turn it off');
                    self.popElem.popover2('destroy');
                    $('#fullPage').off('click');
                    self.shown = false;
                });
                        
                this.popElem.popover2('show');
                this.shown = true;
            }else{
                self.popElem.popover2('destroy');
                $('#fullPage').off('click');
                this.shown = false;
            }
            
        }

    });
    return LayoutView;
});
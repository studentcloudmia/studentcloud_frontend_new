// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapNext.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapNextView = App.Views.ItemView.extend({
		template: template,
        className: 'mapNextModal',
        
        events:{
            'click .btnSalary': 'toggleSalary'
        },
        
        toggleSalary: function(){
            this.$el.find('.modalInner').toggleClass('animated fadeIn nonvisible');
        }

	});
	
	return MapNextView;
	
});
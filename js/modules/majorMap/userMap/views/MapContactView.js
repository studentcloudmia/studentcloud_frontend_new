// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapContact.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapContactView = App.Views.ItemView.extend({
		template: template,
        className: 'mapContactModal',
        
        onRender: function(){
            this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
        },
        
        events: {
            'click .btnSend': 'sendMessage'
        },
        
        sendMessage: function(){
            this.$el.find('form h4').html('<h4>Your message has been sent</h4>');
            this.$el.find('.mapContactInner').html('');
            this.$el.find('.btnSend').hide();
        }

	});
	
	return MapContactView;
	
});
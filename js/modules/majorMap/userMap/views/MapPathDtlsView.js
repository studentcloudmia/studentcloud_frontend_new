// user major map details
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/userMap/templates/mapPathDtls.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapPathDtlsView = App.Views.ItemView.extend({
		template: template,
        className: 'mapPathDtls'

	});
	
	return MapPathDtlsView;
	
});
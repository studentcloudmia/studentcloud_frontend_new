// major map userMap sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/majorMap/userMap/controllers/MainController'
    ],
function(Marionette, App, vent, MainController) {

    var UserMapModule = App.module('MajorMap.UserMapModule');

    UserMapModule.Controller = Marionette.Controller.extend({

        Layout: {},

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
            
			this.layout = App.request("MajorMap:getLayout");
            
            //close controller when layout closes
            this.listenTo(this.layout, 'close', function(){
                this.close();
            }, this);
            
            
            //listen to show event and start our controllers
            this.listenTo(this.layout, 'show', function(){
                this.showHeader();
                this.showUserMap();
            }, this);
            
            //show layout
            this.region.show(this.layout);
        },
        
        showHeader: function() {
            //start student header controller
            var options = {
                region: this.layout.header
            };

            vent.trigger('Components:Student:Header:on', options);
        },

        showUserMap: function() {
            this.mainController = new MainController({
                region: this.layout.mainContent
            });
            this.listenTo(this.mainController, 'close', function(){
                this.close();
            }, this);
        },

        onClose: function() {
			this.layout = null;
        }

    });

    return UserMapModule;
});
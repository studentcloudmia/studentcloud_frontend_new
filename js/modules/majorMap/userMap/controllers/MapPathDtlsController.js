//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapPathDtlsView'
],
function(Marionette, App, vent, MapPathDtlsView) {

    var MapPathDtlsController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapPathDtlsView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MapPathDtlsController;
});

//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapContactView'
],
function(Marionette, App, vent, MapContactView) {

    var MapContactController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapContactView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MapContactController;
});

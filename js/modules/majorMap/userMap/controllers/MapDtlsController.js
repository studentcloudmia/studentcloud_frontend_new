//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapDtlsView'
],
function(Marionette, App, vent, MapDtlsView) {

    var MapDtlsController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapDtlsView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MapDtlsController;
});

//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapChangeView'
],
function(Marionette, App, vent, MapChangeView) {

    var MapChangeController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapChangeView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.listenTo(this.view, 'MapDropped', this.MapDropped);
            
            //get map search controller
            this.mapSearchContr = App.request("MajorMap:getSearch");
            this.listenTo(this.mapSearchContr, 'MapSelected', this.MapSelected);
            vent.trigger('Sidebar:On', {
                controller: this.mapSearchContr
            });
            vent.trigger('Sidebar:Open');
            
            this.region.show(this.view);
        },
        
        MapDropped: function(mapName){
            this.MapSelected({mapName: mapName});
        },
        
        MapSelected: function(options){
            vent.trigger('Sidebar:Close');
            this.view.showNewMap(options.mapName);
        },
        
        onClose: function(){
            vent.trigger('Sidebar:Close');
            vent.trigger('Sidebar:On', {
                blank: true
            });
        }
        
    });

    return MapChangeController;
});

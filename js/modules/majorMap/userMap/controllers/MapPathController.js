//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapPathView'
],
function(Marionette, App, vent, MapPathView) {

    var MapPathController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapPathView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MapPathController;
});

//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapNextView'
],
function(Marionette, App, vent, MapNextView) {

    var MapNextController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MapNextView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MapNextController;
});

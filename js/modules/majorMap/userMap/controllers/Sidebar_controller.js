// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/SidebarView'
],
function(Marionette, App, vent, SidebarView, ClassificationCollectionList) {

    var SideController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new SidebarView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
        },

		show: function(options){
            this.region = options.region;
			//show view
			this.region.show(this.view);
		}

    });

    return SideController;
});
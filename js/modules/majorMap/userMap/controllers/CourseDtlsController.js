//Course Details controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/CourseDtlsView',
//entities
'entities/models/course/CourseModel'
],
function(Marionette, App, vent, CourseDtlsView, CourseModel) {

    var CourseDtlsController = Marionette.Controller.extend({

        initialize: function(options) {
            this.course = options.course;
            this.year = options.year;
            
			this.model = new CourseModel();
			this.view = new CourseDtlsView({model: this.model});
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
			this.listenTo(this.view, 'removeCourse', this.removeCourse);
			
            this.model.fetch({},{id: this.course},{},false);
            
			this.listenTo(this.model, 'change', function(){
                App.modals.show(this.view);
			}, this);
        },
        
        removeCourse: function(){
            this.trigger('removeCourse', {course: this.course, year: this.year});
        }
    });

    return CourseDtlsController;
});
//major map main controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/layout',
//cotrollers
'modules/majorMap/userMap/controllers/MapController',
'modules/majorMap/userMap/controllers/CourseDtlsController',
'modules/majorMap/userMap/controllers/Sidebar_controller',
'modules/majorMap/userMap/controllers/MapDtlsController',
'modules/majorMap/userMap/controllers/MapContactController',
'modules/majorMap/userMap/controllers/MapPathController',
'modules/majorMap/userMap/controllers/MapPathDtlsController',
'modules/majorMap/userMap/controllers/MapChangeController',
'modules/majorMap/userMap/controllers/MapNextController',
//entities
'entities/models/majorMap/UserMapModel'

],
function(Marionette, App, vent, LayoutView, 
    //controllers
    MapController, CourseDtlsController, SideController, MapDtlsController, MapContactController, MapPathController, MapPathDtlsController,
    MapChangeController, MapNextController,
    //entities
    UserMapModel) {

    var MainController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.model = new UserMapModel();
            
			this.layout = new LayoutView();
			
			this.listenTo(this.layout, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.layout, 'show', function(){
                // this.ShowMapDetails(); 
                //show empty sidebar
                vent.trigger('Sidebar:On', {blank: true});       
			}, this);
            
            this.listenTo(this.layout, 'action', this.actionSel);
			
			this.listenTo(this.model, 'change', function(){
    			//show view
                // this.region.show(this.layout);
			}, this);
            this.region.show(this.layout);
            //ensure we get fresh copy from server: forceRefresh
            // this.model.fetch({forceRefresh: true},{},{},false);
        },
        
        ShowMapDetails: function(){
            //start sidebar
            this.sideMapController = new SideController();
            this.listenToOnce(this.sideMapController, 'MapSelected', this.MapSelected);
            vent.trigger('Sidebar:On', {
                controller: this.sideMapController
            });            
            //hide sidebar
            vent.trigger('Sidebar:Close');
            
            this.mapController = new MapController({region: this.layout.majorMapMain, userMapModel: this.model});            
            this.listenTo(this.mapController, 'courseSelected', this.courseDetails);
        },
        
        courseDetails: function(vars){
            this.courseDtlsCont = new CourseDtlsController(vars);
        },
        
        actionSel: function(act){
            switch (act) {
            case "dtls":
                new MapDtlsController({region: App.modalsAlt1});
                break;
            case "next":
                new MapNextController({region: App.modalsAlt1});
                break;
            case "change":
                new MapChangeController({region: App.modalsAlt1});
                break;
            case "pathDtls":
                new MapPathDtlsController({region: App.modalsAlt3});
                break;
            case "connect":
                new MapPathController({region: App.modalsAlt3});
                break;
            case "msg":
                new MapContactController({region: App.modalsAlt1});
                break;
            default:
                
            }
        }
        
    });

    return MainController;
});
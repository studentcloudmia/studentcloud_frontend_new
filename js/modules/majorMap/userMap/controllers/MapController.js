//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/userMap/views/MapView',
//entities
'entities/models/majorMap/MajorMapModel',
'entities/collections/classification/ClassificationCollectionList'
],
function(Marionette, App, vent, MapView, MajorMapModel, ClassificationCollectionList) {

    var MapController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.userMapModel = options.userMapModel;
            
            this.mapId = this.userMapModel.get('major_maps')[0].id;
            
			this.model = new MajorMapModel({id: this.mapId});
            this.classColl = new ClassificationCollectionList();
            
			this.view = new MapView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.model, 'change', this.modelSelected);
			this.listenTo(this.view, 'CourseSelected', this.courseSelected);
			this.listenTo(this.classColl, 'reset', this.classificationReset);
            
            this.model.fetch({},{id: this.mapId},{},false);
        },
        
        modelSelected: function(){
            //build years JSON
            var courses = _.sortBy(this.model.get('courses'), function(course){ return course.year; });
            var coursesTaken = this.userMapModel.get('courses_taken');
            var foundCourses = false;
            
            //add custom years attr
            if(!this.model.has('custom_years') ){
                this.model.set({custom_years: {}}, {silent: true});
            }
            var years = this.model.get('custom_years');
            
            _.each(courses, function(course, index){
                
                course.passed = 0;
                _.each(coursesTaken, function(ct){
                    if(ct.id == course.course_catalog){
                        course.passed = ct.passed;
                    }
                });
                
                if(!years[course.year]){
                    years[course.year] = {};
                    years[course.year].id = course.year;
                    years[course.year].descr = course.year_descr;
                    years[course.year].courses = [];
                }
                
                if(!_.contains(years[course.year].courses, course)){
                    years[course.year].courses.push(course);
                }
                
                foundCourses = true;
            });
            
            if(foundCourses){
                this.model.set({custom_years: years}, {silent: true});
            }
            //set the view model
            this.view.model = this.model;
            if(this.view._isShown){
                //render view
                this.view.render();
            }else{
                //show view
                this.region.show(this.view);
            }
            
            //fetch classifications
            this.classColl.fetch({reset: true},{},{list: 'True'},false);
        },
        
        classificationReset: function(){
            var classif = '';
            var classModel = this.classColl.where({id: this.model.get('classif')})[0];
            if(classModel)
                classif = this.model.get('description') + ' - ' + classModel.get('Description_Short');
            
            //update header details
            this.view.updateName(classif);
        },
        
        courseSelected: function(vars){
            this.trigger('courseSelected', vars);
        }
        
    });

    return MapController;
});
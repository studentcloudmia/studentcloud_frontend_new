define([
'marionette',
'app',
],
function(Marionette, app) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'path/builder(/:id)': 'builder',
            'path/userMap': 'userMap'
        }
    });

    return Router;

});
// single course details view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/majorMap/builder/templates/courseDtls.tmpl'
],function(_, Marionette, vent, App, template){
	
	var CourseDtlsView = App.Views.ItemView.extend({
		template: template,
        
		ui:{
			modalAlert: '#MsgModal'
		},
		
		events:{
			'click .closeModal': 'closeModal',
            'click .removeBtn': 'removeCourse',
			'hidden #MsgModal': 'alertClosed'
		},
		
		onShow: function(){	
			var self = this;
			//show modal
            // this.ui.modalAlert.modal({backdrop: false});
			this.ui.modalAlert.modal('show');
		},
		
		alertClosed: function(){
            this.close();
		},
		
		closeModal: function(e) {
			e.preventDefault();
			this.ui.modalAlert.modal('hide');		
		},
        
        onClose: function(){
            //remove lingering backdrop div
            $('.modal-backdrop').remove();
        },
        
        removeCourse: function(e){
            e.preventDefault();
            e.stopPropagation();
            this.trigger('removeCourse');
            this.ui.modalAlert.modal('hide');
        }

	});
	
	return CourseDtlsView;
	
});
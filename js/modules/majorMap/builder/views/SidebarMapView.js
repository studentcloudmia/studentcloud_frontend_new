// SideView
define([
    'marionette', 
    'app',
	'modules/majorMap/builder/views/SidebarItemMapView',
    'tpl!modules/majorMap/builder/templates/sidebarMap.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var SideView = App.Views.CompositeView.extend({
            template: template,
			itemView: ItemView,
			itemViewContainer: '#maps',
			
            initialize: function(options) {
                //this.timesArr = options.timesArr;
                //this.listenTo(this.collection, 'reset', this.showResults);
                this.listenTo(this, 'childview:MapSelected', this.MapSelected);
            },
            
			events:{
				'submit #searchFields': 'search',
                'click .openSearch': 'showSearch',
                'change .colleges': 'collegeChanged',
                'change .departments': 'deptChanged'
			},
			
			ui:{
                searchResults: '#searchResults',
                searchFields: '#searchFields',
                colleges: '.colleges',
                departments: '.departments',
                subjects: '.subjects'
			},
            
            onRender: function(){
                this.showSearch();                
            },
            
            showSearch: function(){
                this.ui.searchResults.hide();
                this.ui.searchFields.show();
            },
            
            showResults: function(){
                this.ui.searchFields.hide();
                this.ui.searchResults.show();
                //refresh iscroll
                this.getScroller('mapScroller').refresh();
                
            },
            
            search: function(e){
                e.preventDefault();
                e.stopPropagation();
                this.trigger('formSubmitted', Backbone.Syphon.serialize(this) );
            },
            
            updateColl: function(JSON){
                //set colleges
                var html = '<option value=""> </option>';
            
                _.each(JSON, function(item){
                    html = html + '<option value="'+item.CollID+'">'+item.Description_Short+'</option>';
                });
                this.ui.colleges.html(html);
                //reset dept
                this.updateDept([]);
            },
            
            updateDept: function(JSON){
                //set departments
                var html = '<option value=""> </option>';
            
                _.each(JSON, function(item){
                    html = html + '<option value="'+item.DepID+'">'+item.Description_Short+'</option>';
                });
                this.ui.departments.html(html);
                //reset subj
                this.updateSubj([]);
            },
            
            updateSubj: function(JSON){
                //set subjects
                var html = '<option value=""> </option>';
            
                _.each(JSON, function(item){
                    html = html + '<option value="'+item.SubjectID+'">'+item.Description_Short+'</option>';
                });
                this.ui.subjects.html(html);
            },
            
            collegeChanged: function(){
                this.trigger('CollegeChanged', this.ui.colleges.val());
            },
            
            deptChanged: function(){
                this.trigger('DepartmentChanged', this.ui.departments.val());
            },
            
            MapSelected: function(view){
                this.trigger('MapSelected', view.model.id);
            }
        });
	
        return SideView;
	
    });
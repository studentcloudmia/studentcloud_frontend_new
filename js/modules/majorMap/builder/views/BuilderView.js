// major map builder
define([
	'underscore',
	'marionette',
	'vent',
	'app',
	'tpl!modules/majorMap/builder/templates/builder.tmpl'
],function(_, Marionette, vent, App, template){
	
	var BuilderView = App.Views.ItemView.extend({
		template: template

	});
	
	return BuilderView;
	
});
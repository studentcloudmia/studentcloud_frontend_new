// major map new
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/builder/templates/new.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var NewView = App.Views.ItemView.extend({
		template: template,
        tagName: 'div',
        className: 'transparentContainerPadded span7',

        events: {
            'submit form': 'formSubmit'
        },
        
        ui: {
            classifications: '.classifications',
            btn: '.submitBtn'
        },
        
        onRender: function() {
            //start jquery validate
            var self = this;
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit], .datepicker ").jqBootstrapValidation();
            });
            //start date picker
            self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            self.$el.find('.datepicker').val(moment().format('MM/DD/YYYY'));
        },
        
        setClassif: function(classJSON){
            //set classification
            var classHtml = '';
            
            _.each(classJSON, function(classif){
                classHtml = classHtml + '<option value="'+classif.id+'">'+classif.Description_Short+'</option>';
            });
            this.ui.classifications.html(classHtml);
        },
        
        triggerSubmit: function(){
            this.ui.btn.click();
        },
        
        formSubmit: function(e){
            e.preventDefault();
            this.trigger('SaveMap', Backbone.Syphon.serialize(this));
        }

	});
	
	return NewView;
	
});
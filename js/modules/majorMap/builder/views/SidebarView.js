// SideView
define([
    'marionette', 
    'app',
	'modules/majorMap/builder/views/SidebarItemView',
    'tpl!modules/majorMap/builder/templates/sidebar.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var SideView = App.Views.CompositeView.extend({
            template: template,
			itemView: ItemView,
			itemViewContainer: '#maps',
			
            initialize: function(options) {
                //this.timesArr = options.timesArr;
                //this.listenTo(this.collection, 'reset', this.showResults);
                this.listenTo(this, 'childview:MapSelected', this.MapSelected);
            },
            
			events:{
				'submit #searchFields': 'search',
                'submit #searchResults': 'showSearch'
			},
			
			ui:{
                searchResults: '#searchResults',
                searchFields: '#searchFields',
                classifications: '.classifications'
			},
            
            onRender: function(){
                this.showSearch();
            },
            
            showSearch: function(e){
                if(e){
                    e.preventDefault();
                }
                this.ui.searchResults.hide();
                this.ui.searchFields.show();
            },
            
            showResults: function(){
                this.ui.searchFields.hide();
                this.ui.searchResults.show();
            },
            
            search: function(e){
                e.preventDefault();
                e.stopPropagation();
                this.trigger('formSubmitted', Backbone.Syphon.serialize(this) );
            },
            
            updateClassif: function(classJSON){
                //set classification
                var classHtml = '';
            
                _.each(classJSON, function(classif){
                    classHtml = classHtml + '<option value="'+classif.id+'">'+classif.Description_Short+'</option>';
                });
                this.ui.classifications.html(classHtml);
            },
            
            MapSelected: function(view){
                this.trigger('MapSelected', {mapId: view.model.id, mapName: view.model.get('description')});
            }
        });
	
        return SideView;
	
    });
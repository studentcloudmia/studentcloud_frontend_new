// SideView
define([
    'marionette', 
    'app',
	'vent',
    'tpl!modules/majorMap/builder/templates/_sidebarMap.tmpl'
    ],function(Marionette, App, vent, template){

        var SideItemView = App.Views.ItemView.extend({
            template: template,
            tagName: 'div',
            className: 'fc-event fc-event-vert fc-event-start fc-event-end courseDraggable mapCourseSearch animated flipInX',
			
			onRender: function() {
                var self = this;
                //add class id to html elem
                this.$el.attr('data-id', this.model.id);
                this.$el.attr('data-title', this.model.get('title'));
                //this.$el.attr('data-model', JSON.stringify(this.model.toJSON()));
                // this.$el.draggable({ revert: "invalid",helper: "clone" });
    		    this.$el.draggable({
    		        revert: "invalid",
                    handle: "div.fc-event-title",
    		        helper: "clone",
    		        appendTo: 'body',
                    zIndex: 31, //1 greater than sidebar
    		    });
			}
            
        });

        return SideItemView;

    });
// major map new
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/majorMap/builder/templates/mapDetails.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MapView = App.Views.ItemView.extend({
		template: template,
        tagName: 'div',
        className: '',
        
        ui: {
            yearsDiv: '.yearColumnDiv .scroller',
            courseLink: '.courseLink',
            mapcoll: '#mapcoll',
            effDate: '.datepicker',
            leftArrow: '.leftArrow',
            rightArrow: '.rightArrow'
        },
        
        events: {
            'click .courseLink': 'courseSelected',
            'change #mapcoll': 'addColl',
            'click .yearColumnTitle': 'openSidebar'
        },
        
        triggers: {
            'click .leftArrow': 'leftNav',
            'click .rightArrow': 'rightNav'
        },
        
        onRender: function(){
            var columns = _.size(this.model.get('custom_years'));
            
            var width = (columns + 2) * 250;
            this.ui.yearsDiv.css('width', width+'px');

            //activate droppables
            this.activateDroppable();
            
            //create effdate
            this.ui.effDate.pickadate(GC.pickadateDefaults);
            
            //create iScrolls
            this.afterShow();
        },
        
        activateDroppable: function(){
            var self = this;
            _.each(this.$el.find('.yearColumn'), function(mapCol){
                var column = self.$el.find(mapCol);
                column.droppable({
                    greedy: true,
                    accept: ".courseDraggable",
                    hoverClass: "ui-state-highlight",
                    drop: function(event, ui) {
                        //call selected model with dragged model
                        //model json
                        var title = ui.draggable.attr('data-title');
                        var id = ui.draggable.attr('data-id');
                        self.trigger('CourseDropped', {course_catalog: id, course_title: title, year: column.attr('data-id'), year_descr: column.attr('data-descr')});
                    }
                });
            });
        },
        
        courseSelected: function(e){
            e.preventDefault();
            var courseId = this.$el.find(e.currentTarget).data('id');
            var year = this.$el.find(e.currentTarget).closest('.yearColumn').data('id');
            this.trigger('CourseSelected', { year: year, course: courseId });
        },
        
        addColl: function(){
            var selColl = this.ui.mapcoll.val();
            this.trigger('MapCollSelected', selColl);
        },
        
        updateMapColl: function(mapCollJSON){
            var html = '';
            if(mapCollJSON.length > 0){
                html = '<option value="">Select to add</option>';
                _.each(mapCollJSON, function(coll){
                    html = html + '<option value="'+coll.id+'">'+coll.MAJORMAP+'</option>';
                });
            }            
            this.ui.mapcoll.html(html);
        },
        
        getEffDate: function(){
            return this.ui.effDate.val();
        },
        
        openSidebar: function(e){
            e.preventDefault();
            e.stopPropagation();
            this.trigger('OpenSidebar');
        },
        
        /*Eff Date Navigation*/
        leftNav: function(bool){
            if(bool){
                this.ui.leftArrow.removeClass('nonvisible');
            }else{
                this.ui.leftArrow.addClass('nonvisible');
            }
        },
        rightNav: function(bool){
            if(bool){
                this.ui.rightArrow.removeClass('nonvisible');
            }else{
                this.ui.rightArrow.addClass('nonvisible');
            }
        },

	});
	
	return MapView;
	
});
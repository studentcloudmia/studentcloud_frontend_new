define([
'marionette',
'app',
'tpl!modules/majorMap/builder/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            newMajor: ".newMajor",
            majorMapMain: ".majorMapMain"
        }

    });
    return LayoutView;
});
// SideView
define([
    'marionette', 
    'app',
	'vent',
    'tpl!modules/majorMap/builder/templates/_sidebar.tmpl'
    ],function(Marionette, App, vent, template){

        var SideItemView = App.Views.ItemView.extend({
            template: template,
            tagName: 'tr',
            className: 'searchRow',
            
            triggers: {
                'click': 'MapSelected'
            },
            
            onRender: function(){
                //add data-id attribute to item
                this.$el.attr('data-id', this.model.id);
                this.$el.attr('data-description', this.model.get('description'));
                //make the item draggable
                this.$el.draggable({revert: "invalid", helper: "clone"});
            }
        });

        return SideItemView;

    });
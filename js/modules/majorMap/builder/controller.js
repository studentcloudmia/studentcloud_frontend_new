// major map builder sub module
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/majorMap/builder/controllers/MainController'
    ],
function(Marionette, App, vent, MainController) {

    var BuilderModule = App.module('MajorMap.BuilderModule');

    BuilderModule.Controller = Marionette.Controller.extend({

        Layout: {},

        initialize: function(options) {
            var self = this;
            
            //save region where we can show
            this.region = options.region;
            
			this.layout = App.request("MajorMap:getLayout");
            this.mapId = options.mapId;
            
            //close controller when layout closes
            this.listenTo(this.layout, 'close', function(){
                this.close();
            }, this);
            
            
            //listen to show event and start our controllers
            this.listenTo(this.layout, 'show', function(){
                this.showHeader();
                this.showFooter();
                this.showMajorMapBuilder();
            }, this);
            
            //show layout
            this.region.show(this.layout);
        },
        
        showHeader: function() {
            //start header controller
            var links = {};
            var heading = 'Major Map Builder';
            var options = {
                region: this.layout.header,
                heading: heading,
                links: links,
                compact: false
            };

            vent.trigger('Components:Builders:Header:on', options);
        },
        

        showFooter: function() {
            //start footer controller
            var options = {
                region: this.layout.footer
            };

            vent.trigger('Components:Builders:Footer:on', options);
        },

        showMajorMapBuilder: function() {
            this.mainController = new MainController({
                region: this.layout.mainContent,
                mapId: this.mapId
            });

            this.listenTo(this.mainController, 'close', function(){
                this.close();
            }, this);
        },

        onClose: function() {	
			this.layout = null;
        }

    });

    return BuilderModule;
});
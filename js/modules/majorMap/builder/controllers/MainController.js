//major map main controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/builder/views/layout',
//cotrollers
'modules/majorMap/builder/controllers/NewController',
'modules/majorMap/builder/controllers/MapController',
'modules/majorMap/builder/controllers/CourseDtlsController',
'modules/majorMap/builder/controllers/Sidebar_controller',
'modules/majorMap/builder/controllers/Sidebar_Map_controller',
//entities
'entities/collections/majorMap/MajorMapCollection',
'entities/collections/course/CourseCollection'

],
function(Marionette, App, vent, LayoutView, NewController, MapController, CourseDtlsController, SideController, SideMapController, MajorMapCollection, CourseCollection) {

    var MainController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			this.mapId = options.mapId;
            
            this.majorMapColl = new MajorMapCollection();
            this.courseColl = new CourseCollection();
            
			this.layout = new LayoutView();
			
			this.listenTo(this.layout, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.layout, 'show', function(){
                if(this.mapId){
                    this.ShowMapDetails({mapId: this.mapId});
                }else{
                    this.NewMap();
                }                
			}, this);

			
			//show view
			this.region.show(this.layout);
        },

        updateFooter: function(options) {
            vent.trigger('Components:Builders:Footer:updateButtons', options);
        },
        
        NewMap: function(){
            //start sidebar
            this.sidebarController = new SideController({collection: this.majorMapColl});
            this.listenToOnce(this.sidebarController, 'MapSelected', this.MapSelected);
            vent.trigger('Sidebar:On', {
                controller: this.sidebarController
            });
            
            App.navigate('path/builder');
            //reset major region
            this.layout.majorMapMain.reset();
            
            this.newController = new NewController({region: this.layout.newMajor});
            //show save button
            this.updateFooter({save: true});
			
			this.listenToOnce(this.newController, 'MapCreated', this.ShowMapDetails);
        },
        
        ShowMapDetails: function(vars){
            //start sidebar
            this.sideMapController = new SideMapController({collection: this.courseColl});
            this.listenToOnce(this.sideMapController, 'MapSelected', this.MapSelected);
            vent.trigger('Sidebar:On', {
                controller: this.sideMapController
            });
            
            App.navigate('path/builder/'+vars.mapId);
            //reset new major region
            this.layout.newMajor.reset();
            //hide sidebar
            vent.trigger('Sidebar:Close');
            
            this.mapController = new MapController({region: this.layout.majorMapMain, mapId: vars.mapId});
            
            this.listenTo(this.mapController, 'AddMap', this.NewMap);
            this.listenTo(this.mapController, 'courseSelected', this.courseDetails);
          
            //show save and new buttons
            this.updateFooter({ add: true, save: true, del: true });
        },
        
        courseDetails: function(vars){
            this.courseDtlsCont = new CourseDtlsController(vars);
            this.listenToOnce(this.courseDtlsCont, 'removeCourse', this.removeCourse);
        },
        
        removeCourse: function(vars){
            this.mapController.removeCourse(vars);
        },
        
        MapSelected: function(options){
            this.ShowMapDetails({mapId: options.mapId})
        }
        
    });

    return MainController;
});
//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/builder/views/NewView',
//entities
'entities/models/majorMap/MajorMapModel',
'entities/collections/classification/ClassificationCollectionList'

],
function(Marionette, App, vent, NewView, MajorMapModel, ClassificationCollectionList) {

    var NewController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
			this.model = new MajorMapModel();
            this.classColl = new ClassificationCollectionList();
			this.view = new NewView();
            
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.view, 'SaveMap', this.saveMajorMap);
			
			this.listenTo(this.model, 'created', this.mapCreated);
                        
			this.listenTo(this.classColl, 'reset', this.classificationReset);
            this.listenTo(vent, 'Components:Builders:Footer:save', this.save);
            
            this.classColl.fetch({reset: true},{},{list: 'True'},false);
			
			//show view
			this.region.show(this.view);
        },
        
        saveMajorMap: function(variables){
            this.model.clear();
            variables.courses = [];
            this.model.set(variables, {silent: true});
            this.model.save();
        },
        
        classificationReset: function(){
            this.view.setClassif(this.classColl.toJSON());
        },
        
        save: function(){
            this.view.triggerSubmit();
        },
        
        mapCreated: function(){
            this.trigger('MapCreated', { mapId: this.model.id});
        }
        
    });

    return NewController;
});
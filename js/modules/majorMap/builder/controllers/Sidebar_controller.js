// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/builder/views/SidebarView',
'entities/collections/classification/ClassificationCollectionList'
],
function(Marionette, App, vent, SidebarView, ClassificationCollectionList) {

    var SideController = Marionette.Controller.extend({

        initialize: function(options) {
            this.collection = options.collection;
            this.classifColl = new ClassificationCollectionList();
			
			this.view = new SidebarView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.view, 'formSubmitted', this.formSubmitted);
			this.listenTo(this.view, 'MapSelected', this.MapSelected);
            
            
			this.listenTo(this.classifColl, 'reset', this.showView);
			this.listenTo(this.collection, 'reset', this.updateResults, this);
        },

		show: function(options){
            //save region where we can show
            this.region = options.region;
            //fetch classifications
            this.classifColl.fetch({reset: true},{},{list: 'True'},false);
		},

		showView: function(){
			//show view
			this.region.show(this.view);
            //update classification drop down
            this.view.updateClassif(this.classifColl.toJSON());
		},
        
        formSubmitted: function(vars){
            if(vars.description.length > 0){
                //do fetch
                this.collection.fetch({ reset: true }, { }, vars, false);
            }       
        },
        
        updateResults: function(){
            this.view.showResults();
        },
        
        MapSelected: function(options){
            this.trigger('MapSelected', options);
        }

    });

    return SideController;
});
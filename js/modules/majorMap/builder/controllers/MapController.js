//major map new controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/builder/views/MapView',
//entities
'entities/collections/majorMap/MajorMapCollection',
'entities/models/majorMap/MajorMapModel',
'entities/models/majorMap/CourseMapModel',
'entities/collections/classification/ClassificationCollectionList',
'entities/collections/dictionary/DictionaryCollection'

],
function(Marionette, App, vent, MapView, MajorMapCollection, MajorMapModel, CourseMapModel, ClassificationCollectionList, DictionaryCollection) {

    var MapController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;
            this.mapId = options.mapId;
            
            this.collection = new MajorMapCollection();
            
			this.model = new MajorMapModel({id: this.mapId});
            this.courseModel = new CourseMapModel();
            this.classColl = new ClassificationCollectionList();
            //map columns collection
            this.mapCollColl = new DictionaryCollection();
			this.view = new MapView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.view, 'CourseSelected', this.courseSelected);
			this.listenTo(this.view, 'MapCollSelected', this.mapCollSelected);
			this.listenTo(this.view, 'CourseDropped', this.CourseDropped);
			this.listenTo(this.view, 'OpenSidebar', this.OpenSidebar);
			this.listenTo(this.view, 'leftNav', this.prevModel);
			this.listenTo(this.view, 'rightNav', this.nextModel);
            
			this.listenTo(this.collection, 'reset', this.collectionReset);
			this.listenTo(this.courseModel, 'created', this.coursesSaved);
			this.listenTo(this.classColl, 'reset', this.classificationReset);
			this.listenTo(this.mapCollColl, 'reset', this.mapCollReset);
            
            this.listenTo(vent, 'Components:Builders:Footer:save', this.save);
            this.listenTo(vent, 'Components:Builders:Footer:add', this.add);
            this.listenTo(vent, 'Components:Builders:Footer:del', this.del);
            
            this.collection.fetch({reset: true},{id: this.mapId},{historical: 'True'},false);
        },
        
        saveMajorMap: function(courses){
            this.courseModel.clear();
            this.courseModel.set({courses: courses}, {silent: true});
            this.courseModel.save();
        },
        
        coursesSaved: function(){
            var descr = this.model.get('description');
            //courses were saved. Now fetch major map again
            this.collection.fetch({reset: true},{id: this.mapId},{historical: 'True'},false);
            //alert user of successful save
            vent.trigger('Components:Alert:Alert', {
                heading: 'Successful Save',
                message: 'Major Map <strong>' + descr + '</strong> has been successfully updated'
            });
        },
        
        collectionReset: function(){
            //select current model
            // this.model = this.collection.where({status: 'CURRENT'})[0];
            this.model = this.collection.models[0];
            
            this.modelSelected();
        },
        
        modelSelected: function(){
            //build years JSON
            var courses = _.sortBy(this.model.get('courses'), function(course){ return course.year; });
            var foundCourses = false;
            
            //add custom years attr
            if(!this.model.has('custom_years') ){
                this.model.set({custom_years: {}}, {silent: true});
            }
            var years = this.model.get('custom_years');
            
            _.each(courses, function(course, index){
                if(!years[course.year]){
                    years[course.year] = {};
                    years[course.year].id = course.year;
                    years[course.year].descr = course.year_descr;
                    years[course.year].courses = [];
                }
                
                if(!_.contains(years[course.year].courses, course)){
                    years[course.year].courses.push(course);
                }
                
                foundCourses = true;
            });
            
            if(foundCourses){
                this.model.set({custom_years: years}, {silent: true});
            }            
            //set the view model
            this.view.model = this.model;
            if(this.view._isShown){
                //render view
                this.view.render();
            }else{
                //show view
                this.region.show(this.view);
            }
            
            //upate eff date navigation arrows
            this.updateNav();
            
            //fetch classifications
            this.classColl.fetch({reset: true},{},{list: 'True'},false);
            //fetch map columns
            this.mapCollColl.getMapColl();
        },
        
        updateNav: function(){
            //eff date navigation
            this.view.leftNav(this.collection.hasPrev(this.model));
            this.view.rightNav(this.collection.hasNext(this.model));
        },
        
        classificationReset: function(){
            var classif = '';
            var classModel = this.classColl.where({id: this.model.get('classif')})[0];
            if(classModel)
                classif = ' - ' + classModel.get('Description_Short');
            //update header details
            vent.trigger('Components:Builders:Header:changeDetails', {heading: this.model.get('description') + classif});
        },
        
        mapCollReset: function(){
            //copy of current map columns in model
            var mapColumnsSet = _.map(this.model.get('custom_years'), function(num, key){ return parseInt(key); });
            //copy of map columns from server
            var mapColumnsNew = this.mapCollColl.toJSON();
            //remove columns that are already in the model
            var mapColumnsToRemain = _.compact(
                _.map(mapColumnsNew, function(coll, key){ 
                    if(!_.contains(mapColumnsSet, coll.id)){
                        //only return column if it is not in the model
                        return coll;
                    }
                })
            );
            //update column list in view
            this.view.updateMapColl(mapColumnsToRemain);
        },
        
        mapCollSelected: function(selColl){
            //get column selected from collection
            var mapCollModel = this.mapCollColl.where({id: parseInt(selColl)})[0];
            //add column to model
            var years = this.model.get('custom_years');
            //create year attr
            years[mapCollModel.id] = {};
            years[mapCollModel.id].id = mapCollModel.id;
            years[mapCollModel.id].descr = mapCollModel.get('MAJORMAP');
            years[mapCollModel.id].courses = [];
            
            //update years attr in model
            this.model.set({custom_years: years});
            this.modelSelected();
        },
        
        CourseDropped: function(vars){
            //add course to model
            var courses = this.model.get('courses');
            //defaults
            vars.custom_stat = 'New';
            vars.major_map = this.model.id;
            vars.critical = false;
            vars.notes = 'Default Note';
            vars.grade_required = 'C';
            vars.position = 1;
            
            courses.push(vars);

            var courses = this.model.set({courses: courses});
            this.modelSelected()
        },
        
        OpenSidebar: function(){
            vent.trigger('Sidebar:Open');
        },
        
        courseSelected: function(vars){
            this.trigger('courseSelected', vars);
        },
        
        removeCourse: function(vars){
            //loop through all courses and remove the one matching the course and year
            var courses = this.model.get('courses');
            courses = _.compact(
                _.map(courses, function(course, key){ 
                    if(course.course_catalog == vars.course && course.year == vars.year){
                        //do not return this course
                    }else{
                        return course;
                    }           
                })
            );
            //set new courses and unset custom_years
            this.model.set({courses: courses, custom_years: {}}, {silent: true});
            this.modelSelected();
        },
        
        save: function(){
            //get eff date from view
            var effDate = this.view.getEffDate();
            //get models old Eff date
            if(effDate == this.model.get('eff_date')){
                //eff date not changed. Alert user
                vent.trigger('Components:Alert:Confirm', {
                    heading: 'Effective Date Not Changed',
                    message: 'You have not changed the effective date for ' + this.model.get('description'),
                    cancelText: 'Edit',
                    confirmText: 'Save',
                    callee: this,
                    callback: this.effDateNotChanged
                });
            }else{
                //get list of new courses and send to server
                this.effDateChanged();
            }
        },
        
        effDateChanged: function(){
            //get old attributes
            var attributes = this.model.toJSON();
            //create new major map with new effdt
            this.model.clear();
            //save and format eff_date to correct format            
            attributes.eff_date = this.view.getEffDate();
            // attributes.eff_date = moment(this.view.getEffDate(),'MM/DD/YYYY').format('YYYY-MM-DD');
            delete attributes.id;
            this.model.set(attributes);
            this.listenToOnce(this.model, 'created', this.coursesSaved);
            this.model.save();
        },
        
        effDateNotChanged: function(){
            var newCourses = this.buildCourses();
            this.saveMajorMap(newCourses);
        },
        
        buildCourses: function(){
            var newCourses = _.compact(
                _.map(this.model.get('courses'), function(course, key){
                    if(course.custom_stat == 'New'){
                        return course;
                    }
                })
            );
            return newCourses;
        },
        
        add: function(){
            this.trigger('AddMap');
        },
        
        del: function(){
            //ensure user wants to delete map
            vent.trigger('Components:Alert:Confirm', {
                heading: 'Delete Map?',
                message: 'Are you sure you want to delete map ' + this.model.get('description') + ' with Effective Date of ' + this.model.get('eff_date'),
                cancelText: 'Cancel',
                confirmText: 'Delete',
                callee: this,
                callback: this.confirmDel
            });
        },
        
        confirmDel: function(){
            var self = this;
            var id = this.model.id;
            var descr = this.model.get('description');
            var eff_date = this.model.get('eff_date');

            this.listenTo(this.model, 'destroyed', function(e){ 
                /*
                    NOTE: need to get an id from teh collection that is not the id we just removed
                    this is needed so we do not go back to the server to ask for an id that is now deleted
                */
                self.mapId = self.getMapIdFromColl(id);
                if(!self.mapId){
                    //no more maps left. return to create
                    self.add();
                }
                self.collection.fetch({reset: true},{id: self.mapId},{historical: 'True'},false);
                //alert user of successful save
                vent.trigger('Components:Alert:Alert', {
                    heading: 'Successful Delete',
                    message: 'Major Map <strong>' + descr + '</strong> with Effective Date '+ eff_date +' has been successfully deleted'
                });
            });
            
            this.model.destroy();

        },
        
        getMapIdFromColl: function(idRem){
            var idToRet = false;
            //get any id from collection that is not equal to the id just removed
            _.each(this.collection.models, function(model){
                if(model.id != idRem){
                    idToRet = model.id;
                }
            });
            return idToRet;
        },
        
        prevModel: function(){
            this.model = this.collection.prevModel(this.model);
            this.modelSelected();
        },
        
        nextModel: function(){
            this.model = this.collection.nextModel(this.model);
            this.modelSelected();
        }
        
    });

    return MapController;
});
// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/majorMap/builder/views/SidebarMapView',
'entities/collections/college/CollegeCollectionList',
'entities/collections/department/DepartmentCollectionList',
'entities/collections/subject/SubjectsCollectionList'
],
function(Marionette, App, vent, SidebarView, CollegeCollectionList, DepartmentCollectionList, SubjectsCollectionList) {

    var SideController = Marionette.Controller.extend({

        initialize: function(options) {
            this.collection = options.collection;
            
            this.collColl = new CollegeCollectionList();
            this.deptColl = new DepartmentCollectionList();
            this.subjColl = new SubjectsCollectionList();
			
			this.view = new SidebarView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			this.listenTo(this.view, 'formSubmitted', this.formSubmitted);
			this.listenTo(this.view, 'MapSelected', this.MapSelected);
            
			this.listenTo(this.view, 'CollegeChanged', this.CollegeChanged);
			this.listenTo(this.view, 'DepartmentChanged', this.DepartmentChanged);
            
			this.listenTo(this.collColl, 'reset', this.showView);
			this.listenTo(this.deptColl, 'reset', this.resetDept);
			this.listenTo(this.subjColl, 'reset', this.resetSubj);
            
			this.listenTo(this.collection, 'reset', this.updateResults, this);
        },

		show: function(options){
            //save region where we can show
            this.region = options.region;
            //fetch classifications
            this.collColl.fetch({reset: true},{},{list: 'True', inst: 'FIU'},false);
            
            //this.showView();
		},

		showView: function(){
			//show view
			this.region.show(this.view);
            //update department drop down
            this.view.updateColl(this.collColl.toJSON());
		},
        
        resetDept: function(){
            //update department drop down
            this.view.updateDept(this.deptColl.toJSON());            
        },
        
        resetSubj: function(){
            //update subject drop down
            this.view.updateSubj(this.subjColl.toJSON());            
        },
        
        formSubmitted: function(vars){
            //only keep completed vars
            _.each(vars, function(v, i){
                if(_.isNull(v) || _.isUndefined(v) || v.length == 0 ){
                    delete vars[i];
                }                    
            });
            
            if(_.size(vars)  > 0){
                //add custom variable
                vars.custom = 'True';
                //do fetch
                this.collection.fetch({ reset: true }, { }, vars, false);
                //this.collection.fetch({ reset: true }, { }, {search: 'b'}, false);
            }       
        },
        
        updateResults: function(){
            this.view.showResults();
        },
        
        MapSelected: function(mapId){
            this.trigger('MapSelected', mapId);
        },
        
        CollegeChanged: function(collID){
            //trigger dept search
            this.deptColl.fetch({reset: true},{},{list: 'True', inst: 'FIU', parent: collID},false);
        },
        
        DepartmentChanged: function(deptID){
            //trigger subj search
            this.subjColl.fetch({reset: true},{},{list: 'True', inst: 'FIU', parent: deptID},false);
        }

    });

    return SideController;
});
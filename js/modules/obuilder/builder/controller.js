// obuilder catalog
define(['marionette', 'app', 'vent',
//layout
    'modules/obuilder/builder/views/layout',
//controllers
    'modules/obuilder/builder/controllers/Sidebar_controller',
    'modules/obuilder/builder/controllers/BuilderController'
],
        function(Marionette, App, vent, LayoutView, SideController, BuilderController) {

            var BuilderModule = App.module('Obuilder.Builder');

            BuilderModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.layout = new LayoutView();
                    //save region where we can show
                    this.region = options.region;
                    //start sidebar
                    this.sidebarController = new SideController();
                    vent.trigger('Sidebar:On', {
                        controller: this.sidebarController
                    });
                    //listen to layout show
                    this.listenTo(this.layout, 'show', function() {
                        this.showBuilder();
                    }, this);
                    //listen to layout show
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);
                    //listen to overlay close and rebind footer events
                    this.listenTo(vent, 'Region:Overlay:Closed', this.overlayClosed, this);
                    //show layout
                    this.region.show(this.layout);
                    this.bindFooterEvents(true);
                },
                showBuilder: function() {
                    this.builderController = new BuilderController({region: this.layout.builder});
                    this.setupBuilderListeners();
                    this.activateDroppable();
                },
                setupBuilderListeners: function() {
                    this.listenTo(this.builderController, 'PanelSelected', this.PanelSelected, this);
                    this.listenTo(this.builderController, 'ValueSelected', this.ValueSelected, this);
                    this.listenTo(this.builderController, 'Error', this.ShowError, this);
                    this.listenTo(this.builderController, 'modelChanged', this.modelChanged, this);
                    this.listenTo(this.builderController, 'hierarchy', this.showHierarchy, this);
                    this.listenTo(this.builderController, 'changeEffDate', this.changeEffDate, this);
                    this.listenTo(this.builderController, 'modelCreated', function() {
                        this.trigger('modelCreated');
                    }, this);
                    this.listenTo(this.builderController, 'viewRendered', function(model) {
                        //show delete button
                        if (model.get('Trees') == '')
                            this.updateFooter({save: true, del: false});
                        else
                            this.updateFooter({save: true, del: true});
                    }, this);
                    this.listenTo(this.builderController, 'modelUpdated', function() {
                        this.trigger('modelUpdated');
                    }, this);
                    this.listenTo(this.builderController, 'modelDestroyed', function() {
                        this.trigger('modelDestroyed');
                    }, this);
                },
                PanelSelected: function(panel) {
                    //save reference to panel selected
                    this.panelSel = panel;
                    //request sidebar
                    this.panelSideController = {};
                    switch (panel) {
                        case 'Institution':
                            this.panelSideController = App.request("Fbuilder:Institution:SideController");
                            this.changeSidebar();
                            break;
                        case 'College':
                            this.panelSideController = App.request("Fbuilder:College:SideController");
                            this.changeSidebar();
                            break;
                        case 'Department':
                            this.panelSideController = App.request("Fbuilder:Department:SideController");
                            this.changeSidebar();
                            break;
                        case 'Subject':
                            this.panelSideController = App.request("Sbuilder:Subject:SideController");
                            this.changeSidebar();
                            break;
                    }
                },
                ValueSelected: function(options) {
                    if (options.action == 'edit') {
                        this.editController = false;
                        options.region = App.overlays;
                        options.widg = true;
                        options.modelId = options.Value;
                        //show controller in layout.edit
                        switch (options.panel) {
                            case 'Institution':
                                this.editController = App.request("Fbuilder:Institution:MainController", options);
                                break;
                            case 'College':
                                this.editController = App.request("Fbuilder:College:MainController", options);
                                break;
                            case 'Department':
                                this.editController = App.request("Fbuilder:Department:MainController", options);
                                break;
                            case 'Subject':
                                this.editController = App.request("Sbuilder:Subject:MainController", options);
                                break;
                        }
                        //stop listening to footer events until overlay gets closed
                        this.bindFooterEvents(false);
                    } else if (options.action == 'remove') {
                        this.optionsRemove = options;
                        vent.trigger('Components:Alert:Confirm', {
                            heading: 'Remove Element',
                            message: 'Are you sure you would like to remove ' + options.panel + ' ' + options.Value + '?',
                            cancelText: 'Cancel',
                            confirmText: 'Confirm',
                            callee: this,
                            callback: this.confirmedRemoveElem
                        });
                        //this.builderController.removeElem(options.Value, options.panel);
                    }

                },
                confirmedRemoveElem: function() {
                    if (this.optionsRemove) {
                        this.builderController.removeElem(this.optionsRemove.Value, this.optionsRemove.panel);
                        this.optionsRemove = false;
                    }
                },
                ShowError: function(msg) {
                    //close sidebar
                    vent.trigger('Sidebar:Close');
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Error',
                        message: msg
                    });
                },
                modelChanged: function() {
                    this.updateFooter({save: true, del: true});
                },
                save: function() {
                    this.builderController.save();
                },
                changeEffDate: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Effective Date Not Changed',
                        message: 'You have not changed the effective date for this organization',
                        cancelText: 'Edit',
                        confirmText: 'Save',
                        callee: this,
                        callback: this.confirmedSave
                    });
                },
                confirmedSave: function() {
                    //force save
                    this.builderController.forceSave();
                },
                delete: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Delete?',
                        message: 'Are you sure you want to delete the entire Organization Tree?',
                        cancelText: 'Cancel',
                        confirmText: 'Confirm',
                        callee: this,
                        callback: this.confirmedDel
                    });
                },
                confirmedDel: function() {
                    this.builderController.delete();
                },
                bindFooterEvents: function(listenBool) {
                    if (listenBool) {
                        //listen to events
                        this.listenTo(vent, 'Components:Builders:Footer:save', this.save, this);
                        this.listenTo(vent, 'Components:Builders:Footer:del', this.delete, this);
                    } else {
                        //stop listening
                        this.stopListening(vent, 'Components:Builders:Footer:save');
                        this.stopListening(vent, 'Components:Builders:Footer:del');
                    }
                },
                overlayClosed: function() {
                    this.trigger('showFooter');
                    this.bindFooterEvents(true);
                    //return footer to its previous state
                    if (this.prevFooterOptions) {
                        this.updateFooter(this.prevFooterOptions);
                    }
                },
                changeSidebar: function() {
                    vent.trigger('Sidebar:Change', {
                        controller: this.panelSideController
                    });
                    vent.trigger('Sidebar:Open');
                },
                droppedEl: function(options) {
                    //add element to corresponding panel
                    this.builderController.addElem(options, this.panelSel);
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.builder.el).droppable({
                        accept: ".searchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.droppedEl({elemId: ui.draggable.attr("data-elemid"), modelId: ui.draggable.attr("data-id")});
                        }
                    });
                },
                updateFooter: function(options) {
                    this.prevFooterOptions = options;
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                showHierarchy: function() {
                    vent.trigger('Obuilder:hierarchy');
                },
                onClose: function() {
                    //turn off mouseup and touch end in curent region only
                    App.main.$el.off('mouseup touchend');
                }


            });

            return BuilderModule;
        });

// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/obuilder/builder/views/SidebarItemView'
],
function(Marionette, App, vent, SidebarView) {

    var SideController = Marionette.Controller.extend({

        initialize: function(options) {
			
			this.view = new SidebarView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
        },

		show: function(options){
            //save region where we can show
            this.region = options.region;
			//show view
			this.region.show(this.view);
		}

    });

    return SideController;
});
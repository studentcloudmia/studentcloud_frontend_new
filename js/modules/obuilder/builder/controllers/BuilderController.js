// obuilder catalog
define(['underscore', 'marionette', 'app', 'vent',
//view
    'modules/obuilder/builder/views/BuilderView',
//entities
    'entities/collections/oBuilder/OBuilderCollection', 'entities/models/oBuilder/OBuilderModel',
//plugins
    'moment'],
        function(_, Marionette, App, vent, BuilderView, OBuilderCollection, OBuilderModel) {

            BuilderController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.indexOfMod = false;
                    this.model = new OBuilderModel();
                    this.collection = new OBuilderCollection();
                    this.view = new BuilderView({
                        model: this.model
                    });

                    this.initViewListeners();
                    //save region where we can show
                    this.region = options.region;


                    this.listenTo(this.model, 'sync', function() {
                        //only run if model not _destroy
                        //   if (!this.model.get('_destroy')) {
                        //ensure we have trees
                        if (!this.model.get('Trees')) {
                            this.model.set('Trees', [], {
                                silent: true
                            });
                        }
                        this.view.model = this.model;
                        if (this.view._isShown) {
                            //render view
                            this.view.render();
                        } else {
                            //show view
                            this.region.show(this.view);
                        }
                        //      }
                        this.setArrows();

                    }, this);

                    //close when the view closes
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.collection, 'reset', function() {
                        //define model options
                        var options = {};
                        if (this.collection.length > 0) {
                            this.model.id = this.collection.where({Status: 'CURRENT'})[0].id;
                            //fetch model
                            this.model.fetch({}, {}, {}, false);
                        }
                    }, this);

                    //search for historical obuilder data
                    this.fetchHistorcal();
                },
                initViewListeners: function() {
                    this.listenTo(this.view, 'render', function(e) {
                        this.trigger('viewRendered', this.model);
                    }, this);
                    this.listenTo(this.view, 'add', this.add, this);
                    //this.listenTo(this.view, 'render', this.viewRendered, this);
                    this.listenTo(this.view, 'hierarchy', this.hierarchy, this);
                    this.listenTo(this.view, 'PanelSelected', this.PanelSelected, this);
                    this.listenTo(this.view, 'levelValueSelected', this.levelValueSelected, this);
                    this.listenTo(this.view, 'prev', this.getPrevModel, this);
                    this.listenTo(this.view, 'next', this.getNextModel, this);
                    this.listenTo(this.model, 'created', function() {
                        //refetch collection for updated trees
                        this.fetchHistorcal();
                        this.trigger('modelCreated');
                    }, this);
                    this.listenTo(this.model, 'updated', function() {
                        //refetch collection for updated trees
                        this.fetchHistorcal();
                        this.trigger('modelUpdated');
                    }, this);
                    this.listenTo(this.model, 'destroyed', function() {
                        //refetch collection for updated trees
                        this.fetchHistorcal();
                        this.trigger('modelDestroyed');
                    }, this);
                },
                fetchHistorcal: function() {
                    this.collection.fetch({
                        reset: true
                    }, {}, {
                        historical: 'True'
                    }, false);
                },
                addElem: function(options, panel) {
                    var elId = options.elemId;
                    var serverId = options.modelId;

                    //add elem to model
                    var level = this.getLevel(panel);
                    var oldValues = this.view.returnSelValues();
                    var parents = _.initial(oldValues, 4 - level);
                    var indexFound = _.indexOf(oldValues, 'temp');
                    //ensure correct level is being inserted into
                    if (level > 0 && indexFound > 0 && level > indexFound) {
                        this.trigger('Error', 'Cannot have empty parent...');
                        return false;
                    }
                    //find out where element should be placed
                    var currModel = this.model.toJSON();
                    //add institution if no parents
                    if (parents.length == 0) {
                        currModel.Trees.push({
                            model_id: elId,
                            id: serverId,
                            Colleges: []
                        });
                    } else {
                        //navigate tree to insert element
                        var foundInst = false;
                        _.each(currModel.Trees, function(inst) {
                            if (inst.id == parents[0]) {
                                //insert college if working with level 1
                                if (level == 1) {
                                    if (!inst.Colleges)
                                        inst.Colleges = [];
                                    inst.Colleges.push({
                                        CollegeID: elId,
                                        id: serverId,
                                        Departments: []
                                    });
                                    return true;
                                } else {
                                    //continue to department
                                    _.each(inst.Colleges, function(coll) {
                                        if (coll.id == parents[1]) {
                                            //insert department if working with level 2
                                            if (level == 2) {
                                                if (!coll.Departments)
                                                    coll.Departments = [];
                                                coll.Departments.push({
                                                    DepartmentID: elId,
                                                    id: serverId,
                                                    Subjects: []
                                                });
                                                return true;
                                            } else {
                                                //continue to subjects
                                                _.each(coll.Departments, function(dept) {
                                                    if (dept.id == parents[2]) {
                                                        //insert department if working with level 3
                                                        if (level == 3) {
                                                            if (!dept.Subjects)
                                                                dept.Subjects = [];
                                                            dept.Subjects.push({
                                                                SubjectID: elId,
                                                                id: serverId,
                                                            });
                                                            return true;
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                    this.model.set(currModel, {
                        silent: true
                    });
                    this.view.render();
                    this.setArrows();
                    //create values array
                    if (level > -1) {
                        oldValues = _.first(oldValues, level);
                        oldValues.push(serverId);
                        //fill with temp
                        for (i = level; i < 3; i++) {
                            oldValues.push('temp');
                        }
                    }

                    //trigger model changed event
                    this.trigger('modelChanged');
                    this.view.setValues(oldValues);
                },
                removeElem: function(options, panel) {
                    var elId = options.elemId;
                    var serverId = options.modelId;
                    //remove elem from model
                    var level = this.getLevel(panel);
                    var oldValues = this.view.returnSelValues();
                    var parents = _.initial(oldValues, 3 - level);
                    var indexFound = _.indexOf(oldValues, 'temp');
                    var cont = true;
                    //ensure correct level is being removed from
                    if (level > 0 && indexFound > 0 && level > indexFound) {
                        this.trigger('Error', 'Cannot have empty parent...');
                        return false;
                    }
                    //find out where element should be removed from
                    var currModel = this.model.toJSON();
                    //do not remove institution
                    if (parents.length == 0) {
                        this.trigger('Error', 'Cannot remove Institution');
                        return false;
                        //currModel.Trees.push({model_id: elId, Colleges:[]});
                    } else {
                        //navigate tree to remove element
                        var foundInst = false;
                        _.each(currModel.Trees, function(inst) {
                            if (cont && inst.id == parents[0]) {
                                //continue to college
                                _.each(inst.Colleges, function(coll, collI) {
                                    if (cont && coll.id == parents[1]) {
                                        //remove college if working with level 1
                                        if (level == 1) {
                                            inst.Colleges.splice(collI, 1);
                                            cont = false;
                                            return true;
                                        } else {
                                            //continue to departments
                                            _.each(coll.Departments, function(dept, deptI) {
                                                if (cont && dept.id == parents[2]) {
                                                    //insert department if working with level 2
                                                    if (level == 2) {
                                                        coll.Departments.splice(deptI, 1);
                                                        cont = false;
                                                        return true;
                                                    } else {
                                                        //continue to subjects
                                                        _.each(dept.Subjects, function(subj, subjI) {
                                                            if (cont && subj.id == parents[3]) {
                                                                if (level == 3) {
                                                                    dept.Subjects.splice(subjI, 1);
                                                                    cont = false;
                                                                    return true;
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }

                    //update model and render view
                    this.model.set(currModel, {
                        silent: true
                    });
                    this.view.render();
                    this.setArrows();
                    //trigger model changed event
                    this.trigger('modelChanged');
                    //this.view.setValues(oldValues);
                },
                levelValueSelected: function(options) {
                    //get value selected
                    var level = this.getLevel(options.value);
                    var valSel = this.view.returnSelValues()[level];

                    if (valSel !== 'temp') {
                        this.trigger('ValueSelected', {
                            Value: valSel,
                            panel: options.value,
                            action: options.action,
                            item_id: options.item_id
                        });
                    } else {
                        //open sidebar for empty
                        this.trigger('ValueSelected', {
                            panel: options.value,
                            action: options.action,
                            item_id: options.item_id
                        });

                    }
                },
                getLevel: function(panel) {
                    switch (panel) {
                        case 'Institution':
                            return 0;
                            break;
                        case 'College':
                            return 1;
                            break;
                        case 'Department':
                            return 2;
                            break;
                        case 'Subject':
                            return 3;
                            break;
                    }
                },
                save: function() {
                    //ensure the eff date has been changed
                    var newEffDate = this.view.geteff_date();

                    if (this.model.get('eff_date_display') !== newEffDate) {
                        //update eff date
                        this.forceSave();
                    } else {
                        this.trigger('changeEffDate');
                    }
                },
                forceSave: function() {
                    var newEffDate = this.view.geteff_date();
                    var newEffDateId = moment(newEffDate, 'MM/DD/YYYY').format('YYYYMMDD');

                    this.model.set({eff_date: newEffDate, eff_date_id: newEffDateId}, {
                        silent: true
                    });
                    this.model.save();
                },
                delete: function() {
                    this.model.set({}, {silent: true});
                    this.model.destroy();
                    if (this.collection.length == 1)
                        this.model.clear().set(this.model.defaults);
                },
                setArrows: function() {
                    var index = this.collection.findModelByParam(this.model, 'eff_date', true).index + 1;
                    var count = this.collection.length;
                    if (count == 1) {
                        //reach end of coll
                        this.view.updateArrows({
                            left: false,
                            right: false
                        });
                    } else if (index == count) {
                        //reach end of coll
                        this.view.updateArrows({
                            left: true,
                            right: false
                        });
                    } else if (index == 1) {
                        //in the beginning of coll
                        this.view.updateArrows({
                            left: false,
                            right: true
                        });
                    } else {
                        //index is in the middle
                        this.view.updateArrows({
                            left: true,
                            right: true
                        });
                    }
                },
                getPrevModel: function() {
                    this.model.id = this.collection.prevModelByParam(this.model, 'eff_date').id;
                    this.model.fetch({}, {}, {}, false);
                },
                getNextModel: function() {
                    this.model.id = this.collection.nextModelByParam(this.model, 'eff_date').id;
                    this.model.fetch({}, {}, {}, false);
                },
                getDate: function(index) {
                    return moment(this.collection.at(index)
                            .get('eff_date'), 'MM/DD/YYYY')
                            .format('YYYYMMDD');
                },
                add: function() {
                    alert('add');
                },
                hierarchy: function() {
                    //show hierarchy
                    this.trigger('hierarchy');
                },
                PanelSelected: function(panel) {
                    this.trigger('PanelSelected', panel);
                }

            });

            return BuilderController;
        });

define([
    'marionette',
    'app',
    'globalConfig',
    'tpl!modules/obuilder/builder/templates/builder.tmpl'
],
        function(Marionette, App, GC, template) {
            "use strict";

            var BuilderView = App.Views.ItemView.extend({
                template: template,
                tagName: 'div',
                className: 'oBuilderPanel textWhite',
                selectedLevel: '',
                triggerChange: false,
                ui: {
                    treeScroller: '#orgBuilderTreeScroller',
                    values: '#orgBuilderTreeScroller_dummy',
                    heading: '.dwl',
                    eff_date: '.eff_date',
                    leftArrow: '.leftArrow',
                    rightArrow: '.rightArrow'
                },
                triggers: {
                    'click .leftArrow': 'prev',
                    'click .rightArrow': 'next',
                    'click .addBtn': 'add',
                    'click .hierarchyBtn': 'hierarchy'
                },
                events: {
                    'click .actBtn': 'actionSel',
                    'click .dwl button': 'headingSel'
                },
                onBeforeRender: function() {
                    //update model eff_date if empty
                    if (!this.model.get('eff_date_display')) {
                        this.model.set('eff_date_display', moment().format('MM/DD/YYYY'), {silent: true});
                    }
                },
                onRender: function() {
                    var self = this;
                    this.ui.treeScroller.mobiscroll()
                            .treelist({
                        theme: 'minnoware',
                        display: 'inline',
                        mode: 'scroller',
                        labels: ['<button class="oBuilderHeadingBtn btn btn-inverse icomoon-plus"> Institution</button>', '<button class="oBuilderHeadingBtn btn btn-inverse icomoon-plus"> College</button>', '<button class="oBuilderHeadingBtn btn btn-inverse icomoon-plus"> Department</button>', '<button class="oBuilderHeadingBtn btn btn-inverse icomoon-plus"> Subject</button>'],
                        inputClass: 'i-txt hidden',
                        onValueTap: function(item, inst) {
                            var itemId = $(item[0]).data('val');
                            var itemArr = item.context.innerText.match(/[^\r\n]+/g);
                            var BtnClass = 'disabled';

                            var contextString = '';
                            if (itemArr.length > 1) {
                                BtnClass = 'actBtn';
                            }
                            //action buttons
                            var addAction = '<button data-itemid="' + itemId + '" data-action="add" data-heading="' + itemArr[0] + '" class="btn btn-inverse disabled" aria-hidden="true"><h5 class="icomoon-plus"></h5></button>';
                            var editAction = '<button data-itemid="' + itemId + '" data-action="edit" class="btn btn-inverse ' + BtnClass + '" aria-hidden="true"><h5 class="icomoon-pencil"></h5></button>';
                            var remAction = '<button data-itemid="' + itemId + '" data-action="remove" class="btn btn-inverse ' + BtnClass + '" aria-hidden="true"><h5 class="icomoon-remove-2"></h5></button>';
                            contextString = addAction + ' ' + editAction + ' ' + remAction;
                            //record what level did the tap occured
                            self.selectedLevel = item.context.innerText.match(/[^\r\n]+/g)[0].replace(' ', '');
                            self.pop = item.parent();
                            self.pop.popover({
                                html: true,
                                placement: 'top',
                                content: contextString,
                                container: self.el
                            }).popover('show');
                            //hide when click outside
                            App.main.$el.on('mouseup touchend', function(e) {
                                var pop = self.$el.find('.popover');
                                if (pop.has(e.target).length === 0) {
                                    self.pop.popover('destroy');
                                }
                            });
                        }
                    });

                    this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);

                    this.bindUIElements();
                    // this.ui.values.hide();
                },
                actionSel: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    //close popover
                    this.pop.popover('destroy');
                    var action = this.$el.find(e.currentTarget).data('action');

                    if (this.selectedLevel.length > 0) {
                        if (action == 'add') {
                            //this.headingSel(this.$el.find(e.currentTarget).data('heading'));
                        } else {
                            this.trigger('levelValueSelected', {action: action, value: this.selectedLevel, item_id: this.$el.find(e.currentTarget).data('itemid')});
                            this.selectedLevel = '';
                        }
                    }
                    this.triggerChange = true;
                },
                headingSel: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    this.triggerChange = false;
                    this.trigger('PanelSelected', e.target.innerText.replace(' ', ''));
                },
                returnSelValues: function() {
                    return this.ui.values.val().split(' ');
                },
                setValues: function(data) {
                    //data needs to be an array of values
                    this.ui.treeScroller.mobiscroll('setValue', data, false);
                    this.triggerChange = false;
                },
                geteff_date: function() {
                    return this.ui.eff_date.val();
                },
                updateArrows: function(options) {
                    //{left: false, right: true}
                    (options.left) ? this.ui.leftArrow.removeClass('nonvisible') : this.ui.leftArrow.addClass('nonvisible');
                    (options.right) ? this.ui.rightArrow.removeClass('nonvisible') : this.ui.rightArrow.addClass('nonvisible');
                }

            });
            return BuilderView;
        });
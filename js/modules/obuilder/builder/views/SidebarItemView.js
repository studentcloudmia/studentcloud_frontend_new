// SideView
define([
    'marionette', 
    'app',
	'vent',
    'tpl!modules/obuilder/builder/templates/sidebar.tmpl'
    ],function(Marionette, App, vent, template){

        var SideItemView = App.Views.ItemView.extend({
            template: template
        });

        return SideItemView;

    });
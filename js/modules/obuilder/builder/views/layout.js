define([
'marionette',
'app',
'tpl!modules/obuilder/builder/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            builder: '#builder',
            edit: '#edit'
        }

    });
    return LayoutView;
});
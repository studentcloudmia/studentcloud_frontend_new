define(['marionette', 'app', 'tpl!modules/obuilder/hierarchy/templates/hierarchy.tmpl'],

function(Marionette, App, template) {
    "use strict";

    var HierarchyView = App.Views.ItemView.extend({
        template: template,
        tagName: 'form',
        className: 'oBuilderPanel textWhite',

        ui: {
            eff_date: '.eff_date',
            leftArrow: '.leftArrow',
            rightArrow: '.rightArrow'
        },

        triggers: {
            'click .builderBtn': 'builder',
            'click .leftArrow': 'prev',
            'click .rightArrow': 'next'//,
            // 'click a': 'lahkfgvlahsdfkjghv'
        },

        events: {
            'click a': 'highlight'
        },

        onBeforeRender: function() {
            var self = this;
            //update model eff_date if empty
            if (!this.model.get('eff_date')) {
                this.model.set('eff_date', moment()
                    .format('MM/DD/YYYY'));
                this.model.set('Trees', []);
            }
            
            //count all subjects and generate width accordingly
            self.countSub = 0;
            _.each(this.model.get('Trees'), function(inst, instI) {
                _.each(inst.Colleges, function(coll, collI) {
                        // self.countSub = self.countSub + 1;
                    _.each(coll.Departments, function(dept, deptI) {
                        self.countSub = self.countSub + 1;
                        _.each(dept.Subjects, function(subj, subjI) {
                            self.countSub = self.countSub + 1;
                        });
                    });
                });
            });
        },

        onRender: function() {
            var self = this;
            this.makeDragAndDroppables();
            this.$el.find('.datepicker')
                .pickadate({
                dateMax: 365 * 10,
                format: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            var width = (this.countSub * 150) + 'px';
            this.$el.find('.tree').css('width', width);
            this.afterShow();
        },

        makeDragAndDroppables: function() {
            var self = this;
            //make the collHead draggable
            /*this.$el.find('.collChild')
                .draggable({
                revert: "invalid",
                start: function() {
                    self.getScroller('treeScroller')
                        .disable();
                },
                stop: function() {
                    self.getScroller('treeScroller')
                        .enable();
                }
            });*/
            //make the deptHead draggable
            this.$el.find('.deptChild')
                .draggable({
                revert: "invalid",
                start: function() {
                    self.getScroller('treeScroller')
                        .disable();
                },
                stop: function() {
                    self.getScroller('treeScroller')
                        .enable();
                }
            });

            //make the subjHead draggable
            this.$el.find('.subjChild')
                .draggable({
                revert: "invalid",
                start: function() {
                    self.getScroller('treeScroller')
                        .disable();
                },
                stop: function() {
                    self.getScroller('treeScroller')
                        .enable();
                }
            });

            //make droppables
            this.$el.find('.collChild')
                .droppable({
                accept: ".deptChild",
                hoverClass: "ui-state-highlight",
                drop: function(event, ui) {
                    //call selected model with dragged model
                    self.droppedEl($(this), ui);
                    self.makeDragAndDroppables();
                }
            });
            this.$el.find('.deptChild')
                .droppable({
                accept: ".subjChild",
                hoverClass: "ui-state-highlight",
                drop: function(event, ui) {
                    //call selected model with dragged model
                    self.droppedEl($(this), ui);
                    self.makeDragAndDroppables();
                }
            });

        },

        highlight: function(e) {
            e.preventDefault();
            e.stopPropagation();
            this.$el.find('a')
                .removeClass('highlight');
            $(e.currentTarget)
                .addClass('highlight');
        },

        droppedEl: function($parent, child) {
            //child info
            var childName = child.draggable.find('input')
                .first()
                .attr('name');
            //parent info
            //does parent have any children?
            if ($parent.find('ul > li')
                .length < 1) {
                var parentName = $parent.find('input')
                    .attr('name');
            } else {
                var parentName = $parent.find('ul')
                    .first()
                    .find('input')
                    .first()
                    .attr('name');
            }
            
            var bracketsChild = childName.match(/\[{1}.*?\]{1}/g);
            var oldName = childName.replace(bracketsChild[bracketsChild.length - 1], '');

            var bracketsParent = parentName.match(/\[{1}.*?\]{1}/g);
            var diffBrackets = [];
            //find all bracket that are different
            _.each(bracketsChild, function(brac, index) {
                if (brac !== bracketsParent[index]) {
                    //found the bracket
                    diffBrackets.push(index);
                }
            });
            //make child same as parent
            bracketsChild[diffBrackets[0]] = bracketsParent[diffBrackets[0]];
            //all parent inputs
            var parentInputs = $parent.find('> ul')
                .first()
                .find('> li > input')
                .length;
            //change last index bracket for child
            bracketsChild[bracketsChild.length - 2] = '[' + parentInputs + ']';
            //write in the new name for this child
            var newName = '';
            _.each(bracketsChild, function(brac) {
                newName = newName + brac;
            });
            newName = 'Trees' + newName;
            child.draggable.find('input')
                .first()
                .attr('name', newName);
            //all children inputs
            var allChilds = child.draggable.find('li > input');
            //remove last index for newName
            newName = newName.replace(bracketsChild[bracketsChild.length - 1], '');
            var lastChildName = '';
            _.each(allChilds, function(child) {
                //$(child).attr('name', $child.attr('name').replace(oldName, newName));
                lastChildName = child.name.replace(oldName, newName);
                child.name = lastChildName;
            });

            var bracketsLastChild = lastChildName.match(/\[{1}.*?\]{1}/g);
            
            if(bracketsLastChild){
                bracketsLastChild[bracketsLastChild.length - 2] = '[0]';
            }else{
                //special code for subjects
                bracketsLastChild = lastChildName;
                bracketsLastChild[bracketsLastChild.length - 1] = '[0]';
            }
            
            bracketsLastChild = 'Trees' + bracketsLastChild.toString()
                .replace(/,/g, '');

            child.draggable.find('ul')
                .first()
                .attr('data-name', bracketsLastChild);

            child.draggable.removeClass('ui-draggable-dragging')
                .attr("style", "position: relative;");
            $parent.find('ul')
                .first()
                .append(child.draggable.context.outerHTML);
            child.draggable.remove();

            this.trigger('modelChanged');
            this.makeDragAndDroppables();
            //refresh iScroll
            var scroller = this.getScroller('treeScroller');
            scroller.enable();
            scroller.refresh();

        },

        geteff_date: function() {
            return this.ui.eff_date.val();
        },

        updateArrows: function(options) {
            (options.left) ? this.ui.leftArrow.removeClass('nonvisible') : this.ui.leftArrow.addClass('nonvisible');
            (options.right) ? this.ui.rightArrow.removeClass('nonvisible') : this.ui.rightArrow.addClass('nonvisible');
        }



    });
    return HierarchyView;
});

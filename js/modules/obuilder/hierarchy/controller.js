// obuilder catalog
define(['marionette', 'app', 'vent',
//view
'modules/obuilder/hierarchy/views/HierarchyView',
//entities
'entities/collections/oBuilder/OBuilderCollection',
'entities/models/oBuilder/OBuilderModel'],

function(Marionette, App, vent, HierarchyView, OBuilderCollection, OBuilderModel) {

    var HierarchyModule = App.module('Obuilder.Hierarchy');

    HierarchyModule.Controller = Marionette.Controller.extend({

        initialize: function(options) {
            this.model = new OBuilderModel();
            this.collection = new OBuilderCollection();
            this.view = new HierarchyView({
                model: this.model
            });
            //save region where we can show
            this.region = options.region;

            this.listenTo(this.model, 'sync:stop', function(){
                if(this.view._isShown){
                    //render view
                    this.view.render();
                }else{
                    //show view
                    this.region.show(this.view);
                }
                this.modelChanged();
                this.setArrows();
                
            }, this);
            
            //view listeners
            this.listenTo(this.view, 'close', function(){
                this.close();
            }, this);
            
            this.listenTo(this.collection, 'reset', function() {
                //define model options
                var options = {};
                if(this.collection.length > 0){
                    //TODO: Get Current status or closest to it. Need Status from server
                    this.model.id = this.collection.where({Status: 'CURRENT'})[0].id;
                    //fetch model
                    this.model.fetch({}, {}, {}, false);
                }                
            }, this);
            
            this.bindFooterEvents();
            
            this.initViewListeners();
            
            //search for historical obuilder data
            this.fetchHistorcal();
        },
        
        initViewListeners: function(){
            this.listenTo(this.view, 'prev', this.getPrevModel, this);
            this.listenTo(this.view, 'next', this.getNextModel, this);
            this.listenTo(this.view, 'builder', this.showBuilder, this);
            this.listenTo(this.model, 'created', function(){
                //refetch collection for updated trees
                this.fetchHistorcal();
                this.trigger('modelCreated');
            }, this);
            this.listenTo(this.model, 'updated', function(){
                //refetch collection for updated trees
                this.fetchHistorcal();
                this.trigger('modelUpdated');
            }, this);
            
        },
        
        fetchHistorcal: function(){
            this.collection.fetch({
                reset: true
            }, {}, {
                historical: 'True'
            }, false);
        },
        
		bindFooterEvents: function(){
			var self = this;
			self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
            // self.listenTo(vent, 'Components:Builders:Footer:add', self.add, self);
            // self.listenTo(vent, 'Components:Builders:Footer:del', self.del, self);
            // self.listenTo(vent, 'Components:Builders:Footer:prev', self.prev, self);
            // self.listenTo(vent, 'Components:Builders:Footer:next', self.next, self);
		},
        
        updateFooter: function(options){	
            vent.trigger('Components:Builders:Footer:updateButtons', options);
        },
        
        modelChanged: function(){
            //show save button in footer
            this.updateFooter({save: true});
        },
        
        save: function(){
            //ensure the eff date has been changed
            var newEffDate = this.view.geteff_date();
            if(this.model.get('eff_date') !== newEffDate){
                //update eff date
                this.confirmedSave();
            }else{
                this.changeEffDate();
            }
        },
        
        changeEffDate: function(){
            vent.trigger('Components:Alert:Confirm', {
                heading: 'Effective Date Not Changed',
                message: 'You have not changed the effective date for this organization',
                cancelText: 'Edit',
                confirmText: 'Save',
                callee: this,
                callback: this.confirmedSave
            });
        },
        
        confirmedSave: function(){
            var JSONData = this.view.$el.serializeJSON();
            
            //save model
            this.model.set(JSONData, {silent: true});
            this.model.set('eff_date', this.view.geteff_date(),{silent:true});
            // this.model.set('eff_date', moment().add('days', 2).format('MM/DD/YYYY'), {silent: true});
            this.model.save();
        },
        
        setArrows: function() {
            var index = this.collection.findModelByParam(this.model, 'eff_date', true).index + 1;
            var count = this.collection.length;
            if (count == 1) {
                //reach end of coll
                this.view.updateArrows({
                    left: false,
                    right: false
                });
            } else if (index == count) {
                //reach end of coll
                this.view.updateArrows({
                    left: true,
                    right: false
                });
            } else if (index == 1) {
                //in the beginning of coll
                this.view.updateArrows({
                    left: false,
                    right: true
                });
            } else {
                //index is in the middle
                this.view.updateArrows({
                    left: true,
                    right: true
                });
            }
        },
        
        getPrevModel: function() {
            this.model.id = this.collection.prevModelByParam(this.model, 'eff_date').id;
            this.model.fetch({}, {}, {}, false);
        },

        getNextModel: function() {
            this.model.id = this.collection.nextModelByParam(this.model, 'eff_date').id;
            this.model.fetch({}, {}, {}, false);
        },
        
        showBuilder: function(){
            vent.trigger('Obuilder:builder');
        }

    });

    return HierarchyModule;
});

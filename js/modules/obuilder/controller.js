define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/obuilder/views/layout',
    //controllers
    'modules/obuilder/builder/controller',
    'modules/obuilder/hierarchy/controller'    
],
        function(Marionette, App, vent, LayoutView, BuilderModule, HierarchyModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
	                this.listening = false;
					this.layout = new LayoutView();
					
                    App.reqres.setHandler("Obuilder:getLinks", function() {
                        //TODO: These should be generated from menu collection
                        return this.getLinks();
                    });

                    this.listenTo(vent, 'Obuilder:builder', function() {
                        App.navigate('obuilder/builder');
                        this.showBuilder();
                    }, this);

                    this.listenTo(vent, 'Obuilder:hierarchy', function() {
                        App.navigate('obuilder/hierarchy');
                        this.showHierarchy();
                    }, this);

                },

				getLinks: function(){
                    return [];
					return [{
					    name: 'Builder',
					    link: 'obuilder/builder'
					},
                    {
                        name: 'Hierarchy',
                        link: 'obuilder/hierarchy'
                    }
                    ];
				},

                showBuilder: function() {
					//show layout
					App.main.show(this.layout);
					//setup header			
					this.showHeader('Organization Builder');	
					//setup footer
					this.showFooter();
                    //transfer control to show submodule
                    this.controller = new BuilderModule.Controller({region: this.layout.mainContent});
                    this.setupListeners();
                },

                showHierarchy: function() {
					//show layout
					App.main.show(this.layout);
    				//ask for blank sidebar
    				vent.trigger('Sidebar:On', { blank: true });
					//setup header			
					this.showHeader('Organization Hierarchy');
					//setup footer
					this.showFooter();	
                    //transfer control to show submodule
                    this.controller = new HierarchyModule.Controller({region: this.layout.mainContent});
                    this.setupListeners();
                },
                
                setupListeners: function(){
                    if(!this.listening){
                        this.listenTo(this.controller, 'modelCreated', this.modelUpdated, this);
                        this.listenTo(this.controller, 'modelUpdated', this.modelUpdated, this);
                        this.listenTo(this.controller, 'modelDestroyed', this.modelDestroyed, this);
                        this.listenTo(this.controller, 'showFooter', this.showFooter, this);
                        this.listening = true;
                    }
                },
                
                modelUpdated: function(){
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Organization Tree successfully updated'
                    });
                },
                
                modelDestroyed: function(){
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: 'Organization Tree successfully deleted'
                    });
                },

				showHeader: function(heading){
					var options = {
						region: this.layout.header,
						heading: heading,
						links: this.getLinks()
					};			
		            vent.trigger('Components:Builders:Header:on', options);						
				},
				
				showFooter: function(){
					var options = {
						region: this.layout.footer
					};	
		            vent.trigger('Components:Builders:Footer:on', options);					
				},
                        
                onClose: function() {
                    App.reqres.removeHandler("Obuilder:getLinks");		
					this.layout = null;
                }

            });

            return Controller;

        });
define([
'marionette',
'app',
],
function(Marionette, app) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'obuilder/builder': 'showBuilder',
            'obuilder/hierarchy': 'showHierarchy'
        }
    });

    return Router;

});
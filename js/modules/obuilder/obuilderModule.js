// obuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/obuilder/router',
    'modules/obuilder/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var ObuilderModule = App.module('Obuilder');

        //bind to module finalizer event
        ObuilderModule.addFinalizer(function() {
	
        });

        ObuilderModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return ObuilderModule;
    });
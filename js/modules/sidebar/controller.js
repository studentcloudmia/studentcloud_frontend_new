define([
    'marionette',
    'app',
    'vent',
    'modules/sidebar/show/show_controller'
],
        function(Marionette, App, vent, SidebarShowModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save reference to region
                    this.region = options.region;
                    var self = this;

                    //listen to request to use sidebar. Make sure this is run as the controller
                    this.listenTo(vent, 'Sidebar:On', this.show, this);
                    this.listenTo(vent, 'Sidebar:Off', this.sidebarOff, this);
                    this.listenTo(vent, 'Sidebar:Open', this.openSidebar, this);
                    this.listenTo(vent, 'Sidebar:Change', this.change, this);
                    this.listenTo(vent, 'Sidebar:Close', this.closeSidebar, this);

                    App.reqres.setHandler("Sidebar:isOpen", function() {
                        return self.isOpen();
                    });
                },
                show: function(options) {
                    options = options || {};
                    //reset region
                    // this.region.reset();
                    if (options.blank) {
                        //handle sidebar show
                        this.controller = new SidebarShowModule.Controller({region: this.region, blank: true});
                    }
                    else if (typeof options.controller !== 'undefined') {
                        //handle sidebar show
                        this.controller = new SidebarShowModule.Controller({region: this.region, sideController: options.controller});
                    } else {
                        console.warn('Sidebar:Show:Did not send controller!!!');
                    }
                },
                change: function(options) {
                    this.controller.changeContent(options.controller);
                },
                sidebarOff: function() {
                    App.sidebar.reset();
                },
                openSidebar: function() {
                    this.controller.openSidebar();
                },
                closeSidebar: function() {
                    this.controller.closeSidebar();
                },
                isOpen: function() {
                    return this.controller.isOpen();
                }

            });

            return Controller;

        });
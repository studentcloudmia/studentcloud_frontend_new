// sidebar menu popover view
define([
	'marionette',
	'app',
	'iscroll',
	'bootstrap',
	'tpl!modules/sidebar/show/templates/menu.tmpl'
],function(Marionette, App, IScroll, Bootstrap, template){
	
	var MenuView = App.Views.ItemView.extend({
		template: template,
		
		myScroll: {},
		shown: false,
		
		render: function(){
            //render if menu has something
            if(this.collection.length > 0){
    			var data = {cRoute: App.getCurrentRoute(), M: this.collection.toJSON()};
                
    			var compiledTemplate = this.template(data);
			
    			this.popElem = $('.menuIcon');
                //create popover
                this.popElem.popover2({
                    id: 'menuPopover',
                    placement: 'left',
                    trigger: 'none',
                    content: compiledTemplate,
                    horizontalOffset: 15
                });

    			this.setupListeners();
            
                //TODO: Find different way of doing this!!!
                //increase height
                $('#menuPopover .content2').css('height', '500px');
            }
		},
		
		setupListeners: function(){
            var self = this;

            $('a.menuLinks').on('click',
            function(e) {
                //hide popover
                self.closePop();
                //update selected link
                $('a.menuLinks').parent().removeClass('active');
                $(this).parent().addClass('active');
            });

            $('a.accordionMenuBtn').on('click',
            function(e) {
				e.preventDefault();
                //update selected link
				if($(this).parent().hasClass('opened')){					
	                $(this).parent().removeClass('opened');
				}else{
	                $('a.accordionMenuBtn').parent().removeClass('opened');
	                $(this).parent().addClass('opened');					
				}
            });
		},

		openPop: function(){
			if(!this.shown){
	            this.popElem.popover2('show');
				this.shown = true;
			}else{
	            this.closePop();
			}
		},
		
		closePop: function(){
			this.popElem.popover2('fadeOut');
			this.shown = false;
		},
		
		onBeforeClose: function(){
			this.popElem.popover2('destroy');
			this.popElem = null;
			this.shown = false;
			$('a.accordionMenuBtn').off();
			$('a.menuLinks').off();
		}
		
	});
	
	return MenuView;
});
// sidebar handle view
define([
    'marionette',
    'app',
    'globalConfig',
    'tpl!modules/sidebar/show/templates/handle.tmpl'
], function(Marionette, App, GC, template) {

    var HandleView = App.Views.ItemView.extend({
        template: template,
        ui: {
            searchIcon: '.searchIcon',
            menuIcon: '.menuIcon',
            securityIcon: '.securityIcon',
            settingIcon: '.settingIcon',
            logout: '.logout'
        },
        triggers: {
            'click .logout': 'Logout',
            'click .menuIcon': 'ShowMenu',
            'click .securityIcon': 'ShowSecurity',
            'click .searchIcon': 'toggleSidebar',
            'click .midOpen': 'midOpenSidebar',
            'click .mapIcon': 'ShowMap',
            'click .communicationIcon': 'ShowCommunication',
            'swiperight': 'openSidebar',
            'swipeleft': 'closeSidebar'
        },
        onRender: function(){
            //add user's name and username
            if(GC.sessionStorage.isSet('userData')){
                var userData = GC.sessionStorage.get('userData');
                this.$el.find('.userName').html(userData.first_name + ' ' + userData.last_name + '&nbsp;|&nbsp;' + userData.userName);
            }            
        }


    });

    return HandleView;

});
// sidebar security popover view
define([
	'marionette',
	'app',
	'vent',
	'iscroll',
	'tpl!modules/sidebar/show/templates/security.tmpl'
],function(Marionette, App, vent, IScroll, template){
	
	var SecurityView = App.Views.ItemView.extend({
		template: template,
		shown: false,

		render: function(){
			var compiledTemplate = this.template();
			
			this.popElem = $('.securityIcon');
            //create popover
            this.popElem.popover2({
                id: 'securityPopover',
                placement: 'left',
                trigger: 'none',
                content: compiledTemplate,
                horizontalOffset: 15,
				classes: 'wider'
            });

			this.setupListeners();
            
            //TODO: Find different way of doing this!!!
            //increase height
            $('#securityPopover .content2').css('height', '200px');
		},

		setupListeners: function(){
            var self = this;

			$('.ChangePasswordBTN').on('click', function(){
				self.ChangePassword();
			});
		},

		ChangePassword: function(){
            var data= {
				OldPassword: $('.OldPassword').val(), 
				NewPasswordConfirm: $('.NewPassword').val(), 
				NewPassword: $('.NewPassword2').val()
			}
			//reset all fields
			$('.OldPassword').val('');
			$('.NewPassword').val('');
			$('.NewPassword2').val('');
			
			this.listenTo(this.model, 'change', this.showalert, this);
			
			this.model.ChangePassword(data);
		},

		showalert: function() {
			
			if (this.model.get('detail') == "Password Changed"){
				$(".ChangePasswordFields").hide();
			}
			
			vent.trigger('Components:Alert:Alert', {
				heading: 'Oops',
				message: this.model.get('detail')
			});
		},

		openPop: function(){
			if(!this.shown){
	            this.popElem.popover2('show');
				this.shown = true;
			}else{
	            this.closePop();
			}
		},
		
		closePop: function(){
			this.popElem.popover2('fadeOut');
			this.shown = false;
		},
		
		onBeforeClose: function(){
			this.popElem.popover2('destroy');
			this.popElem.off();
			this.popElem = null;
			this.shown = false;
			$('.ChangePasswordBTN').off();
		}
		
	});
	
	return SecurityView;
});
define([
'marionette',
'app',
'tpl!modules/sidebar/show/templates/handle_layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var HandleLayoutView = App.Views.Layout.extend({
        template: template,
		tagName: 'div',
		
        regions: {
            menu: '#sidebar_menu',
			security: '#sidebar_security',
            handle: '#sidebar_handle' //placing handle view at the end so it can be closed last
        }

    });
    return HandleLayoutView;
});

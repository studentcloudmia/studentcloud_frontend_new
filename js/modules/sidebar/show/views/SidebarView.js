// sidebar view
define([
    'marionette', 
    'app',
    'moment',
    'tpl!modules/sidebar/show/templates/sidebar.tmpl'
    ],function(Marionette, App, Moment, template){
	
        var SideView = App.Views.ItemView.extend({
            template: template
		
        });
	
        return SideView;
	
    });
define([
'marionette',
'app',
'tpl!modules/sidebar/show/templates/layout.tmpl'
],
function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,
		tagName: 'div',
		id: 'slide',
		className: 'sidebarClosed animated',
		
		triggers:{
			'swiperight': 'closeSidebar',
			'swipeleft': 'openSidebar'
		},
		
        regions: {
            sidebarMain: "#sidebarMain",
            sidebarHandle: "#sidebarHandle"
        }

    });
    return LayoutView;
});
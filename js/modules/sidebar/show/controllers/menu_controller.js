// sidebar menu controller
define([
'marionette',
'app',
'vent',
//views
'modules/sidebar/show/views/MenuView',
//collection
'entities/collections/access/MenuCollection'
],
function(Marionette, App, vent, MenuView, MenuCollection) {

    var MenuController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;

			this.collection = new MenuCollection();
			this.collection.fetch({reset: true},{},{},false);
			this.listenTo(this.collection, 'reset', function(){
                console.log('reset menu');
			    this.view.render();
			});

			this.view = new MenuView({collection: this.collection});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        },

		openPop: function(){
            var isOpen = App.request('Sidebar:isOpen');
            var self = this;
            if(isOpen){
                vent.trigger('Sidebar:Close');
                setTimeout(function(){
                    //waiting for sidebar to close
                    self.view.openPop();
                },1000);
            }else{
                self.view.openPop();
            }
		},

		closePop: function(){
			this.view.closePop();
		}

    });

    return MenuController;
});
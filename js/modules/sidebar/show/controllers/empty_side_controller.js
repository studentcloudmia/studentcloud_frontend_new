// sidebar content controller
define([
'marionette',
'app',
'vent',
//views
'modules/sidebar/show/views/SidebarView'
],
function(Marionette, App, vent, ContentView) {

    var SideController = Marionette.Controller.extend({

        initialize: function(options) {
			
			this.view = new ContentView();
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
        },

		show: function(options){
            //save region where we can show
            this.region = options.region;
			
			//show view
			this.region.show(this.view);
		}

    });

    return SideController;
});
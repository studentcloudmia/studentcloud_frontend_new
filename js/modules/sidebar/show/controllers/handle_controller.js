// sidebar handle controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/sidebar/show/views/HandleView'
],
        function(Marionette, App, vent, HandleView) {

            var HandleController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;

                    this.view = new HandleView();

                    this.setupViewListeners();

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    //show view
                    this.region.show(this.view);
                },
                setupViewListeners: function() {

                    this.listenTo(this.view, 'Logout', function() {
                        this.trigger('Logout');
                    }, this);

                    this.listenTo(this.view, 'toggleSidebar', function() {
                        this.trigger('toggleSidebar');
                    }, this);

                    this.listenTo(this.view, 'midOpenSidebar', function() {
                        this.trigger('midOpenSidebar');
                    }, this);

                    this.listenTo(this.view, 'ShowMenu', function() {
                        this.trigger('ShowMenu');
                    }, this);

                    this.listenTo(this.view, 'ShowSecurity', function() {
                        this.trigger('ShowSecurity');
                    }, this);

                    this.listenTo(this.view, 'ShowMap', function() {
                        this.trigger('ShowMap');
                    }, this);

                    this.listenTo(this.view, 'ShowCommunication', function() {
                        this.trigger('ShowCommunication');
                    }, this);
                }

            });

            return HandleController;
        });
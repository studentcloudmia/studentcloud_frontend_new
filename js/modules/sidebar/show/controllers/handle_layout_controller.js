// sidebar show
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/sidebar/show/views/handle_layout',
//controllers
    'modules/sidebar/show/controllers/handle_controller',
    'modules/sidebar/show/controllers/menu_controller',
    'modules/sidebar/show/controllers/security_controller',
],
        function(Marionette, App, vent, HandleLayoutView, HandleController, MenuController, SecurityController) {

            var HandleLayoutController = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;

                    //setup layout and add to region
                    this.layout = new HandleLayoutView();

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHandle();
                        this.showMenu();
                        this.showSecurity();
                        this.setupListeners();
                    }, this);

                    //show layout
                    this.region.show(this.layout);
                },
                showHandle: function() {
                    //start handle controller
                    this.handleController = new HandleController({region: this.layout.handle});
                    this.listenTo(this.handleController, 'Logout', function() {
                        App.execute('Logout');
                    }, this);
                    this.listenTo(this.handleController, 'toggleSidebar', function() {
                        this.trigger('toggleSidebar');
                    }, this);
                    this.listenTo(this.handleController, 'midOpenSidebar', function() {
                        this.trigger('midOpenSidebar');
                    }, this);
                    this.listenTo(this.handleController, 'ShowMenu', function() {
                        this.toggleMenu();
                    }, this);
                    this.listenTo(this.handleController, 'ShowSecurity', function() {
                        this.toggleSecurity();
                    }, this);
                    this.listenTo(this.handleController, 'ShowMap', function() {
                        this.ShowMap();
                    }, this);
                    this.listenTo(this.handleController, 'ShowCommunication', function() {
                        this.ShowCommunication();
                    }, this);

                },
                showMenu: function() {
                    //start menu controller
                    this.menuController = new MenuController({region: this.layout.menu});
                },
                showSecurity: function() {
                    //start security controller
                    this.securityController = new SecurityController({region: this.layout.security});
                },
                ShowMap: function() {
                    //trigger to show map
                    vent.trigger('dashboard:map');
                },
                ShowCommunication: function() {
                    //trigger to show map
                    vent.trigger('Communication:show');
                },
                setupListeners: function() {
                    self = this;
                    $(App.main.el).on('click', function() {
                        self.closeAllPop();
                        self.trigger('closeSidebar');
                    });
                    $(this.region.el).on('click', function() {
                        self.closeAllPop();
                        self.trigger('closeSidebar');
                    });
                },
                closeAllPop: function() {
                    this.menuController.closePop();
                    this.securityController.closePop();
                },
                toggleMenu: function() {
                    this.securityController.closePop();
                    this.menuController.openPop();
                },
                toggleSecurity: function() {
                    this.menuController.closePop();
                    this.securityController.openPop();
                },
                onClose: function() {
                    $(App.main.el).off();
                    $(this.region.el).off();
                }

            });

            return HandleLayoutController;
        });
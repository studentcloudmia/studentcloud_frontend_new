// sidebar menu controller
define([
'marionette',
'app',
'vent',
//views
'modules/sidebar/show/views/SecurityView',
//model
'entities/models/access/ChangePasswordModel'
],
function(Marionette, App, vent, SecurityView, ChasngePasswordModel) {

    var SecurityController = Marionette.Controller.extend({

        initialize: function(options) {
            //save region where we can show
            this.region = options.region;

			this.model = new ChasngePasswordModel();
			
			this.view = new SecurityView({model: this.model});
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
			
			//show view
			this.region.show(this.view);
        },

		openPop: function(){
            var isOpen = App.request('Sidebar:isOpen');
            var self = this;
            if(isOpen){
                vent.trigger('Sidebar:Close');
                setTimeout(function(){
                    //waiting for sidebar to close
                    self.view.openPop();
                },1000);
            }else{
                self.view.openPop();
            }
		},

		closePop: function(){
			this.view.closePop();
		}

    });

    return SecurityController;
});
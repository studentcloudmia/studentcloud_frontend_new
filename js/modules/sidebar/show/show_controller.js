// sidebar show
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/sidebar/show/views/layout',
//controllers
    'modules/sidebar/show/controllers/handle_layout_controller',
    'modules/sidebar/show/controllers/empty_side_controller'
],
        function(Marionette, App, vent, LayoutView, HandleLayoutController, EmptySideController) {

            var SidebarShowModule = App.module('Sidebar.Show');

            SidebarShowModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;

                    if (options.blank == true) {
                        this.sideController = new EmptySideController();
                    } else {
                        this.sideController = options.sideController;
                    }

                    //setup layout and add to region
                    this.layout = new LayoutView();

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHandle();
                        this.showContent();
                        this.bindLayoutEvents();
                    }, this);

                    //show layout
                    this.region.show(this.layout);
                },
                showHandle: function() {
                    //start handle controller
                    this.handleLayoutController = new HandleLayoutController({region: this.layout.sidebarHandle});

                    this.listenTo(this.handleLayoutController, 'toggleSidebar', function() {
                        this.toggleSidebar();
                    }, this);

                    this.listenTo(this.handleLayoutController, 'midOpenSidebar', function() {
                        this.openSidebarMid();
                    }, this);

                    this.listenTo(this.handleLayoutController, 'closeSidebar', function() {
                        this.closeSidebar();
                    }, this);

                },
                changeContent: function(controller) {
                    this.sideController = controller;
                    this.showContent();
                },
                showContent: function() {
                    //start content controller
                    this.sideController.show({region: this.layout.sidebarMain});
                },
                openSidebar: function() {
                    if (!this.isOpen()) {
                        vent.trigger('Communication:close');
                        this.handleLayoutController.closeAllPop();
                        this.layout.$el.removeClass('sidebarClosed slideOutRight').addClass('sidebarOpen slideInRight');
                        this.layout.$el.find('.moreLeft').removeClass('forceHidden');
                    }
                },
                closeSidebar: function() {
                    if (this.isOpen()) {
                        //close the sidebar
                        this.layout.$el.removeClass('sidebarOpen sidebarOpenMid sidebarOpenMax slideInRight').addClass('sidebarClosed slideOutRight');
                        this.layout.$el.find('.moreRight').addClass('forceHidden');
                        this.layout.$el.find('.moreLeft').addClass('forceHidden');
                        this.layout.$el.find('.moreMid').addClass('forceHidden');
                        this.layout.$el.find('.moreMax').addClass('forceHidden');

                        //notify sidebar is closed
                        vent.trigger('Sidebar:is:Closed');
                    }
                },
                openSidebarMid: function() {
                    if (this.layout.$el.hasClass('sidebarOpen')) {
                        this.layout.$el.removeClass('sidebarOpen slideInRight').addClass('sidebarOpenMid ');
                        this.layout.$el.find('.moreMid').removeClass('forceHidden');
                    }
                    else if (this.layout.$el.hasClass('sidebarOpenMid')) {
                        this.layout.$el.removeClass('sidebarOpenMid').addClass('sidebarOpenMax');
                        this.layout.$el.find('.moreLeft').addClass('forceHidden');
                        this.layout.$el.find('.moreRight').removeClass('forceHidden');
                        this.layout.$el.find('.moreMax').removeClass('forceHidden');
                    }
                    else if (this.layout.$el.hasClass('sidebarOpenMax')) {
                        this.layout.$el.removeClass('sidebarOpenMax').addClass('sidebarOpen');
                        this.layout.$el.find('.moreRight').addClass('forceHidden');
                        this.layout.$el.find('.moreLeft').removeClass('forceHidden');
                        this.layout.$el.find('.moreMid').addClass('forceHidden');
                        this.layout.$el.find('.moreMax').addClass('forceHidden');
                    }
                },
                toggleSidebar: function() {
                    if (this.isOpen()) {
                        this.closeSidebar();
                    } else {
                        this.openSidebar();
                    }
                },
                isOpen: function() {
                    return (this.layout.$el.hasClass('sidebarOpen') || this.layout.$el.hasClass('sidebarOpenMid') || this.layout.$el.hasClass('sidebarOpenMax'));
                },
                bindLayoutEvents: function() {
                    this.listenTo(this.layout, 'closeSidebar', function() {
                        this.closeSidebar();
                    }, this);
                    this.listenTo(this.layout, 'openSidebar', function() {
                        this.openSidebar();
                    }, this);
                }

            });

            return SidebarShowModule;
        });
// sidebar module
define([
'marionette',
'app',
'vent',
'modules/sidebar/controller'
],
function(Marionette, App, vent, Controller) {

    var SidebarModule = App.module('Sidebar');

    //bind to module finalizer event
    SidebarModule.addFinalizer(function() {
		App.sidebar.reset();
		this.started = false;
    });

	SidebarModule.addInitializer(function(options) {
		this.started = true;
		//start controller with sidebar region
		this.initController();
		window.side = this;
	});
	
	SidebarModule.initController = function(){
		//restart this controller even if its closed.
		//controller will only be fully closed when module if stopped
		if(this.started){
			this.controller = new Controller({region: App.sidebar});
			this.listenTo(this.controller, 'close', this.initController, this);
		}
		
	}

    return SidebarModule;
});
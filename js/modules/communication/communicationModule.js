// Communication module
define([
    'marionette',
    'app',
    'vent',
    'modules/communication/controller'
],
        function(Marionette, App, vent, Controller) {

            var CommunicationModule = App.module('Communication');

            //bind to module finalizer event
            CommunicationModule.addFinalizer(function() {

            });

            CommunicationModule.addInitializer(
                    function(options) {
                        new Controller();
                    });

            return CommunicationModule;
        });
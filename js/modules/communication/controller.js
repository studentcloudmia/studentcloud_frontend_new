define([
    'marionette',
    'app',
    'vent',
    //controllers
    'modules/communication/friends/controller'
],
        function(Marionette, App, vent, FriendsModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.communicationShwo = false;



                    this.listenTo(vent, 'Communication:show', function() {
                        if (this.communicationShwo) {
                            self.closeChild();
                        } else {
                            self.communicationShwo = true;
                            switch (App.getCurrentRoute()) {
                                case 'enroll':
                                    self.openChild('calendar');
                                    break;
                                default:
                                    self.openChild('home');
                            }
                        }
                        this.listenTo(vent, 'BaseRouter:route', function() {
                            var open = false;
                            if (this.friendsModule) {
                                this.closeChild();
                                open = true;
                            }
                            if (Backbone.history.fragment == 'enroll') {
                                if (open) {
                                    self.communicationShwo = true;
                                    this.friendsModule = new FriendsModule({show: 'calendar', region: App.communication});
                                }
                            }
                        });
                        this.listenToOnce(vent, 'Communication:close', this.closeChild);


                    }, this);

                },
                openChild: function(where) {
                    var isOpen = App.request('Sidebar:isOpen');
                    var self = this;
                    if (isOpen) {
                        vent.trigger('Sidebar:Close');
                        setTimeout(function() {
                            //waiting for sidebar to close
                            self.friendsModule = new FriendsModule({show: where, region: App.communication});
                        }, 1000);
                    } else {
                        self.friendsModule = new FriendsModule({show: where, region: App.communication});
                    }
                },
                closeChild: function() {
                    this.friendsModule.close();
                    this.communicationShwo = false;
                }

            });

            return Controller;

        });
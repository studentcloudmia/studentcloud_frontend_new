// calendar controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/CalendarView',
    //model
    'entities/models/friends/FriendModel',
    //collection
    'entities/collections/friends/FriendCollection'
],
        function(Marionette, App, vent, view, FriendModel, FriendCollection) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.model = new FriendModel;
                    this.collection = new FriendCollection();
                    this.schedule = new FriendCollection();
                    this.collection.fetch({}, {request: 'request'}, {approve: 'True'}, false);
                    this.listenTo(this.collection, 'sync:stop', this.show);
                    App.navigate('enroll', {trigger: true});
                    this.termId = App.request("Academics:Enroll:Term");
                    this.listenTo(vent, 'Academics:Enroll:TermUpdate', function(termId) {
                        this.termId = termId;
                    });
                },
                show: function() {
                    this.view = new view({collection: this.collection});
                    console.log('this.view ', this.view)
                    this.region.show(this.view);
                    this.viewListeners();
                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'getFriendSchedule', this.getFriendSchedule, this);
                    this.listenTo(this.view, 'removeFriendSchedule', this.removeFriendSchedule, this);
                },
                removeFriendSchedule: function(uid) {
                    console.log('this.schedule ', this.schedule)
                    console.log('removing where freind = ', uid)
                    this.scheduleList = _.reject(this.schedule.models, function(item) {
                        return item.get('friend') == uid;
                    });
                    console.log('this.schedule2 ', this.scheduleList);
                },
                getFriendSchedule: function(uid) {
                    var self = this;
                    this.schedule.fetch({remove: false}, {request: 'schedule', uid: uid, term: this.termId}, {}, false);
                    this.length = 0;
                    this.listenTo(this.schedule, 'sync:stop', function() {
                        _.each(this.schedule.models, function(item, index) {
                            console.log('INDEX ', index);
                            console.log('self length ', self.length)
                            if (index >= self.length) {
                                item.set('friend', uid);
                            }
                            self.length = self.schedule.length - 1;
                        });
                        console.log('collection ', this.schedule)
                    });
                }
            });
            return Controller;
        });
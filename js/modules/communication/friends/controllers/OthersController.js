// others controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/OthersView',
    'modules/communication/friends/controllers/OfficeInfoController'
],
        function(Marionette, App, vent, view, OfficeInfoController) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.view = new view();
                    this.region.show(this.view);
                    this.listenTo(vent, 'Communication:Friends:officeInfoModal', function(template) {
                        self.officeInfoController = new OfficeInfoController({region: App.modalsAlt2, template: template});
                    });
                }
            });
            return Controller;
        });
// office info controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/OfficeInfoView'
],
        function(Marionette, App, vent, view) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.template = options.template;
                    if (this.template == 'videoChat')
                        this.view = new view({templ: self.template, model: options.model});
                    else
                        this.view = new view({templ: self.template});
                    this.region.show(this.view);
                    this.listenTo(vent, 'Communication:friends:closeVideoUpload', function() {
                        self.view.close();
                    });
                    this.listenTo(this.view, 'close', this.close);
                }
            });
            return Controller;
        });
// footer controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/FooterView'
],
        function(Marionette, App, vent, view) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.curr = options.curr;
                    this.view = new view({curr: this.curr});
                    this.region.show(this.view);
                    this.listenTo(this.view, 'expandView', function() {
                        this.trigger('expandView');
                    }, this);
                    this.listenTo(this.view, 'collapseView', function() {
                        this.trigger('collapseView');
                    }, this);
                    this.listenTo(this.view, 'privacyView', function() {
                        this.trigger('privacyView');
                    }, this);
                    this.listenTo(this.view, 'settings', function() {
                        this.trigger('settings');
                    }, this);
                }
            });
            return Controller;
        });
// privacy controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/PrivacyView'
],
        function(Marionette, App, vent, view) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.view = new view();
                    this.region.show(this.view);
                }
            });
            return Controller;
        });
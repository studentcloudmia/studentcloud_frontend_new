// expand controller
define([
    'marionette',
    'app',
    'vent',
    'globalConfig',
    //views
    'modules/communication/friends/views/ExpandView',
    'entities/models/communication/ChatModel',
    'entities/collections/communication/ChatCollection'
],
        function(Marionette, App, vent, GC, view, ChatModel, ChatCollection) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.model = new ChatModel();
                    this.view = new view({model: this.model});
                    this.region.show(this.view);
                    this.listenTo(this.view, 'close', this.close);
                    this.viewListeners();
                    this.getMessages = new ChatCollection();
                    this.listenTo(this.view, 'loadMessages', function() {

                        this.getMessages.fetch({}, {get_internal: 'get_internal'}, {}, false);
                    });
                    this.listenTo(this.getMessages, 'sync:stop', function() {
                        console.log('append', self.getMessages)
                        self.view.clearContaine();
                        self.chatDummyModel = new ChatModel();
                        if (self.getMessages.length > 0) {
                            //  if (self.getMessages.at(0).get('attachment') != '') {
                            _.each(self.getMessages.models, function(item2) {
                                console.log('item2 ', item2)
                                if (item2.get('attachment') != '') {
                                    self.chatDummyModel.fetch({async: false}, {get_attachment: 'get_attachment', id: item2.get('id'), t: 't'}, {}, false);
                                    //setTimeout(function() {
                                    self.view.appendMessage(item2);
                                    // }, 300);
                                }
                                else {
                                    self.view.appendMessage(item2);
                                }
                            });
                            //}
                        }

                    });
                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'headerLink', function(link) {
                        self.trigger('headerLink', link);
                    });
                    this.listenTo(this.view, 'close', function() {
                        self.close();
                    });
                    this.listenTo(this.view, 'SubMenu:networkView', function(link) {
                        self.trigger('SubMenu:networkView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:otherView', function(link) {
                        self.trigger('SubMenu:otherView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:classesView', function(link) {
                        self.trigger('SubMenu:classesView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:advisorView', function(link) {
                        self.trigger('SubMenu:advisorView', link);
                    });
                    this.listenTo(this.view, 'sendMessage', function(message) {
                        var date = moment().format('YYYY-MM-DDTH:mm:ss');
                        this.model.set({
                            messageid: [
                                {
                                    created_by: "",
                                    enable: 1,
                                    recipient: "jsmith",
                                    recipient_type: 2,
                                    send_on: date,
                                    viewed_on: null,
                                    to_email: "test2"
                                },
                                {
                                    created_by: "",
                                    enable: 1,
                                    recipient: "beth",
                                    recipient_type: 2,
                                    send_on: date,
                                    viewed_on: null,
                                    to_email: "test2"
                                }
                            ],
                            enable: 1,
                            description: GC.sessionStorage.get('userData').first_name + " " + GC.sessionStorage.get('userData').last_name,
                            delivery_method: 88,
                            message_type: 79,
                            content: message,
                            subject: "test",
                            from_email: "test",
                            attachment: ""
                        });
                        self.listenToOnce(self.model, 'created', function() {
                            self.getMessages.fetch({loader: false}, {get_internal: 'get_internal'}, {}, false);
                        });
                        this.model.save({}, {loader: false});
                    });
                    this.listenTo(this.view, 'Communication:Friends:upload:video', function() {
                        console.log('got trigger for :upload:video')
                        var date = moment().format('YYYY-MM-DDTH:mm:ss');
                        self.model.set({
                            messageid: [
                                {
                                    created_by: "",
                                    enable: 1,
                                    recipient: "jsmith",
                                    recipient_type: 2,
                                    send_on: date,
                                    viewed_on: null,
                                    to_email: "test2"
                                },
                                {
                                    created_by: "",
                                    enable: 1,
                                    recipient: "beth",
                                    recipient_type: 2,
                                    send_on: date,
                                    viewed_on: null,
                                    to_email: "test2"
                                }
                            ],
                            enable: 1,
                            description: GC.sessionStorage.get('userData').first_name + " " + GC.sessionStorage.get('userData').last_name,
                            delivery_method: 88,
                            message_type: 79,
                            content: '',
                            subject: "test",
                            from_email: "test"
                        });
                        self.listenToOnce(self.model, 'created', function(e) {
                            self.getMessages.fetch({}, {get_internal: 'get_internal'}, {}, false);
                        });
                        self.model.save({}, {loader: false});
                    });
                    this.listenTo(this.view, 'deleteMessage', function(id) {
                        self.delModel = new ChatModel();
                        self.delModel.set({id: id});
                        self.delModel.destroy();
                    });
                }

            });
            return Controller;
        });
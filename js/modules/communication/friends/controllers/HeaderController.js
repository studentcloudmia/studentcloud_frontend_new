// header controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/HeaderView'
],
        function(Marionette, App, vent, view) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    var self = this;
                    this.region = options.region;
                    this.curr = options.curr;
                    this.view = new view({curr: this.curr});
                    this.region.show(this.view);
                    this.viewListeners();
                },
                viewListeners: function() {
                    var self = this;
                    this.listenTo(this.view, 'headerLink', function(link) {
                        self.trigger('headerLink', link);
                        this.trigger('collapseView');
                    });
                    this.listenTo(this.view, 'close', function() {
                        self.close();
                    });
                    this.listenTo(this.view, 'SubMenu:networkView', function(link) {
                        self.trigger('SubMenu:networkView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:otherView', function(link) {
                        self.trigger('SubMenu:otherView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:classesView', function(link) {
                        self.trigger('SubMenu:classesView', link);
                    });
                    this.listenTo(this.view, 'SubMenu:advisorView', function(link) {
                        self.trigger('SubMenu:advisorView', link);
                    });
                }
            });
            return Controller;
        });
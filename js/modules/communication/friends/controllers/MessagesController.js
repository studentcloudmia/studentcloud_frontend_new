// messages controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/communication/friends/views/MessagesView',
    'modules/communication/friends/controllers/ChatController'
],
        function(Marionette, App, vent, view, ChatController) {
            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    if (options.viewAs) {
                        this.view = new view({viewAs: 'expanded'});
                    }
                    else {
                        this.view = new view();
                    }
                    this.region.show(this.view);
                    this.listenTo(this.view, 'viewChat', this.showChat);
                    this.listenTo(vent, 'Communication:Friends:viewChat', this.showChat);
                },
                showChat: function(e) {
                    var self = this;
                    self.template = e.currentTarget.id;
                    this.chatController = new ChatController({region: this.region, template: self.template});
                    this.listenTo(this.chatController, 'showAllMesseges', function() {
                        self.view = new view({viewAs: 'expanded'});
                        self.region.show(self.view);
                        self.listenTo(self.view, 'viewChat', this.showChat);
                    });
                }
            });
            return Controller;
        });
// footer view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/footer.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid',
        expanded: false,
        onShow: function() {
            var self = this;
            this.listenTo(vent, 'Communication:Friends:Expandedvie:Exit', function() {
                self.expanded = false;
            });
            this.$el.find('.privacyView').on('click', function() {
                self.trigger('privacyView');
            });
            this.$el.find('.settings').on('click', function() {
                self.trigger('settings');
            });
            this.$el.find('.expandView').on('click', function() {
                if (!self.expanded) {
                    self.trigger('expandView');
                    $(this).html('Collapse Page');
                    vent.trigger('Communication:Friends:CloseHeader');
                    self.expanded = true;
                }
                else {
                    self.trigger('collapseView');
                    $(this).html('Expand Page');
                    vent.trigger('Communication:Friends:OpenHeader');
                    self.expanded = false;
                }
            });
        }
    });

    return View;

});
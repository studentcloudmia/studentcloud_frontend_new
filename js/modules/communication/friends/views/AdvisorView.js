// advisor view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/advisor.tmpl',
    'tpl!modules/communication/friends/templates/advisor2.tmpl'
], function(_, Marionette, vent, App, GC, template, advisor2) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'advisor animated fadeIn',
        initialize: function() {
            var self = this;
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.listenTo(vent, 'Communication:Friends:Collapseview', this.collapseView);
            if (GC.sessionStorage.get('userData').userName == 'jsmith') {
                this.template = advisor2;
            }
        },
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_2').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 120, end: 240}
            });
            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                var currID = this.id.replace('img_', '');
                self.$el.find('#showBubbles_' + currID).trigger('click');
            });
        },
        expandView: function() {
            this.$el.addClass('hidden');
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.subMenuAdvisor'});
        },
        collapseView: function() {
            this.$el.removeClass('hidden');
        }
    });
    return View;

});
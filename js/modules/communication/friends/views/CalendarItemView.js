// Calendar Item View
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/communication/friends/templates/_calendar.tmpl'
], function(Marionette, App, vent, template) {

    var CalendarItemView = App.Views.ItemView.extend({
        template: template,
        className: 'eachWrapper',
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_' + this.model.get('id')).circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                self.$el.find('.showBubble').trigger('click');
            });
            //selecting item
            this.$el.find('.selectOne').on('click', function() {
                if (self.$el.hasClass('selected')) {
                    self.trigger('removeFriendSchedule');
                    self.$el.find('.checkedIcon').removeClass('icomoon-checked').addClass('icomoon-unchecked');
                    self.$el.find('.userImg').removeClass('selectedImage');
                    self.$el.removeClass('selected text-orange');
                }
                else {
                    self.trigger('getFriendSchedule');
                    self.$el.find('.checkedIcon').removeClass('icomoon-unchecked').addClass('icomoon-checked');
                    self.$el.find('.userImg').addClass('selectedImage');
                    self.$el.addClass('selected text-orange');
                }
                self.trigger('multipleSelected');
            });
        }
    });
    return CalendarItemView;
});
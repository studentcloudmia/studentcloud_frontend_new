// home view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/home.tmpl',
    'tpl!modules/communication/friends/templates/home2.tmpl'
], function(_, Marionette, vent, App, GC, template, home2) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'network animated fadeIn',
        initialize: function() {
            if (GC.sessionStorage.get('userData').userName == 'jsmith') {
                this.template = home2;
            }
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.listenTo(vent, 'Communication:Friends:Collapseview', this.collapseView);
        },
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 120, end: 240}
            });
            this.$el.find('.circleMenu_2').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 120, end: 240}
            });
            this.$el.find('.circleMenu_3').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_4').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });

            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                var currID = this.id.replace('img_', '');
                self.$el.find('#showBubbles_' + currID).trigger('click');
            });
        },
        expandView: function() {
            this.$el.addClass('hidden');
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.network'});
        },
        collapseView: function() {
            this.$el.removeClass('hidden');
        }
    });

    return View;

});
// privacy view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/setting.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'container animated fadeIn',
        onShow: function() {
            var self = this;
            this.$el.find('.agree').on('click', function() {
                self.$el.html('<h2 class="thankYou animated fadeIn text-orange">Thank You</h2>');
                setTimeout(function() {
                    self.$el.addClass('fadeOut');
                    self.exit();
                }, 1000);

            });
            this.$el.find('.exit').on('click', function() {
                self.$el.addClass('fadeOut');
                self.exit();
            });
            this.$el.find('.privacyView').on('click', function() {
                vent.trigger('Communication:Friends:Show:privacy');
            });
        },
        exit: function() {
            var self = this;
            setTimeout(function() {
                self.close();
            }, 1000);
        },
        onClose: function() {
            vent.trigger('Communication:close');
            vent.trigger('Communication:show');
        }
    });

    return View;

});
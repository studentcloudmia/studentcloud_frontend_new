// office info view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/financialInfo.tmpl',
    'tpl!modules/communication/friends/templates/admissionsInfo.tmpl',
    'tpl!modules/communication/friends/templates/athleticInfo.tmpl',
    'tpl!modules/communication/friends/templates/chatOptions.tmpl'
], function(_, Marionette, vent, App, GC, financial, admissions, athletic, chatOptions) {

    var View = App.Views.ItemView.extend({
        className: 'officeInfo',
        template: financial,
        initialize: function(options) {
            var self = this;
            if (options.templ == "financial") {
                this.template = financial;
            }
            else if (options.templ == 'admissions') {
                this.template = admissions;
            }
            else if (options.templ == 'athletic') {
                this.template = athletic;
            }
            else if (options.templ == 'videoChat') {
                this.template = chatOptions;
            }
        },
        onShow: function() {
            var self = this;
            this.$el.find('.attachVideo').on('click', function() {
                self.$el.find('.uploader').click();
            });
            this.$el.find('.uploader').on('change', function() {
                self.upload($(this));
                self.listenTo(self.model, 'change:attachment', function() {
                    self.$el.find('.videoTag').html('<span style="text-align: center;" class="label span12 label-primary"><i class="icomoon-checkmark"/> Video Ready! </span>');
                    self.$el.find('.videoContainer').removeClass('displayNone');
                    //vent.trigger('Communication:Friends:upload:video');
                });
            });
            this.$el.find('.uploadVid').on('click', function() {
                vent.trigger('Communication:Friends:upload:video');
            });
            self.$el.find('#player_80').load();
        },
        upload: function(e) {
            self = this;
            var file = e[0].files[0];
            self.model.readFile(file);
        }
    });
    return View;

});
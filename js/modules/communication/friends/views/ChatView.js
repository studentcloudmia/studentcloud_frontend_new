// chat view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/chat.tmpl',
    'tpl!modules/communication/friends/templates/chat2.tmpl',
    'tpl!modules/communication/friends/templates/chat2J.tmpl',
    'tpl!modules/communication/friends/templates/chat3.tmpl',
    'tpl!modules/communication/friends/templates/chat4.tmpl'
], function(_, Marionette, vent, App, GC, template, chat2, chat2J, chat3, chat4) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'expandedViewContainer animated fadeIn',
        initialize: function(options) {
            var self = this;
            this.showChat(options);
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.count = 0;
        },
        showChat: function(options) {
            var self = this;
            this.tmplOpt = options.tmpl;
            if (options.tmpl == 'chat') {
                this.template = template;
                this.onShow();
                this.chat = 'chat';
            }
            if (options.tmpl == 'chat2') {
                self.trigger('OnChat2');
                if (GC.sessionStorage.get('userData').userName == 'jsmith') {
                    this.template = chat2J;
                }
                else {
                    this.template = chat2;
                }
                this.onShow();
                this.chat = 'chat2';
            }
            if (options.tmpl == 'chat3') {
                this.template = chat3;
                this.onShow();
                this.chat = 'chat3';
            }
            if (options.tmpl == 'chat4') {
                this.template = chat4;
                this.onShow();
                this.chat = 'chat4';
            }
        },
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_2').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                var currID = this.id.replace('img_', '');
                self.$el.find('#showBubbles_' + currID).trigger('click');
            });
            this.$el.find('.viewActiveChat').on('click', function(e) {
                vent.trigger('Communication:Friends:viewChat', e);
            });
            this.$el.find('.sendMessage').on('click', function() {
                self.sendMessage();
            });
            this.$el.find('.videoChat').on('click', function() {
                self.pop = self.$el;
                self.pop.popover({
                    html: true,
                    placement: 'left',
                    trigger: 'none',
                    content: '<button style="margin-bottom: 2px!important;width: 60px!important;" class="enrlActBtn btn btn-info" aria-hidden="true"><h5 class="icomoon-calendar"></h5><p>Schedule</p></button><br><button  style="margin-bottom: 2px!important;width: 60px!important;"  class="enrlActBtn btn btn-info" aria-hidden="true"><h5 class="icomoon-video-2"></h5><p>Begin</p></button><br><button  style="margin-bottom: 2px!important;width: 60px!important;" class="enrlActBtn attachVideo btn btn-info" aria-hidden="true"><h5 class="icomoon-attachment"></h5><p>Attach</p></button><input type="file" class="hidden uploader_video">',
                    container: 'body'
                }).popover('show');

                //hide when click outside
                App.communication.$el.on('mouseup touchend', function(e) {
                    var pop = self.$el.find('.popover');
                    if (pop.has(e.target).length === 0) {
                        self.pop.popover('destroy');
                    }
                });
                self.uploadVideo();
                //self.trigger('chatVideoOptions');
            });

        },
        uploadVideo: function() {
            var self = this;
            $('.attachVideo').on('click', function() {
                $('.uploader_video').click();
            });
            $('.uploader_video').on('change', function() {
                self.upload($(this));
                self.listenToOnce(self.model, 'change:attachment', function() {
                    this.trigger('Communication:Friends:upload:video');
                });
            });
        },
        upload: function(e) {
            self = this;
            var file = e[0].files[0];
            self.model.readFile(file);
        },
        sendMessage: function() {
            var self = this;
            if (self.$el.find('.messageWritter').val() != '') {
                var message = self.$el.find('.messageWritter').val();
                var date = moment().format('MMMM Do YYYY, h:mm:ss a');
                self.$el.find('.messageWritter').val('');

                self.refreshIScroll();
                self.trigger('sendMessage', message);
            }
        },
        clearContaine: function() {
            this.$el.find('.liveMessages').html('');
        },
        appendMessage: function(message) {
            var self = this;
            if (message.get('attachment') != '') {
                var fDate = moment(message.get('messageid')[0].send_on, 'YYYY-MM-DDTH:mm:ss').format('MMMM Do YYYY, h:mm:ss a');
                var colorWho;
                if (message.get('created_by') == GC.sessionStorage.get('userData').userName) {
                    colorWho = 'response';
                }
                if (message.get('created_by') == 'jsmith') {
                    self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div  class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/JamesSmith.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle ' + colorWho + '">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><video  style="pointer-events: none; width: 300px; height: 200px;" id="player_' + message.get('id') + '"><source class="videoTag" src="' + GC.api.url.replace('api', '') + "static/test_" + message.get('id') + '.mp4' + '"/></video></div><div id="delControl_' + message.get('id') + '" style="margin-bottom:20px" class="videoControls"> <i id="play_' + message.get('id') + '" class="icomoon-play vidControl"></i> <i id="pause_' + message.get('id') + '" class="icomoon-pause vidControl"></i> </div></div>');
                }
                else
                {
                    self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/sample_student1.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle ' + colorWho + '">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><video  style="pointer - events: none; width: 300px; height: 200px; " id="player_' + message.get('id') + '"><source class="videoTag" src="' + GC.api.url.replace('api', '') + "static/test_" + message.get('id') + '.mp4' + '"/></video></div><div  id="delControl_' + message.get('id') + '"  style="margin-bottom:20px" class="videoControls"> <i id="play_' + message.get('id') + '" class="icomoon-play vidControl"></i> <i id="pause_' + message.get('id') + '" class="icomoon-pause vidControl"></i> </div></div>');
                }


                var player = this.$el.find('#player_' + message.get('id'));
                player[0].load();
            }
            else if (message.get('messageid')[0].send_on != '') {
                if (this.tmplOpt == 'chat2') {
                    var fDate = moment(message.get('messageid')[0].send_on, 'YYYY-MM-DDTH:mm:ss').format('MMMM Do YYYY, h:mm:ss a');
                    if (message.content != '') {
                        var date = '';
                        if (message.get('created_by') == GC.sessionStorage.get('userData').userName) {

                            if (message.get('created_by') == 'jsmith')
                                self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/JamesSmith.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle response">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><div style="margin-top:15px!important" class="response row-fluid descrTitle"><div class="span12 response pull-left chatInfo"><span class="span11 response descr pull-right" style="margin-top: 0px;">' + '"' + message.get('content') + '"' + '</span></div></div></div>');
                            else
                                self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/sample_student1.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle response">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><div style="margin-top:15px!important" class="response row-fluid descrTitle"><div class="span12 response pull-left chatInfo"><span class="span11 response descr pull-right" style="margin-top: 0px;">' + '"' + message.get('content') + '"' + '</span></div></div></div>');
                        }
                        else {
                            if (message.get('created_by') == 'jsmith')
                                self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/JamesSmith.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><div style="margin-top:15px!important" class="row-fluid descrTitle"><div class="span12 pull-left chatInfo"><span class="span11 descr pull-right" style="margin-top: 0px;">' + '"' + message.get('content') + '"' + '</span></div></div></div>');
                            else
                                self.$el.find('.liveMessages').append('<div id=delWrap_' + message.get('id') + '><div class="imgContainer"><img id="img_2" class="img-circle userImg friendImgSmall iscrollPassThrough" src="imgs/sample_student1.png" alt="friend-image"> </div><span class="span9 pull-left descrTitle">' + message.get('description') + ' <a id="del_' + message.get('id') + '"  class="pull-right deleteMess"><i class="icomoon-close"></i></a><br>' + fDate + '</span><div style="margin-top:15px!important" class="row-fluid descrTitle"><div class="span12 pull-left chatInfo"><span class="span11 descr pull-right" style="margin-top: 0px;">' + '"' + message.get('content') + '"' + '</span></div></div>');
                        }
                    }
                }
            }
            this.videoControl();
            self.refreshIScroll();
            self.scroller = self.getScroller('chatScroller');
            self.count = self.scroller.maxScrollY - 70;
            self.scroller.scrollTo(0, self.count, 200);
            $(".deleteMess").unbind("click");
            $('.deleteMess').on('click', function() {
                self.trigger('deleteMessage ', this.id.replace('del_', ''));
                var sel = this.id.replace('del_', '');
                $('#delWrap_' + this.id.replace('del_', '')).addClass('animated fadeOut');
                setTimeout(function() {
                    $('#delWrap_' + sel).remove();
                    $('#delControl_' + sel).remove();
                }, 300);
                self.refreshIScroll();
            });
        },
        videoControl: function() {
            var self = this;
            this.$el.find('.vidControl').on('click', function() {
                $('.vidControl').css('color', '#fff');
                $(this).css('color', '#f1563f');
                if ($(this)[0].id.indexOf("play") >= 0) {
                    self.$el.find('#player_' + $(this)[0].id.replace('play_', '')).get(0).play();
                }
                else if ($(this)[0].id.indexOf("pause") >= 0) {
                    self.$el.find('#player_' + $(this)[0].id.replace('pause_', '')).get(0).pause();
                }
            });
        },
        expandView: function() {
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.chat', chat: this.chat});
            this.trigger('showAllMesseges');
            this.close();
        }
    });

    return View;

});
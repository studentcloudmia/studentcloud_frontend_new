// others view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/other.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'other animated fadeIn',
        initialize: function() {
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.listenTo(vent, 'Communication:Friends:Collapseview', this.collapseView);
        },
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.icomoon-info').on('click', function() {
                vent.trigger('Communication:Friends:officeInfoModal', this.id);
            });
        },
        expandView: function() {
            this.$el.addClass('hidden');
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.subMenuOthers'});
        },
        collapseView: function() {
            this.$el.removeClass('hidden');
        }
    });
    return View;

});
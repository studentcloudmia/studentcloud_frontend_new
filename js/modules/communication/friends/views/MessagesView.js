// messages view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/messages.tmpl',
    'tpl!modules/communication/friends/templates/messages2.tmpl'
], function(_, Marionette, vent, App, GC, template, messages2) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'messages animated fadeIn',
        chatFilter: false,
        videoFilter: false,
        emailFilter: false,
        initialize: function(options) {
            var self = this;
            if (GC.sessionStorage.get('userData').userName == 'jsmith') {
                this.template = messages2;
            }
            if (options) {
                if (options.viewAs == 'expanded') {
                    self.isExpanded = true;
                }
            }

            this.listenTo(vent, 'Communication:Friends:Collapseview:On', function() {
                self.$el.addClass("hidden");
            });
            this.listenTo(this, 'filterApplied', function() {
                _.each(self.$el.find('.iconFilters'), function(item) {
                    if ($('#' + item.id).hasClass('selectedFont')) {
                        $('.' + item.id).removeClass('displayNone animated fadeOut');
                        $('.' + item.id).addClass('animated fadeIn');
                    }
                    else {
                        $('.' + item.id).removeClass('displayNone animated fadeIn');
                        $('.' + item.id).addClass('animated fadeOut displayNone');
                    }
                });
            });
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.listenTo(vent, 'Communication:Friends:Collapseview', function() {
                this.$el.removeClass('hidden');
                self.isExpanded = false;
            });
        },
        onShow: function() {
            var self = this;
            this.$el.find('.circleMenu_1').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_2').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_3').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });
            this.$el.find('.circleMenu_4').circleMenu({
                item_diameter: 35,
                pointer_diameter: 20,
                circle_radius: 105,
                trigger: 'click',
                angle: {start: 130, end: 230}
            });

            //Sow circle manu bubbles
            this.$el.find('.userImg').on('click', function() {
                var currID = this.id.replace('img_', '');
                self.$el.find('#showBubbles_' + currID).trigger('click');
            });
            this.$el.find('.iconFilters').on('click', function() {
                if ($(this).hasClass('selectedFont')) {
                    $(this).removeClass('selectedFont');
                    $(this).addClass('notSelected');
                    self.chatFilter = true;
                }
                else {
                    $(this).addClass('selectedFont');
                    $(this).removeClass('notSelected');
                    self.chatFilter = false;
                }
                self.trigger('filterApplied');
            });

            this.$el.find('.viewChat').on('click', function(e) {
                if (self.isExpanded) {
                    vent.trigger('viewChat:Expanded', e);
                }
                else {
                    self.trigger('viewChat', e);
                }
            });
        },
        expandView: function() {
            this.isExpanded = true;
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.chat'});
        }
    });

    return View;

});
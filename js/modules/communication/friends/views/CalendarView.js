// calendar view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'modules/communication/friends/views/CalendarItemView',
    'tpl!modules/communication/friends/templates/calendar.tmpl'
], function(_, Marionette, vent, App, GC, CalendarItemView, template) {

    var View = App.Views.CompositeView.extend({
        template: template,
        itemView: CalendarItemView,
        itemViewContainer: '.friedsList',
        initialize: function() {
            var self = this;
            this.listenTo(this, 'childview:getFriendSchedule', this.triggerGetSchedule, this);
            this.listenTo(this, 'childview:removeFriendSchedule', this.triggerRemoveSchedule, this);
            this.listenTo(vent, 'Communication:Friends:Collapseview', this.collapseView);
            this.listenTo(vent, 'Communication:Friends:Expandedview', this.expandView);
            this.listenTo(vent, 'Communication:Friends:Collapseview:On', function() {
                self.$el.addClass("hidden");
                console.log('self.$el ', self.$el)
            });
        },
        triggerGetSchedule: function(view) {
            this.trigger('getFriendSchedule', view.model.get('friend'));
        },
        triggerRemoveSchedule: function(view) {
            this.trigger('removeFriendSchedule', view.model.get('friend'));
        },
        expandView: function() {
            this.$el.addClass('hidden');
            vent.trigger('Communication:Friends:Expandedview:From', {id: '.calendar'});
        },
        collapseView: function() {
            this.$el.removeClass('hidden');
        }
    });

    return View;

});
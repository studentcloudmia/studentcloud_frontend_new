// header view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/communication/friends/templates/header.tmpl',
    'tpl!modules/communication/friends/templates/header2.tmpl'
], function(_, Marionette, vent, App, GC, template, header2) {

    var View = App.Views.ItemView.extend({
        template: template,
        className: 'row-fluid',
        initialize: function(options) {
            this.curPage = options.curr;
            if (GC.sessionStorage.get('userData').userName == 'jsmith')
            {
                this.template = header2;
            }

        },
        onShow: function() {
            var self = this;
            if (self.curPage.indexOf("sub") >= 0) {
                this.$el.find('.' + self.curPage).addClass('text-black');
            }
            else {
                this.$el.find('.' + self.curPage).addClass('text-orange');
                if (self.curPage == 'home')
                    $('#network').attr('src', 'imgs/logoOrange.png');
            }

            this.$el.find('.iconLink').on('click', function() {
                if ($(this).hasClass('submenu'))
                    self.chatSubMenuShow();
                else
                    self.chatSubMenuHide();
                self.$el.find('.iconLink').removeClass('text-orange');
                $('.subMenu').removeClass('text-black');
                $('#network').attr("src", 'imgs/SC Icon.png');
                $(this).attr("src", 'imgs/logoOrange.png');
                $(this).addClass('text-orange');
                self.trigger('headerLink', $(this)[0].id);
            });
            _.each(this.$el.find('.iconLink'), function(item) {
                if ($(item).hasClass('text-orange'))
                    $(item).click();
            });
            this.$el.find('.subMenuNetwork').on('click', function() {
                $('.subMenu').removeClass('text-black');
                $('.iconLink').removeClass('text-orange');
                $(this).addClass('text-black');
                self.trigger('SubMenu:networkView', this.id);
            });
            this.$el.find('.subMenuOthers').on('click', function() {
                $('.subMenu').removeClass('text-black');
                $('.iconLink').removeClass('text-orange');
                $(this).addClass('text-black');
                self.trigger('SubMenu:otherView', this.id);
            });
            this.$el.find('.subMenuClasses').on('click', function() {
                $('.subMenu').removeClass('text-black');
                $('.iconLink').removeClass('text-orange');
                $(this).addClass('text-black');
                self.trigger('SubMenu:classesView', this.id);
            });
            this.$el.find('.subMenuAdvisor').on('click', function() {
                $('.subMenu').removeClass('text-black');
                $('.iconLink').removeClass('text-orange');
                $(this).addClass('text-black');
                self.trigger('SubMenu:advisorView', this.id);
            });



        },
        chatSubMenuHide: function() {
            this.$el.find('.subMenu').addClass('hidden');
        },
        chatSubMenuShow: function() {
            this.$el.find('.subMenu').removeClass('hidden');
        }
    });

    return View;

});
// friends sub module
define(['marionette',
    'app',
    'vent',
    //views
    'modules/communication/views/layout',
    //controllers
    'modules/communication/friends/controllers/HeaderController',
    'modules/communication/friends/controllers/HomeController',
    'modules/communication/friends/controllers/CalendarController',
    'modules/communication/friends/controllers/FooterController',
    'modules/communication/friends/controllers/MessagesController',
    'modules/communication/friends/controllers/ExpandController',
    'modules/communication/friends/controllers/PrivacyController',
    'modules/communication/friends/controllers/NetworkController',
    'modules/communication/friends/controllers/OthersController',
    'modules/communication/friends/controllers/ClassesController',
    'modules/communication/friends/controllers/AdvisorController',
    'modules/communication/friends/controllers/SettingsController'
],
        function(Marionette, App, vent, LayoutView,
                //controllers
                HeaderController, HomeController, CalendarController,
                FooterController, MessagesController, ExpandController, PrivacyController, NetworkController,
                OthersController, ClassesController, AdvisorController, SettingsController) {

            var FriendsModule = App.module('Communication.Friends');

            FriendsModule = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = new LayoutView();
                    this.showWho = options.show;
                    this.region = options.region;
                    this.region.show(this.layout);
                    switch (this.showWho) {
                        case 'home':
                            self.homeController = new HomeController({region: self.layout.mainContent});
                            self.curLink = "home";
                            break;
                        case 'calendar':
                            self.calendarController = new CalendarController({region: self.layout.mainContent});
                            self.curLink = "calendar";
                            break;
                    }

                    this.listenTo(vent, 'Communication:Friends:CloseHeader', function() {
                        self.headerController.view.close();
                    });

                    this.listenTo(vent, 'Communication:Friends:OpenHeader', function() {
                        self.headerController = new HeaderController({region: this.layout.header, curr: this.curLink});
                        this.headerListeners();
                    });

                    this.headerController = new HeaderController({region: this.layout.header, curr: this.showWho});
                    this.footerController = new FooterController({region: this.layout.footer, curr: this.showWho});
                    this.headerListeners();
                    this.footerListeners();
                    this.listenTo(vent, 'Communication:Friends:Show:side', function() {
                        self.messagesController = new MessagesController({region: self.layout.mainContent, viewAs: true});
                        self.curLink = 'chat';
                    });
                    this.listenTo(vent, 'Communication:Friends:Show:settings', this.settings);
                    this.listenTo(vent, 'Communication:Friends:Show:privacy', this.privacyView);
                },
                headerListeners: function() {
                    this.listenTo(this.headerController, 'headerLink', this.switchTo);
                    this.listenTo(this.headerController, 'expandView', this.expandView);
                    this.listenTo(this.headerController, 'collapseView', this.collapseView);
                    this.listenTo(this.headerController, 'SubMenu:networkView', this.networkView);
                    this.listenTo(this.headerController, 'SubMenu:otherView', this.otherView);
                    this.listenTo(this.headerController, 'SubMenu:classesView', this.classesView);
                    this.listenTo(this.headerController, 'SubMenu:advisorView', this.advisorView);
                },
                footerListeners: function() {
                    this.listenTo(this.footerController, 'expandView', this.expandView);
                    this.listenTo(this.footerController, 'collapseView', this.collapseView);
                    this.listenTo(this.footerController, 'privacyView', this.privacyView);
                    this.listenTo(this.footerController, 'settings', this.settings);
                },
                expandView: function() {
                    console.log('who ', this.showWho)
                    this.expandController = new ExpandController({region: this.layout.expand, curr: this.showWho});
                    vent.trigger('Communication:Friends:Expandedview');
                },
                collapseView: function() {
                    if (this.expandController)
                        this.expandController.view.exit();
                    vent.trigger('Communication:Friends:Collapseview');
                },
                privacyView: function() {
                    this.privacyController = new PrivacyController({region: this.region});
                },
                settings: function() {
                    this.settingsController = new SettingsController({region: this.region});
                },
                networkView: function(link) {
                    var self = this;
                    this.curLink = link;
                    this.networkController = new NetworkController({region: this.layout.mainContent});
                },
                otherView: function(link) {
                    var self = this;
                    this.curLink = link;
                    this.othersController = new OthersController({region: this.layout.mainContent});
                },
                classesView: function(link) {
                    var self = this;
                    this.curLink = link;
                    this.slassesController = new ClassesController({region: this.layout.mainContent});
                },
                advisorView: function(link) {
                    var self = this;
                    this.curLink = link;
                    this.advisorController = new AdvisorController({region: this.layout.mainContent});
                },
                switchTo: function(link) {
                    var self = this;
                    this.curLink = link;
                    switch (link) {
                        case 'network':
                            self.homeController = new HomeController({region: self.layout.mainContent});
                            break;
                        case 'calendar':
                            self.calendarController = new CalendarController({region: self.layout.mainContent});
                            break;
                        case 'chat':
                            self.messagesController = new MessagesController({region: self.layout.mainContent});
                            break;
                    }

                },
                onClose: function() {
                    this.layout.close();
                }

            });

            return FriendsModule;
        });
define([
    'marionette',
    'app',
    'tpl!modules/communication/templates/layout.tmpl'
],
        function(Marionette, App, template) {
            "use strict";
            var LayoutView = App.Views.Layout.extend({
                template: template,
                className: 'container animated fadeInRight',
                regions: {
                    header: "#communicationHeader",
                    mainContent: ".mainContent",
                    footer: "#communicationFooter",
                    expand: '#expandedView'
                },
                onShow: function() {
                    console.log('showing layout')
                    $('#communications').css({'border': '1px solid #fff'});
                },
                onClose: function() {
                    $('#communications').css({'border': 'none'});
                }

            });
            return LayoutView;
        });

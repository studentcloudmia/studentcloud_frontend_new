// AppController.js
define([
'marionette',
'app',
],
function(Marionette, app, LoginItemView, DashboardLayoutManager, SidebarLayoutManager) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'login': 'login',
            '': 'defaultRoute',
            //'*actions': 'catchAll'
        }
    });

    return Router;

});
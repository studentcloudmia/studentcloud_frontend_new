define([
'marionette',
'app',
'vent',
'globalConfig',
'modules/access/login/login_controller',
'modules/access/logout/logout_controller',
'modules/access/statusAgent/status_agent_controller',
//check login model
'entities/models/access/CheckLoginModel'
],
function(Marionette, App, vent, GC, AccessLoginModule, AccessLogoutModule, StatusAgentModule, CheckLoginModel) {

    var Controller = Marionette.Controller.extend({

        initialize: function(options) {
            //initialize the status agent and start listening to HTTP status
            new StatusAgentModule.Controller();
			//initialize the logout controller to start listening for requests
			new AccessLogoutModule.Controller();
            
            //listen to vent events to navigate
            this.listenTo(vent, 'Access:Login:Show', function(){
                App.navigate('login');
                this.login();
            });
        },

        login: function() {
			vent.trigger('Sidebar:Off');
            //transfer control to show submodule
			new AccessLoginModule.Controller({ region: App.main });
        },

		defaultRoute: function(){
			//var model = new CheckLoginModel();
			//if(model.isLoggedIn()){
            if(GC.sessionStorage.isSet('userData')){
				//user is logged in. Go to dashboard
				vent.trigger('dashboard:show');
			}else{
				//user is not logged in go to login page
				vent.trigger('Access:Login:Show');
			}
		},
		
		catchAll: function(){
			console.log('CatchAll Route');
			this.defaultRoute();			
		}

    });

    return Controller;

});
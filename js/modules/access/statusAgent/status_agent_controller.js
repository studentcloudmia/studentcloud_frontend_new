// access module
define([
'marionette',
'app',
'vent'
],
function(Marionette, App, vent, Clouds, LoginView, LoginModel) {

    var StatusAgentModule = App.module('Access.StatusAgent');

    StatusAgentModule.Controller = Marionette.Controller.extend({

        initialize: function(options) {
            //listen to status codes and act appropriately
            
            this.listenTo(vent, 'Sync:Code:403', this.forbidden);
            this.listenTo(vent, 'Sync:Code:406', this.notAcceptable);
            this.listenTo(vent, 'Sync:Code:500', this.serverError);
        },
        
        forbidden: function(data){
            //only show error if not muted
            if(! data.options.muteError){
                vent.trigger('Components:Alert:Alert', {
                    heading: 'Forbidden',
                    message: 'You are currently forbidden from accessing this resource.'
                });
                vent.trigger('Access:Login:Show');
            }
        },
        
        serverError: function(data){
            if(! data.options.muteError){
                vent.trigger('Components:Alert:Alert', {
                    heading: 'API Server Error',
                    message: 'An error was encountered with the API Server.'
                });
            }
        },
        
        notAcceptable: function(data){
            //only show error if not muted
            if(! data.options.muteError){
                var errors = $.parseJSON(data.jqXHR.responseText).error_code;
                vent.trigger('Components:Alert:Alert', {
                    heading: 'Error',
                    message: this.JSONtoList(errors)
                });
            }
        },
        
        JSONtoList: function(data){
            var list = '<ul>';
            if(typeof data !== 'object'){
                list = list + '<li>'+data+'</li>';
            }else{
                _.each(data, function(elem, index){
                    list = list + '<li><strong>'+index+':</strong> '+elem+'</li>';
                });
            }            
            return list + '</ul>';
        }

    });

    return StatusAgentModule;
});
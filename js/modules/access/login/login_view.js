// login view
define([
    'marionette',
    'app',
    'tpl!modules/access/login/template/login.tmpl'
], function(Marionette, App, template) {

    var LoginView = App.Views.ItemView.extend({
        template: template,
        tagName: "div",
        className: "logInWrapper",
        
        initialize: function(){
            $('.logoImage').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
            });
        },
        
        ui: {
            username: '.username',
            password: '.password'
        },
        triggers: {
            'submit': 'login:submit',
            'click .FBLoginBtn': 'FBLogin',
            'click .apply': 'gotoApplication'
        },
        clearPassword: function() {
            this.ui.password.val('');
        },
        onClose: function(){
            $('.logoImage').off();
        }


    });

    return LoginView;

});
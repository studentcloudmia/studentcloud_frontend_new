// access module
define([
    'marionette',
    'app',
    'vent',
    'globalConfig',
//clouds background
    'clouds',
//views
    'modules/access/login/login_view',
//models
    'entities/models/access/LoginModel'
],
        function(Marionette, App, vent, GC, Clouds, LoginView, LoginModel) {

            var AccessLoginModule = App.module('Access.Login');

            AccessLoginModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {

                    //setup clouds
                    //Clouds.initialize();
                    //App.setBackground('backgrounds/nursing.jpg');
                    //App.setBackgroundColor('#f1563f');
                    App.setBackground('backgrounds/blackboard3.jpg');

                    //save region where we can show
                    this.region = options.region;
                    //init model to be used
                    this.model = new LoginModel();
                    this.model.fetch({}, {}, {}, false);
                    //init view
                    this.loginView = new LoginView({
                        model: this.model
                    });
                    //listen to close event to close self
                    this.listenTo(this.loginView, 'close', function() {
                        this.close();
                    }, this);
                    //bind to view and model events
                    this.listenTo(this.loginView, 'login:submit', this.loginSubmit, this);
                    this.listenTo(this.loginView, 'FBLogin', this.FBGetLoginStatus, this);
                    this.listenTo(this.loginView, 'gotoApplication', function() {
                        App.navigate('application', {trigger: true});
                    }, this);

                    this.listenTo(this.model, 'created', this.modelSynced, this);
                    //show our view
                    this.region.show(this.loginView);
                    //setup login with facebook
                    this.setupFBLogin();
                },
                //facebook setup
                setupFBLogin: function() {
                    window.fbAsyncInit = function() {
                        FB.init({
                            appId: '517804084949789',
                            channelUrl: '//http://ec2-54-225-119-193.compute-1.amazonaws.com:8080/fb_channel.html',
                            status: true,
                            cookie: true,
                            xfbml: true
                        });
                    };

                    // Load the SDK Asynchronously
                    (function(d) {
                        var js,
                                id = 'facebook-jssdk',
                                ref = d.getElementsByTagName('script')[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js = d.createElement('script');
                        js.id = id;
                        js.async = true;
                        js.src = "//connect.facebook.net/en_US/all.js";
                        ref.parentNode.insertBefore(js, ref);
                    }(document));
                },
                FBGetLoginStatus: function() {
                    var self = this;
                    //first test if popup blocker is active
                    //if(!App.popupBlockerActivated()){
                    FB.getLoginStatus(function(response) {
                        if (response.status === 'connected') {
                            // connected
                            var accessToken = response.authResponse.accessToken;
                            self.attemptLogin({
                                access_token: accessToken,
                                login_type: "Facebook"
                            });
                            //make API call to log them in
                        } else {
                            // not_logged_in
                            self.FBLogin();
                        }
                    }, true);
                    // }else{
                    //     vent.trigger('Components:Alert:Alert', {
                    //         heading: 'Popup Blocked',
                    //         message: 'Your browser has popup blocker activated. Please deactivate the popup blocker.'
                    //     });
                    // }
                },
                FBLogin: function() {
                    var self = this;
                    FB.login(function(response) {
                        if (response.authResponse) {
                            // connected
                            var accessToken = response.authResponse.accessToken;
                            self.attemptLogin({
                                access_token: accessToken,
                                login_type: "Facebook"
                            });
                        } else {
                            // cancelled
                        }
                    });
                },
                attemptLogin: function(options) {
                    this.model.clear();
                    this.model.save(options);
                },
                loginSubmit: function() {
                    var data = Backbone.Syphon.serialize(this.loginView);
                    //do not post, send directly to dashboard
                    //vent.trigger('dashboard:show');

                    this.loginView.clearPassword();
                    //set data in model and save
                    this.model.clear({
                        silent: true
                    }).save(data);
                },
                modelSynced: function() {
                    var self = this;
                    //if we received ID then login was a success
                    if (this.model.has('first_name')) {
                        this.loggedInAs = this.model.get('username');
                        var userData = {userName: this.loggedInAs, first_name: this.model.get('first_name'), last_name: this.model.get('last_name')};
                        GC.sessionStorage.set('userData', userData);
                        vent.trigger('dashboard:show');
                        // App.navigate('dashboard');
                    }
                    //we need to match the external acct with an internal one
                    else if (this.model.get('matched') == false) {
                        vent.trigger('Components:Alert:Login', {
                            heading: 'Match Accounts',
                            message: 'We need to match your ' + self.model.get('login_type') + ' account with our internal account.',
                            login_type: this.model.get('login_type'),
                            callback: self.matchAccts,
                            callee: self
                        });
                    } else if (this.model.get('external_login_check') == false) {
                        //have user login with external source
                        self.FBGetLoginStatus();
                    }
                    else {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Oops',
                            message: this.model.get('error_code')
                        });
                        this.model.clear({silent: true});
                    }
                },
                matchAccts: function(username, password, login_type) {
                    this.attemptLogin({
                        access_token: this.model.get('access_token'),
                        login_type: login_type,
                        username: username,
                        password: password
                    });
                },
                onClose: function() {
                    //Clouds.close();
                    App.setBackground();
                }

            });

            return AccessLoginModule;
        });
// access module
define([
'marionette',
'app',
'vent',
'globalConfig',
//models
'entities/models/access/LogoutModel'
],
function(Marionette, App, vent, GC, LogoutModel) {

    var AccessLogoutModule = App.module('Access.Logout');

    AccessLogoutModule.Controller = Marionette.Controller.extend({

        initialize: function() {
			this.listenTo(vent, 'Logout:Error', this.logoutError);
			//add application listener for logout
			App.commands.setHandler("Logout", function(){
            	//init model to be used
            	var model = new LogoutModel();
				var resp = model.logout();
                //unset session variable
                GC.sessionStorage.remove('userData');
				App.navigate('login', {trigger: true});
			});
        },
        
        logoutError: function(){
            App.navigate('login', {trigger: true});
        },

		onClose: function(){
			App.commands.removeHandler('Logout');
		}

    });

    return AccessLogoutModule;
});
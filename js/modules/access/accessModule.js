// access module
define([
'marionette',
'app',
'modules/access/router',
'modules/access/controller'
],
function(Marionette, App, Router, Controller) {

    var AccessModule = App.module('Access');

    // AccessModule.startWithParent = false;

    //bind to module finalizer event
    AccessModule.addFinalizer(function() {
        
    });

	AccessModule.addInitializer(function(options) {
		new Router({
			controller: new Controller()
		});
	});

    return AccessModule;
});
// dashboard map module
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/dashboard/map/views/layout'
    ],
    function(Marionette, App, vent,
        //views
        Layout
        ) {

        var DashboardMapModule = App.module('Dashboard.Map');

        DashboardMapModule.Controller = Marionette.Controller.extend({
            initialize: function(options) {
                //save region where we can show
                this.region = options.region;
                this.layout = new Layout();

                this.listenTo(this.layout, 'close', function(){
                    this.close();
                }, this);
                
                //listen to vent BaseRouter:route and close
                this.listenTo(vent, 'BaseRouter:route', function(){
                    this.close();
                }, this);

                this.listenTo(this.layout, 'show', function(){
                    //hide App.main
                    App.main.$el.hide();
                    //make map the new background
                    App.setBackground('mapImage2.jpg');
                    App.clearBackgroundOpacity();
                    
                    //display header
                    this.showHeader();
                }, this);

                this.region.show(this.layout);

				//ask for blank sidebar
				vent.trigger('Sidebar:On', { blank: true });
            },
        
            showHeader: function() {
                //start student header controller
                var options = {
                    region: this.layout.header
                };

                vent.trigger('Components:Student:Header:on', options);
            },
            
            onClose: function(){
                //ensure we reset the region to clean DOM
                this.region.reset();
                //revert background image
                App.setBackground();
                //show App.main
                App.main.$el.show();
            }

        });

        return DashboardMapModule;
    });
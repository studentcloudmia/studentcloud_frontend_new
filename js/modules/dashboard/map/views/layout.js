define(['marionette', 
        'app', 
        'tpl!modules/dashboard/map/template/layout.tmpl'], 
        function(Marionette, App, template) {
            "use strict";

            var LayoutView = App.Views.Layout.extend({
                template: template,
                
                EntranceAnimClass: 'fadeInDown',

                regions: {
                    header: '.mapHeader',
                    mapIconsArea: '.mapIconsArea',
                    mapArea: '.mapArea'
                },
                
                ui:{
                    classWidg: '.mapWidg1',
                    friendWidg: '.friendWidg'
                },
                
                //TODO: this code needs to be placed in a view
                events: {
                    'click .cal': 'calSel',
                    'click .friends': 'friendsSel',
                    'click .fave': 'faveSel',
                    'click .findMe': 'findMeSel'
                },
                
                onShow: function() {
                    //drop menus in one by one
                    var self = this;
                    var interval = 200;
                    _.each(this.$el.find('.circleBack'), function(menu) {
                        // menu.removeClass('hidden');
                        setTimeout(function() {
                            $(menu).removeClass('hidden').addClass(self.EntranceAnimClass);
                        }, interval);
                        interval = interval + 100;
                    });
                    
                    this.$el.find('.circleMenu_mapWidg1').circleMenu({
                        item_diameter: 35,
                        pointer_diameter: 40,
                        circle_radius: 90,
                        trigger: 'click',
                        angle: {start: 140, end: 220}
                    });
                    
                    _.each([2,3,4], function(friendNum){
                        self.$el.find('.circleMenu_mapWidg'+friendNum).circleMenu({
                            item_diameter: 35,
                            pointer_diameter: 40,
                            circle_radius: 60,
                            trigger: 'click',
                            angle: {start: 140, end: 220}
                        });
                    });
                    
                    this.bindMenuClicks();
                },
                
                toggleCalMenu: function(){
                    this.$el.find('#showBubbles_mapWidg1').trigger('click');
                    //this.$el.find('.circleMenu_mapWidg1').circleMenu('open');
                },
                
                toggleFriendMenu: function(friendNum){
                    this.$el.find('#showBubbles_mapWidg'+friendNum).trigger('click');
                },
                
                calSel: function(){
                    var className = '.cal';
                    this.removeElems();
                    this.selectButton(className);
                    if(this.selected(className)){
                        //ensure we hide the menu
                        this.$el.find('.circleMenu_mapWidg1').circleMenu('close');
                        this.ui.classWidg.removeClass('hidden').addClass(this.EntranceAnimClass);
                    }
                },
                
                friendsSel: function(){
                    var className = '.friends';
                    this.removeElems();
                    this.selectButton(className);  
                    if(this.selected(className)){
                        //ensure we hide the menu
                        this.$el.find('.circleMenu_mapWidg2').circleMenu('close');
                        this.ui.friendWidg.removeClass('hidden').addClass(this.EntranceAnimClass);
                    }             
                },
                
                faveSel: function(){
                    var className = '.fave';
                    this.removeElems();
                    this.selectButton(className); 
                    if(this.selected(className)){
                        console.warn('not implemented yet');
                    }             
                },
                
                findMeSel: function(){
                    var className = '.findMe';
                    this.removeElems();
                    this.selectButton(className);  
                    if(this.selected(className)){
                        console.warn('not implemented yet');
                    }             
                },
                
                selectButton: function(className){
                    if(this.selected(className)){
                        this.$el.find('.circleBack').removeClass('selected');
                    }else{
                        this.$el.find('.circleBack').removeClass('selected');
                        this.$el.find(className).addClass('selected');
                    }                    
                },
                
                selected: function(className){
                    return this.$el.find(className).hasClass('selected');
                },
                
                removeElems: function(){
                    this.ui.classWidg.removeClass(this.EntranceAnimClass).addClass('hidden');
                    this.ui.friendWidg.removeClass(this.EntranceAnimClass).addClass('hidden');
                },
                
                bindMenuClicks: function() {
                    var self = this; 
                    /* cal clicks */
                    this.$el.find('.mapWidg1 > .header').on('click', function(e) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        e.stopPropagation();
                        self.toggleCalMenu();
                    });
                    this.$el.find('.mapWidg1 > .body').on('click', function(e) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        e.stopPropagation();
                        self.toggleCalMenu();
                    });
                    
                    /* friends click */
                    _.each([2,3,4], function(friendNum){
                        self.$el.find('.mapWidg'+friendNum+' > .header').on('click', function(e) {
                            e.stopImmediatePropagation();
                            e.preventDefault();
                            e.stopPropagation();
                            self.toggleFriendMenu(friendNum);
                        });
                        self.$el.find('.mapWidg'+friendNum+' > .body').on('click', function(e) {
                            e.stopImmediatePropagation();
                            e.preventDefault();
                            e.stopPropagation();
                            self.toggleFriendMenu(friendNum);
                        });
                        
                    });
                }

            });
    return LayoutView;
    });

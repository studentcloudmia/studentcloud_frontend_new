// map view
define([
    'marionette', 
    'app',
    'tpl!modules/dashboard/map/template/map.tmpl'
    ],function(Marionette, App, template){
	
        var MapItemView = App.Views.ItemView.extend({
            template: template
        });
	
        return MapItemView;
	
    });
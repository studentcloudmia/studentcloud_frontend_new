
define([
'marionette',
'app',
],
function(Marionette, app, LoginItemView, DashboardLayoutManager, SidebarLayoutManager) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'dashboard': 'show'
        }
    });

    return Router;

});
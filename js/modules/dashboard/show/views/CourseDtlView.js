// catalog widg view
define([
    'marionette',
    'app',
    'vent',
    'moment',
    'tpl!modules/dashboard/show/template/crseDtls.tmpl'
], function(Marionette, App, vent, Moment, template) {

    var CourseDtlView = App.Views.ItemView.extend({
            template: template,
            className: 'iScrollWrapper catalogCrseDtls'
    });

    return CourseDtlView;

});
// login view
define([
    'marionette', 
    'app',
    'modules/dashboard/show/views/classSchedule_itemView',
    'tpl!modules/dashboard/show/template/classSchedule.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var ClassSchedule = App.Views.CompositeView.extend({
            itemView: ItemView,
            template: template,
            itemViewContainer: '#classSchedule'
            
        });
	
        return ClassSchedule;
	
    });
// catalog widg view
define([
    'marionette',
    'app',
    'vent',
    'moment',
    'tpl!modules/dashboard/show/template/_catalog.tmpl'
], function(Marionette, App, vent, Moment, template) {

    var CatalogItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'h5',
        className: 'text-truncate',
        
        triggers: {
            'click': 'selected'
        }
        
    });

    return CatalogItemView;

});
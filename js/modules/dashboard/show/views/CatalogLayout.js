
define(
[
'marionette', 
'app',
'tpl!modules/dashboard/show/template/catalogLayout.tmpl'
],function(Marionette, App, template) {
    "use strict";

    var CatalogLayout = App.Views.Layout.extend({
        template: template,
        className: 'catalogWidg',

        regions: {
            main: '.mainDiv'
        },
        
        ui: {
            'headerNavNoBack': '.headerNavNoBack',
            'headerNavWithBack': '.headerNavWithBack'
        },
        
        triggers: {
            // 'click .backBtn': 'back',
            'swiperight': 'back'
        },
        
        events: {
            'change .catSearch': 'search'
        },
        
        search: function(){
            this.trigger('search', this.$el.find('.catSearch').val());
        },
        
        childSelected: function(v){
            this.trigger('selected', v.model.get('value'));
        },
        
        showBackButton: function(){
            this.ui.headerNavNoBack.hide();
            this.ui.headerNavWithBack.show();
        },
        
        hideBackButton: function(){
            this.ui.headerNavNoBack.show();
            this.ui.headerNavWithBack.hide();
        }
        
    });
	return CatalogLayout;
});
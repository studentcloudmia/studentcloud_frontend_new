// login view
define([
    'marionette', 
    'app',
    'tpl!modules/dashboard/show/template/_financialAidPendingItem.tmpl'
    ],function(Marionette, App, template){
	
        var FinancialAidPendingItemView = App.Views.ItemView.extend({
            template: template,
            tagName: 'div',
            className: 'item  text-orange textalert'
                
        });
	
        return FinancialAidPendingItemView;
	
    });
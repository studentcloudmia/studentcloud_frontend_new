// login view
define([
    'marionette', 
    'app',
    'tpl!modules/dashboard/show/template/majorMap.tmpl'
    ],function(Marionette, App, template){
	
        var MajorMapView = App.Views.ItemView.extend({
            template: template
		
        });
	
        return MajorMapView;
	
    });
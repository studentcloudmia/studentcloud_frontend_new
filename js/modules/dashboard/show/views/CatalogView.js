// catalog widg view
define([
    'marionette',
    'app',
    'vent',
    'moment',
    'modules/dashboard/show/views/CatalogItemView',
    'tpl!modules/dashboard/show/template/catalog.tmpl'
], function(Marionette, App, vent, Moment, CatalogItemView, template) {

    var CatalogView = App.Views.CompositeView.extend({
            template: template,
            itemView: CatalogItemView,
            itemViewContainer: '.scroller',
            className: 'row-fluid textWhite iScrollWrapper',
            
            initialize: function(){
                this.listenTo(this, 'childview:selected', this.childSelected);
            },
            
            childSelected: function(v){
                this.trigger('selected', v.model.get('value'));
            }
    });

    return CatalogView;

});
// user search view
define([
    'marionette',
    'app',
    'vent',
    'moment',
    'tpl!modules/dashboard/show/template/search.tmpl'
], function(Marionette, App, vent, Moment, template) {

    var SearchView = App.Views.ItemView.extend({
        template: template,
        className: 'studSearchWidg',
        ui: {
            search: '#studentSearch',
            lastSearchList: '.scroller'
        },
        events: {
            'change #studentSearch': 'search'
        },
        search: function() {
            vent.trigger('AdminDashboard:Student:Search:widget', this.getSearchText());
        },
        getSearchText: function() {
            var val = $(this.ui.search).val();
            $(this.ui.search).val('');
            $(this.ui.search).blur();
            return val;
        },
        addLastSearch: function(json){
            var self = this;
            var count = 0;
                var userHtml = '';
            //add prev search
            _.each(json, function(srch){
                if(count == 0){
                    userHtml = userHtml + '<div class="row-fluid">';
                }
                userHtml = userHtml + '<div class="span6 text-truncate"><a href="#admin/student/view/'+srch.value_search+'">' + srch.value_search + ' - ' + srch.first_name + ' ' + srch.last_name + '</a></div>';
                // userHtml = '<li><a href="#admin/student/view/'+srch.value_search+'">'+srch.value_search+'</a></li>';
                

                count = count + 1;
                
                if(count == 2){
                    userHtml = userHtml + '</div>';
                    count = 0;
                }
            });     
            console.log('userHtml: ', userHtml);
            self.ui.lastSearchList.append(userHtml);       
        }
    });
    
    /*
    <div class="row-fluid">
        <div class="span6 text-truncate">
            alex22<br/>
            Felipe Diep
        </div>
        <div class="span6 text-truncate">
            alex22<br/>
            Felipe Diep
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6 text-truncate">
            alex22<br/>
            Felipe Diep
        </div>
        <div class="span6 text-truncate">
            alex22<br/>
            Felipe Diep
        </div>
    </div>
    */


    return SearchView;

});
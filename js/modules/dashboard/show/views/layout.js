
define(
[
'marionette', 
'app',
'tpl!modules/dashboard/show/template/layout.tmpl'
],function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: '.dashHeader',
            mainWidg: "#mainWidg",
            topRightWidg: "#topRightWidg",
            midRightWidg: "#midRightWidg",
            bottomRight: '#bottomRight',
            fullImage: '#fullImage'
        },
        
        events: {
            'click': 'exitFullImage',     
            'click .smallImage': 'fullScreenImage'   
        },
        
        exitFullImage: function(){
            if(this.imgLoaded){
                //remove image
                $('#fullImage').html(' ');
                /*remove backdrop*/
                $('#fullImage').css({
                    margin: '',
                    position: '',
                    top: '',
                    bottom: '',
                    left: '',
                    right: '',
                    zIndex: '',
                    backgroundColor: ''
                });
                this.imgLoaded = false;
                console.log('removed image');
            }                
        },
        
        fullScreenImage: function(e){
            var self = this;
            var $img = this.$el.find(e.currentTarget).clone(),
                imageWidth = $img[0].width, //need the raw width due to a jquery bug that affects chrome
                imageHeight = $img[0].height, //need the raw height due to a jquery bug that affects chrome
                maxWidth = $(window).width(),
                maxHeight = $(window).height(),
                widthRatio = maxWidth / imageWidth,
                heightRatio = maxHeight / imageHeight;

            var ratio = widthRatio; //default to the width ratio until proven wrong

            if (widthRatio * imageHeight > maxHeight) {
                ratio = heightRatio;
            }
            console.log('ratio: ', ratio);
            //now resize the image relative to the ratio
            $img.attr('width', imageWidth * ratio)
                .attr('height', imageHeight * ratio);

            //and center the image vertically and horizontally
            $img.css({
                margin: 'auto',
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 20
            });
            $img.removeClass('smallImage');
            
            /*add backdrop*/
            $('#fullImage').css({
                margin: 'auto',
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 19,
                backgroundColor: 'rgba(0, 0, 0, 0.60)'
            });
            
            $img.appendTo('#fullImage');
            console.log('added image');
            setTimeout(function(){
                self.imgLoaded = true;
                console.log('set imgLoaded to true');
            }, 200);
            
        }
    });
	return LayoutView;
});
// login view
define([
    'marionette', 
    'app',
    'modules/dashboard/show/views/financialAidPending_itemView',
    'tpl!modules/dashboard/show/template/faPending.tmpl'
    ],function(Marionette, App, ItemView, template){
	
        var FinancialAidPendingCompositeView = App.Views.CompositeView.extend({
            itemView: ItemView,
            template: template,
            itemViewContainer: '.carousel-inner',
            
            ui:{
                carousel: '#pendingFormsFinAid'
            },
            
            onCompositeRendered : function(){
                var self = this;
				
				//setup carousel
				self.ui.carousel.carousel({
				    interval: 3000
				});
			
				self.ui.carousel.swiperight(function() {
				    self.ui.carousel.carousel('prev');
				});
			
				self.ui.carousel.swipeleft(function() {
				    self.ui.carousel.carousel('next');
				});
            },

			onBeforeDomRemove: function() {
			    this.ui.carousel.carousel('pause');
			    this.ui.carousel.carousel = null;
			}
        });
	
        return FinancialAidPendingCompositeView;
	
    });
// login view
define([
    'marionette', 
    'app',
    'tpl!modules/dashboard/show/template/_classScheduleItem.tmpl'
    ],function(Marionette, App, template){
	
        var ClassScheduleItemView = App.Views.ItemView.extend({
            template: template,
            tagName: 'div',
            className: 'roundBox'
                
        });
	
        return ClassScheduleItemView;
	
    });
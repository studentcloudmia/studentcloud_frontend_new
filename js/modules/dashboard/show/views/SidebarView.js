// login view
define([
    'marionette', 
    'app',
    'moment',
    'tpl!modules/dashboard/show/template/sidebar.tmpl'
    ],function(Marionette, App, Moment, template){
	
        var SideView = App.Views.ItemView.extend({
            template: template
		
        });
	
        return SideView;
	
    });
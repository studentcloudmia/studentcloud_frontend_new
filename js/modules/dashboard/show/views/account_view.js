// login view
define([
    'marionette', 
    'app',
    'tpl!modules/dashboard/show/template/account.tmpl'
    ],function(Marionette, App, template){
	
        var AccountView = App.Views.ItemView.extend({
            template: template
		
        });
	
        return AccountView;
	
    });
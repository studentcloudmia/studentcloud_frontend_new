// access module
define([
    'marionette',
    'app',
    'vent',
    //controllers
    //TODO: Use correct controller
    'modules/adminDashboard/show/sidebar_controller',
    'modules/dashboard/show/catalogController',
    //views
    'modules/dashboard/show/views/layout',
    'modules/dashboard/show/views/search_view',
    //collections
    'entities/collections/user/UserCollection',
    'entities/collections/user/LastUserSearchCollection'
    ],
    function(Marionette, App, vent,
        //controllers
        SideController,
        CatalogController,
        //views
        Layout, SearchView,
        //collection
        UserCollection, LastUserSearchCollection
        ) {

        var DashboardShowModule = App.module('Dashboard.Show');

        DashboardShowModule.Controller = Marionette.Controller.extend({
            initialize: function(options) {
                //save region where we can show
                this.region = options.region;
                this.layout = new Layout();
                this.collection = new UserCollection();
                this.lastSearchColl = new LastUserSearchCollection();

                this.listenTo(this.layout, 'close', function(){
                    this.close();
                }, this);

                this.listenTo(this.layout, 'show', function(){
                    //request header
                    vent.trigger('Components:Student:Header:on', {region: this.layout.header});
                    this.showMainWidg();
                    this.showTopRightWidg();
                    this.showCatalog();

                    this.listenToOnce(this.lastSearchColl, 'reset', this.showUserSearch);
                    this.lastSearchColl.fetch({reset: true});
                    
                }, this);

                this.region.show(this.layout);

				//ask for sidebar
                // vent.trigger('Sidebar:On', { blank: true });
                vent.trigger('Sidebar:On', {controller: new SideController({collection: this.collection})});
            },
            
            showTopRightWidg: function(){
                //trigger vent for major map widget
                vent.trigger('MajorMap:widget', {region: this.layout.midRightWidg}); 
            },
            
            showMainWidg: function(){                
                vent.trigger('Academics:Schedule:widget', {region: this.layout.mainWidg});
            },

            showUserSearch: function() {
                this.searchView = new SearchView();
                this.layout.topRightWidg.show(this.searchView);
                //add last searched
                this.searchView.addLastSearch(this.lastSearchColl.toJSON());
                this.layout.refreshIScroll();
            },
            
            showCatalog: function(){
                new CatalogController({region: this.layout.bottomRight});
            }

        });

        return DashboardShowModule;
    });
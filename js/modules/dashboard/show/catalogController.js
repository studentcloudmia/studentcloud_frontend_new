// access module
define([
        'marionette',
        'app',
        'vent',
        //views
        'modules/dashboard/show/views/CatalogLayout',
        'modules/dashboard/show/views/CatalogView',
        'modules/dashboard/show/views/CourseDtlView',
        //entities
        'entities/collections/course/CourseCatalogCollection',
        'entities/models/BaseModel'
    ],
    function(Marionette, App, vent, CatalogLayout, CatalogView, CourseDtlView, CourseCatalogCollection, BaseModel) {
        CatalogController = Marionette.Controller.extend({
            initialize: function(options) {
                //save region where we can show
                this.region = options.region;
                this.collection = new CourseCatalogCollection();
                this.layout = new CatalogLayout();
                
                this.region.show(this.layout);
                
                this.callsArr = [];
                this.listenTo(this.layout, 'back', this.goBack);
                this.listenTo(this.layout, 'search', this.search);
                this.listenTo(this.collection, 'reset', this.collReset);
                
                this.showList();
                
                this.collectionFetch();
            },
            
            selectedTriggered: function(val){
                console.log('value selected: ', val);
                //add val to array and call corresponding function
                this.callsArr.push(val);
                this.collectionFetch();
            },
            
            goBack: function(){
                this.callsArr.pop();
                //fetch or show view
                if(this.callsArr.length == 2){
                    //we only need to show the view 
                    this.showList();
                }else{
                    this.collectionFetch();
                }                
            },
            
            showList: function(){
                this.view = new CatalogView({collection: this.collection});
                this.layout.main.show(this.view);
                this.listenTo(this.view, 'selected', this.selectedTriggered);
            },
            
            collectionFetch: function(){
                //letters, subjects, courses, dtls
                switch (this.callsArr.length) {
                case 0:
                    this.collection.getLetters();
                    break;
                case 1:
                    this.collection.getSubjects(this.callsArr[0]);
                    break;
                case 2:
                    this.collection.getCourses(this.callsArr[0], this.callsArr[1]);
                    break;
                case 3:
                    this.showCourseDtls();
                    break;
                default:
                    alert('Array out of bounds...');
                }
            },
            
            showCourseDtls: function(){
                var selectedCourse = this.callsArr[this.callsArr.length - 1];
                //get the model for selected course
                var courseJSON = this.collection.where({ value:  selectedCourse})[0].get('model');
                //show course detail view
                this.courseModel = new BaseModel(courseJSON);
                console.log('courseModel: ', this.courseModel);
                this.courseDtlView = new CourseDtlView({model: this.courseModel});
                this.layout.main.show(this.courseDtlView);
                this.layout.afterShow();
            },
            
            collReset: function(){
                //show back button if not in first page and coll has items
                if(this.callsArr.length > 0 && this.collection.length > 0){
                    this.layout.showBackButton();
                }else{
                    this.layout.hideBackButton();
                }
            },
            
            search: function(text){
                //tell App we want to do a catalog search
                vent.trigger('Cbuilder:searchCatalog', text);
            }

        });

        return CatalogController;
    });

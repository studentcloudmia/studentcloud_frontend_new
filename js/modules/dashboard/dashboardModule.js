// access module
define([
    'marionette',
    'app',
    'vent',
    'modules/dashboard/router',
    'modules/dashboard/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var DashboardModule = App.module('Dashboard');

        //bind to module finalizer event
        DashboardModule.addFinalizer(function() {
	
        });

        DashboardModule.addInitializer(function(options) {
		
            var controller = new Controller();
		
            new Router({
                controller: controller
            });            
		
        });

        return DashboardModule;
    });
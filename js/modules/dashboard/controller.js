define([
'marionette',
'app',
'vent',
'modules/dashboard/show/show_controller',
'modules/dashboard/map/map_controller'
],
function(Marionette, App, vent, DashboardShowModule, DashboardMapModule) {

    var Controller = Marionette.Controller.extend({

        initialize: function(options) {
			this.listenTo(vent, 'dashboard:show', function(){
                App.navigate('dashboard');
                this.show();
            }, this);
            
			this.listenTo(vent, 'dashboard:map', function(){
                this.showMap();
            }, this);
            
            this.mapShown = false;
            
        },

        show: function() {
            //transfer control to show submodule
            new DashboardShowModule.Controller({ region: App.main });
        },
        
        showMap: function(){
            if(this.mapShown){
                this.mapController.close();
            }else{
                this.mapController = new DashboardMapModule.Controller({ region: App.dashMap });
                this.mapShown = true;
                //listen to the controller close event and make sure we set mapShown to false
                this.listenToOnce(this.mapController, 'close', function(){
                    this.mapShown = false;
                });
            }
            
        }

    });

    return Controller;

});
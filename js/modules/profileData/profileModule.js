// alerts module
define([
'marionette',
'app',
'vent',
'modules/profileData/controller'
],
function(Marionette, App, vent, Controller) {

    var ProfileModule = App.module('ProfileData.Profile');

    ProfileModule.addInitializer(function() {
		this.controller = new Controller({region: App.modals});
    });

    ProfileModule.addFinalizer(function() {
		this.controller.close();
		this.controller = null;
    });

    return ProfileModule;
});
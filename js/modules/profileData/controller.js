define([
    'marionette',
    'app',
    'vent',
//controllers
    'modules/profileData/menu/menu_controller'
],
        function(Marionette, App, vent,
                //sub module
                MenuModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    this.listenTo(vent, 'Profile:Show:Overlay', this.showOverlay, this);
                },
                showOverlay: function(options) {
                    new MenuModule.Controller({opts: options, region: options.region});
                }

            });

            return Controller;

        });
// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
    //views
    'modules/profileData/menu/views/profile_view',
    //collections
    'entities/collections/dictionary/DictionaryCollection'

],
        function(Marionette, App, vent, ProfileView, DictionaryCollection) {

            var ProfileController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.region = options.region;
                    //set historical collection to what was passed by in options
                    this.historicalCol = options.collection;
                    this.modelOldeff_date;
                    //create multiple new instances of dictionary
                    this.prefix = new DictionaryCollection();
                    this.suffix = new DictionaryCollection();
                    this.mStatus = new DictionaryCollection();
                    this.gender = new DictionaryCollection();
                    //use collection that was passed to get models
                    this.historicPassed();
                },
                //Get the current model from the collection passed:
                historicPassed: function() {
                    //get the model with status 'CURRENT' from the passed historical collection
                    this.model = this.historicalCol.where({Status: "CURRENT"})[0];
                    //update this.modelOldeff_date
                    this.modelOldeff_date = this.model.get('eff_date');
                    //call selectedModel() and pass the model id of this.model
                    this.selectedModel({modelId: this.model.getId()});
                },
                selectedModel: function(options) {
                    //if model id is passed and eff_date is null get model from collection by ID ( used for regular)
                    if (options.effdt == null && options.modelId != null) {
                        //Just call show() because this.model has been set by historicalPassed()
                        this.show();
                        this.setHistoricNavigation(this.model);
                    }
                    //if eff date is passed get model from coll by eff date ( used for historical )
                    else if (options.effdt != null) {
                        //this.model has not been set, setting it now in here:
                        //get model matching eff_date from hitorical collection
                        this.model = this.historicalCol.where({eff_date: options.effdt})[0];
                        //update this.modelOldeff_date
                        this.modelOldeff_date = this.model.get('eff_date');
                        this.show();
                        this.setHistoricNavigation(this.model);
                    }

                },
                //gets called when triggered in the view (NEXT or PREVIOUS)
                historicalNav: function(option) {
                    //call selected model and pass the effective date as an object
                    this.selectedModel({effdt: option});
                },
                show: function() {
                    //fetch dictionaries:
                    this.prefix.getPrefix();
                    this.suffix.getSuffix();
                    this.mStatus.getMaritalStatus();
                    this.gender.getGender();
                    //initiate new instance of view with model
                    this.view = new ProfileView({model: this.model});
                    //show view in region
                    this.region.show(this.view);
                    this.setListeners();
                },
                setListeners: function() {
                    /************ START OF DICTIONARY LISTENERS ************/
                    this.listenTo(this.prefix, 'sync', function() {
                        this.view.resetPrefix(this.prefix.toJSON());
                    }, this);
                    this.listenTo(this.suffix, 'sync', function() {
                        this.view.resetSuffix(this.suffix.toJSON());
                    }, this);
                    this.listenTo(this.mStatus, 'sync', function() {
                        this.view.resetMaritalStatus(this.mStatus.toJSON());
                    }, this);
                    this.listenTo(this.gender, 'sync', function() {
                        this.view.resetGender(this.gender.toJSON());
                    }, this);
                    /************ END OF DICTIONARY LISTENERS ************/

                    /************ START OF ENTITIES LISTENERS ************/
                    this.listenTo(this.historicalCol, 'reset', function() {
                        vent.trigger('Profile:Updated:Profile');
                    }, this);
                    this.listenTo(this.model, 'updated', this.updated, this);
                    //if error saving need to refetch collection to keep url:
                    //if error saving need to refetch collection to keep url:
                    this.listenTo(this.model, 'saveError', this.refetch, this);
                    /************ END OF ENTITIES LISTENERS ************/

                    /************ START OF EVENT LISTENERS ************/
                    this.listenTo(vent, 'Profile:Historic:Nav', this.historicalNav, this);
                    /************ END OF EVENT LISTENERS ************/

                    /************ START OF VIEW LISTENERS ************/
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    /************ END OF VIEW LISTENERS ************/

                    //FOOTER EVENTS
                    this.bindFooterEvents();
                },
                setHistoricNavigation: function(model) {
                    var curIndex = this.historicalCol.indexOf(model);
                    //nav thru historical records
                    //sets navigators objects in view to show or hide and, sets the nav value:(eff_date, status)
                    if (this.historicalCol.length > 1) {
                        if (curIndex < this.historicalCol.length - 1) {
//                            this.view.showLeft({eff_date: this.historicalCol.at(curIndex + 1).get('eff_date'),
//                                status: this.historicalCol.at(curIndex + 1).get('Status')});
                            vent.trigger('Profile:HistoricNav:ShowLeft',
                                    {eff_date: this.historicalCol.at(curIndex + 1).get('eff_date'),
                                        status: this.historicalCol.at(curIndex + 1).get('Status')})
                        }
                        else if (curIndex == this.historicalCol.length - 1) {
                            //this.view.hideLeft();
                            vent.trigger('Profile:HistoricNav:HideLeft')
                        }
                        if (curIndex > 0 && curIndex <= this.historicalCol.length - 1) {
//                            this.view.showRight({eff_date: this.historicalCol.at(curIndex - 1).get('eff_date'),
//                                status: this.historicalCol.at(curIndex - 1).get('Status')});
                            vent.trigger('Profile:HistoricNav:ShowRight',
                                    {eff_date: this.historicalCol.at(curIndex - 1).get('eff_date'),
                                        status: this.historicalCol.at(curIndex - 1).get('Status')});
                        }
                        else if (curIndex == 0) {
                            //this.view.hideRight();
                            vent.trigger('Profile:HistoricNav:HideRight')
                        }

                    }
                    else if (this.historicalCol.length == 1)
                    {
                        //this.view.hideRight();
                        //this.view.hideLeft();
                        vent.trigger('Profile:HistoricNav:HideRight');
                        vent.trigger('Profile:HistoricNav:HideLeft');
                    }

                },
                save: function() {
                    //update model (data validation result)
                    if (this.view.updateModel()) {
                        //verify eff_dt has been changed
                        if (this.modelOldeff_date == this.model.get('eff_date')) {
                            vent.trigger('Components:Alert:Confirm', {
                                heading: 'Effective Date Not Changed',
                                message: 'You have not changed the effective date for ' + this.model.getId(),
                                cancelText: 'Edit',
                                confirmText: 'Save',
                                callee: this,
                                callback: this.confirmedSave
                            });
                        } else {
                            this.confirmedSave();
                        }
                    }
                },
                confirmedSave: function() {
                    console.log('confirmed save')
                    //validating eff_date here
                    //TODO: find a way to validate eff date automatically in the form, maybe new form validator?
                    if (this.view.ui.eff_date.val() == '') {
                        vent.trigger('Components:Alert:Alert', {
                            heading: 'Invalid Effective Date',
                            message: 'Effective date cannot be empty'
                        });
                    } else
                    {
                        this.model.save();
                    }

                },
                updated: function() {
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.model.getId() + ' Updated'
                    });
                    this.refetch();
                },
                refetch: function() {
                    this.historicalCol.fetch({reset: true}, {list: 'list', personal: 'personal'}, {uid: this.model.getId(), fuzzy_search: 1}, false);
                },
                del: function() {
                    vent.trigger('Components:Alert:Confirm', {
                        heading: 'Delete?',
                        message: 'Are you sure you would like to delete ' + this.model.getId(),
                        callee: this,
                        callback: this.confirmedDel
                    });
                },
                confirmedDel: function() {
                    this.model.id = 'list/personal/' + this.model.id;
                    this.model.destroy();
                    this.listenTo(this.model, 'sync', function() {
                        this.refetch();
                    });
                },
                bindFooterEvents: function() {
                    var self = this;
                    self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
                    self.listenTo(vent, 'Components:Builders:Footer:add', self.add, self);
                    self.listenTo(vent, 'Components:Builders:Footer:del', self.del, self);
                    self.listenTo(vent, 'Components:Builders:Footer:prev', self.prev, self);
                    self.listenTo(vent, 'Components:Builders:Footer:next', self.next, self);
                }
            });
            return ProfileController;
        });
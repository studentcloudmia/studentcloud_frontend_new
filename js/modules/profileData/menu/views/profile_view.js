// login view
define(['marionette',
    'app',
    'globalConfig',
    'bootstrap',
    'tpl!modules/profileData/menu/templates/profile.tmpl'
], function(Marionette, App, GC, Bootstrap, template) {

    var ModalView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'centerContent',
        ui: {
            eff_date: '.eff_date',
            validationBtn: '.validationBtn'
        },
        onRender: function() {
            var self = this;
            self.$el.find("input,select,textarea").not("[type=submit] , .datepicker , .picker__select--month, .picker__select--year").jqBootstrapValidation();

            self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            self.$el.find('.ssnFormat').mask('999-99-9999');
            self.bindUIElements();
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                console.log("self.ui.validationBtn ", self.ui.validationBtn)
                self.ui.validationBtn.trigger('click');
                return false;
            } else {
                var data = Backbone.Syphon.serialize(this);
                console.log('data ', data.ssn);
                data.ssn = data.ssn.replace("-", "");
                self.model.set(data, {silent: true});
                return true;
            }
        },
        resetPrefix: function(prefix) {
            //inject options to prefix select
            var self = this;
            _.each(prefix, function(p) {

                if (p.id == self.model.get('prefix')) {
                    $('.prefixSelector').append('<option selected value="' + p.id + '">' + p.PREFIX + '</option>');

                }
                //compare the prefix from the server with prefix the user has if prefix match use as default selected
                else {
                    $('.prefixSelector').append('<option  value="' + p.id + '">' + p.PREFIX + '</option>');

                }
            });
        },
        resetSuffix: function(suffix) {
            //inject options to suffix select
            var self = this;
            _.each(suffix, function(p) {

                if (p.id == self.model.get('suffix')) {
                    $('.suffixSelector').append('<option selected value="' + p.id + '">' + p.SUFFIX + '</option>');

                }
                //compare the suffix from the server with suffix the user has if suffix match use as default selected
                else {
                    $('.suffixSelector').append('<option  value="' + p.id + '">' + p.SUFFIX + '</option>');

                }
            });
        },
        resetMaritalStatus: function(marital) {
            //inject options to marital select
            var self = this;
            _.each(marital, function(p) {

                if (p.id == self.model.get('marital_status')) {
                    $('.maritalSelector').append('<option selected value="' + p.id + '">' + p.MARITAL + '</option>');

                }
                //compare the MARITAL from the server with MARITAL the user has if MARITAL match use as default selected
                else {
                    $('.maritalSelector').append('<option  value="' + p.id + '">' + p.MARITAL + '</option>');

                }
            });
        },
        resetGender: function(gender) {
            //inject options to gender select
            var self = this;
            _.each(gender, function(p) {

                if (p.id == self.model.get('gender')) {
                    $('.genderSelector').append('<option selected value="' + p.id + '">' + p.SEX + '</option>');

                }
                //compare the MARITAL from the server with MARITAL the user has if MARITAL match use as default selected
                else {
                    $('.genderSelector').append('<option  value="' + p.id + '">' + p.SEX + '</option>');

                }
            });
        }
    });
    return ModalView;
});

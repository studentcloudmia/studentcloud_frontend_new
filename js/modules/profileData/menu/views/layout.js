define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/profileData/menu/templates/layout.tmpl'
],
        function(Marionette, App, vent, template) {
            "use strict";
            var LayoutView = App.Views.Layout.extend({
                template: template,
                regions: {
                    header: '#profileNav',
                    mainContent: '#profileContent',
                    footer: '#bottomMenu'
                },
                ui: {
                    profileText: '.profileText',
                    contactText: '.contactText',
                    identificationText: '.identificationText',
                    histRight: '#historicRight',
                    histLeft: '#historicLeft',
                    histTextRight: '#histTextRight',
                    histTextLeft: '#histTextLeft',
                    modal: '#profileModal'
                },
                events: {
                    'click #profileLink': 'profileLink',
                    'click #contactLink': 'contactLink',
                    'click #identificationLink': 'identificationLink',
                    'click #histTextRight': 'historicalNav',
                    'click #histTextLeft': 'historicalNav'

                },
                triggers: {
                    'click .close': 'modalClose'
                },
                onRender: function() {
                    $(this.ui.modal).modal('show');
                    var self = this;
                    this.listenTo(this, 'Profile:Show:Profile', function() {
                        self.ui.profileText.addClass('active');
                        self.ui.contactText.removeClass('active');
                        self.ui.identificationText.removeClass('active');
                    }, this);
                    this.listenTo(this, 'Profile:Show:Contact', function() {
                        self.ui.profileText.removeClass('active');
                        self.ui.contactText.addClass('active');
                        self.ui.identificationText.removeClass('active');
                    }, this);
                    this.listenTo(this, 'Profile:Show:Identification', function() {
                        self.ui.profileText.removeClass('active');
                        self.ui.contactText.removeClass('active');
                        self.ui.identificationText.addClass('active');
                    }, this);
                    //show profile as default when render
                    this.trigger('Profile:Show:Profile');
                },
                profileLink: function(e) {
                    this.trigger('Profile:Show:Profile');
                    return false;
                },
                contactLink: function(e) {
                    this.trigger('Profile:Show:Contact');
                    return false;
                },
                identificationLink: function(e) {
                    this.trigger('Profile:Show:Identification');
                    return false;
                },
                showRight: function(opt) {
                    this.ui.histRight.removeClass('hidden');
                    this.ui.histTextRight.data('eff_date', opt.eff_date);
                    this.ui.histTextRight.html(opt.eff_date + " - " + opt.status);
                },
                hideRight: function() {
                    this.ui.histRight.addClass('hidden');
                },
                showLeft: function(opt) {
                    this.ui.histLeft.removeClass('hidden');
                    this.ui.histTextLeft.data('eff_date', opt.eff_date);
                    this.ui.histTextLeft.html(opt.eff_date + " - " + opt.status);
                },
                hideLeft: function() {
                    this.ui.histLeft.addClass('hidden');
                },
                historicalNav: function(e) {
                    vent.trigger('Profile:Historic:Nav', $(e.target).data('eff_date'));
                }
            });
            return LayoutView;
        });
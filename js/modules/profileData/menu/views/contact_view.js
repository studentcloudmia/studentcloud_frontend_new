// contact view
define(['marionette',
    'app',
    'globalConfig',
    'bootstrap',
    'tpl!modules/profileData/menu/templates/contact.tmpl'
], function(Marionette, App, GC, Bootstrap, template) {

    var ModalView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'centerContent',
        ui: {
            addressType: '#addressType',
            eff_date: '.eff_date',
            phoneTypeSelect: '.phoneTypeSelector',
            validationBtn: '.validationBtn',
            states: '.state',
            newAddress: '.newAddress',
            newAddressReq: '.newAddressReq'
        },
        triggers: {
            'click .delAddress': 'deleteAddress'
        },
        events: {
            'keyup .phoneNumber': 'addNewPhone',
            'click .delPhone': 'deletePhone',
            'click .gotoAddress': 'gotoAddress'

        },
        deletePhone: function(e) {
            this.trigger('deletePhone', e.currentTarget);
        },
        render: function() {
            //empty. Default render, called from function resetaddressType
        },
        onRender: function() {
            this.countPhoneTypes = 1;
            this.countPhone = 0;
            var self = this;

            self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);

            self.bindUIElements();
        },
        gotoAddress: function(e) {
            var self = this;
            this.$el.find('.addressTypesNav > li').removeClass('active');
            $(e.target.parentElement).addClass('active');
            var scroll_y = e.currentTarget.id.replace('address_', '');
            this.addressScroller = self.getScroller('profile_address');
            console.log('scroll to elm ', scroll_y)
            this.addressScroller.scrollToElement('.AddressType_' + scroll_y, 1000);
            return false;
        },
        resetaddressType: function(addressTypes) {
            var self = this;
            //store address types to used outside if needed
            self.addressTypes = addressTypes;
            this.$el.html(this.template({model: this.model.attributes, addressTypes: this.addressTypes}));
            this.injectAddressTypes(this.addressTypes);
            this.onRender();
            this.afterShow();

            self.$el.find('.zipFormat').mask('99999-9999');
        },
        injectAddressTypes: function(addressTypes) {
            var self = this;
            self.$el.find('.addressTypesNav').html('');
            var ignore = false;
            _.each(addressTypes, function(aType) {
                var active;
                if (!ignore) {
                    _.each(self.model.toJSON().Addresses, function(address, i) {
                        if (aType.id == address.address_type) {
                            //found a match, mark it as the selected value
                            active = 'active';
                            ignore = true;
                        }
                        else {
                            if (!ignore)
                                active = '';
                        }
                    });
                }
                self.$el.find('.addressTypesNav').append('<li class="' + active + '"><a id="address_' + aType.id + '" class="gotoAddress" >' + aType.ADDRESS + '</a></li>');

            }
            );
        },
        resetPhoneType: function(phoneTypes) {
            var self = this;
            //store phone types to used outside if needed
            self.phoneTypes = phoneTypes;

            //IF - phones array is not empty
            if (self.model.get('Phones').length > 0) {
                this.hasPhones = true;
                _.each(phoneTypes, function(aType) {
                    _.each(self.model.toJSON().Phones, function(phone, i) {
                        //if phone type match inject it
                        if (aType.id == phone.phone_type) {
                            self.countPhone++;
                            self.$el.find('.phoneSection').append('<div style="clear:both"> </div> <div class=" PhoneType_' + phone.phone_type + '"> <div class="pull-left" style="margin: 4px 0 0 0; pointer-events: auto;"> <button id="' + phone.phone_type + '" class="delPhone btn btn-mini btn-danger del-Terms-Offered pull-right" type="button">x</button> </div> <div class="span4" style="height: 40px!important"> <div class="controls inside"> <select style="width:100%" class="phoneTypeSelector" name="[Phones][' + i + ']phone_type" > </select> </div> </div> <div class="inputFormat span6"> <div class="controls inside noLabel "> <input class="span12 phoneFormat" placeholder="Phone Number" type="text" value="' + phone.phone_number + '" name="[Phones][' + i + ']phone_number"/> </div> </div> </div>');
                        }
                    });
                });
                //if injected phones are less that phone type add "add new"
                if (phoneTypes.length > this.countPhone)
                    self.$el.find('.phoneSectionNew').html('<div style="clear:both"> </div> <div class="pull-left" style="margin: 4px 0 0 0; width:20px;"> </div> <div class="span4" style="height: 40px!important;"> <div class="controls inside"> <select style="width:100%" class="phoneTypeSelector" name="[Phones][' + self.model.toJSON().Phones.length + ']phone_type" > </select> </div> </div> <div class="inputFormat span6"> <div class="controls inside noLabel "> <input class="span12 phoneNumber phoneFormat" placeholder="Phone Number" type="text" name="[Phones][' + self.model.toJSON().Phones.length + ']phone_number"/> </div> </div>');
            }
            //IF - phone array is empty, show add new
            else {
                this.hasPhones = false;
                self.$el.find('.phoneSectionNew').append(' <div class="pull-left" style="margin: 4px 0 0 0; width:20px;"> </div> <div class="span4" style="height: 40px!important;"> <div class="controls inside"> <select style="width:100%" class="phoneTypeSelector" name="[Phones][' + self.model.toJSON().Phones.length + ']phone_type" > </select> </div> </div> <div class="inputFormat span6"> <div class="controls inside noLabel "> <input class="span12 phoneNumber phoneFormat" placeholder="Phone Number" type="text" name="[Phones][' + self.model.toJSON().Phones.length + ']phone_number"/> </div> </div>');
            }
            this.injectPhoneTypes(this.phoneTypes);
            this.$el.find('.phoneFormat').mask('999-999-9999');
            this.render();
        },
        resetStates: function(states) {
            var self = this;
            //inject options to countries select
            if (states.length > 0) {
                this.ui.states.html('');
                _.each(states, function(state) {
                    //mark as selected if state matches model state
                    var sel = (state.id == self.model.get('Addresses').State) ? 'selected' : '';
                    var statesVal = state.Description_Short + ' - ' + state.Description_Long
                    self.ui.states.append('<option value="' + state.id + '" ' + sel + '>' + statesVal + '</option>');
                });
            }
            else {
                this.ui.states.html('<option selected>No States</option>');
            }
        },
        injectPhoneTypes: function(phoneTypes) {
            var self = this;
            //clear dropdowns
            self.$el.find('.phoneSection .phoneTypeSelector').html('');
            self.$el.find('.phoneSectionNew .phoneTypeSelector').html('');

            var self = this;
            _.each(phoneTypes, function(aType) {
                _.each(self.model.toJSON().Phones, function(phone, i) {
                    if (aType.id == phone.phone_type) {
                        //found a match, mark it as the selected value
                        self.$el.find('.phoneSection  .PhoneType_' + phone.phone_type + ' .phoneTypeSelector').append('<option selected value="' + aType.id + '">' + aType.PHONE + '</option>');
                    }
                    else {
                        //no match, just inject to the dropdown
                        self.$el.find('.phoneSection .PhoneType_' + phone.phone_type + ' .phoneTypeSelector').append('<option value="' + aType.id + '">' + aType.PHONE + '</option>');
                    }
                });

                //inject to the add new dropdown
                self.$el.find('.phoneSectionNew .phoneTypeSelector').append('<option value="' + aType.id + '">' + aType.PHONE + '</option>');
            });
        },
        addNewPhone: function(e) {
            var self = this;
            //show an extra add when ENTER key is pressed
            if (e.keyCode == 13) {
                if (self.$el.find('.phoneNumber').val().length > 0) {
                    this.showAddNewPhone();
                }
                if (self.$el.find('.phoneNumber').val().length == 0) {
                    //hide extra add
                    this.removeAddNewPhone();
                }
            }
        },
        showAddNewPhone: function() {
            var self = this;
            if (!self.hasPhones) {
                //ensures that you only add as many 'add new' inputs as there are phone types
                if (self.countPhoneTypes >= this.phoneTypes.length) {

                }
                else {
                    self.$el.find('.phoneSectionNew').append('<div style="clear:both"></div> <div class="pull-left addNew_' + (self.model.toJSON().Phones.length + 1) + '" style="margin: 4px 0 0 0; width:20px;">  <button id="test" class="delNewPhone btn btn-mini btn-danger del-Terms-Offered pull-right" type="button">x</button> </div> <div class="span4" style="height: 40px!important;"> <div class="controls inside"> <select style="width:100%" class="phoneTypeSelector" name="[Phones][' + (self.model.toJSON().Phones.length + 1) + ']phone_type" > </select> </div> </div> <div class="inputFormat span6"> <div class="controls inside noLabel "> <input class="span12 phoneNumber phoneFormat" placeholder="Phone Number" type="text" name="[Phones][' + (self.model.toJSON().Phones.length + 1) + ']phone_number"/> </div> </div>');
                    self.injectPhoneTypes(this.phoneTypes);
                    self.countPhoneTypes++;
                }
            }
            else {
                //ensures that you only add as many 'add new' inputs as there are phone types,
                //and checks those types that are already saved
                if (self.countPhoneTypes >= this.phoneTypes.length - self.model.toJSON().Phones.length) {

                }
                else {
                    self.$el.find('.phoneSectionNew').append('<div style="clear:both"></div> <div class="pull-left addNew_' + (self.model.toJSON().Phones.length + 1) + '" style="margin: 4px 0 0 0; width:20px;"> </div> <div class="span4" style="height: 40px!important;"> <div class="controls inside"> <select style="width:100%" class="phoneTypeSelector" name="[Phones][' + (self.model.toJSON().Phones.length + 1) + ']phone_type" > </select> </div> </div> <div class="inputFormat span6"> <div class="controls inside noLabel "> <input class="span12 phoneNumber phoneFormat" placeholder="Phone Number" type="text" name="[Phones][' + (self.model.toJSON().Phones.length + 1) + ']phone_number"/> </div> </div>');
                    self.injectPhoneTypes(this.phoneTypes);
                    self.countPhoneTypes++;
                }
            }
        },
        removeAddNewPhone: function() {
            var self = this;
            self.$el.find('.phoneSectionNew').remove('div .phoneSectionNew .addNew_' + (self.model.toJSON().Phones.length + 1));
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            self.$el.find("input,select,textarea").not("[type=submit] , .datepicker , .picker__select--month, .picker__select--year").jqBootstrapValidation()

            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
            } else {
                var data = Backbone.Syphon.serialize(this);

                _.each(self.addressTypes, function(type, i) {
                    if (data.Addresses[i].Address_Part1 == "") {
                        delete data.Addresses[i];
                    }
                });
                TempPhones = data.Phones;
                data.Phones = [];
                _.each(TempPhones, function(phone) {
                    if (!phone.phone_number == "") {
                        data.Phones.push({'phone_number': phone.phone_number,
                            'phone_type': phone.phone_type});
                    }
                });
                self.model.set(data, {silent: true});
                return true;
            }
        }

    });

    return ModalView;

});

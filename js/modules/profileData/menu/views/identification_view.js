// login view
define(['marionette',
    'app',
    'globalConfig',
    'bootstrap',
    'tpl!modules/profileData/menu/templates/identification.tmpl'
], function(Marionette, App, GC, Bootstrap, template) {

    var ModalView = App.Views.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'centerContent',
        ui: {
            eff_date: '.eff_date',
            validationBtn: '.validationBtn'
        },
        triggers: {
            'click #saveBtn': 'save:userPersonal',
            'click #delBtn': 'delete:userPersonal'
        },
        onRender: function() {
            var self = this;
            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker , .picker__select--month, .picker__select--year").jqBootstrapValidation();
            });
            self.$effDate = self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
            self.bindUIElements();
        },
        resetPassportCountry: function(countries) {
            var self = this;
            var skip = false;
            if (typeof self.model.countries == 'undefined') {
                $('#passport_country').append('<option selected value="" > -Passport Country- </option>');
                skip = true;
            }
            _.each(countries, function(p) {
                if (skip && p.id == self.model.get('passport_country')) {
                    $('#passport_country').append('<option selected value="' + p.id + '">' + p.Description_Long + '</option>');
                }
                else {
                    $('#passport_country').append('<option  value="' + p.id + '">' + p.Description_Long + '</option>');
                }
            });
        },
        resetCitizenshipCountry: function(countries) {
            var self = this;
            var skip = false;
            if (typeof self.model.countries == 'undefined') {
                $('#citizenship_country').append('<option selected value="" > -Country- </option>');
                skip = true;
            }
            _.each(countries, function(p) {
                if (skip && p.id == self.model.get('citizenship_country')) {
                    $('#citizenship_country').append('<option selected value="' + p.id + '">' + p.Description_Long + '</option>');
                }
                else {
                    $('#citizenship_country').append('<option  value="' + p.id + '">' + p.Description_Long + '</option>');
                }
            });
        },
        resetCitizenshipStatus: function(citizenship_status) {
            var self = this;
            var skip = false;
            if (typeof self.model.citizenship_status == 'undefined') {
                $('#citizenship_status').append('<option selected value="" > -Status- </option>');
                skip = true;
            }
            _.each(citizenship_status, function(p) {
                if (skip && p.id == self.model.get('citizenship_status')) {
                    $('#citizenship_status').append('<option selected value="' + p.id + '">' + p.CITIZENSHIPSTATUS + '</option>');
                }
                else {
                    $('#citizenship_status').append('<option  value="' + p.id + '">' + p.CITIZENSHIPSTATUS + '</option>');
                }
            });
        },
        resetVisaType: function(visa_type) {
            var self = this;
            var skip = false;
            if (typeof self.model.visa_type == 'undefined') {
                $('#visa_type').append('<option selected value="" > Type- </option>');
                skip = true;
            }
            _.each(visa_type, function(p) {
                if (skip && p.id == self.model.get('visa_type')) {
                    $('#visa_type').append('<option selected value="' + p.id + '">' + p.VISATYPE + '</option>');
                }
                else {
                    $('#visa_type').append('<option  value="' + p.id + '">' + p.VISATYPE + '</option>');
                }
            });
        },
        resetVisaStatus: function(visa_status) {
            var self = this;
            var skip = false;
            if (typeof self.model.visa_status == 'undefined') {
                $('#visa_status').append('<option selected value="" > -Status- </option>');
                skip = true;
            }
            _.each(visa_status, function(p) {
                if (skip && p.id == self.model.get('visa_status')) {
                    $('#visa_status').append('<option selected value="' + p.id + '">' + p.VISASTATUS + '</option>');
                }
                else {
                    $('#visa_status').append('<option  value="' + p.id + '">' + p.VISASTATUS + '</option>');
                }
            });
        },
        resetEthnicGroup: function(ethnic_group) {
            var self = this;
            var skip = false;
            if (typeof self.model.ethnic_group == 'undefined') {
                $('#ethnic_group').append('<option selected value="" > -Ethnic Group- </option>');
                skip = true;
            }
            _.each(ethnic_group, function(p) {
                if (skip && p.id == self.model.get('ethnic_group')) {
                    $('#ethnic_group').append('<option selected value="' + p.id + '">' + p.ETHINICITY + '</option>');
                }
                else {
                    $('#ethnic_group').append('<option  value="' + p.id + '">' + p.ETHINICITY + '</option>');
                }
            });
        },
        resetMilitaryVeteranStatus: function(military_veteran_status) {
            var self = this;
            var skip = false;
            if (typeof self.model.military_veteran_status == 'undefined') {
                $('#military_veteran_status').append('<option selected value=""> -Military Status- </option>');
                skip = true;
            }
            _.each(military_veteran_status, function(p) {
                if (skip && p.id == self.model.get('military_veteran_status')) {
                    $('#military_veteran_status').append('<option selected value="' + p.id + '">' + p.VETERANSTATUS + '</option>');
                }
                else {
                    $('#military_veteran_status').append('<option  value="' + p.id + '">' + p.VETERANSTATUS + '</option>');
                }
            });


        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;
            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        }

    });
    return ModalView;
});

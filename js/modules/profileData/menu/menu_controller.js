// sbuilder delivery
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/profileData/menu/controllers/profile_controller',
    'modules/profileData/menu/controllers/contact_controller',
    'modules/profileData/menu/controllers/identification_controller',
    //views
    'modules/profileData/menu/views/layout',
    //collection
    'entities/collections/user/UserCollection'
],
        function(Marionette, App, vent, ProfileController, ContactController, IdentificationController, Layout, UserCollection) {

            var MenuModule = App.module('ProfileData.Menu');
            MenuModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.modelId = options.opts.modelId;
                    this.layout = new Layout();
                    this.historicalCol = new UserCollection();
                    this.setupMenuListeners();
                    //fetch collection
                    this.historicalCol.fetch({reset: true}, {list: 'list', personal: 'personal'}, {uid: this.modelId, fuzzy_search: 1}, false);
                    this.listenToOnce(this.historicalCol, 'reset', function() {
                        this.show();
                    }, this);
                    //listen to layout show to show footer
                    this.listenTo(this.layout, 'show', function() {
                        this.showFooter();
                    }, this);
                    this.listenTo(vent, 'Components:UserComponents:Closing:header', function() {
                        this.layout.close();
                    });
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.layout, 'modalClose', function() {
                        this.layout.close();
                    }, this);


                },
                show: function() {
                    this.region.show(this.layout);
                },
                setupMenuListeners: function() {
                    this.listenTo(this.layout, "Profile:Show:Profile", this.profile, this);
                    this.listenTo(this.layout, "Profile:Show:Contact", this.contact, this);
                    this.listenTo(this.layout, "Profile:Show:Identification", this.identification, this);

                    //after update refetch to show current information
                    this.listenTo(vent, "Profile:Updated:Profile", this.profile, this);
                    this.listenTo(vent, "Profile:Updated:Contact", this.contact, this);
                    this.listenTo(vent, "Profile:Updated:Identification", this.identification, this);

                    //historic nav
                    this.listenTo(vent, "Profile:HistoricNav:ShowLeft", this.showLeft, this);
                    this.listenTo(vent, "Profile:HistoricNav:HideLeft", this.hideLeft, this);
                    this.listenTo(vent, "Profile:HistoricNav:ShowRight", this.showRight, this);
                    this.listenTo(vent, "Profile:HistoricNav:HideRight", this.hideRight, this);

                },
                profile: function() {
                    new ProfileController({region: this.layout.mainContent, collection: this.historicalCol});
                },
                contact: function() {
                    new ContactController({region: this.layout.mainContent, collection: this.historicalCol});
                },
                identification: function() {
                    new IdentificationController({region: this.layout.mainContent, collection: this.historicalCol});
                },
                showFooter: function() {
                    //start footer controller
                    var options = {
                        region: this.layout.footer
                    };
                    vent.trigger('Components:Builders:Footer:on', options);
                    this.updateFooter({save: true, del: true});
                    //TODO: mode to view
                    $('#bottomMenu .span5').removeClass('span5').addClass('span12');
                    $('#bottomMenu #addBtn').css('display', 'none');
                    $('#bottomMenu .btn').css('margin-top', '20px');
                    $('#bottomMenu #collNav').remove();
                },
                updateFooter: function(options) {
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                showLeft: function(options) {
                    this.layout.showLeft({eff_date: options.eff_date,
                        status: options.status});
                },
                hideLeft: function() {
                    this.layout.hideLeft();
                },
                showRight: function(options) {
                    this.layout.showRight({eff_date: options.eff_date,
                        status: options.status});
                },
                hideRight: function() {
                    this.layout.hideRight();
                }
            });
            return MenuModule;
        });

define([
    'marionette'
],
        function(Marionette) {

            var Router = Marionette.AppRouter.extend({
                appRoutes: {
                    'admin/student/view(/:id)': 'show'
                }
            });

            return Router;

        });
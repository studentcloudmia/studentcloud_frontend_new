define([
    'marionette',
    'app',
    'vent',
    'modules/adminStudentView/show/show_controller'
],
        function(Marionette, App, vent, AdminStudentViewShowModule) {

            var Controller = Marionette.Controller.extend({
                initialize: function() {
                    //listen to search trigger (outside search)
                    this.listenTo(vent, 'AdminStudentView:Search', this.outsideSearch, this);
                },
                outsideSearch: function() {
                    this.show();

                },
                //User navigates to url, may or may not have ID
                show: function() {
                    this.adinStudentVIewController = new AdminStudentViewShowModule.Controller({region: App.main});
                }
            });

            return Controller;

        });
// access module
define([
    'marionette',
    'app',
    'vent',
    'modules/adminStudentView/router',
    'modules/adminStudentView/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var AdminStudentViewModule = App.module('AdminStudentView');

        //bind to module finalizer event
        AdminStudentViewModule.addFinalizer(function() {
	
        });

        AdminStudentViewModule.addInitializer(function(options) {
		
            var controller = new Controller();
		
            new Router({
                controller: controller
            });            
		
        });

        return AdminStudentViewModule;
    });
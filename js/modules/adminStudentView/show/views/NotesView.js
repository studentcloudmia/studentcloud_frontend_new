// student snapshot message view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/notes.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var NotesView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapNotes',
        
        onRender: function(){
            this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
        },

	});
	
	return NotesView;
	
});
// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/adminStudentView/show/template/_sidebar.tmpl'
], function(Marionette, App, vent, template) {

    var SideItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'searchRow',
        events: {
            'click': 'triggerSelected'
        },
        onRender: function() {
            //add data-id attribute to item
            this.$el.attr('data-id', this.model.id);
            this.$el.attr('data-serverid', this.model.getId());
            //make the item draggable
            this.$el.draggable({revert: "invalid", helper: "clone"});
        },
        triggerSelected: function() {
            App.navigate('/admin/student/view/' + this.model.get('uid'));
            vent.trigger('AdminStudentView:CheckUrl');
        }
    });

    return SideItemView;

});
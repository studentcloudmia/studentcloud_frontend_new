// student snapshot apt view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/apt.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var AptView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapApt',
        
        onRender: function(){
            this.$el.find('.datepicker').pickadate(GC.pickadateDefaults);
        },

	});
	
	return AptView;
	
});
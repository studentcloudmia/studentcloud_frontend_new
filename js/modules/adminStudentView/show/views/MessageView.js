// student snapshot message view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/message.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MessageView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapMessage'

	});
	
	return MessageView;
	
});
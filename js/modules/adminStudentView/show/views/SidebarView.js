// SideView
define([
    'marionette',
    'app',
    'modules/adminStudentView/show/views/SidebarItemView',
    'tpl!modules/adminStudentView/show/template/sidebar.tmpl'
], function(Marionette, App, ItemView, template) {

    var SideView = App.Views.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: 'tbody',
        events: {
            'change #searchField': 'search'
        },
        ui: {
            search: '#searchField'
        },
        search: function() {
            this.trigger('search', this.getSearchText());
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.val('');
            this.ui.search.blur();
            return val;
        }
    });

    return SideView;

});
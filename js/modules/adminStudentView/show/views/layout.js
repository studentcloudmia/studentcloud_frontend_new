
define(
[
'marionette', 
'app',
'tpl!modules/adminStudentView/show/template/layout.tmpl'
],function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,

        regions: {
            header: "#adminStudentViewHeader"           
        },

        events:{
            'click .headingButtons h3 > span': 'actSelected'
        },
        
        actSelected: function(e){
            e.preventDefault();
            e.stopPropagation();
            var act = $(e.currentTarget).data('action');
            // console.log('action', act);
            this.trigger('action', act);
        }
    });
	return LayoutView;
});
// student snapshot user view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/user.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var UserView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapUser'

	});
	
	return UserView;
	
});
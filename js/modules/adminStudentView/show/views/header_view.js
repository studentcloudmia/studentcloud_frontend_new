// login view
define([
    'marionette', 
    'app',
    'moment',
    'tpl!modules/adminStudentView/show/template/header.tmpl'
    ],function(Marionette, App, Moment, template){
	
        var WeatherView = App.Views.ItemView.extend({
            template: template
		
        });
	
        return WeatherView;
	
    });
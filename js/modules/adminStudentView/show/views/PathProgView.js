// student snapshot path progress view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/pathProg.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var PathProgView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapPathProg'

	});
	
	return PathProgView;
	
});
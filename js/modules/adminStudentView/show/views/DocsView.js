// student snapshot docs view
define([
	'underscore',
	'marionette',
	'vent',
	'app',
    'globalConfig',
	'tpl!modules/adminStudentView/show/template/docs.tmpl'
],function(_, Marionette, vent, App, GC, template){
	
	var MessageView = App.Views.ItemView.extend({
		template: template,
        className: 'studSnapDocs',
        
        events: { 
            'click .smallImage': 'fullScreenImage'   
        },
        
        exitFullImage: function(){
            if(this.imgLoaded){
                //remove image
                $('#fullImage').html(' ');
                /*remove backdrop*/
                $('#fullImage').css({
                    margin: '',
                    position: '',
                    top: '',
                    bottom: '',
                    left: '',
                    right: '',
                    zIndex: '',
                    backgroundColor: ''
                });
            
                $('#modalsAlt2').css({zIndex: 6});
                $('.modal-backdrop').css({zIndex: 3});
                $('#fullImage').off('click');
                
                this.imgLoaded = false;
                console.log('removed image');
            }                
        },
        
        fullScreenImage: function(e){
            var self = this;
            var $img = this.$el.find(e.currentTarget).clone(),
                imageWidth = $img[0].width, //need the raw width due to a jquery bug that affects chrome
                imageHeight = $img[0].height, //need the raw height due to a jquery bug that affects chrome
                maxWidth = $(window).width(),
                maxHeight = $(window).height(),
                widthRatio = maxWidth / imageWidth,
                heightRatio = maxHeight / imageHeight;

            var ratio = widthRatio; //default to the width ratio until proven wrong

            if (widthRatio * imageHeight > maxHeight) {
                ratio = heightRatio;
            }
            console.log('ratio: ', ratio);
            //now resize the image relative to the ratio
            $img.attr('width', imageWidth * ratio)
                .attr('height', imageHeight * ratio);

            //and center the image vertically and horizontally
            $img.css({
                margin: 'auto',
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 20
            });
            $img.removeClass('smallImage');
            
            /*add backdrop*/
            $('#fullImage').css({
                margin: 'auto',
                position: 'absolute',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                zIndex: 19,
                backgroundColor: 'rgba(0, 0, 0, 0.60)'
            });
            
            $('#modalsAlt2').css({zIndex: 1});
            $('.modal-backdrop').css({zIndex: 1});
            
            $('#fullImage').on('click', function(){
                self.exitFullImage();
            });
            
            $img.appendTo('#fullImage');
            
            setTimeout(function(){
                self.imgLoaded = true;
                console.log('set imgLoaded to true');
            }, 200);
            
        },
        
        onClose: function(){
            $('#fullImage').off('click');
        }

	});
	
	return MessageView;
	
});
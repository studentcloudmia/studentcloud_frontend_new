// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/adminStudentView/show/views/SidebarView'
],
        function(Marionette, App, vent, SidebarView) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.collection;

                    this.view = new SidebarView({collection: this.collection});

                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);

                    this.listenTo(this.view, 'search', this.search, this);

                },
                show: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    //show view
                    this.region.show(this.view);
                },
                search: function(sText) {
                    this.collection.fetch({reset: true}, {}, {uid: sText}, false);
                }

            });

            return SideController;
        });
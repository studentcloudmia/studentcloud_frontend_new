//student snapshot user controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/UserView'
],
function(Marionette, App, vent, UserView) {

    var UserController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new UserView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return UserController;
});

//student snapshot notes controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/NotesView'
],
function(Marionette, App, vent, NotesView) {

    var NotesController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new NotesView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return NotesController;
});

//student snapshot docs controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/DocsView'
],
function(Marionette, App, vent, DocsView) {

    var DocsController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new DocsView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return DocsController;
});

//student snapshot apt controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/AptView'
],
function(Marionette, App, vent, AptView) {

    var AptController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new AptView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return AptController;
});

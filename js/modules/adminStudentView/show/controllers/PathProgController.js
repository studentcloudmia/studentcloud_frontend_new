//student snapshot path progress controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/PathProgView'
],
function(Marionette, App, vent, PathProgView) {

    var PathProgController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new PathProgView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return PathProgController;
});

//student snapshot message controller
define([
'marionette',
'app',
'vent',
//views
'modules/adminStudentView/show/views/MessageView'
],
function(Marionette, App, vent, MessageView) {

    var MessageController = Marionette.Controller.extend({

        initialize: function(options) {
			this.view = new MessageView();
            this.region = options.region;
			
			this.listenTo(this.view, 'close', function(){
				this.close();
			}, this);
            
            this.region.show(this.view);
        }
        
    });

    return MessageController;
});

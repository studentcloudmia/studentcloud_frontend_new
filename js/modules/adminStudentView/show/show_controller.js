// admin student view show controller
define([
    'marionette',
    'app',
    'vent',
    //controllers
    'modules/adminStudentView/show/sidebar_controller',
    'modules/adminStudentView/show/controllers/MessageController',
    'modules/adminStudentView/show/controllers/DocsController',
    'modules/adminStudentView/show/controllers/NotesController',
    'modules/adminStudentView/show/controllers/AptController',
    'modules/adminStudentView/show/controllers/PathProgController',
    'modules/adminStudentView/show/controllers/UserController',
    //views
    'modules/adminStudentView/show/views/layout',
    'modules/adminStudentView/show/views/header_view',
    //collections
    'entities/collections/user/UserCollection'
],
        function(Marionette, App, vent,
                //controllers
                SideController,
                MessageController,
                DocsController,
                NotesController,
                AptController,
                PathProgController,
                UserController,
                //views
                Layout, HeaderView,
                //collections
                UsersCollection) {

            var AdminStudentViewShowModule = App.module('AdminStudentView.Show');

            AdminStudentViewShowModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.layout = new Layout();
                    this.collection = new UsersCollection();
                    this.userCollection = new UsersCollection();
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);
                    

                    this.listenTo(this.layout, 'action', this.actionSelected);
                    
                    this.listenTo(this.collection, 'sync', function() {
                        this.showHeader(this.collection.at(0));
                    }, this);
                    this.activateDroppable();
                    this.listenTo(vent, 'AdminStudentView:CheckUrl', this.checkUrl, this);
                    this.checkUrl();
                },
                checkUrl: function() {
                    this.url = App.getCurrentRoute().substr(App.getCurrentRoute().lastIndexOf('/') + 1), '';
                    if (this.url == 'view') {
                        //if no ID in url, show enpty header
                        //URL has no model ID, show page to search
                        this.show();
                        vent.trigger("Sidebar:Open");
                    }
                    //if ID in url fetch model, show header of model
                    else if (this.url != 'adminDashboard' && this.url != '') {
                        //Get model ID directly from URL, fetch model
                        this.modelId = this.url;
                        this.selectedModel(this.modelId);
                    }
                },
                show: function() {
                    this.region.show(this.layout);
                    //ask for sidebar
                    vent.trigger('Sidebar:On', {controller: new SideController({collection: this.userCollection})});
                },
                showHeader: function(model) {
                    if (typeof model != 'undefined') {
                        //start header controller
                        var links = [{
                                name: 'Anthropology',
                                link: 'path/userMap'
                            }];
                        var heading = this.collection.at(0).get('first_name') + ' ' + this.collection.at(0).get('last_name') + ' | ' + this.collection.at(0).get('username');
                        var opts = {
                            region: this.layout.header,
                            heading: heading,
                            links: links,
                            modelId: this.collection.at(0).get('username')
                        };
                        vent.trigger('Components:UserComponents:UserHeader:on', opts);
                    }
                },
                selectedModel: function(modelId) {
                    this.collection.fetch({}, {list: 'list', personal: 'personal'}, {uid: modelId, basic: 1}, false);
                    this.show();
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.header.$el).droppable({
                        accept: ".searchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.selectedModel(ui.draggable.attr("data-id"));
                        }
                    });
                },
                
                actionSelected: function(act){
                    switch (act) {
                    case "notes":
                        new NotesController({region: App.modalsAlt1});
                        break;
                    case "appt":
                        new AptController({region: App.modalsAlt1});
                        break;
                    case "docs":
                        new DocsController({region: App.modalsAlt2});
                        break;
                    case "pathProg":
                        new PathProgController({region: App.modalsAlt3});
                        break;
                    case "user":
                        new UserController({region: App.modalsAlt2});
                        break;
                    case "msg":
                        new MessageController({region: App.modalsAlt2});
                        break;
                    default:
                        console.warn('No action provided for: ', act);
                    }
                }
            });

            return AdminStudentViewShowModule;
        });
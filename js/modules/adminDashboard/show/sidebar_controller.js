// sidebar content controller
define([
    'marionette',
    'app',
    'vent',
//views
    'modules/adminDashboard/show/views/SidebarView'
],
        function(Marionette, App, vent, SidebarView) {

            var SideController = Marionette.Controller.extend({
                initialize: function(options) {
                    this.collection = options.collection;
                    this.view = new SidebarView({collection: this.collection});
                    this.listenTo(this.view, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.collection, 'reset', function() {
                        this.singleResult();
                    }, this);
                    //search from sidebar or from widget:
                    this.listenTo(this.view, 'search', this.search, this);
                    this.listenTo(vent, 'AdminDashboard:Student:Search:widget', this.search);
                },
                show: function(options) {
                    this.region = options.region;
                    this.region.show(this.view);
                },
                search: function(searchString) {
                    var sText;
                    if (typeof searchString != 'string') {
                        sText = this.view.getSearchText();
                    }
                    else {
                        sText = searchString;
                    }

                    this.collection.fetch({reset: true}, {list: 'list', personal: 'personal'}, {uid: sText}, false);
                },
                //if only one result in the search display the result right away
                singleResult: function() {
                    if (this.collection.length == 1) {
                        if (this.collection.at(0).id != "undefined/00000101") {
                            App.navigate('/admin/student/view/' + this.collection.at(0).getId());
                            vent.trigger('AdminStudentView:Search');
                        }
                    }
                    else {
                        vent.trigger('Sidebar:Open');
                    }
                }

            });

            return SideController;
        });
// admin dahsboard show controller
define([
    'marionette',
    'app',
    'vent',
    //controllers
    'modules/adminDashboard/show/sidebar_controller',
    //views
    'modules/adminDashboard/show/views/layout',
    'modules/adminDashboard/show/views/search_view',
    //collections
    'entities/collections/user/UserCollection'
],
        function(Marionette, App, vent,
                //controllers
                SideController,
                //views
                Layout, SearchView,
                //collections
                UserCollection) {

            var AdminDashboardShowModule = App.module('AdminDashboard.Show');

            AdminDashboardShowModule.Controller = Marionette.Controller.extend({
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    this.layout = new Layout();
                    this.collection = new UserCollection();
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);
                    this.listenTo(this.layout, 'show', function() {
                        vent.trigger("Components:Student:Header:on", {region: this.layout.header});
                        this.showmiddleLeftWidg();

                    }, this);
                    this.region.show(this.layout);
                    //ask for sidebar
                    vent.trigger('Sidebar:On', {controller: new SideController({collection: this.collection})});
                },
                showmiddleLeftWidg: function() {
                    this.searchView = new SearchView();
                    this.layout.topRightWidg.show(this.searchView);
                }
            });

            return AdminDashboardShowModule;
        });
// login view
define([
    'marionette',
    'app',
    'tpl!modules/adminDashboard/show/template/notification.tmpl'
], function(Marionette, App, template) {

    var NotificationView = App.Views.ItemView.extend({
        template: template,
        initialize: function() {
        }
    });

    return NotificationView;

});
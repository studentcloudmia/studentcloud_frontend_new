// SideView
define([
    'marionette',
    'app',
    'modules/adminDashboard/show/views/SidebarItemView',
    'tpl!modules/adminDashboard/show/template/sidebar.tmpl'
], function(Marionette, App, ItemView, template) {

    var SideView = App.Views.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: 'tbody',
        triggers: {
            'change #searchField': 'search'
        },
        ui: {
            search: '#searchField'
        },
        getSearchText: function() {
            console.log('sarch string ', this.ui.search.val())
            var val = this.ui.search.val();
            this.ui.search.val('');
            this.ui.search.blur();
            return val;
        }
    });

    return SideView;

});
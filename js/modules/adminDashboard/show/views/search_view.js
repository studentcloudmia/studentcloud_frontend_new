// login view
define([
    'marionette',
    'app',
    'vent',
    'moment',
    'tpl!modules/adminDashboard/show/template/search.tmpl'
], function(Marionette, App, vent, Moment, template) {

    var SearchView = App.Views.ItemView.extend({
        template: template,
        ui: {
            search: '#studentSearch'
        },
        events: {
            'change #studentSearch': 'search'
        },
        search: function() {
            vent.trigger('AdminDashboard:Student:Search:widget', this.getSearchText());
        },
        getSearchText: function() {
            var val = $(this.ui.search).val();
            $(this.ui.search).val('');
            $(this.ui.search).blur();
            return val;
        }
    });


    return SearchView;

});
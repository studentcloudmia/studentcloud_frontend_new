// login view
define([
    'marionette',
    'app',
    'moment',
    'tpl!modules/adminDashboard/show/template/weather.tmpl'
], function(Marionette, App, Moment, template) {

    var WeatherView = App.Views.ItemView.extend({
        template: template,
        onBeforeRender: function() {
            //add data to model
            var data = {
                fullDate: moment().format("MMM Do YYYY"),
                day: moment().format('dddd')
            };
            this.model.set('Date', data, {
                silent: true
            });
        },
        initialize: function(options) {
        }

    });

    return WeatherView;

});

define(
        [
            'marionette',
            'app',
            'tpl!modules/adminDashboard/show/template/layout.tmpl'
        ], function(Marionette, App, template) {
    "use strict";

    var LayoutView = App.Views.Layout.extend({
        template: template,
        regions: {
            header: ".dashHeader",
            mainWidg: "#mainWidg",
            topRightWidg: "#topRightWidg"
        }
    });
    return LayoutView;
});
// SideView
define([
    'marionette',
    'app',
    'vent',
    'tpl!modules/adminDashboard/show/template/_sidebar.tmpl'
], function(Marionette, App, vent, template) {

    var SideItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'searchRow',
        events: {
            'click': 'triggerSelected'
        },
        onRender: function() {
            //add data-id attribute to item
            this.$el.attr('data-id', '/admin/student/view/' + this.model.get('uid'));
            this.$el.attr('data-serverid', this.model.id);
        },
        triggerSelected: function() {
            App.navigate(this.$el.attr("data-id"));
            vent.trigger('AdminStudentView:Search');
        }
    });

    return SideItemView;

});
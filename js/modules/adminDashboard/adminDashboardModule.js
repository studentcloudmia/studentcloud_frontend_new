// access module
define([
    'marionette',
    'app',
    'vent',
    'modules/adminDashboard/router',
    'modules/adminDashboard/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var AdminDashboardModule = App.module('AdminDashboard');

        //bind to module finalizer event
        AdminDashboardModule.addFinalizer(function() {
	
        });

        AdminDashboardModule.addInitializer(function(options) {
		
            var controller = new Controller();
		
            new Router({
                controller: controller
            });            
		
        });

        return AdminDashboardModule;
    });
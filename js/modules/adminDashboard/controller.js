define([
'marionette',
'app',
'vent',
'modules/adminDashboard/show/show_controller'
],
function(Marionette, App, vent, AdminDashboardShowModule) {

    var Controller = Marionette.Controller.extend({

        initialize: function(options) {
			this.listenTo(vent, 'AdminDashboard:show', function(){
                App.navigate('adminDashboard');
                this.show();
            }, this);
        },

        show: function() {
            //transfer control to show submodule
            new AdminDashboardShowModule.Controller({region: App.main});
        }

    });

    return Controller;

});

define([
'marionette'
],
function(Marionette) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'adminDashboard': 'show'
        }
    });

    return Router;

});
// fbuilder college
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/fbuilder/college/controllers/mainContent_controller',
    'modules/fbuilder/sidebar/controllers/Sidebar_controller',
    'modules/fbuilder/builders_controller',
    //models
    'entities/collections/college/CollegeCollection',
    'entities/collections/BaseCollection',
    'entities/models/college/CollegeModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, CollegeCollection, BaseCollection, CollegeModel) {

            var FbuilderCollegeModule = App.module('Fbuilder.College');

            FbuilderCollegeModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Fbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new CollegeCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new CollegeCollection();
                    this.collegeModel = new CollegeModel();
                    this.coll = new CollegeCollection();
                    this.model = new CollegeModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Colleges'});

                    opts = {
                        entityHeading: 'College',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        collegeModel: this.collegeModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout,
                        widg: options.widg,
                        modelId: options.modelId
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new CollegeModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return FbuilderCollegeModule;
        });
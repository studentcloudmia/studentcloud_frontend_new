// fbuilder classification mainContent view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/fbuilder/classification/templates/mainContent.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        tagName: 'form',
        id: 'Classification',
        className: 'formHeigh',
        ui: {
            countries: '#country',
            states: '#state',
            idInput: '#id',
            validationBtn: '.validationBtn',
            eff_date: '.eff_date'
        },
        onRender: function() {
            var self = this;

            //mask inputs
            self.$el.find('.phoneFormat').mask('999-999-9999');
            //start jquery validate
            self.$effDate = self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);

            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker, .picker__select--month, .picker__select--year").jqBootstrapValidation();
            });
            //readonly id if model is not new
            if (!this.model.isNew()) {
                this.ui.idInput.attr("readonly", true);
            }
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;

            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        }
    });

    return MainContentView;

});
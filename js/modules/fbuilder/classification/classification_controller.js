// fbuilder classification
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/fbuilder/classification/controllers/mainContent_controller',
    'modules/fbuilder/sidebar/controllers/Sidebar_controller',
    'modules/fbuilder/builders_controller',
    //models
    'entities/collections/classification/ClassificationCollection',
    'entities/collections/BaseCollection',
    'entities/models/classification/ClassificationModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, ClassificationCollection, BaseCollection, ClassificationModel) {

            var FbuilderClassificationModule = App.module('Fbuilder.Classification');

            FbuilderClassificationModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Fbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new ClassificationCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new ClassificationCollection();
                    this.classificationModel = new ClassificationModel();
                    this.coll = new ClassificationCollection();
                    this.model = new ClassificationModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Classifications'});


                    opts = {
                        entityHeading: 'Classification',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        classificationModel: this.classificationModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout,
                        widg: options.widg,
                        modelId: options.modelId
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    ;
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new ClassificationModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }




            });

            return FbuilderClassificationModule;
        });
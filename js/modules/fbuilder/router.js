define([
'marionette',
'app',
],
function(Marionette, app) {

	var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'fbuilder/institution': 'showInstitution',
            'fbuilder/college': 'showCollege',
            'fbuilder/department': 'showDepartment',
            'fbuilder/classification': 'showClassification'
        }
    });

    return Router;

});
// fbuilder module
define([
    'marionette',
    'app',
    'vent',
    'modules/fbuilder/router',
    'modules/fbuilder/controller'
    ],
    function(Marionette, App, vent, Router, Controller) {

        var FbuilderModule = App.module('Fbuilder');

        //bind to module finalizer event
        FbuilderModule.addFinalizer(function() {
	
        });

        FbuilderModule.addInitializer(function(options) {
		
            new Router({
                controller: new Controller()
            });
		
        });

        return FbuilderModule;
    });
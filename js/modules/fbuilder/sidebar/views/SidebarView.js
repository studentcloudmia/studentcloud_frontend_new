// SideView
define([
    'marionette',
    'app',
    'modules/fbuilder/sidebar/views/SidebarItemView',
    'tpl!modules/fbuilder/sidebar/templates/sidebar.tmpl'
], function(Marionette, App, ItemView, template) {

    var SideView = App.Views.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: 'tbody',
        initialize: function(options) {
            this.setSearchString = options.searchString;
        },
        triggers: {
            'change #searchField': 'search'
        },
        ui: {
            search: '#searchField'
        },
        onShow: function() {
            this.$el.find('#searchField').attr('placeholder', 'Search ' + this.setSearchString);
        },
        getSearchText: function() {
            var val = this.ui.search.val();
            this.ui.search.val('');
            this.ui.search.blur();
            return val;
        }
    });
    return SideView;
});
// fbuilder institution
define(['marionette',
    'app',
    'vent'
],
        function(Marionette, App, vent) {

            var BuildersController = App.module('Fbuilder.BuildersController');

            BuildersController.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    //save region where we can show
                    this.region = options.region;
                    //save heading
                    this.entityHeading = options.entityHeading;
                    //options passed by caller
                    this.sideController = options.sideController;
                    this.collection = options.collection;
                    this.historicalCol = options.historicalCol;
                    this.coll = options.coll;
                    this.model = options.model;
                    this.sidebarController = options.sidebarController;
                    this.layout = options.layout;
                    this.widg = options.widg;
                    this.sentModelId = options.modelId;

                    //listen to layout Historic Nav to to navigate historic dates
                    this.listenTo(this.layout, 'Historic:Nav', this.historicalNav, this);


                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    //listen to show event and start our controllers
                    this.listenTo(this.layout, 'show', function() {
                        this.showHeader();
                        //this.showMain();
                        this.showFooter();
                    }, this);

                    this.listenTo(vent, 'Fbuilder:Selected:Model', this.historicalRecords, this);

                    this.listenTo(this.historicalCol, 'reset', this.historicReturned, this);

                    //show layout
                    this.region.show(this.layout);
                    if (!this.widg) {
                        //ask for sidebar
                        vent.trigger('Sidebar:On', {controller: this.sidebarController});
                        this.getCount();
                        this.activateDroppable();
                    } else {
                        var self = this;
                        this.model.fetch({success: function() {
                                self.historicalRecords(self.model);
                            }}, {id: this.sentModelId}, {}, false);
                    }

                    this.listeningToMain = false;

                },
                showHeader: function() {
                    //start header controller
                    var links = App.request("Fbuilder:getLinks");
                    var heading = App.request("Fbuilder:getHeading");
                    var options = {
                        region: this.layout.header,
                        heading: heading,
                        links: links,
                        compact: this.widg
                    };

                    vent.trigger('Components:Builders:Header:on', options);
                },
                showMain: function() {
                    //hide sidebar
                    vent.trigger('Sidebar:Close');
                    //update header details
                    vent.trigger('Components:Builders:Header:changeDetails', {model: this.model});
                    //start header controller
                    this.mainController = App.request("Builders:Get:New:MainContentController", this.model);
                    this.setupMainListeners();

                },
                showFooter: function() {
                    //start footer controller
                    var options = {
                        region: this.layout.footer
                    };

                    vent.trigger('Components:Builders:Footer:on', options);
                    this.bindFooterEvents();
                },
                updateFooter: function(options) {
                    vent.trigger('Components:Builders:Footer:updateButtons', options);
                },
                getCount: function() {
                    this.coll.fetch({}, {}, {length: 'True'}, false);
                    this.listenTo(this.coll, 'sync', this.countReturn, this);
                },
                countReturn: function() {
                    var count = this.coll.at(0).get('RowCount');
                    //delete reference to coll
                    this.coll = null;
                    if (count > 0) {
                        //open sidebar to allow search
                        vent.trigger('Sidebar:Open');
                        //update footer buttons
                        this.updateFooter({add: true});
                    } else {
                        //show user empty form to allow them to add
                        this.add();
                    }
                },
                bindFooterEvents: function() {
                    var self = this;
                    self.listenTo(vent, 'Components:Builders:Footer:save', self.save, self);
                    self.listenTo(vent, 'Components:Builders:Footer:add', self.add, self);
                    self.listenTo(vent, 'Components:Builders:Footer:del', self.del, self);
                    self.listenTo(vent, 'Components:Builders:Footer:prev', self.prev, self);
                    self.listenTo(vent, 'Components:Builders:Footer:next', self.next, self);
                },
                setupMainListeners: function() {
                    this.listenTo(this.mainController, 'created', this.created, this);
                    this.listenTo(this.mainController, 'destroyed', this.destroyed, this);
                    this.listenTo(this.mainController, 'updated', this.updated, this);
                },
                save: function() {
                    this.mainController.save();
                },
                add: function() {
                    //empty hist collection and remove future and past buttons
                    if (this.historicalCol) {
                        this.historicalCol.reset({}, {silent: true});
                    }
                    this.layout.hideLeft();
                    this.layout.hideRight();
                    //show user empty form to allow them to add
                    this.model = App.request("Builders:Get:New:Model", this.model);
                    this.showMain();
                    //update footer buttons
                    this.updateFooter({save: true});
                },
                del: function() {
                    this.mainController.del();
                },
                prev: function() {
                    this.navModel = this.collection.prevModel(this.navModel);
                    this.historicalRecords(this.navModel.id);
                },
                next: function() {
                    this.navModel = this.collection.nextModel(this.navModel);
                    this.historicalRecords(this.navModel.id);
                },
                historicalNav: function(option) {
                    //call selected model and pass the effective date
                    this.selectedModel({effdt: option});

                },
                created: function() {
                    this.updateFooter({save: true, add: true, del: true});
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.entityHeading + ' Created'
                    });
                    //refresh histColl
                    this.historicalFetch();
                },
                updated: function() {
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.entityHeading + ' Updated'
                    });
                    //refresh histColl
                    this.historicalFetch();
                },
                destroyed: function() {
                    //redo search
                    this.sidebarController.search();
                    //if this was the last model then remove from search and show add
                    if (this.historicalCol.length == 0) {
                        this.updateFooter();
                        this.add();
                    } else {
                        //refresh histColl
                        this.historicalFetch(true);
                    }
                    vent.trigger('Components:Alert:Alert', {
                        heading: 'Success',
                        message: this.entityHeading + ' Deleted'
                    });

                },
                //selected model takes either an eff_date or a model id
                selectedModel: function(options) {
                    //if model id is passed and eff_date is null get model from collection by ID ( used for regular)
                    if (options.effdt == null) {
                        this.showMain();
                        this.navModel = this.collection.where({model_id: options.modelId})[0];
                        this.setHistoricNavigation(this.model);
                    }
                    //if eff date is passed get model from coll by eff date ( used for historical )
                    else if (options.effdt != null) {
                        this.model = this.historicalCol.where({eff_date: options.effdt})[0];
                        this.showMain();
                        this.navModel = this.collection.where({model_id: this.model.get("model_id")})[0];
                        this.setHistoricNavigation(this.model);
                    }
                    if (!this.widg) {
                        //update footer buttons
                        if (this.collection.hasPrev(this.navModel) && this.collection.hasNext(this.navModel))
                            this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: true})
                        else if (this.collection.hasNext(this.navModel))
                            this.updateFooter({save: true, del: true, add: true, collNav: true, next: true, prev: false})
                        else if (this.collection.hasPrev(this.navModel))
                            this.updateFooter({save: true, del: true, add: true, collNav: true, next: false, prev: true});
                        else
                            this.updateFooter({save: true, del: true, add: true, collNav: false, next: false, prev: false});
                    } else {
                        this.updateFooter({save: true, del: true});
                    }

                },
                //fetched collection returns
                historicReturned: function() {
                    this.model = this.historicalCol.where({Status: "CURRENT"})[0];
                    //call selected model with model ID (first time)
                    this.selectedModel({modelId: this.model.get("model_id")});
                },
                setHistoricNavigation: function(model) {
                    var curIndex = this.historicalCol.indexOf(model);
                    //nav thru historical records
                    //sets triggers to layout in vent for show hide navigators and to set the nav value
                    if (this.historicalCol.length > 1) {
                        if (curIndex < this.historicalCol.length - 1) {
                            this.layout.showLeft({
                                eff_date: this.historicalCol.at(curIndex + 1).get('eff_date'),
                                status: this.historicalCol.at(curIndex + 1).get('Status'),
                                counter: this.historicalCol.length - curIndex - 1
                            });
                        }

                        else if (curIndex == this.historicalCol.length - 1) {
                            this.layout.hideLeft();
                        }
                        if (curIndex > 0 && curIndex <= this.historicalCol.length - 1) {
                            this.layout.showRight({
                                eff_date: this.historicalCol.at(curIndex - 1).get('eff_date'),
                                status: this.historicalCol.at(curIndex - 1).get('Status'),
                                counter: curIndex
                            });
                        }
                        else if (curIndex == 0) {
                            this.layout.hideRight();
                        }


                    }
                    else if (this.historicalCol.length == 1)
                    {
                        this.layout.hideRight();
                        this.layout.hideLeft();
                    }

                },
                historicalRecords: function(modelId) {
                    if (!this.widg) {
                        this.model = this.collection.get(modelId);
                    }
                    //this.model has already been set
                    this.historicalFetch();
                },
                activateDroppable: function() {
                    var self = this;
                    $(this.layout.mainContent.el).droppable({
                        accept: ".searchRow",
                        hoverClass: "ui-state-highlight",
                        drop: function(event, ui) {
                            //call selected model with dragged model
                            self.historicalRecords(ui.draggable.attr("data-id"));
                        }
                    });
                },
                historicalFetch: function(changeId) {
                    _.defaults(changeId, false);
                    var useId = 0;
                    if (changeId) {
                        useId = this.historicalCol.at(0).id;
                    } else {
                        useId = this.model.id;
                    }
                    this.historicalCol.fetch({reset: true}, {id: useId}, {historical: 'True'}, false);

                }

            });

            return BuildersController;
        });
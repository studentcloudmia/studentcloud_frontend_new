// fbuilder institution
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/fbuilder/institution/controllers/mainContent_controller',
    'modules/fbuilder/sidebar/controllers/Sidebar_controller',
    'modules/fbuilder/builders_controller',
    //models
    'entities/collections/institution/InstitutionCollection',
    'entities/collections/BaseCollection',
    'entities/models/institution/InstitutionModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, InstitutionCollection, BaseCollection, InstitutionModel) {

            var FbuilderInstitutionModule = App.module('Fbuilder.Institution');

            FbuilderInstitutionModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Fbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new InstitutionCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new InstitutionCollection();
                    this.institutionModel = new InstitutionModel();
                    this.coll = new InstitutionCollection();
                    this.model = new InstitutionModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Institutions'});


                    opts = {
                        entityHeading: 'Institution',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        institutionModel: this.institutionModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout,
                        widg: options.widg,
                        modelId: options.modelId
                    };
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new InstitutionModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }




            });

            return FbuilderInstitutionModule;
        });
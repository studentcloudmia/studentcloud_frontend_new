define([
    'underscore',
    'marionette',
    'app',
    'vent',
    //views
    'modules/fbuilder/views/layout',
    //controllers
    'modules/fbuilder/institution/institution_controller',
    'modules/fbuilder/college/college_controller',
    'modules/fbuilder/department/department_controller',
    'modules/fbuilder/classification/classification_controller',
    //sidebar controllers
    'modules/fbuilder/sidebar/controllers/Sidebar_controller',
    //fbuilder collections
    'entities/collections/institution/InstitutionCollection',
    'entities/collections/college/CollegeCollection',
    'entities/collections/department/DepartmentCollection'
],
        function(_, Marionette, App, vent, LayoutView, FbuilderInstitutionModule, FbuilderCollegeModule, FbuilderDepartmentModule, FbuilderClassificationModule,
                //sidebar controllers
                SideController,
                //sidebar collections
                InstitutionCollection, CollegeCollection, DepartmentCollection) {

            var Controller = Marionette.Controller.extend({
                initialize: function(options) {

                    App.reqres.setHandler("Fbuilder:getLayout", function() {
                        var layout = new LayoutView();
                        return layout;
                    });

                    App.reqres.setHandler("Fbuilder:getHeading", function() {
                        return 'Foundation Builder';
                    });

                    App.reqres.setHandler("Fbuilder:getLinks", function() {
                        //TODO: These should be generated from menu collection
                        return [{
                                name: 'Institution',
                                link: 'fbuilder/institution'
                            }, {
                                name: 'College',
                                link: 'fbuilder/college'
                            }, {
                                name: 'Department',
                                link: 'fbuilder/department'
                            }, {
                                name: 'Classification',
                                link: 'fbuilder/classification'
                            }];
                    });

                    this.listenTo(vent, 'Fbuilder:showInstitution', function() {
                        App.navigate('fbuilder/institution');
                        this.showInstitution();
                    }, this);

                    this.listenTo(vent, 'Fbuilder:showCollege', function() {
                        App.navigate('fbuilder/college');
                        this.showCollege();
                    }, this);

                    this.listenTo(vent, 'Fbuilder:showDepartment', function() {
                        App.navigate('fbuilder/department');
                        this.showDepartment();
                    }, this);

                    this.listenTo(vent, 'Fbuilder:showClassification', function() {
                        App.navigate('fbuilder/department');
                        this.showClassification();
                    }, this);

                    /*allow others to call for your sidebar controllers*/
                    App.reqres.setHandler("Fbuilder:Institution:SideController", function() {
                        return new SideController({collection: new InstitutionCollection(), searchString: 'Institutions'});
                    });
                    App.reqres.setHandler("Fbuilder:College:SideController", function() {
                        return new SideController({collection: new CollegeCollection(), searchString: 'Colleges'});
                    });
                    App.reqres.setHandler("Fbuilder:Department:SideController", function() {
                        return new SideController({collection: new DepartmentCollection(), searchString: 'Departments'});

                    });
                    var self = this;
                    /*allow others to call for your main content controllers*/
                    App.reqres.setHandler("Fbuilder:Institution:MainController", function(options) {
                        self.showInstitution(options);
                    });
                    App.reqres.setHandler("Fbuilder:College:MainController", function(options) {
                        self.showCollege(options);
                    });
                    App.reqres.setHandler("Fbuilder:Department:MainController", function(options) {
                        self.showDepartment(options);
                    });
                },
                showInstitution: function(options) {
                    options = options || {};
                    _.defaults(options, {
                        region: App.main,
                        widg: false,
                        modelId: ''
                    });
                    //transfer control to show submodule
                    new FbuilderInstitutionModule.Controller(options);
                },
                showCollege: function(options) {
                    options = options || {};
                    _.defaults(options, {
                        region: App.main,
                        widg: false,
                        modelId: ''
                    });
                    //transfer control to show submodule
                    new FbuilderCollegeModule.Controller(options);
                },
                showDepartment: function(options) {
                    options = options || {};
                    _.defaults(options, {
                        region: App.main,
                        widg: false,
                        modelId: ''
                    });
                    //transfer control to show submodule
                    new FbuilderDepartmentModule.Controller(options);
                },
                showClassification: function(options) {
                    options = options || {};
                    _.defaults(options, {
                        region: App.main,
                        widg: false,
                        modelId: ''
                    });
                    //transfer control to show submodule
                    new FbuilderClassificationModule.Controller(options);
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Fbuilder:getLayout");
                    App.reqres.removeHandler("Fbuilder:getHeading");
                    App.reqres.removeHandler("Fbuilder:getLinks");
                    App.reqres.removeHandler("Fbuilder:Institution:SideController");
                    App.reqres.removeHandler("Fbuilder:College:SideController");
                    App.reqres.removeHandler("Fbuilder:Department:SideController");
                    App.reqres.removeHandler("Fbuilder:Institution:MainController");
                    App.reqres.removeHandler("Fbuilder:College:MainController");
                    App.reqres.removeHandler("Fbuilder:Department:MainController");
                }

            });

            return Controller;

        });
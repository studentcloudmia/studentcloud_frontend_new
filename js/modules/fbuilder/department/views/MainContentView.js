// fbuilder department mainContent view
define([
    'underscore',
    'marionette',
    'vent',
    'app',
    'globalConfig',
    'tpl!modules/fbuilder/department/templates/mainContent.tmpl'
], function(_, Marionette, vent, App, GC, template) {

    var MainContentView = App.Views.ItemView.extend({
        template: template,
        tagName: 'form',
        id: 'Department',
        className: 'formHeigh',
        ui: {
            countries: '#country',
            states: '#state',
            idInput: '#id',
            validationBtn: '.validationBtn',
            eff_date: '.eff_date'
        },
        events: {
            'change #country': 'countryChange'
        },
        onRender: function() {
            var self = this;

            //mask inputs
            self.$el.find('.phoneFormat').mask('999-999-9999');
            self.$el.find('.zipFormat').mask('99999-9999');

            self.$effDate = self.$el.find('.datepicker').pickadate(GC.pickadateDefaults);

            $(function() {
                self.$el.find("input,select,textarea").not("[type=submit] , .datepicker, .picker__select--month, .picker__select--year").jqBootstrapValidation();
            });
            //readonly id if model is not new
            if (!this.model.isNew()) {
                this.ui.idInput.attr("readonly", true);
            }
        },
        resetCountries: function(countries) {
            //inject options to countries select
            var self = this;
            this.ui.countries.html('');
            _.each(countries, function(country) {
                //mark as selected if country matches model country
                if (self.model.isNew()) {
                    var sel = (country.Description_Long == 'United States') ? 'selected' : '';
                } else {
                    var sel = (country.id == self.model.get('Address').Country) ? 'selected' : '';
                }

                self.ui.countries.append('<option value="' + country.id + '" ' + sel + '>' + country.Description_Long + '</option>');
            });
            //update states with select option
            this.ui.states.html('<option selected>No States</option>');
            //trigger select on countries
            this.ui.countries.change();
        },
        resetStates: function(states) {
            //inject options to countries select
            if (states.length > 0) {
                var self = this;
                this.ui.states.html('');
                _.each(states, function(state) {
                    //mark as selected if state matches model state
                    var sel = (state.id == self.model.get('Address').State) ? 'selected' : '';
                    var statesVal = state.Description_Short + ' - ' + state.Description_Long
                    self.ui.states.append('<option value="' + state.id + '" ' + sel + '>' + statesVal + '</option>');
                });
                //trigger select on states
                this.ui.states.change();
            }
            else {
                this.ui.states.html('<option selected>No States</option>');
            }
        },
        countryChange: function() {
            var country = this.ui.countries.val();
            this.trigger('countryChange', country);
        },
        updateModel: function() {
            //ensure the form is valid
            var self = this;
            if (self.$el.jqBootstrapValidation("hasErrors")) {
                self.ui.validationBtn.trigger('click');
                return false;

            } else {
                var data = Backbone.Syphon.serialize(this);
                self.model.set(data, {silent: true});
                return true;
            }
        }
    });

    return MainContentView;

});
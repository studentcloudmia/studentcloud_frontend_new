// fbuilder department
define(['marionette',
    'app',
    'vent',
    //controllers
    'modules/fbuilder/department/controllers/mainContent_controller',
    'modules/fbuilder/sidebar/controllers/Sidebar_controller',
    'modules/fbuilder/builders_controller',
    //models
    'entities/collections/department/DepartmentCollection',
    'entities/collections/BaseCollection',
    'entities/models/department/DepartmentModel'
],
        function(Marionette, App, vent, MainContentController, SideController, BuildersController, DepartmentCollection, BaseCollection, DepartmentModel) {

            var FbuilderDepartmentModule = App.module('Fbuilder.Department');

            FbuilderDepartmentModule.Controller = Marionette.Controller.extend({
                Layout: {},
                initialize: function(options) {
                    var self = this;
                    this.layout = App.request("Fbuilder:getLayout");
                    //save region where we can show
                    this.region = options.region;
                    this.collection = new DepartmentCollection();
                    //Using base collection to fetch historical records. Since they have same id, can't use actual collection
                    this.historicalCol = new DepartmentCollection();
                    this.departmentModel = new DepartmentModel();
                    this.coll = new DepartmentCollection();
                    this.model = new DepartmentModel();
                    this.sidebarController = new SideController({collection: this.collection, searchString: 'Departments'});

                    opts = {
                        entityHeading: 'Department',
                        sideController: this.sidebarController,
                        collection: this.collection,
                        historicalCol: this.historicalCol,
                        departmentModel: this.departmentModel,
                        coll: this.coll,
                        model: this.model,
                        region: options.region,
                        sidebarController: this.sidebarController,
                        layout: this.layout,
                        widg: options.widg,
                        modelId: options.modelId
                    },
                    this.buildersController = new BuildersController.Controller(opts);

                    //close controller when layout closes
                    this.listenTo(this.layout, 'close', function() {
                        this.close();
                    }, this);

                    App.reqres.setHandler("Builders:Get:New:MainContentController", function(model) {
                        return new MainContentController({region: self.layout.mainContent, model: model});
                    });
                    App.reqres.setHandler("Builders:Get:New:Model", function() {
                        return new DepartmentModel();
                    });
                },
                onClose: function() {
                    //remove all handlers
                    App.reqres.removeHandler("Builders:Get:New:MainContentController");
                    App.reqres.removeHandler("Builders:Get:New:Model");
                }



            });

            return FbuilderDepartmentModule;
        });
define([
'jquery',
'underscore',
'underscoreTemplateHelper'
],
function($, _, _Helper) {
	
	//if and only if function definition
    var iff = function(condition, outputString) {
        return condition ? outputString: "";
    }
	//add iff to global template helpers
    _.addTemplateHelpers({iff: iff});
    return iff;
});

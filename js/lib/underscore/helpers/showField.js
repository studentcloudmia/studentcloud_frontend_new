define([
'jquery',
'underscore',
'underscoreTemplateHelper'
],
function($, _, _Helper) {

    var displayAppField = function(field) {
	
        //initiate input field
        var input = "";
        var customClass = "input-large ";
        var attribute = "";
        //add asterisk and class for required fields
        if (field.Req) {
            field.Name = '<strong>' + field.Name + '</strong> *';
            customClass = customClass + "requiredField ";
            attribute = attribute + "required";
            attribute = attribute + " data-validation-required-message=" + field.Error;
        }

        if (field.Pattern.length > 0) {
            attribute = attribute + ' pattern="' + field.Pattern + '"';
        }

        if (!field.Visible) {
            field.Type = "hidden";
        }

        if (field.Value == "null") {
            field.Value = "";
        }

        if (field.Type == "date" && field.Value.length > 0) {
            //format date 2013-05-14T00:00:00
            field.Value = moment(field.Value, 'YYYY-MM-DDTHH:mm:ss').format('MM/DD/YYYY');
        }

        switch (field.Type)
        {
        case "text":
        case "tel":
        case "number":
        case "email":
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls"><input id="' + field.id + '" name="' + field.id + '" type="' + field.Type + '" value="' + field.Value + '" class="' + customClass + '" ' + attribute + '>';
            break;
        case "check-single":
            input = '<div class="controls"><label class="checkbox"><input id="' + field.id + '" name="' + field.id + '" type="checkbox" class="' + customClass + '" ' + attribute + '> ' + field.Name + '</label>';
            break;
        case "check-multiple":
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls">';
            input = input + '<label class="checkbox"><input type="checkbox" name="' + field.id + '" value="1"> Option 1</label>';
            input = input + '<label class="checkbox"><input type="checkbox" name="' + field.id + '" value="2"> Option 2</label>';
            input = input + '<label class="checkbox"><input type="checkbox" name="' + field.id + '" value="3"> Option 3</label>';
            break;
        case "textarea":
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls"><textarea rows="4" class="' + customClass + '" name="' + field.id + '" id="' + field.id + '" value="' + field.Value + '" pattern="' + field.Pattern + '" ' + attribute + '></textarea>';
            break;
        case "date":
            customClass = customClass + "datepicker ";
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls"><input id="' + field.id + '" name="' + field.id + '" type="date" value="' + field.Value + '" class="datepicker' + customClass + '" ' + attribute + '>';
            break;
        case "select":
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls"><select id="' + field.id + '" name="' + field.id + '" class="' + customClass + '" ' + attribute + '><option> </option><option value="M">Male</option><option value="F">Female</option></select>';
            break;
        case "radio":
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls">';
            input = input + '<label class="radio"><input type="radio" name="' + field.id + '" id="' + field.id + '" value="Y">Yes</label>';
            input = input + '<label class="radio"><input type="radio" name="' + field.id + '" id="' + field.id + '" value="N">No</label>';
            break;
        case "hidden":
            input = '<input id="' + field.id + '" name="' + field.id + '" type="' + field.Type + '" value="' + field.Value + '" class="' + customClass + '" ' + attribute + '>';
            break;
        default:
            input = '<label class="control-label" for="' + field.id + '">' + field.Name + '</label><div class="controls"><input placeholder="' + field.Type + ' not yet defined" id="' + field.id + '" name="' + field.id + '" type="text" class="' + customClass + '" ' + attribute + '>';

        }
        //insert validation image for none hidden fields
        if (field.Type !== "hidden")
        input = input + '<span class="icon-form-validation" data-icomoon="&#xe0fb;"></span></div>';

        return input;
    }

    _.addTemplateHelpers({displayAppField: displayAppField});
    return displayAppField;
});

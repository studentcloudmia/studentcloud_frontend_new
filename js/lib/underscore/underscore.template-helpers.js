/*
	License:
	Copyright (c) 2012 David Beck, Rotunda Software

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
	
	Site: https://github.com/rotundasoftware/underscore-template-helpers
*/

define([
'jquery',
'underscore'
],
function($, _) {
	
    var originalUnderscoreTemplateFunction = _.template;
    templateHelpers = {};

    _.mixin({
        addTemplateHelpers: function(newHelpers) {
            var test = _.extend(templateHelpers, newHelpers);
        },

        template: function(text, data, settings) {
	
            // replace the built in _.template function with one that supports the addApp.templateHelpers
            // function above. Basically the combo of the addApp.templateHelpers function and this new
            // template function allows us to mix in global "helpers" to the data objects passed
            // to all our templates when they render. This replacement template function just wraps
            // the original _.template function, so it sould be pretty break-resistent moving forward.
            if (data)
            {
                // if data is supplied, the original _.template function just returns the raw value of the
                // render function (the final rentered html/text). So in this case we just extend
                // the data param with our App.templateHelpers and return raw value as well.
                _.extend(data, templateHelpers);
                // extend data with our helper functions
                return originalUnderscoreTemplateFunction.apply(this, arguments);
                // pass the buck to the original _.template function
            }

            var template = originalUnderscoreTemplateFunction.apply(this, arguments);

            var wrappedTemplate = function(data) {
                if (data) _.defaults(data, templateHelpers);
                return template.apply(this, arguments);
            };

            return wrappedTemplate;
        }
    });
});
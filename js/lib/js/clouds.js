// clouds.js
define([''],function(){
	var Clouds = {
	    initialize: function(){
            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];
            for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
                window.cancelRequestAnimationFrame = window[vendors[x] + 'CancelRequestAnimationFrame'];
            }
            if (!window.requestAnimationFrame) window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date()
                    .getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() {
                    callback(currTime + timeToCall);
                },
                timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

            if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
            
            this.run = true;
            this.animated = [];
            this.layers = [];
            this.objects = [];

            this.world = document.getElementById('world');
            this.viewport = document.getElementById('sky');
            //add class to viewport
            this.viewport.className = 'sky';

            this.d = 0;
            this.p = 400;
            this.worldXAngle = 0;
            this.worldYAngle = 0;

            this.viewport.style.webkitPerspective = this.p;
            this.viewport.style.MozPerspective = this.p;
            this.viewport.style.oPerspective = this.p;

            this.generate();
            //this.update();
            requestAnimationFrame( this.update );
	    },
        
        createCloud: function() {

            var div = document.createElement('div');
            div.className = 'cloudBase';
            var x = 256 - (Math.random() * 512);
            var y = 256 - (Math.random() * 512);
            var z = 256 - (Math.random() * 512);
            var t = 'translateX( ' + x + 'px ) translateY( ' + y + 'px ) translateZ( ' + z + 'px )';
            div.style.webkitTransform = t;
            div.style.MozTransform = t;
            div.style.oTransform = t;
            this.world.appendChild(div);

            for (var j = 0; j < 5 + Math.round(Math.random() * 10); j++) {
                var cloud = document.createElement('div');
                cloud.style.opacity = 0;
                cloud.style.opacity = .8;
                cloud.className = 'cloudLayer';

                var x = 256 - (Math.random() * 512);
                var y = 256 - (Math.random() * 512);
                var z = 100 - (Math.random() * 200);
                var a = Math.random() * 360;
                var s = .25 + Math.random();
                x *= .2;
                y *= .2;
                cloud.data = {
                    x: x,
                    y: y,
                    z: z,
                    a: a,
                    s: s,
                    speed: .1 * Math.random()
                };
                var t = 'translateX( ' + x + 'px ) translateY( ' + y + 'px ) translateZ( ' + z + 'px ) rotateZ( ' + a + 'deg ) scale( ' + s + ' )';
                cloud.style.webkitTransform = t;
                cloud.style.MozTransform = t;
                cloud.style.oTransform = t;

                div.appendChild(cloud);
                this.layers.push(cloud);
            }

            return div;
        },
        
        generate: function() {
            this.objects = [];
            if (this.world.hasChildNodes()) {
                while (this.world.childNodes.length >= 1) {
                    this.world.removeChild(world.firstChild);
                }
            }
            for (var j = 0; j < 15; j++) {
                this.objects.push(this.createCloud());
            }
        },

        updateView: function() {
            var t = 'translateZ( ' + this.d + 'px ) rotateX( ' + this.worldXAngle + 'deg) rotateY( ' + this.worldYAngle + 'deg)';
            this.world.style.webkitTransform = t;
            this.world.style.MozTransform = t;
            this.world.style.oTransform = t;
        },

        onContainerMouseWheel: function(event) {

            event = event ? event : window.event;
            this.d = this.d - (event.detail ? event.detail * -5 : event.wheelDelta / 8);
            this.updateView();

        },

        update: function() {
            var self = Clouds;
            if(self.run){
                for (var j = 0; j < self.layers.length; j++) {
                    var layer = self.layers[j];
                    layer.data.a += layer.data.speed;
                    var t = 'translateX( ' + layer.data.x + 'px ) translateY( ' + layer.data.y + 'px ) translateZ( ' + layer.data.z + 'px ) rotateY( ' + (-self.worldYAngle) + 'deg ) rotateX( ' + (-self.worldXAngle) + 'deg ) rotateZ( ' + layer.data.a + 'deg ) scale( ' + layer.data.s + ')';
                    layer.style.webkitTransform = t;
                    layer.style.MozTransform = t;
                    layer.style.oTransform = t;
                }

                var id = requestAnimationFrame(self.update);
                self.animated.push(id);
            }
        },
        
        close: function(){
            this.run = false;
            //cancel all animations
            for (var j = 0; j < this.animated.length; j++) {
                cancelAnimationFrame(this.animated[j]);
            }
            //remove class from viewport
            this.viewport.className = '';
            //remove all clouds
            if (this.world.hasChildNodes()) {
                while (this.world.childNodes.length >= 1) {
                    this.world.removeChild(world.firstChild);
                }
            }
        }
        
	}
    
	return Clouds;
});

/**
 * jQuery.popover2 plugin v1.1.2
 * By Davey IJzermans
 * See http://wp.me/p12l3P-gT for details
 * http://daveyyzermans.nl/
 * 
 * Released under MIT License.
 */

; (function($) {
    //define some default plugin options
    var defaults = {
		id: 'default',
		//default id. If id is not changed then it will not be placed in div
        verticalOffset: 10,
        //offset the popover2 by y px vertically (movement depends on position of popover2. If position == 'bottom', positive numbers are down)
        horizontalOffset: 10,
        //offset the popover2 by x px horizontally (movement depends on position of popover2. If position == 'right', positive numbers are right)
        title: false,
        //heading, false for none
        content: false,
        //content of the popover2
        url: false,
        //set to an url to load content via ajax
        classes: '',
        //classes to give the popover2, i.e. normal, wider or large
        position: 'auto',
        //where should the popover2 be placed? Auto, top, right, bottom, left or absolute (i.e. { top: 4 }, { left: 4 })
        fadeSpeed: 160,
        //how fast to fade out popover2s when destroying or hiding
        trigger: 'click',
        //how to trigger the popover2: click, hover or manual
        preventDefault: true,
        //preventDefault actions on the element on which the popover2 is called
        stopChildrenPropagation: true,
        //prevent propagation on popover2 children
        hideOnHTMLClick: false,
        //hides the popover2 when clicked outside of it
        animateChange: true,
        //animate a popover2 reposition
        autoReposition: true,
        //automatically reposition popover2 on popover2 change and window resize
        anchor: false
        //anchor the popover2 to a different element
    }
    var popover2s = [];
    var _ = {
        calc_position: function(popover2, position) {
            var data = popover2.popover2("getData");
            var options = data.options;
            var $anchor = options.anchor ? $(options.anchor) : popover2;
            var el = data.popover2;

            var coordinates = $anchor.offset();
            var y1,
            x1;

            if (position == 'top') {
                y1 = coordinates.top - el.outerHeight();
                x1 = coordinates.left - el.outerWidth() / 2 + $anchor.outerWidth() / 2;
            } else if (position == 'right') {
                y1 = coordinates.top + $anchor.outerHeight() / 2 - el.outerHeight() / 2;
                x1 = coordinates.left + $anchor.outerWidth();
            } else if (position == 'left') {
                y1 = coordinates.top + $anchor.outerHeight() / 2 - el.outerHeight() / 2;
                x1 = coordinates.left - el.outerWidth();
            } else {
                //bottom
                y1 = coordinates.top + $anchor.outerHeight();
                x1 = coordinates.left - el.outerWidth() / 2 + $anchor.outerWidth() / 2;
            }

            x2 = x1 + el.outerWidth();
            y2 = y1 + el.outerHeight();
            ret = {
                x1: x1,
                x2: x2,
                y1: y1,
                y2: y2
            };

            return ret;
        },
        pop_position_class: function(popover2, position) {
            var remove = "popover2-top popover2-right popover2-left";
            var arrow = "top-arrow"
            var arrow_remove = "right-arrow bottom-arrow left-arrow";

            if (position == 'top') {
                remove = "popover2-right popover2-bottom popover2-left";
                arrow = 'bottom-arrow';
                arrow_remove = "top-arrow right-arrow left-arrow";
            } else if (position == 'right') {
                remove = "popover2-yop popover2-bottom popover2-left";
                arrow = 'left-arrow';
                arrow_remove = "top-arrow right-arrow bottom-arrow";
            } else if (position == 'left') {
                remove = "popover2-top popover2-right popover2-bottom";
                arrow = 'right-arrow';
                arrow_remove = "top-arrow bottom-arrow left-arrow";
            }

            popover2
            .removeClass(remove)
            .addClass('popover2-' + position)
            .find('.arrow')
            .removeClass(arrow_remove)
            .addClass(arrow);
        }
    };
    var methods = {
        /**
		 * Initialization method
		 * Merges parameters with defaults, makes the popover2 and saves data
		 * 
		 * @param object
		 * @return jQuery
		 */
        init: function(params) {
            return this.each(function() {
                var options = $.extend({},
                defaults, params);

                var $this = $(this);
                var data = $this.popover2('getData');

                if (!data) {
					var idToUse = "";
					if(options.id !== "default")
						idToUse = "id=" + options.id;
					
                    var popover2 = $('<div '+idToUse+' class="popover2" />')
                    .addClass(options.classes)
                    .append('<div class="arrow" />')
                    .append('<div class="wrap"></div>')
                    .appendTo('body')
                    .hide();

                    if (options.stopChildrenPropagation) {
                        popover2.children().bind('click.popover2',
                        function(event) {
                            event.stopPropagation();
                        });
                    }

                    if (options.anchor) {
                        if (!options.anchor instanceof jQuery) {
                            options.anchor = $(options.anchor);
                        }
                    }

                    var data = {
                        target: $this,
                        popover2: popover2,
                        options: options
                    };

                    if (options.title) {
                        $('<div class="title" />')
                        .html(options.title instanceof jQuery ? options.title.html() : options.title)
                        .appendTo(popover2.find('.wrap'));
                    }
                    if (options.content) {
                        $('<div class="content2" />')
                        .html(options.content instanceof jQuery ? options.content.html() : options.content)
                        .appendTo(popover2.find('.wrap'));
                    }

                    $this.data('popover2', data);
                    popover2s.push($this);

                    if (options.url) {
                        $this.popover2('ajax', options.url);
                    }

                    $this.popover2('reposition');
                    $this.popover2('setTrigger', options.trigger);

                    if (options.hideOnHTMLClick) {
                        var hideEvent = "click.popover2";
                        if ("ontouchstart" in document.documentElement)
                        hideEvent = 'touchstart.popover2';
                        $('html').unbind(hideEvent).bind(hideEvent,
                        function(event) {
                            $('html').popover2('fadeOutAll');
                        });
                    }

                    if (options.autoReposition) {
                        var repos_function = function(event) {
                            $this.popover2('reposition');
                        };
                        $(window)
                        .unbind('resize.popover2').bind('resize.popover2', repos_function)
                        .unbind('scroll.popover2').bind('scroll.popover2', repos_function);
                    }
                }
            });
        },
        /**
		 * Reposition the popover2
		 * 
		 * @return jQuery
		 */
        reposition: function() {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;
                    var options = data.options;
                    var $anchor = options.anchor ? $(options.anchor) : $this;
                    var coordinates = $anchor.offset();

                    var position = options.position;
                    if (! (position == 'top' || position == 'right' || position == 'left' || position == 'auto')) {
                        position = 'bottom';
                    }
                    var calc;

                    if (position == 'auto') {
                        var positions = ["bottom", "left", "top", "right"];
                        var scrollTop = $(window).scrollTop();
                        var scrollLeft = $(window).scrollLeft();
                        var windowHeight = $(window).outerHeight();
                        var windowWidth = $(window).outerWidth();

                        $.each(positions,
                        function(i, pos) {
                            calc = _.calc_position($this, pos);

                            var x1 = calc.x1 - scrollLeft;
                            var x2 = calc.x2 - scrollLeft + options.horizontalOffset;
                            var y1 = calc.y1 - scrollTop;
                            var y2 = calc.y2 - scrollTop + options.verticalOffset;

                            if (x1 < 0 || x2 < 0 || y1 < 0 || y2 < 0)
                            //popover2 is left off of the screen or above it
                            return true;
                            //continue
                            if (y2 > windowHeight)
                            //popover2 is under the window viewport
                            return true;
                            //continue
                            if (x2 > windowWidth)
                            //popover2 is right off of the screen
                            return true;
                            //continue
                            position = pos;
                            return false;
                        });

                        if (position == 'auto') {
                            //position is still auto
                            return;
                        }
                    }

                    calc = _.calc_position($this, position);
                    var top = calc.top;
                    var left = calc.left;
                    _.pop_position_class(popover2, position);

                    var marginTop = 0;
                    var marginLeft = 0;
                    if (position == 'bottom') {
                        marginTop = options.verticalOffset;
                    }
                    if (position == 'top') {
                        marginTop = -options.verticalOffset;
                    }
                    if (position == 'right') {
                        marginLeft = options.horizontalOffset;
                        marginTop = options.verticalOffset;
                    }
                    if (position == 'left') {
                        marginLeft = -options.horizontalOffset;
                    }

                    var css = {
                        left: calc.x1,
                        top: calc.y1,
                        marginTop: marginTop,
                        marginLeft: marginLeft
                    };

                    if (data.initd && options.animateChange) {
                        popover2.css(css);
                    } else {
                        data.initd = true;
                        popover2.css(css);
                    }
                    $this.data('popover2', data);
                }
            });
        },
        /**
		 * Remove a popover2 from the DOM and clean up data associated with it.
		 * 
		 * @return jQuery
		 */
        destroy: function() {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                $this.unbind('.popover2');
                $(window).unbind('.popover2');
                data.popover2.remove();
                $this.removeData('popover2');
            });
        },
        /**
		 * Show the popover2
		 * 
		 * @return jQuery
		 */
        show: function() {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;					
					/*Innoware custom: Hide all other popovers before show*/
					// $this.popover2('hideAll');
					/*End Innoware Custom*/
                    $this.popover2('reposition');
                    popover2.clearQueue().css({
                        zIndex: 950
                    }).show();
                }
            });
        },
        /**
		 * Hide the popover2
		 * 
		 * @return jQuery
		 */
        hide: function() {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    data.popover2.hide().css({
                        zIndex: 949
                    });
                }
            });
        },
        /**
		 * Fade out the popover2
		 * 
		 * @return jQuery
		 */
        fadeOut: function(ms) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;
                    var options = data.options;
                    popover2.delay(100).css({
                        zIndex: 949
                    }).fadeOut(ms ? ms: options.fadeSpeed);
                }
            });
        },
        /**
		 * Hide all popover2s
		 * 
		 * @return jQuery
		 */
        hideAll: function() {
            return $.each(popover2s,
            function(i, pop) {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;
                    popover2.hide();
                }
            });
        },
        /**
		 * Fade out all popover2s
		 * 
		 * @param int
		 * @return jQuery
		 */
        fadeOutAll: function(ms) {
            return $.each(popover2s,
            function(i, pop) {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;
                    var options = data.options;
                    popover2.css({
                        zIndex: 949
                    }).fadeOut(ms ? ms: options.fadeSpeed);
                }
            });
        },
        /**
		 * Set the event trigger for the popover2. Also cleans the previous binding. 
		 * 
		 * @param string
		 * @return jQuery
		 */
        setTrigger: function(trigger) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var popover2 = data.popover2;
                    var options = data.options;
                    var $anchor = options.anchor ? $(options.anchor) : $this;

                    if (trigger === 'click') {
                        $anchor.unbind('click.popover2').bind('click.popover2',
                        function(event) {
                            if (options.preventDefault) {
                                event.preventDefault();
                            }
                            event.stopPropagation();
                            $this.popover2('show');
                        });
                        popover2.unbind('click.popover2').bind('click.popover2',
                        function(event) {
                            event.stopPropagation();
                        });
                    } else {
                        $anchor.unbind('click.popover2');
                        popover2.unbind('click.popover2')
                    }

                    if (trigger === 'hover') {
                        $anchor.add(popover2).bind('mousemove.popover2',
                        function(event) {
                            $this.popover2('show');
                        });
                        $anchor.add(popover2).bind('mouseleave.popover2',
                        function(event) {
                            $this.popover2('fadeOut');
                        });
                    } else {
                        $anchor.add(popover2).unbind('mousemove.popover2').unbind('mouseleave.popover2');
                    }

                    if (trigger === 'focus') {
                        $anchor.add(popover2).bind('focus.popover2',
                        function(event) {
                            $this.popover2('show');
                        });
                        $anchor.add(popover2).bind('blur.popover2',
                        function(event) {
                            $this.popover2('fadeOut');
                        });
                        $anchor.bind('click.popover2',
                        function(event) {
                            event.stopPropagation();
                        });
                    } else {
                        $anchor.add(popover2).unbind('focus.popover2').unbind('blur.popover2').unbind('click.popover2');
                    }
                }
            });
        },
        /**
		 * Rename the popover2's title
		 * 
		 * @param string
		 * @return jQuery
		 */
        title: function(text) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var title = data.popover2.find('.title');
                    var wrap = data.popover2.find('.wrap');
                    if (title.length === 0) {
                        title = $('<div class="title" />').appendTo(wrap);
                    }
                    title.html(text);
                }
            });
        },
        /**
		 * Set the popover2's content
		 * 
		 * @param html
		 * @return jQuery
		 */
        content: function(html) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var content = data.popover2.find('.content');
                    var wrap = data.popover2.find('.wrap');
                    if (content.length === 0) {
                        content = $('<div class="content2" />').appendTo(wrap);
                    }
                    content.html(html);
                }
            });
        },
        /**
		 * Read content with AJAX and set popover2's content.
		 * 
		 * @param string
		 * @param object
		 * @return jQuery
		 */
        ajax: function(url, ajax_params) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    var ajax_defaults = {
                        url: url,
                        success: function(ajax_data) {
                            var content = data.popover2.find('.content');
                            var wrap = data.popover2.find('.wrap');
                            if (content.length === 0) {
                                content = $('<div class="content2" />').appendTo(wrap);
                            }
                            content.html(ajax_data);
                        }
                    }
                    var ajax_options = $.extend({},
                    ajax_defaults, ajax_params);
                    $.ajax(ajax_options);
                }
            });
        },
        setOption: function(option, value) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.popover2('getData');

                if (data) {
                    data.options[option] = value;
                    $this.data('popover2', data);
                }
            });
        },
        getData: function() {
            var ret = [];
            this.each(function() {
                var $this = $(this);
                var data = $this.data('popover2');

                if (data) ret.push(data);
            });

            if (ret.length == 0) {
                return;
            }
            if (ret.length == 1) {
                ret = ret[0];
            }
            return ret;
        }
    };

    $.fn.popover2 = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.popover2');
        }
    }
})(jQuery);
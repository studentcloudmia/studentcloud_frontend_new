require.config({
    urlArgs: "v=" + (new Date()).getTime(),
    waitSeconds: 45,
    paths: {
        //libraries
        underscore: 'lib/underscore',
        backbone: 'lib/backbone/backbone',
        marionette: 'lib/backbone/backbone.marionette',
        'backbone.wreqr': 'lib/backbone/backbone.wreqr',
        'backbone.eventbinder': 'lib/backbone/backbone.eventbinder',
        'backbone.babysitter': 'lib/backbone/backbone.babysitter',
        'backbone.syphon': 'lib/backbone/backbone.syphon',
        'backbone.touch': 'lib/backbone/backbone.touch',
        jquery: 'lib/jquery/jquery.min',
        //jqmobile: 'lib/jquery/jquery.mobile.custom.min',
        hammer: 'lib/jquery/hammer',
        jQueryHammer: 'lib/jquery/jquery.hammer',
        tpl: 'lib/tpl',
        bootstrap: 'lib/bootstrap/bootstrap',
        bootstrap_switch: 'lib/bootstrap/bootstrap-switch',
        bootstrap_slider: 'lib/bootstrap/bootstrap-slider',
        bootstrap_progress: 'lib/bootstrap/bootstrap-progressbar',
        ui: 'lib/jquery/jquery-ui.min',
        //plugins
        domReady: 'lib/underscore/domReady',
        energize: 'lib/js/energize',
        serializeObject: 'lib/jquery/jquery.serializeObject',
        circleMenu: 'lib/jquery/jquery.circleMenu',
        less: 'lib/js/less',
        less_config: 'lib/js/less_config',
        iscroll: 'lib/js/iscroll',
        fullcalendar: 'lib/jquery/fullcalendar',
        storageApi: 'lib/jquery/jquery.storageapi',
        json2: 'lib/js/JSON2',
        backstretch: 'lib/jquery/jquery.backstretch',
        moment: 'lib/js/moment.min',
        jqToolsOverlay: 'lib/jquery/jquery.tools.overlay',
        jqToolsExpose: 'lib/jquery/jquery.tools.expose',
        formValidator: 'lib/bootstrap/jqBootstrapValidation',
        formMasking: 'lib/jquery/jquery.mask.min',
        touchPunch: 'lib/jquery/jquery.ui.touch-punch.min',
        serializeJSON: 'lib/jquery/jquery.serializeJSON.min',
        popover: 'lib/jquery/jquery.popover',
        pickadate: 'lib/pickadate/loadPickadate',
        cookie: 'lib/jquery/jquery.cookie',
        colorpicker: 'lib/jquery/jquery.colorpicker',
        clouds: 'lib/js/clouds',
        mobiscroll: 'lib/mobiscroll/load'
    },
    shim: {
        jquery: {
            exports: '$'
        },
        hammer: {
            deps: ['jquery'],
            exports: 'Hammer'
        },
        jQueryHammer: {
            deps: ['hammer']
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            exports: 'Backbone',
            deps: ['jquery', 'underscore', 'bootstrap', 'bootstrap_progress']
        },
        'backbone.touch': ['backbone'],
        ui: {
            deps: ['jquery']
        },
        bootstrap: {
            deps: ['ui', 'serializeJSON', 'serializeObject', 'circleMenu', 'popover', 'pickadate', 'mobiscroll', 'formMasking', 'jqToolsOverlay', 'bootstrap_switch', 'bootstrap_slider'] //adding deps on bootstrap to bring it to every page
        },
        bootstrap_switch: {
            deps: ['jquery']
        },
        bootstrap_slider: {
            deps: ['jquery']
        },
        bootstrap_progress: {
            deps: ['bootstrap']
        },
        //plugins
        iscroll: {
            exports: 'IScroll'
        },
        fullcalendar: {
            deps: ['jquery'],
            exports: 'fullCalendar'
        },
        serializeObject: {
            deps: ['jquery']
        },
        circleMenu: {
            deps: ['jquery']
        },
        storageApi: {
            deps: ['jquery']
        },
        backstretch: {
            deps: ['jquery']
        },
        jqToolsOverlay: {
            deps: ['jquery', 'jqToolsExpose']
        },
        jqToolsExpose: {
            deps: ['jquery']
        },
        formValidator: {
            deps: ['bootstrap']
        },
        formMasking: {
            deps: ['jquery']
        },
        serializeJSON: {
            deps: ['jquery']
        },
        touchPunch: {
            deps: ['ui']
        },
        popover: {
            deps: ['jquery']
        },
        colorpicker: {
            deps: ['jquery']
        },
        cookie: {
            deps: ['jquery']
        },
        'lib/mobiscroll/mobiscroll.datetime': ['lib/mobiscroll/mobiscroll.core'],
        'lib/mobiscroll/mobiscroll.select': ['lib/mobiscroll/mobiscroll.core'],
        'lib/mobiscroll/mobiscroll.list': ['lib/mobiscroll/mobiscroll.core'],
        'lib/mobiscroll/mobiscroll.android-ics': ['lib/mobiscroll/mobiscroll.core'],
        'lib/mobiscroll/mobiscroll.ios': ['lib/mobiscroll/mobiscroll.core'],
        'lib/mobiscroll/mobiscroll.minnoware': ['lib/mobiscroll/mobiscroll.core'],
        mobiscroll: {
            deps: ['jquery']
        }
    }
});

require([
    'jquery',
    'app',
    'backbone',
    //run less config
    'less',
    //load all base views
    'views/_base/loadViews',
    //load all modules
    'modules/loadModules',
    //image background plugin
    'backstretch',
    //bring in syphon
    'backbone.syphon',
    //bring form validation
    'formValidator',
    //touchpunch to enable drag and drop in touch devices
    'touchPunch',
            //energize for touch screens
            // 'energize',
            //'backbone.touch'
],
        function(
                $,
                App,
                Backbone) {
            "use strict";
            $(document).ready(function() {
                //start application
                App.start();
            });
        });


//create custom region for overlays
define(['underscore', 'marionette', 'vent'],
        function(_, Marionette, vent) {
            Marionette.Region.Overlay = Marionette.Region.extend({
                initialize: function(options) {
                    _.extend(this, Backbone.Events);
                },
                onShow: function(view) {
                    var options,
                            _this = this;
                    this.setupBindings(view);
                    options = this.getDefaultOptions(_.result(view, "overlay"));
                    this.$el.overlay(options);
                },
                getDefaultOptions: function(options) {
                    var self = this;
                    if (options == null) {
                        options = {};
                    }
                    return _.defaults(options, {
                        closeOnClick: true,
                        closeOnEsc: true,
                        effect: 'default',
                        fixed: true,
                        mask: {
                            color: '#000000',
                            loadSpeed: 200,
                            opacity: 0.4,
                            zIndex: 1030
                        },
                        load: true,
                        speed: 'normal',
                        top: '5%',
                        close: 'nothing',
                        onClose: function(e) {
                            self.closeOverlay();
                        }
                    });
                },
                setupBindings: function(view) {
                    this.listenTo(view, "overlay:close", this.closeOverlay);
                    this.listenTo(view, "overlay:resize", this.resizeOverlay);
                    this.listenTo(view, "overlay:title", this.titleizeOverlay);
                },
                closeOverlay: function() {
                    this.stopListening();
                    this.close();
                },
                resizeDialog: function() {
                    console.warn('resizeDialog not implemented');
                },
                titleizeDialog: function(title) {
                    console.warn('titleizeDialog not implemented');
                },
                onClose: function() {
                    this.$el.removeAttr('style');
                    this.$el.html('');
                    $('#exposeMask').remove();
                    //tell the world overlay has closed
                    vent.trigger('Region:Overlay:Closed');
                }

            });

            return Marionette.Region.Overlay;
        });

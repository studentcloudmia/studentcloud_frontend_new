//overide marionette region open
//This will cause a view to slide down from the top of the region, instead of just appearing in place.
define(['underscore', 'marionette'],
function(_, Marionette) {

    Marionette.Region.prototype.open = function(view){
      this.$el.hide();
      this.$el.html(view.el);
      this.$el.show();
      //this.$el.show().removeClass('animated fadeOut');
      //this.$el.show().addClass('animated fadeIn');
    }

    Marionette.Region.prototype.onClose = function(view){
      //this.$el.addClass('animated fadeOut');
    }

});
//overide backbone syn function to add some events
define(['underscore', 'marionette'],
function(_, Marionette) {
    
	_.extend(Backbone.Marionette.Application.prototype,{
	
		navigate: function(route, options){
			//set default for options if empty
			options = options || {};
			//remove leading slash if any
			if(route.charAt(0) == "/"){
				route = route.substring(1);
			}
			//add hash if not present
			if(route.charAt(0) !== "#"){
				route = "#" + route;
			}
			Backbone.history.navigate(route, options);
		},
	
		getCurrentRoute: function(){
			var frag = Backbone.history.fragment
			if(_.isEmpty(frag)){
				return null;
			}
			return frag;
		},			
		
		startHistory: function(){
			if(Backbone.history)
				Backbone.history.start()
		},			
		
		register: function(instance, id){
			this._registry = this._registry || {};
			this._registry[id] = instance;
		},
		
		unregister: function(instance, id){
			this._registry.splice(id,1);
		},
		
		resetRegistry: function(){
			var oldCount = this.getRegistrySize();
			_.each(this._registry, function(controller){
				controller.region.close();
			});
			var msg = 'There were '+oldCount+' controllers in the registry, there are now '+this.getRegistrySize();
			if(this.getRegistrySize() > 0){
				console.warn(msg, this._registry);
			}else{
				console.log(msg);
			}
		},			
		
		getRegistrySize: function(){
			return _.size(this._registry);
		},
        
        setBackground: function(imageName){
            //remove any .backstretch div out there
            $('.backstretch').remove();
            if(imageName){
                //$("#fullPage").backstretch("../imgs/"+imageName);
            }else{
                
                //$("#fullPage").backstretch("../imgs/backgrounds/blackboard3.jpg");
                //$.backstretch("../imgs/backgrounds/blackboard3.jpg");
                //$.backstretch("../imgs/backgrounds/nursing.jpg");
            }
            //set opacity for image
            //$('.backstretch').css('opacity', '0.4');
        },
        
        setBackgroundColor: function(color){
            //remove any .backstretch div out there
            $('.backstretch').remove();
            $('body').css('background-color', color)
            //set opacity for image
            //$('.backstretch').css('opacity', '0.4');
        },
        
        clearBackgroundOpacity: function(){
            //$('.backstretch').css('opacity', '1');
        },
        
        popupBlockerActivated: function(){
            var myTest = window.open("about:blank", "", "directories=no,height=100,width=100,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
            if (!myTest) {
                return true;
            } else {
                myTest.close();
                return false;
            }

        }
		
	});
});
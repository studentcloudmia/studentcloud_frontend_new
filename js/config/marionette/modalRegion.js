//create custom region for modals
define(['underscore', 'marionette', 'vent'],
        function(_, Marionette, vent) {
            Marionette.Region.Modal = Marionette.Region.extend({
                initialize: function(options) {
                    _.extend(this, Backbone.Events);
                },
                onShow: function(view) {
                    var options,
                    _this = this;
                    this.setupBindings(view);
                    options = this.getDefaultOptions(_.result(view, "modal"));
                    this.$el.modal(options);
                    
                    this.$el.on('hidden', function () {
                        _this.closeModal();
                    });
                    
                    //remove modal-backdrop
                    $('.modal-backdrop').css('z-index','3');
            
                    //listen to route event and close
                    this.listenTo(vent, 'BaseRouter:route', function(){
        				this.closeModal();
        			}, this);
                },
                getDefaultOptions: function(options) {
                    var self = this;
                    if (options == null) {
                        options = {};
                    }
                    return _.defaults(options, {
                        backdrop: true,
                        keyboard: true,
                        show: true,
                        hidden: function(e) {
                            self.closeModal();
                        }
                    });
                },
                setupBindings: function(view) {
                    this.listenTo(view, "modal:close", this.closeModal);
                },
                closeModal: function() {
                    this.$el.modal('hide');
                    this.stopListening();
                    this.close();
                },
                onClose: function() {
                    //tell the world overlay has closed
                    vent.trigger('Region:Modal:Closed');
                }

            });

            return Marionette.Region.Modal;
        });

//overide backbone syn function to add some events
define(['jquery','underscore', 'backbone', 'globalConfig', 'vent'],
function($, _, Backbone, GC, vent) {
    var _sync = Backbone.sync;

    Backbone.sync = function(method, entity, options) {
        var self = this;
        //console.log('Sync: ', method, entity, options);
        
        //change method from PUT to POST if var forcePost is true
        if(method == "update" && entity.forcePost == true){
            method = 'create';
        }
		//set default for options if empty
		options = options || {};
        var sentOptions = options;
        
		//add methods to options var if not outer api
        if (!options.outerAPI) {
            _.defaults(options, {
                beforeSend: function(xhr, settings) {
                    entity.trigger("sync:start", this);
                    //add headers
                    xhr.setRequestHeader('Accept', 'application/json');
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.setRequestHeader('x-api-key', 'sdhjfgdjshfkasuifgkhjdflgksafgjfg');
                    //add CSRF header
                    prepareCSRF(xhr);
                },
                complete: function(jqXHR, status) {
                    //trigger application wide event to show returned status code
                    vent.trigger('Sync:Code:' + jqXHR.status, {jqXHR: jqXHR, options: sentOptions});
                    //save CSRF token
                    var csrftoken = jqXHR.getResponseHeader('X-CSRFToken');
                    if (csrftoken) {
                        //save token in session
                        GC.sessionStorage.set('csrftoken', csrftoken);
                    }
                    entity.trigger("sync:stop", this);
                }
            });
        }


        var sync = _sync(method, entity, options);

        if (!entity._fetch && method == "read")
        	entity._fetch = sync;
            
            //console.log('SYNC: ', method);
    };

    // methods = {
    //     beforeSend: function(xhr, settings) {
    //         this.trigger("sync:start", this);
    //         //add headers
    //         xhr.setRequestHeader('Accept', 'application/json');
    //         xhr.setRequestHeader('Content-Type', 'application/json');
    //         xhr.setRequestHeader('x-api-key', 'sdhjfgdjshfkasuifgkhjdflgksafgjfg');
    //         //add CSRF header
    //         prepareCSRF(xhr);
    //     },
    //     complete: function(jqXHR, status) {
    //         //trigger application wide event to show returned status code
    //         vent.trigger('Sync:Code:'+jqXHR.status, jqXHR);
    //         //save CSRF token
    //         var csrftoken = jqXHR.getResponseHeader('X-CSRFToken');
    //         if(csrftoken){
    //             //save token in session
    //             GC.sessionStorage.set('csrftoken', csrftoken);
    //         }            
    //         this.trigger("sync:stop", this);
    //     }
    // };
    
    /*functions to prepare CSRF header*/
    prepareCSRF = function(xhr, settings) {
        var csrftoken = GC.sessionStorage.get('csrftoken');
        if (csrftoken) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    };

});
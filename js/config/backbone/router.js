//overide backbone syn function to add some events
define(['jquery', 'underscore', 'backbone', 'vent'],
        function($, _, Backbone, vent) {
            _.extend(Backbone.Router.prototype, {
                // Initialize is an empty function by default. Override it with your own
                // initialization logic.
                initialize: function() {

                    this.listenTo(this, 'all', function(e) {
                        vent.trigger('BaseRouter:' + e);
                    });

                }
            });

        });
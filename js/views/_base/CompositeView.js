// base CompositeView
define([
    'marionette',
    'app',
    'views/_base/View',
    'views/_base/EmptyItemView'
], function(Marionette, App, Views, EmptyView) {

    Views.CompositeView = Marionette.CompositeView.extend({
        //base CompositeView code
        itemViewEventPrefix: "childview",
        emptyView: EmptyView,
        oldRender: Backbone.Marionette.CompositeView.prototype.render,
        render: function() {
            //call old render
            this.oldRender.call(this);
            this.listenToOnce(this, 'show', this.afterShow);
        }
    });

});
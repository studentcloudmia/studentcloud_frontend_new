// base View
define([
    'underscore',
    'marionette',
    'app',
    'hammer',
    'jQueryHammer'
],
        function(_, Marionette, App, Hammer) {

            var Views = App.module('Views');

            Views.addInitializer(function() {
                _.extend(Marionette.View.prototype, {
                    //add overlay object to all views
                    overlay: {},
                    modal: {},
                    scrollers: {},
                    afterShow: function() {
                        this.$el.hammer();
                        // Hammer.plugins.showTouches();
                        // Hammer.plugins.fakeMultitouch();

                        var scrollers = this.$el.find('.iScrollWrapper');
                        //create scroller if undefined
                        if (!this.scrollers[this.cid] && scrollers.length > 0) {
                            this.scrollers[this.cid] = [];
                        }
                        //initiate any iScrolls or bootstrap switches
                        var self = this;
                        _.each(scrollers, function(scroll) {
                            if (!$(scroll).hasClass('inScrolling')) {
                                var options = {
                                    // click: true,
                                    scrollX: (typeof $(scroll).data('scroll-scrollx') == 'undefined') ? false : true, //default false
                                    scrollY: (typeof $(scroll).data('scroll-scrollx') == 'undefined') ? true : false, //default true
                                    momentum: (typeof $(scroll).data('scroll-momentum') == 'undefined'), //default true
                                    snap: (typeof $(scroll).data('scroll-snap') != 'undefined'), //default false
                                    snapSpeed: (typeof $(scroll).data('scroll-snap') != 'undefined') ? 400 : '',
                                    keyBindings: (typeof $(scroll).data('scroll-snap') != 'undefined') ? true : false,
                                    bounceEasing: (typeof $(scroll).data('scroll-bounceeasing') != 'undefined') ? $(scroll).data('scroll-bounceeasing') : 'elastic',
                                    bounceTime: (typeof $(scroll).data('scroll-bounceeasing') != 'undefined') ? 1200 : 1200,
                                    //scrollbars
                                    scrollbars: false,
                                    mouseWheel: (typeof $(scroll).data('scroll-mousewheel') == 'undefined') ? true : $(scroll).data('scroll-mousewheel'), //default true,
                                    interactiveScrollbars: false,
                                    onBeforeScrollStart: function(e) {
                                        e.stopPropagation();
                                    }
                                };
                                if ('ontouchstart' in document && !('callPhantom' in window)) {
                                    options.click = true;
                                }
                                //add indicators if set
                                if (typeof $(scroll).data('scroll-indicators') != 'undefined') {
                                    options.indicators = {
                                        el: document.getElementById($(scroll).data('scroll-indicators')),
                                        resize: false
                                    }
                                }

                                var myScroll = new IScroll(scroll, options);

                                self.scrollers[self.cid].push(myScroll);

                                //$(scroll).addClass('inScrolling');

                                self.fixIscroll();
                            }
                        });

                        //initiate switches
                        _.each(this.$el.find('.switch'), function(sw) {
                            //ensure we do not activate more than once
                            if (!$(sw).hasClass('has-switch')) {
                                $(sw).bootstrapSwitch();
                            }
                        });

                        self.focused = [];
                        self.inputs = ['input', 'textarea'];

                        _.each(self.inputs, function(val) {
                            //fix scrolling by ipad keyboard
                            _.each(self.$el.find(val), function(input) {
                                input.onfocus = function() {
                                    self.focused.push(input);
                                }
                            });

                            _.each(self.$el.find(val), function(input) {
                                input.onblur = function() {
                                    self.focused.pop();
                                    //wait milliseconds before checking arr
                                    setTimeout(function() {
                                        if (self.focused.length == 0) {
                                            $('html, body').animate({scrollTop: 0}, 'fast');
                                        }
                                    }, 200)

                                }
                            });
                        });


                    },
                    fixIscroll: function() {
                        var self = this;
                        //pass through class
                        _.each($('.iscrollPassThrough'), function(selectField) {
                            self.stopPorp(selectField);
                        });
                        //fix inputs
                        _.each($('input'), function(selectField) {
                            self.stopPorp(selectField);
                        });
                        //fix selects
                        _.each($('select'), function(selectField) {
                            self.stopPorp(selectField);
                        });
                        //fix buttons
                        _.each($('button'), function(selectField) {
                            self.stopPorp(selectField);
                        });
                    },
                    //iScroll refresh function
                    refreshIScroll: function() {
                        _.each(this.scrollers, function(viewScrollers) {
                            _.each(viewScrollers, function(scroll) {
                                scroll.refresh();
                            });
                        });
                        this.fixIscroll();
                    },
                    //get scroller with classname
                    getScroller: function(className) {
                        var foundScroller = false;
                        _.each(this.scrollers, function(viewScrollers, viewCid) {
                            _.each(viewScrollers, function(scroller) {
                                if ($(scroller.wrapper).hasClass(className))
                                    foundScroller = scroller;
                            });
                        });
                        return foundScroller;
                    },
                    //make close method call a onBeforeDomRemove
                    oldClose: Backbone.Marionette.View.prototype.close,
                    close: function(callback) {
                        if (this.onBeforeDomRemove) {
                            this.onBeforeDomRemove();
                        }
                        this.oldClose.call(this);

                        //remove view from scollers
                        this.scrollers[this.cid] = [];
                    },
                    stopPorp: function(field) {
                        //TODO: try to do both of these events in one function
                        field.addEventListener('touchstart' /*'mousedown'*/, function(e) {
                            e.stopPropagation();
                        }, false);
                        field.addEventListener('mousedown' /*'mousedown'*/, function(e) {
                            e.stopPropagation();
                        }, false);
                    }
                });
            });

            return Views;

        });
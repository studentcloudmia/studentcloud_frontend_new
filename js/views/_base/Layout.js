// base Layout
define([
	'marionette', 
	'app',
	'views/_base/View'
],function(Marionette, App, Views){
	
	 Views.Layout = Marionette.Layout.extend({
		//base layout code
        oldRender: Backbone.Marionette.Layout.prototype.render,
        render: function(){
            //call old render 
            this.oldRender.call(this);
            this.listenToOnce(this, 'show', this.afterShow);
        }
	});
	
});
// base ItemView
define([
	'marionette', 
	'app',
	'views/_base/View'
],function(Marionette, App, Views){
	
	 Views.ItemView = Marionette.ItemView.extend({
		//base item view code
        oldRender: Backbone.Marionette.ItemView.prototype.render,
        render: function(){
            //call old render 
            this.oldRender.call(this);
            this.listenToOnce(this, 'show', this.afterShow);
        }
	});
	
});
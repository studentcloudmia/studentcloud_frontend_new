// SideView
define([
    'marionette',
    'app',
    'tpl!views/templates/_empty.tmpl'
], function(Marionette, App, template) {

    var EmptyItemView = App.Views.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'searchRow'
    });

    return EmptyItemView;

});
// base CollectionView
define([
	'marionette', 
	'app',
	'views/_base/View'
],function(Marionette, App, Views){
	
	 Views.CollectionView = Marionette.CollectionView.extend({
		//base collection view code
		itemViewEventPrefix: "childview",
        oldRender: Backbone.Marionette.CollectionView.prototype.render,
        render: function(){
            //call old render 
            this.oldRender.call(this);
            this.listenToOnce(this, 'show', this.afterShow);
        }
	});
	
});
/*global $*/
define(
        ['marionette', 'globalConfig', 'config/runAll'],
        function(Marionette, GC) {
            "use strict";

            var App = new Marionette.Application();

            //create app regions
            App.addRegions({
                loading: '#loading',
                modalAlerts: '#modalAlerts',
                modals: '#modals',
                modalsAlt1: Marionette.Region.Modal.extend({el: "#modalsAlt1"}),
                modalsAlt2: Marionette.Region.Modal.extend({el: "#modalsAlt2"}),
                modalsAlt3: Marionette.Region.Modal.extend({el: "#modalsAlt3"}),
                overlays: Marionette.Region.Overlay.extend({el: "#overlays"}),
                main: '#main',
                sidebar: '#sidebar',
                dashMap: '#dashMap',
                communication: '#communications',
                emptyFullPage: '#emptyFullPage'
            });

            //app config
            App.addInitializer(function() {
                //do not allow scroll
                document.addEventListener('touchmove', function(e) {
                    e.preventDefault();
                }, false);

                //setup CORS
                (function() {

                    var proxiedSync = Backbone.sync;

                    Backbone.sync = function(method, model, options) {
                        options || (options = {});

                        if (!options.crossDomain) {
                            options.crossDomain = true;
                        }

                        if (!options.xhrFields) {
                            options.xhrFields = {
                                withCredentials: true
                            };
                        }

                        return proxiedSync(method, model, options);
                    };
                })();

                //setup ajax calls
                //ensure all calls are json and include valid API key
                $.ajaxSetup({
                    timeout: 50000
                });

                //start backbone history after init
                App.on('initialize:after', function() {
                    //run globalConfig init
                    GC.initialize();
                    this.setBackground();
                    this.startHistory();
                    //window.app = this;
                });

            });

            return App;

        });

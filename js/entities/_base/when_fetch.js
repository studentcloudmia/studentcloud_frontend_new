define(['jquery','underscore','app'],function($, _, App){
	App.commands.setHandler("when:fetched", function(entity, callback){
		console.log('when:fetched ',entity.fetch);
		$.when(entity._fetch).done(function(){
			callback();
		});
	});
});


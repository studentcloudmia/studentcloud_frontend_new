define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var ScheduleModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Enrollment/InstructorClass/";
                }

            });

            return ScheduleModel;

        });

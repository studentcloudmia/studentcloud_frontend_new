define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var NoteModel = BaseModel.extend({
                
                urlRoot: function() {
                    return GC.api.url + "SC/UserNotes/";
                },

                formatDates: {
                    formatDate: true,
                    dateFormatIn: 'YYYY-MM-DD',
                    dateFormat: 'MM/DD/YYYY',
                    toFormat: ['udate']
                },

                formatTimes: {
                    formatTime: true,
                    timeFormatIn: 'HH:mm:ss',
                    timeFormat: 'hh:mm:ss A',
                    toFormat: ['utime']
                },

                defaults: {
                    "title": "",
                    "location": "",
                    "udate": moment().format('MM/DD/YYYY'),
                    "utime": moment().format('hh:mm:00 A'),
                    "data": ""
                }

            });

            return NoteModel;

        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var dictionaryModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Dictionary/";
                }

            });

            return dictionaryModel;

        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var DeliveryModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Delivery/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": ""
                }

            });

            return DeliveryModel;

        });

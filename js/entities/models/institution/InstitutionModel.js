define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var institutionModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Institution/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": "",
                    "Phone_Number": "",
                    "Fax_Number": "",
                    "Address": {
                        "Address_Part1": "",
                        "Address_Part2": "",
                        "Address_Part3": "",
                        "Address_Part4": "",
                        "ZipCode": "",
                        "Country_LDesc": "",
                        "City": "",
                        "State_SDesc": ""
                    }
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                customParse: function(resp) {
                    resp.name = resp.model_id;
                    return resp;
                }
            });
            return institutionModel;

        });

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var StudentModel = BaseModel.extend({
        
        idAttribute: 'StudentID',
        
        urlRoot: function() {
            return GC.api.url + "student/";
        },
         fetch: function(){
           
           this.set({Name: 'John Smith', StudentID: '4',
                    eff_date: '5/5/2013'});
           
        }         
	
    });

    return StudentModel;

});

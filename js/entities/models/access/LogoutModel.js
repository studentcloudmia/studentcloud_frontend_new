define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'globalConfig'
], function(_, Backbone, BaseModel, GC) {
  
	var LogoutModel = BaseModel.extend({
		
		url : function() {
	        return GC.api.url + "SC/logout/";
	    },
	
		logout: function(){
            //attempt facebook logout if connected
            //this.logoutFB();
			//make logout call none async
            var resp = true;
			this.fetch({async: true, error: function(){
			    //caught error so send user to login page
                resp = false;
			}},{},{},false);
            return resp;
		},
        
        setupFBLogin: function() {
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '517804084949789',
                    channelUrl: '//http://ec2-54-225-119-193.compute-1.amazonaws.com:8080/fb_channel.html',
                    status: true,
                    cookie: true,
                    xfbml: true
                });
            };

            // Load the SDK Asynchronously
            (function(d) {
                var js,
                id = 'facebook-jssdk',
                ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            } (document));
        },
        
        logoutFB: function(){
            this.setupFBLogin();
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    // connected
                    FB.logout(function(response) {
                      // user is now logged out
                    });
                    //make API call to log them in
                } else {
                    // not_logged_in
                }
            }, true);
        }
		
	});

	return LogoutModel;

});

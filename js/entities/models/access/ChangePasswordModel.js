define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var ChangePasswordModel = BaseModel.extend({
        //ensure model always POSTS
        forcePost: true,

        urlRoot: function() {
            return GC.api.url + "access/password/reset/";
        },

        ChangePassword: function(params) {
            //make post call to change password
            this.set(params, {silent:true});
            $.ajaxSetup({
                async: false
            });
            this.save();
            $.ajaxSetup({
                async: true
            });
        }

    });

    return ChangePasswordModel;

});

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var LoginModel = BaseModel.extend({

        initialize: function() {

            },

        urlRoot: function() {
            return GC.api.url + "public/access/checklogin/";
        },

        isLoggedIn: function() {
            var self = this;
            self.fetch({
				async: false
            },
            {},
            {},
            false);
			//update GC.loggedIn
			GC.loggedIn = self.get('CheckLogin');
			//return
			return self.get('CheckLogin');
		}
	
    });

    return LoginModel;

});

define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'app'
], function(_, Backbone, BaseModel, App) {
  
	var LoginLinkModel = BaseModel.extend({
		
		url : function() {
	        return App.api.url + "public/access/login/link/";
	    }
		
	});

	return LoginLinkModel;

});

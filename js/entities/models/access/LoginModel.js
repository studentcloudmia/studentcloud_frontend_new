define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
], function(_, Backbone, BaseModel, GC) {

    var LoginModel = BaseModel.extend({
        //ensure model always POSTS
        forcePost: true,
        urlRoot: function() {
            return GC.api.url + "SC/sc_login/";
        }

    });

    return LoginModel;

});

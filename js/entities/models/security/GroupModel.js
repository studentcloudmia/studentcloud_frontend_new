define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'globalConfig'
], function(_, Backbone, BaseModel, GC) {
  
	var GroupModel = BaseModel.extend({
		
		url : function() {
	        return GC.api.url + "group/";
	    }
		
	});

	return GroupModel;

});

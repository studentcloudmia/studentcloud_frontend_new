define(['underscore', 'backbone', 'globalConfig', 'vent', 'entities/_base/when_fetch'],
        function(_, Backbone, GC, vent) {

            var BaseModel = Backbone.Model.extend({
                _xhr: [],
                _idAttribute: null,
                forcePost: false,
                initialize: function() {
                    this._xhr = [];
                },
                //custom parse function
                customParse: function(resp) {
                    return resp;
                },
                //localStorage
                localStorage: {
                    isStoredInLocalStorage: false,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                //eff_date constants
                eff_date: {
                    isEffDate: false,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                formatDates: {
                    formatDate: false,
                    dateFormatIn: 'YYYY-MM-DD',
                    dateFormat: 'MM/DD/YYYY',
                    toFormat: [
                        'end_date',
                        'start_date',
                        'exam_date'
                    ]
                },
                formatTimes: {
                    formatTime: false,
                    timeFormatIn: 'HH:mm:ss',
                    timeFormat: 'hh:mm:ss A',
                    toFormat: ['utime']
                },
                getId: function() {
                    var ret = "";
                    if (this._idAttribute) {
                        ret = this.get(this._idAttribute);
                    } else {
                        ret = this.get(this.idAttribute);
                    }
                    //return ret if not undefined
                    return (ret != 'undefined') ? ret : '';
                },
                //return key to be used for local storage
                getKey: function() {
                    return _.result(this, 'url');
                },
                transformWithMoment: function(coll, keyName, transformOptions) {
                    var self = this;
                    _.each(coll, function(val, key, collection) {
                        var isObj = _.isObject(collection[key]);
                        if (isObj) {
                            self.transformWithMoment(collection[key]);
                        } else if (key == keyName && (!_.isNull(collection[key]) && collection[key] != '')) {
                            collection[key] = moment(collection[key], transformOptions.from).format(transformOptions.to);
                        }
                    });
                    return coll;
                },
                fetch: function(options, params, queries, abort) {
                    //add id to model url is not new
                    if (!this.isNew()) {
                        this.url = _.result(this, 'urlRoot') + this.id + '/';
                    }

                    // build new params
                    var newUrl = '';
                    if (params) {
                        $.each(params,
                                function(key, val) {
                                    newUrl += val + '/';
                                });
                    }

                    newUrl = _.result(this, 'url') + newUrl;
                    // build new queries
                    var urlQueries = '';
                    if (queries) {
                        var i = 0;
                        $.each(queries,
                                function(key, val) {
                                    if (i == 0) {
                                        urlQueries += '?' + key + '=' + val;
                                    } else {
                                        urlQueries += '&' + key + '=' + val;
                                    }

                                    i++;
                                });
                        newUrl += urlQueries;
                    }

                    // update collection/model url
                    if (options) {
                        options.url = newUrl;
                    } else {
                        options = {
                            url: newUrl
                        };
                    }

                    //update timeout option
                    if (!options.timeout)
                        options.timeout = GC.timeout;
                    // abort stored xhr
                    if (abort) {
                        this.abort();
                    }
                    //update getKey for local storage
                    this.getKey = options.url;
                    // store xhr to access if needed
                    var currentFetch = Backbone.Model.prototype.fetch.call(this, options);
                    this._xhr.push(currentFetch);
                    // return xhr
                    return currentFetch;
                },
                abort: function() {
                    if (!this._xhr > 0)
                        return;
                    this._xhr.forEach(function(element, index, array) {
                        array[index].abort();
                    });
                    this._xhr = [];
                },
                //custom save
                save: function(data, options) {
                    var isNew;
                    if (options == null) {
                        options = {};
                    }
                    isNew = this.isNew();
                    _.defaults(options, {
                        wait: true,
                        success: _.bind(this.saveSuccess, this, isNew, options.collection),
                        error: _.bind(this.saveError, this)
                    });
                    this.unset("_errors");
                    //format eff_date if eff dated model
                    if (this.eff_date.isEffDate) {
                        var currEff_Date = this.get('eff_date');
                        var formatedEff_Date = moment(currEff_Date, this.eff_date.dateFormat);
                        //verify formatted date and only continue if true
                        if (formatedEff_Date.isValid()) {
                            //format date for server
                            formatedEff_Date = formatedEff_Date.format(this.eff_date.dateURLFormat);
                            this.set({eff_date: formatedEff_Date}, {silent: true});
                        } else {
                            alert('eff_date is NOT valid. Canceling request');
                            return false;
                        }
                    }
                    //format multiple dates in a model
                    if (this.formatDates && this.formatDates.formatDate) {
                        var modelToJSON = this.toJSON();
                        var self = this;
                        _.each(this.formatDates.toFormat, function(dt) {
                            self.transformWithMoment(modelToJSON, dt, {from: self.formatDates.dateFormat, to: self.formatDates.dateFormatIn});
                        });
                        this.set(modelToJSON, {silent: true});
                    }
                    //format multiple times in a model
                    if (this.formatTimes && this.formatTimes.formatTime) {
                        var modelToJSON = this.toJSON();
                        var self = this;
                        _.each(this.formatTimes.toFormat, function(time) {
                            self.transformWithMoment(modelToJSON, time, {from: self.formatTimes.timeFormat, to: self.formatTimes.timeFormatIn});
                        });
                        this.set(modelToJSON, {silent: true});
                    }

                    if (!isNew) {
                        options.url = _.result(this, 'urlRoot') + this.id + '/';
                    }

                    return BaseModel.__super__.save.call(this, data, options);
                },
                saveSuccess: function(isNew, collection) {
                    if (isNew) {
                        if (collection) {
                            collection.add(this);
                        }
                        if (collection) {
                            collection.trigger("model:created", this);
                        }
                        return this.trigger("created", this);
                    } else {
                        if (collection == null) {
                            collection = this.collection;
                        }
                        if (collection) {
                            collection.trigger("model:updated", this);
                        }
                        return this.trigger("updated", this);
                    }
                },
                saveError: function(model, xhr, options) {
                    //encapsulate in try catch incase data returned is not in JSON format
                    try {
                        if (!(xhr.status === 500 || xhr.status === 406)) {
                            //this will throw exception if responseText if not JSON
                            var _ref = $.parseJSON(xhr.responseText);
                            return this.set({
                                _errors: (_ref != null) ? _ref.error_code : void 0
                            });
                        }
                        if (xhr.status === 406) {
                            //triggering save erro event
                            this.trigger('saveError');
                        }
                    } catch (err) {
                        console.warn('Caught error in saveError functions: ', err);
                    }

                    //ensure loader stops
                    vent.trigger('Components:Loader:Hide');
                },
                //custom destroy
                destroy: function(options) {

                    var self = this;
                    if (options == null) {
                        options = {};
                    }
                    _.defaults(options, {
                        wait: true,
                        url: ''
                    });
                    this.set({
                        _destroy: true
                    });
                    //update model url
                    options.url = _.result(this, 'urlRoot') + this.id + '/';
                    options.success = function() {
                        //trigger destroyed event
                        self.trigger('destroyed', this);
                    };
                    var returnVal = BaseModel.__super__.destroy.call(this, options);
                    return returnVal;
                },
                isDestroyed: function() {
                    return this.get("_destroy");
                },
                syncStart: function() {
                    vent.trigger('Components:Loader:Show');
                },
                syncStop: function() {
                    vent.trigger('Components:Loader:Hide');
                },
                parse: function(resp) {
                    //format eff_date
                    if (this.eff_date.isEffDate) {
                        this.transformWithMoment(resp, 'eff_date', {from: this.eff_date.dateURLFormat, to: this.eff_date.dateFormat});
                    }

                    //format multiple dates in a model
                    if (this.formatDates && this.formatDates.formatDate) {
                        var self = this;
                        _.each(this.formatDates.toFormat, function(dt) {
                            self.transformWithMoment(resp, dt, {from: self.formatDates.dateFormatIn, to: self.formatDates.dateFormat});
                        });
                    }

                    //format multiple times in a model
                    if (this.formatTimes && this.formatTimes.formatTime) {
                        var self = this;
                        _.each(this.formatTimes.toFormat, function(time) {
                            self.transformWithMoment(resp, time, {from: self.formatTimes.timeFormatIn, to: self.formatTimes.timeFormat});
                        });
                    }

                    //call custom parse function
                    resp = this.customParse(resp);
                    return resp;
                },
                /********************************************
                 SYNC override
                 Override sync function to use local storage
                 *********************************************/
                sync: function(method, model, options) {
                    _.defaults(options, {loader: true});
                    //use loader by default
                    if (options.loader) {
                        this.listenToOnce(this, 'sync:start', this.syncStart, this);
                        this.listenToOnce(this, 'sync:stop', this.syncStop, this);
                    }

                    var key, now, timestamp, refresh;
                    if (method === 'read' && this.localStorage.isStoredInLocalStorage) {
                        // only override sync if it is a fetch('read') request
                        key = _.result(this, 'getKey');
                        if (key) {
                            now = new Date()
                                    .getTime();
                            timestamp = GC.localStorage.get(key + ":timestamp");
                            refresh = options.forceRefresh;
                            if (refresh || !timestamp || ((now - timestamp) > this.localStorage.maxRefresh)) {
                                // make a network request and store result in local storage
                                var success = options.success;
                                options.success = function(resp, status, xhr) {
                                    // check if this is an add request in which case append to local storage data instead of replace
                                    if (options.add && resp.values) {
                                        // clone the response
                                        var newData = JSON.parse(JSON.stringify(resp));
                                        // append values
                                        var prevData = GC.localStorage.get(key);
                                        newData.values = prevData.values.concat(resp.values);
                                        // store new data in local storage
                                        GC.localStorage.set(key, newData);
                                    } else {
                                        // store resp in local storage
                                        GC.localStorage.set(key, resp);
                                    }
                                    var now = new Date()
                                            .getTime();
                                    GC.localStorage.set(key + ":timestamp", now);
                                    success(resp, status, xhr);
                                };
                                // call normal backbone sync
                                Backbone.sync(method, model, options);
                            } else {
                                // provide data from local storage instead of a network call
                                var data = GC.localStorage.get(key);
                                // simulate a normal async network call
                                setTimeout(function() {
                                    options.success(data, 'success', null);
                                }, 0);
                            }
                        }
                    } else {
                        Backbone.sync(method, model, options);
                    }
                }

            });
            return BaseModel;
        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var classificationModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Classification/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": "",
                    "Phone_Number": "",
                    "Fax_Number": "",
                    "Address": {
                        "Address_Part1": "",
                        "Address_Part2": "",
                        "Address_Part3": "",
                        "Address_Part4": "",
                        "ZipCode": "",
                        "Country": "",
                        "City": "",
                        "State": ""
                    }
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                customParse: function(resp) {
                    resp.name = resp.model_id;
                    return resp;
                }

            });

            return classificationModel;

        });

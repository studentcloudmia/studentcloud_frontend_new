define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var UserMapModel = BaseModel.extend({

        //localStorage
        localStorage: {
            isStoredInLocalStorage: true,
            maxRefresh: 300000 //5 minutes (5 * 60000)
        },
        
        urlRoot: function() {
            return GC.api.url + "MajorMap/usermajormap/";
        }
	
    });

    return UserMapModel;

});

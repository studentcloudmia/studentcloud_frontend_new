define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var MajorMapModel = BaseModel.extend({
        
        forcePost: true,

        eff_date: {
            isEffDate: true,
            dateFormat: 'MM/DD/YYYY',
            dateURLFormat: 'YYYY-MM-DD'
        },
        
        urlRoot: function() {
            return GC.api.url + "MajorMap/MajorMap/";
        }
        
	
    });

    return MajorMapModel;

});

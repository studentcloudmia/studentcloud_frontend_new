define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var CourseMapModel = BaseModel.extend({

        urlRoot: function() {
            return GC.api.url + "MajorMap/CourseMap/";
        }
	
    });

    return CourseMapModel;

});

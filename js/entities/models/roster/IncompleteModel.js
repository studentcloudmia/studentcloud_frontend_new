define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var IncompleteModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Roster/incomplete/";
                },
                defaults: {
                    "missing_assigment": "",
                    "grade_earned_date": "",
                    "completion_date": "",
                    "i_justification": "",
                    "missing_assig_perc": "",
                    "id": ""
                },
                formatDates: {
                    formatDate: true,
                    dateFormatIn: 'YYYY-MM-DD',
                    dateFormat: 'MM/DD/YYYY',
                    toFormat: [
                        'completion_date'
                    ]
                }
            });

            return IncompleteModel;

        });

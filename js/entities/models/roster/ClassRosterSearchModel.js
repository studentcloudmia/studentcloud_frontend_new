define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var RosterModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Roster/";
                }
            });

            return RosterModel;

        });

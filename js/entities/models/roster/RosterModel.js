define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var RosterModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Roster/classgrade/";
                },
                defaults: {
                    action: '',
                    assigned_grade: '',
                    attendance_option: '',
                    classif: '',
                    credits_enrolled: '',
                    enrollment_id: '',
                    first_name: '',
                    last_name: '',
                    major_map: [],
                    official_grade: '',
                    picture: '',
                    roster_status: '',
                    sclass: '',
                    status: '',
                    uid: ''
                }
            });

            return RosterModel;

        });

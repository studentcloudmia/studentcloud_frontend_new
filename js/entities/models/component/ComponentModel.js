define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var ComponentModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Component/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": ""
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                customParse: function(resp) {
                    resp.name = resp.model_id;
                    return resp;
                }

            });

            return ComponentModel;

        });

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'app'
],
function(_, Backbone, BaseModel, App) {

    var SidebarModel = BaseModel.extend({

        initialize: function() {

		},

        urlRoot: function() {
            return App.api.url + "api/sidebar/";
        },
        
        fetch: function(){
           this.set({first_name: 'Felipe', last_name: 'Diep', id: '2064634'});
        }
	
    });

    return SidebarModel;

});

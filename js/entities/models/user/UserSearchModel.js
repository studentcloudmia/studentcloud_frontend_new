define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var UserSearchModel = BaseModel.extend({
                idAttribute: 'value_search'
            });

            return UserSearchModel;

        });

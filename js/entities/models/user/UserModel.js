define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var userModel = BaseModel.extend({
                _idAttribute: 'uid',
                urlRoot: function() {
                    return GC.api.url + "SC/user/";
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                formatDates: {
                    formatDate: true,
                    dateFormatIn: 'YYYY-MM-DD',
                    dateFormat: 'MM/DD/YYYY',
                    toFormat: [
                        'DOB',
                        'asof'
                    ]
                }
            });

            return userModel;

        });

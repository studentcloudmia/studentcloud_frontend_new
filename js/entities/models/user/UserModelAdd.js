define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var userModel = BaseModel.extend({
                _idAttribute: 'uid',
                urlRoot: function() {
                    return GC.api.url + "SC/user/add_super/";
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM-DD-YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                }


            });

            return userModel;

        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {
            var ChatModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "MessageCenter/Message/";
                },
                defaults: {
                    messageid: [
                        {
                            id: "",
                            created_by: "",
                            enable: "",
                            recipient: "",
                            recipient_type: "",
                            send_on: "",
                            viewed_on: "",
                            to_phone: ""
                        }
                    ],
                    created_by: "",
                    enable: "",
                    description: "",
                    delivery_method: "",
                    message_type: "",
                    content: "",
                    subject: "",
                    from_phone: "",
                    attachment: ""
                },
                readFile: function(file, callback) {
                    var reader = new FileReader();
                    reader.onload = (function(theFile, self) {
                        return function(e) {
                            self.set({attachment: e.target.result});
                            _.isUndefined(callback) ? null : callback();
                        };
                    })(file, this);

                    reader.readAsDataURL(file);
                }
            });
            return ChatModel;
        });

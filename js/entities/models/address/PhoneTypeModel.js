define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var phoneTypeModel = BaseModel.extend({
                initialize: function() {

                },
                urlRoot: function() {
                    return GC.api.url + "dictionary/personal/phone/";
                }

            });

            return phoneTypeModel;

        });

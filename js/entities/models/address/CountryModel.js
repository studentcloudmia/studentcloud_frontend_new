define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var countryModel = BaseModel.extend({
                initialize: function() {

                },
                urlRoot: function() {
                    return GC.api.url + "Generic/Country/";
                }

            });

            return countryModel;

        });

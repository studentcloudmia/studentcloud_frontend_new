define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var addressTypeModel = BaseModel.extend({
                initialize: function() {

                },
                urlRoot: function() {
                    return GC.api.url + "dictionary/personal/address/";
                }

            });

            return addressTypeModel;

        });

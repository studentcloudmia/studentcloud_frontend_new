define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var stateModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Country/";
                }

            });

            return stateModel;

        });

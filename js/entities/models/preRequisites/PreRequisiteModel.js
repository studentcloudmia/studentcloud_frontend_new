define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var PreRequisiteModel = BaseModel.extend({
        
        idAttribute: 'preReqID',
	
        baseUrl: function() {
            return GC.api.url + "sbuilder/prerequisites/";
        },

        defaults: {
            "Description_Short": ""
        }
	
    });

    return PreRequisiteModel;

});

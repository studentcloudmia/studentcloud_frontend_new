define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'app'
], function(_, Backbone, BaseModel, App) {
  
	var SectionModel = BaseModel.extend({
	
		url : function() {
	        return App.api.url + "public/application/sectiondetail/"
	    }
	
	});

	return SectionModel;

});

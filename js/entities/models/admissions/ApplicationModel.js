define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'entities/collections/admissions/SectionsCollection',
	'app'
], function(_, Backbone, BaseModel, SectionsCollection, App) {
  
	var ApplicationModel = BaseModel.extend({
		
		sectionsColl: new SectionsCollection(),
		
		idAttribute: "Application_Number",
		
		url : function() {
	        return App.api.url + "public/application/apps/";
	    },
	
		sectionLoaded: function(selected) {
			var secFound = $.grep(this.get('Sections'), function(section){ return section.id == selected; });

			if(secFound[0].Parts)
				return true;
			
			return false;
		},
	
		numberSectionsLoaded: function() {
			var number = 0;
			
			//count sections loaded			
			var allSections = self.get('Sections');
			_.each(allSections, function(section, key, list){ 
				if(section.Parts) number++;
			});
			return number;			
		},
		
		updateSection: function(newSection){
			var self = this; 
			//retreive all app sections
			var allSections = self.get('Sections');
			//loop through all app sections and update with section sent
			_.each(allSections, function(section, key, list){ 
				if(section.id == newSection.id){
					allSections[key] = newSection;
					//do not trigger a change event
					self.set({Sections: allSections}, {silent: true});
					//exit loop
					return false;
				} 
			});
		}
	
	});

	return ApplicationModel;

});

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var FaveModel = BaseModel.extend({

        forcePost: true,
        isNew: function(){
            return false;
        },
        urlRoot: function() {
            return GC.api.url + "Enrollment/FavoriteClass/";
        }
	
    });

    return FaveModel;

});

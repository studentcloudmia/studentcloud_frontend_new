define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var SaveModel = BaseModel.extend({

        forcePost: true,
        isNew: function(){
            return false;
        },
        urlRoot: function() {
            return GC.api.url + "Enrollment/save/";
        }
	
    });

    return SaveModel;

});

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var EnrollModel = BaseModel.extend({

        forcePost: true,
        
        urlRoot: function() {
            return GC.api.url + "Enrollment/enroll/";
        }
	
    });

    return EnrollModel;

});

define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var ClassModel = BaseModel.extend({

        urlRoot: function() {
            return GC.api.url + "Enrollment/class/";
        },
        
        parse: function(cls) {
            //loop through response and calculate computed_location and computed_sched

                var computed_sched = '';
                var computed_days = '';
                _.each(cls.schedule, function(sched){
                    computed_days = computed_days + moment().day(sched.day).format('dd');
                    computed_sched = moment(sched.start_time, 'HH:mm:ss').format('hh:mm a') + ' ' + moment(sched.end_time, 'HH:mm:ss').format('hh:mm a');
                    cls.computed_location = sched.building_description + ' ' + sched.room_description;
                    
                });
                cls.computed_sched = computed_days + ' ' + computed_sched;

            
            return cls;
        },
	
    });

    return ClassModel;

});

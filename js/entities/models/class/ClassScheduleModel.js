define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var classScheduleModel = BaseModel.extend({
                _idAttribute: 'id',
                urlRoot: function() {
                    return GC.api.url + "ClassBuilder/SClass/";
                },
                defaults: {
                    "course": "",
                    "total_seats_avail": "",
                    "total_seats_waitlist": "",
                    "minimun_required": "",
                    "class_term": "",
                    "exam_code": "",
                    "exam_date": "",
                    "exam_start_time": "",
                    "exam_end_time": "",
                    "exam_location": "",
                    "instructor_edit": "",
                    "consent": "",
                    "class_status": "",
                    "start_date": "",
                    "end_date": "",
                    "topic": "",
                    "note": "",
                    "note_freeform": "",
                    "lms_provider": "",
                    "lms_file": "",
                    "scheduleclass_id": [
                        {
                            "section": "",
                            "room": "",
                            "scheduleequip_id": [
                                {
                                    "equipment": "",
                                    "quantity": ""
                                }
                            ]
                        }
                    ],
                    "class_id": [
                        {
                            "instructor": "",
                            "instructor_type": ""
                        }
                    ],
                    "textbookclass_id": [
                    ]
                },
                formatDates: {
                    formatDate: true,
                    dateFormatIn: 'YYYY-MM-DD',
                    dateFormat: 'MM/DD/YYYY',
                    toFormat: [
                        'end_date',
                        'start_date',
                        'exam_date'
                    ]
                },
                use_id: true
            });

            return classScheduleModel;

        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var ClassNoteModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "ClassBuilder/Note/";
                }

            });

            return ClassNoteModel;

        });

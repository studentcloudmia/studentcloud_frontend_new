define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var NotificationModel = BaseModel.extend({

        urlRoot: function() {
            return GC.api.url + "MessageCenter/Message/get_alert_notifications/";
        }
	
    });

    return NotificationModel;

});

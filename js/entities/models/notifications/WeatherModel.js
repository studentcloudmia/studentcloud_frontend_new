define(['underscore', 'backbone', 'entities/models/BaseModel', 'globalConfig', 'utilities'],

function(_, Backbone, BaseModel, GC, Utilities) {

    var WeathernModel = BaseModel.extend({

        initialize: function(options) {
            options = options || {};
            _.defaults(options,{
                forecast: false
            });
            this.forecast = options.forecast;
            
            this.proc = 'http://';
            if(Utilities.isHttps()){
                this.proc = 'https://';
            }
            
            //api key: 61dcee4ef22ee9f4
            //site: http://www.wunderground.com/weather/api/d/61dcee4ef22ee9f4/edit.html
            //images: //http://icons.wxug.com/i/c/j/partlycloudy.gif
        },

        //localStorage
        localStorage: {
            isStoredInLocalStorage: true,
            maxRefresh: 1800000 //30 minutes (30 * 60000)
        },

        urlRoot: function() {
            //TODO: Get user's location            
            if(this.forecast){
                return this.proc + "api.wunderground.com/api/61dcee4ef22ee9f4/forecast10day/q/CA/Anaheim.json";
            }else{
                return this.proc + "api.wunderground.com/api/61dcee4ef22ee9f4/geolookup/conditions/q/CA/Anaheim.json";
            }
        },
        
        customParse: function(resp){
            if(!this.forecast){
                // resp.current_observation.icon_url = 'http://icons.wxug.com/i/c/i/'+resp.current_observation.icon+'.gif';
                resp.current_observation.icon_url = 'http://icons-ak.wxug.com/i/c/k/'+resp.current_observation.icon+'.gif';
            }else{
                var newReading = {};
                //loop though forecast and mold to our liking
                _.each(resp.forecast.simpleforecast.forecastday, function(reading){
                    reading.dateDisp = moment(reading.date.month + '/' + reading.date.day + '/' + reading.date.year, 'M/D/YYYY').format('MM/DD/YYYY');
                    newReading[moment(reading.dateDisp, 'MM/DD/YYYY').format('YYYYMMDD')] = reading;
                });
                resp = newReading;
            }
            return resp;
        },
        
        getWeather: function(){
            this.fetch({dataType: 'jsonp', outerAPI: true},{},{},false);
        }

    });

    return WeathernModel;

});

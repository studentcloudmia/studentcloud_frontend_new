define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var GradeModel = BaseModel.extend({
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                urlRoot: function() {
                    return GC.api.url + "Generic/Grade/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": ""
                },
                customParse: function(resp) {
                    resp.name = resp.model_id;
                    return resp;
                }
            });

            return GradeModel;

        });

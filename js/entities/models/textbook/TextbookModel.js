define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var TextbookModel = BaseModel.extend({
                _idAttribute: 'ISBN',
                urlRoot: function() {
                    return GC.api.url + "ClassBuilder/Textbook/";
                },
                defaults: {
                    "title": "",
                    "status": "",
                    "edition": "",
                    "publisher": "",
                    "year_publisher": "",
                    "cost": "",
                    "created_by": "",
                    "enable": ""
                }

            });

            return TextbookModel;

        });

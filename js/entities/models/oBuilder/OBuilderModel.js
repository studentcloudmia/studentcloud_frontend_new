define(['underscore', 'backbone', 'entities/models/BaseModel', 'globalConfig'],
        function(_, Backbone, BaseModel, GC) {

            var OBuilderModel = BaseModel.extend({
                idAttribute: 'eff_date_id',
                //ensure model always POSTS
                forcePost: true,
                //eff_date constants
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                },
                urlRoot: function() {
                    return GC.api.url + "OrgBuilder/";
                },
                defaults: {
                    'eff_date': moment().format('MM/DD/YYYY'),
                    'eff_date_display': moment().format('MM/DD/YYYY'),
                    'eff_date_id': moment().format('YYYYMMDD'),
                    'Status': 'CURRENT'
                },
                customParse: function(resp) {
                    if (_.size(resp) > 0) {
                        // resp.eff_date = moment(resp.eff_date, 'YYYY-MM-DD').format('YYYYMMDD');
                        resp.eff_date_display = resp.eff_date;
                        resp.eff_date_id = moment(resp.eff_date, 'MM/DD/YYYY').format('YYYYMMDD');
                        //resp.id = resp.eff_date;
                    }
                    return resp;
                }
            });

            return OBuilderModel;

        });

define(['underscore', 'backbone', 'entities/models/BaseModel', 'globalConfig'],
        function(_, Backbone, BaseModel, GC) {

            var OBuilderModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "OrgBuilder/";
                }
            });

            return OBuilderModel;

        });

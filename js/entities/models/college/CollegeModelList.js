define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var collegeModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/College/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": "",
                    "Phone_Number": "",
                    "Fax_Number": "",
                    "Address": {
                        "Address_Part1": "",
                        "Address_Part2": "",
                        "Address_Part3": "",
                        "Address_Part4": "",
                        "ZipCode": "",
                        "Country": "",
                        "City": "",
                        "State": ""
                    }
                }

            });

            return collegeModel;

        });

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var buildingModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "ClassBuilder/Building/";
                }
            });

            return buildingModel;

        });

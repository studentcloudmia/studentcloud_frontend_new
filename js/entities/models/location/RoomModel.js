define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var roomModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "ClassBuilder/Building/room/";
                }
            });

            return roomModel;

        });

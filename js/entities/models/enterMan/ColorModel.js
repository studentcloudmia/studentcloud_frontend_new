define([
'underscore',
'backbone',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseModel, GC) {

    var ColorModel = BaseModel.extend({
        //ensure model always POSTS
        forcePost: true,
        
        urlRoot: function() {
            return GC.api.url + "enterprisemanager/config/colors/";
        }
	
    });

    return ColorModel;

});

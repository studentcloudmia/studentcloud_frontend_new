define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig',
    'moment'
],
        function(_, Backbone, BaseModel, GC) {

            var dummyModel = BaseModel.extend({
                _idAttribute: 'id',
                setAddressUrl: function(modelId) {
                    this.id = modelId;
                    this.urlRoot = GC.api.url + 'SC/user/list/personal/address/';
                },
                setPhoneUrl: function(modelId) {
                    this.id = modelId;
                    this.urlRoot = GC.api.url + 'SC/user/list/personal/phone/';
                },
                isNew: function(boolena) {
                    return boolena;
                }


            });

            return dummyModel;

        });

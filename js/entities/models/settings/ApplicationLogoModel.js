define([
'underscore',
'backbone',
'entities/models/BaseModel',
'app'
],
function(_, Backbone, BaseModel, App) {

    var applicationLogoModel = BaseModel.extend({

        initialize: function() {

		},

        urlRoot: function() {
            return App.api.url + "api/finaid/applicationlogo/";
        },
        
        fetch: function(){
           this.set({name: 'img1.jpg'});   
        }
	
    });

    return applicationLogoModel;

});

define([
	'underscore',
	'backbone',
	'entities/models/BaseModel',
	'app'
], function(_, Backbone, BaseModel, App) {
  
	var WidgetModel = BaseModel.extend({
		
		initialize: function() {
			
		},
		
		url : function() {
	        return App.apiUrl + "widget/"
	    }
		
	});

	return WidgetModel;

});

define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var courseModel = BaseModel.extend({
                _idAttribute: 'course_catalog_id',
                urlRoot: function() {
                    return GC.api.url + "CourseBuilder/CourseCatalog/";
                },
                defaults: {
                    "eff_date": "",
                    "title": "",
                    "CollID": "",
                    "DepID": "",
                    "description": "",
                    "min_units": "",
                    "max_units": "",
                    "count_units": "",
                    "subject": "",
                    "classif": "",
                    "grade": "",
                    "repeat": "",
                    "graded_comp": "",
                    "allow_mutiple": "",
                    "total_units_allowed": "",
                    "total_completions_allowed": "",
                    "cip_code": "",
                    "hegis_code": "",
                    "delivery": "",
                    "terms": [],
                    "prerequisites": [],
                    "corequisites": [],
                    "equivalences": []
                }

            });
            return courseModel;
        });

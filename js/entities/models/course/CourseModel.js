define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var courseModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "CourseBuilder/CourseCatalog/";
                },
                defaults: {
                    "eff_date": "",
                    "title": "",
                    "CollID": "",
                    "DepID": "",
                    "description": "",
                    "min_units": "",
                    "max_units": "",
                    "count_units": "",
                    "subject": "",
                    "classif": "",
                    "grade": "",
                    "repeat": "",
                    "graded_comp": "",
                    "component": 1,
                    "allow_mutiple": "",
                    "total_units_allowed": "",
                    "total_completions_allowed": "",
                    "cip_code": "",
                    "hegis_code": "",
                    "delivery": "",
                    "terms": [],
                    "prerequisites": [],
                    "corequisites": [],
                    "equivalences": []
                },
                customParse: function(resp) {
                    resp.name = resp.course_catalog_id;
                    return resp;
                },
                eff_date: {
                    isEffDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateURLFormat: 'YYYY-MM-DD'
                }
            });
            return courseModel;
        });

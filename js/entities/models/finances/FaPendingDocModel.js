define([
'underscore',
'backbone',
'entities/models/BaseModel',
'app'
],
function(_, Backbone, BaseModel, App) {

    var FaPendingDocModel = BaseModel.extend({

        initialize: function() {

		},

        urlRoot: function() {
            return App.api.url + "api/finaid/pendingdoc/";
        },
        
        fetch: function(){
           
           this.set({Doc: 'EFC Form'});
           
        }
	
    });

    return FaPendingDocModel;

});

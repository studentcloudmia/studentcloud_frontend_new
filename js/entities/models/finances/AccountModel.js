define([
'underscore',
'backbone',
'entities/models/BaseModel',
'app'
],
function(_, Backbone, BaseModel, App) {

    var AccountModel = BaseModel.extend({

        initialize: function() {

		},

        urlRoot: function() {
            return App.api.url + "api/account/";
        },
        
        fetch: function(){
           
           this.set({balance: '$2,654.00'});
           
        }
	
    });

    return AccountModel;

});

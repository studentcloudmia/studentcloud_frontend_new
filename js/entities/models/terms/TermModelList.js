define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseModel, GC) {

            var TermModel = BaseModel.extend({
                urlRoot: function() {
                    return GC.api.url + "Generic/Term/";
                },
                defaults: {
                    "model_id": "",
                    "eff_date": "",
                    "Description_Short": "",
                    "Description_Long": ""
                },
                formatDates: {
                    formatDate: true,
                    dateFormat: 'MM/DD/YYYY',
                    dateFormatIn: 'YYYY-MM-DD',
                    toFormat: [
                        'start_date',
                        'end_date'
                    ]
                }

            });

            return TermModel;

        });

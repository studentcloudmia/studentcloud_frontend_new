define([
    'underscore',
    'backbone',
    'entities/models/BaseModel',
    'app'
],
        function(_, Backbone, BaseModel, App) {

            var FriendModel = BaseModel.extend({
                initialize: function() {

                },
                urlRoot: function() {
                    return App.api.url + "Friends/";
                }

            });

            return FriendModel;

        });

define([
    'entities/collections/BaseCollection',
    'entities/models/college/CollegeModelList',
    'globalConfig'
],
        function(BaseColl, CollegeModel, GC) {

            var CollegeCollection = BaseColl.extend({
                model: CollegeModel,
                url: function() {
                    return GC.api.url + "Generic/College/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }

            });

            return CollegeCollection;

        });

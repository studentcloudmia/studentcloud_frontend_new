define([
    'entities/collections/BaseCollection',
    'entities/models/college/CollegeModel',
    'globalConfig'
],
        function(BaseColl, CollegeModel, GC) {

            var CollegeCollection = BaseColl.extend({
                model: CollegeModel,
                url: function() {
                    return GC.api.url + "Generic/College/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return CollegeCollection;

        });

define([
'underscore',
'backbone',
'entities/collections/BaseCollection',
'entities/models/enterMan/ColorModel',
'globalConfig'
],
function(_, Backbone, BaseColl, ColorModel, GC) {

    var ColorCollection = BaseColl.extend({

        model:ColorModel,

        url: function() {
            return GC.api.url + "enterprisemanager/config/colors/";
        }
	
    });

    return ColorCollection;

});

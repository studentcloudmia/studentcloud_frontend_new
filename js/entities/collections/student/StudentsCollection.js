define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/student/StudentModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, StudentModel, GC) {

            var StudentCollection = BaseColl.extend({
                model: StudentModel,
                url: function() {

                    return GC.api.url + "student/";
                },
                fetch: function() {

                    this.add([
                        {
                            StudentID: '1',
                            Name: 'John Smith',
                            eff_date: '5/5/2013',
                            Status: 'Active'
                        },
                        {
                            StudentID: '2',
                            Name: 'Alejandro Smith',
                            eff_date: '5/5/2013',
                            Status: 'Active'
                        },
                        {
                            StudentID: '3',
                            Name: 'Pedro Smith',
                            eff_date: '5/5/2013',
                            Status: 'Active'
                        }
                    ], {
                        silent: true
                    });

                    this.trigger('reset');
                }

            });

            return StudentCollection;

        });

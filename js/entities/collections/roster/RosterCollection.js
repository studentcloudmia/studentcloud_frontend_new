define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/roster/RosterModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, RosterModel, GC) {

            var RosterCollection = BaseColl.extend({
                model: RosterModel,
                url: function() {
                    return GC.api.url + "Roster/classgrade/";
                }
            });

            return RosterCollection;

        });

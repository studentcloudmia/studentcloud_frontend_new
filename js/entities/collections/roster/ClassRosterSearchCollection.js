define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/roster/ClassRosterSearchModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, RosterModel, GC) {

            var ClassRosterSearchCollection = BaseColl.extend({
                model: RosterModel,
                url: function() {
                    return GC.api.url + "Roster/";
                }

            });

            return ClassRosterSearchCollection;

        });

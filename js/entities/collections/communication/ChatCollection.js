define([
    'entities/collections/BaseCollection',
    'entities/models/communication/ChatModel',
    'globalConfig'
],
        function(BaseColl, ChatModel, GC) {

            var ChatCollection = BaseColl.extend({
                model: ChatModel,
                url: function() {
                    return GC.api.url + "MessageCenter/Message/";
                }

            });

            return ChatCollection;

        });

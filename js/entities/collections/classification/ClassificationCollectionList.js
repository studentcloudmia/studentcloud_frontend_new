define([
    'entities/collections/BaseCollection',
    'entities/models/classification/ClassificationModelList',
    'globalConfig'
],
        function(BaseColl, ClassificationModel, GC) {

            var ClassificationCollection = BaseColl.extend({
                model: ClassificationModel,
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "Generic/Classification/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return model.get('Description_Long');
                }

            });

            return ClassificationCollection;

        });

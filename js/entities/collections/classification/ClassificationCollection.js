define([
    'entities/collections/BaseCollection',
    'entities/models/classification/ClassificationModel',
    'globalConfig'
],
        function(BaseColl, ClassificationModel, GC) {

            var ClassificationCollection = BaseColl.extend({
                model: ClassificationModel,
                url: function() {
                    return GC.api.url + "Generic/Classification/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return ClassificationCollection;

        });

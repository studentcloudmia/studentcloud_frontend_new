define([
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(BaseColl, BaseModel, GC) {

            var ExamCodeCollection = BaseColl.extend({
                model: BaseModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/Exam/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('description');
                }
            });

            return ExamCodeCollection;

        });

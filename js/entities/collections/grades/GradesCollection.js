define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/grades/GradeModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, GradeModel, GC) {

            var GradesCollection = BaseColl.extend({
                model: GradeModel,
                url: function() {
                    return GC.api.url + "Generic/Grade/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return GradesCollection;

        });

define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/grades/GradeModelList',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, GradeModel, GC) {

            var GradesCollection = BaseColl.extend({
                model: GradeModel,
                url: function() {
                    return GC.api.url + "Generic/Grade/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }

            });

            return GradesCollection;

        });

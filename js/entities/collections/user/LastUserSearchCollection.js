define([
    'entities/collections/BaseCollection',
    'entities/models/user/UserSearchModel',
    'globalConfig'
],
        function(BaseColl, UserSearchModel, GC) {

            var LastUserSearchCollection = BaseColl.extend({
                model: UserSearchModel,
                url: function() {
                    return GC.api.url + "SC/UserSearch/";
                }

            });

            return LastUserSearchCollection;

        });

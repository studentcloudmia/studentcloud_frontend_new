define([
    'entities/collections/BaseCollection',
    'entities/models/user/UserModel',
    'globalConfig'
],
        function(BaseColl, UserModel, GC) {

            var userCollection = BaseColl.extend({
                model: UserModel,
                url: function() {
                    return GC.api.url + "SC/user/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'MM/DD/YYYY');
                }


            });

            return userCollection;

        });

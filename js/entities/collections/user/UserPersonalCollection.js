define([
    'entities/collections/BaseCollection',
    'entities/models/user/UserPersonalModel',
    'globalConfig'
],
        function(BaseColl, UserPersonalModel, GC) {

            var userPersonalCollection = BaseColl.extend({
                model: UserPersonalModel,
                url: function() {
                    return GC.api.url + "SC/user/personal/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return moment(model.get('eff_date'), 'MM/DD/YYYY');
                }

            });

            return userPersonalCollection;

        });

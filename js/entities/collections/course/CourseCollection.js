define([
    'entities/collections/BaseCollection',
    'entities/models/course/CourseModel',
    'globalConfig'
],
        function(BaseColl, CourseModel, GC) {

            var catalogCollection = BaseColl.extend({
                model: CourseModel,
                url: function() {
                    return GC.api.url + "CourseBuilder/CourseCatalog/";
                },
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return catalogCollection;

        });

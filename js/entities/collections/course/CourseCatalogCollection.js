define([
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(BaseColl, BaseModel, GC) {

            var CourseCatalogCollection = BaseColl.extend({
                model: BaseModel,
                url: function() {
                    return GC.api.url + "CourseBuilder/CourseCatalog/Public/";
                },
                comparator: function(model) {
                    return model.get('value');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 120000 //2 minute (2 * 60000)
                },
                
                getLetters: function(){
                    this.stage = 'letters';
                    this.fetch({reset: true});
                },
                
                getSubjects: function(letter){
                    this.stage = 'subjects';
                    this.fetch({reset: true}, {letter: letter}, {}, false);
                },
                
                getCourses: function(letter, subj){
                    this.stage = 'courses';
                    this.fetch({reset: true}, {letter: letter, subj: subj}, {}, false);
                },
                
                parse: function(response){
                    switch (this.stage) {
                    case 'letters':
                        response = this.parseLetters(response);
                        break;
                    case 'subjects':
                        response = this.parseSubjects(response);
                        break;
                    case 'courses':
                        response = this.parseCourses(response);
                        break;
                    default:
                        console.warn('CourseCatalogCollection default switch');
                        return response;
                    }
                    return response;
                },
                
                parseCourses: function(response){
                    var newR = [];
                    _.each(response, function(m, i){
                        _.each(m, function(dtl){
                            var thisM = {};
                            
                            thisM.value = dtl.course_catalog_id;
                            thisM.details = dtl.title;
                            thisM.model = dtl;
                            
                            newR.push(thisM);
                        });
                    });
                    return newR;
                },
                
                parseSubjects: function(response){
                    var newR = [];
                    _.each(response, function(m, i){
                        var thisM = {};
                        thisM.value = i;
                        thisM.details = '';
                        _.each(m, function(dtl){
                            thisM.details = thisM.details + dtl.title + ', ';
                        });
                        //remove last comma
                        thisM.details = thisM.details.replace(/, +$/, "");
                        newR.push(thisM);
                    });
                    return newR;
                },
                
                parseLetters: function(response){
                    var newR = [];
                    _.each(response, function(m, i){
                        var thisM = {};
                        thisM.value = i;
                        thisM.details = '';
                        _.each(m, function(dtl){
                            thisM.details = thisM.details + dtl.subject + ', ';
                        });
                        //remove last comma
                        thisM.details = thisM.details.replace(/, +$/, "");
                        newR.push(thisM);
                    });
                    return newR;
                }

            });

            return CourseCatalogCollection;

        });

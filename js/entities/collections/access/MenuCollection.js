define([
    'underscore',
    'backbone',
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, BaseColl, BaseModel, GC) {

            var MenuCollection = BaseColl.extend({
                model: BaseModel,
                //localStorage
                localStorage: {
                    isStoredInLocalStorage: false,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "get_navigation/";
                },
                parse: function(resp) {
                    return resp.menu;
                },
                fetch: function() {

                    this.reset(
                            [{
                                    Name: 'Dashboard',
                                    Link: 'dashboard'
                                },
                                {
                                    Name: 'Admin Dashboard',
                                    Link: 'adminDashboard'
                                },
                                {
                                    Name: 'Enroll',
                                    Link: 'enroll'
                                },
                                {
                                    Name: 'Foundation Builder',
                                    Link: '',
                                    Menus: [{
                                            Name: 'Institution',
                                            Link: 'fbuilder/institution'
                                        }, {
                                            Name: 'College',
                                            Link: 'fbuilder/college'
                                        }, {
                                            Name: 'Department',
                                            Link: 'fbuilder/department'
                                        }, {
                                            Name: 'Classification',
                                            Link: 'fbuilder/classification'
                                        }]
                                },
                                {
                                    Name: 'Structure Builder',
                                    Link: '',
                                    Menus: [{
                                            Name: 'Grade Type',
                                            Link: 'sbuilder/grades'
                                        }, {
                                            Name: 'Terms Offered',
                                            Link: 'sbuilder/terms'
                                        }, {
                                            Name: 'Delivery Type',
                                            Link: 'sbuilder/delivery'
                                        }, {
                                            Name: 'Component',
                                            Link: 'sbuilder/component'
                                        }, {
                                            Name: 'Subjects',
                                            Link: 'sbuilder/subjects'
                                        }]
                                },
                                {
                                    Name: 'Organization Builder',
                                    Link: 'obuilder/builder'
                                },
                                {
                                    Name: 'Course Catalog Builder',
                                    Link: 'cbuilder/catalog'
                                },
                                {
                                    Name: 'Class Builder',
                                    Link: 'csbuilder/class'
                                },
                                {
                                    Name: 'Path Builder',
                                    Link: 'path/builder'
                                },
                                {
                                    Name: 'Roster',
                                    Link: 'roster'
                                },
                                {
                                    Name: 'Prospects',
                                    Link: 'admissions/prospects'
                                },
                                {
                                    Name: 'Security',
                                    Link: '',
                                    Menus: [{
                                            Name: 'Assignment',
                                            Link: 'security/assign'
                                        }, {
                                            Name: 'Role Builder',
                                            Link: 'security/roleBuilder'
                                        }]
                                },
                                {
                                    Name: 'Enterprise Manager',
                                    Link: '',
                                    Menus: [{
                                            Name: 'Colors',
                                            Link: 'enterman/colors'
                                        }, {
                                            Name: 'Logos',
                                            Link: 'enterman/images'
                                        }]
                                }
                            ]);

                    this.trigger('reset');
                }


            });

            return MenuCollection;

        });

define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/component/ComponentModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, ComponentModel, GC) {

            var ComponentCollection = BaseColl.extend({
                model: ComponentModel,
                url: function() {
                    return GC.api.url + "Generic/Component/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return ComponentCollection;

        });

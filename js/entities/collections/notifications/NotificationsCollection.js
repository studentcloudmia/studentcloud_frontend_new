define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/notifications/NotificationModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, NotificationModel, GC) {

            var NotificationsCollection = BaseColl.extend({
                model: NotificationModel,
                url: function() {
                    return GC.api.url + "MessageCenter/Message/get_alert_notifications/";
                }

            });

            return NotificationsCollection;

        });

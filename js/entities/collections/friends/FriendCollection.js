define([
    'entities/collections/BaseCollection',
    'entities/models/friends/FriendModel',
    'globalConfig'
],
        function(BaseColl, FriendModel, GC) {

            var FriendCollection = BaseColl.extend({
                model: FriendModel,
                url: function() {
                    return GC.api.url + "Friends/";
                },
                comparator: function(model) {
                    return model.get('uid');

                }
            });
            return FriendCollection;
        });

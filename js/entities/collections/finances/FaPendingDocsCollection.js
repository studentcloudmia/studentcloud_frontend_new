define([
'underscore',
'backbone',
'entities/collections/BaseCollection',
'entities/models/finances/FaPendingDocModel',
'globalConfig'
],
function(_, Backbone, BaseColl, FaPendingDocModel, GC) {

    var FaPendingDocsCollection =BaseColl.extend({

        model:FaPendingDocModel,
    
        initialize: function() {

		},

        url: function() {
            return GC.api.url + "api/finaid/pendingdoc/";
        },
        
        fetch: function(){
           
           this.add([
                {Doc: 'Selective Service Registration Form'},
                {Doc: 'Form 2'},
                {Doc: 'Form 1'}
              ],{silent:true});

           this.trigger('reset');
        }
	
    });

    return FaPendingDocsCollection;

});

define(['underscore', 'backbone', 'entities/collections/class/ClassColl', 'globalConfig'],

function(_, Backbone, ClassColl, GC) {

    var FaveCollection = ClassColl.extend({

        url: function() {
            return GC.api.url + "Enrollment/FavoriteClass/";
        }

    });

    return FaveCollection;

});

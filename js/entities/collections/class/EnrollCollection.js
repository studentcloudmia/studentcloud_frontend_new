define(['underscore', 'backbone', 'entities/collections/BaseCollection', 'entities/models/class/EnrollModel', 'globalConfig'],

function(_, Backbone, BaseColl, EnrollModel, GC) {

    var EnrollCollection = BaseColl.extend({

        model: EnrollModel,

        url: function() {
            return GC.api.url + "Enrollment/enroll/";
        },
        
        parse: function(resp){
            //remove dropped classes
            var newResp = [];
            _.each(resp, function(cls, index){
                if(cls.status != "Drop"){
                    newResp.push(cls);
                }
            });
            return newResp;
        },
        
        addClass: function(classJSON){
            var tClass = new this.model(classJSON);
            this.add(tClass);
        }

    });

    return EnrollCollection;

});
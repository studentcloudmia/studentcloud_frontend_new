define(['underscore', 'backbone', 'entities/collections/class/ClassColl', 'globalConfig'],

function(_, Backbone, ClassColl, GC) {

    var SearchCollection = ClassColl.extend({

        url: function() {
            return GC.api.url + "Enrollment/search/";
            //?campus=string&course_catalog_id=string&can_enroll=1&start_time=14:00:00&end_time15:00:00&days=33,34&title=string
        }

    });

    return SearchCollection;

});

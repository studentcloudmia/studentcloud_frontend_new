define(['underscore', 'backbone', 'entities/collections/BaseCollection', 'entities/models/class/EnrollModel', 'globalConfig'],

function(_, Backbone, BaseColl, EnrollModel, GC) {

    var AutoEnrollCollection = BaseColl.extend({

        model: EnrollModel,

        url: function() {
            return GC.api.url + "Enrollment/suggested/criteria/";
            //?campus=string&course_catalog_id=string&can_enroll=1&start_time=14:00:00&end_time15:00:00&days=33,34&title=string
        }

    });

    return AutoEnrollCollection;

});

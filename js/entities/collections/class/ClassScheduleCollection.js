define([
    'entities/collections/BaseCollection',
    'entities/models/class/ClassScheduleModel',
    'globalConfig'
],
        function(BaseColl, ClassScheduleModel, GC) {

            var ClassScheduleCollection = BaseColl.extend({
                model: ClassScheduleModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/SClass/";
                }

            });

            return ClassScheduleCollection;

        });

define([
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(BaseColl, BaseModel, GC) {

            var ClassTopicsCollection = BaseColl.extend({
                model: BaseModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/Topic/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('description');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return ClassTopicsCollection;

        });

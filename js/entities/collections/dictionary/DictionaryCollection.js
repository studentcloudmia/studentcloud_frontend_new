define([
    'entities/collections/BaseCollection',
    'entities/models/dictionary/DisctionaryModel',
    'globalConfig'
],
        function(BaseColl, DictionaryModel, GC) {

            var dictionaryCollection = BaseColl.extend({
                model: DictionaryModel,
                dictType: '',
                url: function() {
                    return GC.api.url + "Dictionary/";
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                comparator: function(model) {
                    switch (this.dictType) {
                        case 'getMapColl':
                            return model.get('MAJORMAP');
                            break;
                        default:
                            return model.get('id');
                    }
                },
                getPrefix: function() {
                    this.fetch({}, {type: 'personal', val: 'prefix'}, {}, false);
                },
                getSuffix: function() {
                    this.fetch({}, {type: 'personal', val: 'suffix'}, {}, false);
                },
                getMaritalStatus: function() {
                    this.fetch({}, {type: 'personal', val: 'marital'}, {}, false);
                },
                getGender: function() {
                    this.fetch({}, {type: 'personal', val: 'sex'}, {}, false);
                },
                getVisaTypes: function() {
                    this.fetch({}, {type: 'personal', val: 'visatype'}, {}, false);
                },
                getVisaStatus: function() {
                    this.fetch({}, {type: 'personal', val: 'visastatus'}, {}, false);
                },
                getEthnicity: function() {
                    this.fetch({}, {type: 'personal', val: 'ethinicity'}, {}, false);
                },
                getVeteranStatus: function() {
                    this.fetch({}, {type: 'personal', val: 'veteranstatus'}, {}, false);
                },
                getCitizenshipStatus: function() {
                    this.fetch({}, {type: 'personal', val: 'citizenshipstatus'}, {}, false);
                },
                getAddressType: function() {
                    this.fetch({}, {type: 'personal', val: 'address'}, {}, false);
                },
                getPhoneType: function() {
                    this.fetch({}, {type: 'personal', val: 'phone'}, {}, false);
                },
                getDays: function() {
                    this.fetch({reset: true}, {type: 'generic', val: 'day'}, {}, false);
                },
                getMapColl: function() {
                    //set this.dictType for comparator
                    this.dictType = 'getMapColl';
                    this.fetch({reset: true}, {type: 'generic', val: 'mapcoll'}, {}, false);
                },
                getClassStatus: function() {
                    this.fetch({reset: true}, {type: 'generic', val: 'classstatus'}, {}, false);
                },
                getNotificationType: function() {
                    this.fetch({reset: true}, {type: 'generic', val: 'messagetype'}, {}, false);                    
                },
                getMessageCenterType: function() {
                    this.fetch({reset: true}, {type: 'generic', val: 'message_center'}, {}, false);                    
                }

            });

            return dictionaryCollection;

        });

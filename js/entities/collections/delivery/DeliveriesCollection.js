define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/delivery/DeliveryModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, DeliveryModel, GC) {

            var DeliveryCollection = BaseColl.extend({
                model: DeliveryModel,
                url: function() {
                    return GC.api.url + "Generic/Delivery/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return DeliveryCollection;

        });

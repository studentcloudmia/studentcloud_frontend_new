define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/delivery/DeliveryModelList',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, DeliveryModel, GC) {

            var DeliveryCollection = BaseColl.extend({
                model: DeliveryModel,
                url: function() {
                    return GC.api.url + "Generic/Delivery/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return DeliveryCollection;

        });

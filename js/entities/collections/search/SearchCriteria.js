define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, BaseModel, GC) {

            var SearchCriteriaCollection = BaseColl.extend({
                model: BaseModel,
                url: function() {

                    return GC.api.url + "searchCriteria/";
                },
                fetch: function() {

                    this.add([
                        {
                            id: 'first_name',
                            description: 'First Name'
                        },
                        {
                            id: 'last_name',
                            description: 'Last Name'
                        },
                        {
                            id: 'bio',
                            description: 'Bio'
                        },
                        {
                            id: 'work',
                            description: 'Work'
                        },
                        {
                            id: 'school',
                            description: 'School'
                        },
                        {
                            id: 'email',
                            description: 'Email'
                        },
                        {
                            id: 'source',
                            description: 'Source'
                        },
                        {
                            id: 'team',
                            description: 'Team'
                        }

                    ], {
                        silent: true
                    });

                    this.trigger('reset');
                },
                comparator: function(model) {
                    return model.get('description');
                }
            });

            return SearchCriteriaCollection;

        });

define([
'underscore',
'backbone',
'entities/collections/BaseCollection',
'entities/models/BaseModel',
'globalConfig'
],
function(_, Backbone, BaseColl, BaseModel, GC) {

    var UserGroupCollection = BaseColl.extend({
        //use defualt BaseModel
        model: BaseModel,
        
        url: function() {
            //ensure we have userid before returning url
            if(this.userid != null && typeof this.userid == "string" && this.userid.length > 0)
                return GC.api.url + "usergroups/" + this.userid;
            else
                throw new Error("No userid specified.");
        },
        
        getUserGroups: function(userid){
            //save userid before fetch
            this.userid = userid;
            this.fetch({},{},{},false);
        }
	
    });

    return UserGroupCollection;

});

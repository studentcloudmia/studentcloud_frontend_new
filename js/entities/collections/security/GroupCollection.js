define([
'underscore',
'backbone',
'entities/collections/BaseCollection',
'entities/models/security/GroupModel',
'globalConfig'
],
function(_, Backbone, BaseColl, GroupModel, GC) {

    var GroupCollection = BaseColl.extend({

        model: GroupModel,
        
        url: function() {
                return GC.api.url + "group/";
        }
	
    });

    return GroupCollection;

});

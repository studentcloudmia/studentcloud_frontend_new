define([
    'entities/collections/BaseCollection',
    'entities/models/oBuilder/OBuilderModel',
    'globalConfig'
],
        function(BaseColl, OBuilderModel, GC) {

            var OBuilderCollection = BaseColl.extend({
                model: OBuilderModel,
                url: function() {
                    return GC.api.url + "OrgBuilder/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return OBuilderCollection;

        });

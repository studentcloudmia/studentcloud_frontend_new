define([
	'underscore',
	'backbone',
	'entities/collections/BaseCollection',
	'entities/models/admissions/SectionModel',
	'app'
], function(_, Backbone, BaseColl, SectionModel, App) {
  
	var SectionsCollection = BaseColl.extend({
		
		model: SectionModel,
		
		url : function() {
	        return App.api.url + "public/application/sectiondetails/";
	    }
	
	});

	return SectionsCollection;

});

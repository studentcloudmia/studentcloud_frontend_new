define([
    'entities/collections/BaseCollection',
    'entities/models/admissions/prospects/ProspectsModel',
    'globalConfig'
],
        function(BaseColl, ProspectsModel, GC) {

            var ProspectsCollection = BaseColl.extend({
                model: ProspectsModel,
                url: function() {
                    return GC.api.url + "Social/search/";
                },
                comparator: function(model) {
                    return -model.get('id');

                }
            });
            return ProspectsCollection;
        });

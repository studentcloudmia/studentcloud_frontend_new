define([
    'entities/collections/BaseCollection',
    'entities/models/department/DepartmentModel',
    'globalConfig'
],
        function(BaseColl, DepartmentModel, GC) {

            var DepartmentCollection = BaseColl.extend({
                model: DepartmentModel,
                url: function() {
                    return GC.api.url + "Generic/Department/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }
            });

            return DepartmentCollection;

        });

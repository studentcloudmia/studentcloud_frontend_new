define([
    'entities/collections/BaseCollection',
    'entities/models/department/DepartmentModelList',
    'globalConfig'
],
        function(BaseColl, DepartmentModel, GC) {

            var DepartmentCollection = BaseColl.extend({
                model: DepartmentModel,
                url: function() {
                    return GC.api.url + "Generic/Department/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return DepartmentCollection;

        });

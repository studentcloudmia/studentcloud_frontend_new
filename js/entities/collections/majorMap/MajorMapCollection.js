define([
    'entities/collections/BaseCollection',
    'entities/models/majorMap/MajorMapModel',
    'globalConfig'
],
        function(BaseColl, MajorMapModel, GC) {

            var MajorMapCollection = BaseColl.extend({
                model: MajorMapModel,
                url: function() {
                    return GC.api.url + "MajorMap/MajorMap/";
                },
                
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return moment(model.get('eff_date'), 'MM/DD/YYYY');
                }

            });

            return MajorMapCollection;

        });

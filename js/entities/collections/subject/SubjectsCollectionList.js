define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/subject/SubjectModelList',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, SubjectModel, GC) {

            var SubjectCollection = BaseColl.extend({
                model: SubjectModel,
                url: function() {
                    return GC.api.url + "Generic/Subject/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return SubjectCollection;

        });

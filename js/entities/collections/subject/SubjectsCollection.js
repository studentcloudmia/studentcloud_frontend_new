define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/subject/SubjectModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, SubjectModel, GC) {

            var SubjectCollection = BaseColl.extend({
                model: SubjectModel,
                url: function() {
                    return GC.api.url + "Generic/Subject/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }
            });

            return SubjectCollection;

        });

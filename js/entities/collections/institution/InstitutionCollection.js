define([
    'entities/collections/BaseCollection',
    'entities/models/institution/InstitutionModel',
    'globalConfig'
],
        function(BaseColl, InstitutionModel, GC) {

            var institutionCollection = BaseColl.extend({
                model: InstitutionModel,
                url: function() {
                    return GC.api.url + "Generic/Institution/";
                },
                //TODO: extend from effectivedated collection

                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }

            });

            return institutionCollection;

        });

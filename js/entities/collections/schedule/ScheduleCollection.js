define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/schedule/ScheduleModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, ScheduleModel, GC) {

            var ScheduleCollection = BaseColl.extend({
                model: ScheduleModel,
                url: function() {
                    return GC.api.url + "Enrollment/InstructorClass/";
                }

            });

            return ScheduleCollection;

        });

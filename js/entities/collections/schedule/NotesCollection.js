define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/schedule/NoteModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, NoteModel, GC) {

            var NotesCollection = BaseColl.extend({
                model: NoteModel,
                url: function() {
                    return GC.api.url + "SC/UserNotes/";
                }

            });

            return NotesCollection;

        });

define([
    'entities/collections/BaseCollection',
    'entities/models/BaseModel',
    'globalConfig'
],
        function(BaseColl, BaseModel, GC) {

            var lmsProviderCollection = BaseColl.extend({
                model: BaseModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/LMS_Provider/";
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }

            });

            return lmsProviderCollection;

        });

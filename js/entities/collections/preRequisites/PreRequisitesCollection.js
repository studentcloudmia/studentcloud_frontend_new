define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/preRequisites/PreRequisiteModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, PreRequisiteModel, GC) {

            var PreRequisiteCollection = BaseColl.extend({
                model: PreRequisiteModel,
                url: function() {
                    return GC.api.url + "sbuilder/term/";
                }

            });

            return PreRequisiteCollection;

        });

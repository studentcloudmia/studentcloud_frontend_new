define([
    'underscore',
    'backbone',
    'entities/collections/BaseCollection',
    'entities/models/address/PhoneTypeModel',
    'globalConfig'
],
        function(_, Backbone, BaseColl, PhoneTypeModel, GC) {

            var phoneTypeCollection = BaseColl.extend({
                model: PhoneTypeModel,
                initialize: function() {
                    //update constants
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "dictionary/personal/phone/";
                },
                comparator: function(model) {
                    return model.get('PHONE');
                }

            });

            return phoneTypeCollection;

        });

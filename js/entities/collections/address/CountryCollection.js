define([
    'underscore',
    'backbone',
    'entities/collections/BaseCollection',
    'entities/models/address/CountryModel',
    'globalConfig'
],
        function(_, Backbone, BaseColl, CountryModel, GC) {

            var countryCollection = BaseColl.extend({
                model: CountryModel,
                initialize: function() {
                    //update constants
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "Generic/Country/";
                },
                comparator: function(model) {
                    return model.get('Description_Long');
                }

            });

            return countryCollection;

        });

define([
    'underscore',
    'backbone',
    'entities/collections/BaseCollection',
    'entities/models/address/StateModel',
    'globalConfig'
],
        function(_, Backbone, BaseColl, StateModel, GC) {

            var stateCollection = BaseColl.extend({
                model: StateModel,
                initialize: function() {
                    //update constants
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "Generic/Country/";
                },
                comparator: function(model) {
                    return model.get('Description_Short');
                }


            });

            return stateCollection;

        });

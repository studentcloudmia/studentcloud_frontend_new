define([
    'underscore',
    'backbone',
    'entities/collections/BaseCollection',
    'entities/models/address/AddressTypeModel',
    'globalConfig'
],
        function(_, Backbone, BaseColl, AddressTypeModel, GC) {

            var addressTypeCollection = BaseColl.extend({
                model: AddressTypeModel,
                initialize: function() {
                    //update constants
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                url: function() {
                    return GC.api.url + "dictionary/personal/address/";
                },
                comparator: function(model) {
                    return model.get('ADDRESS');
                }

            });

            return addressTypeCollection;

        });

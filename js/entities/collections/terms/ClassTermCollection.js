define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/terms/TermModelList',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, TermModelList, GC) {

            var ClassTermCollection = BaseColl.extend({
                model: TermModelList, //base model

                url: function() {
                    return GC.api.url + "ClassBuilder/ClassTerm/";
                },
                comparator: function(model) {
                    return -model.get('start_date');
                }
            });

            return ClassTermCollection;

        });

define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/terms/TermModelList',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, TermModel, GC) {

            var TermsCollection = BaseColl.extend({
                model: TermModel,
                url: function() {
                    return GC.api.url + "Generic/Term/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -model.get('Description_Long');
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return TermsCollection;

        });

define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/terms/TermModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, TermModel, GC) {

            var TermsCollection = BaseColl.extend({
                model: TermModel,
                url: function() {
                    return GC.api.url + "Generic/Term/";
                },
                //TODO: extend from effectivedated collection
                comparator: function(model) {
                    return -moment(model.get('eff_date'), 'YYYY-MM-DD');
                }
            });

            return TermsCollection;

        });

define([
    'underscore',
    'backbone',
    'moment',
    'entities/collections/BaseCollection',
    'entities/models/textbook/TextbookModel',
    'globalConfig'
],
        function(_, Backbone, moment, BaseColl, TextbookModel, GC) {

            var TextbookCollection = BaseColl.extend({
                model: TextbookModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/Textbook/";
                }
            });

            return TextbookCollection;

        });

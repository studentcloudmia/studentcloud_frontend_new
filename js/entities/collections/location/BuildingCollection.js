define([
    'entities/collections/BaseCollection',
    'entities/models/location/BuildingModel',
    'globalConfig'
],
        function(BaseColl, BuildingModel, GC) {

            var buildingCollection = BaseColl.extend({
                model: BuildingModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/Building/";
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return buildingCollection;

        });

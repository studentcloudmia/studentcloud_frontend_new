define([
    'entities/collections/BaseCollection',
    'entities/models/location/RoomModel',
    'globalConfig'
],
        function(BaseColl, RoomModel, GC) {

            var roomCollection = BaseColl.extend({
                model: RoomModel,
                url: function() {
                    return GC.api.url + "ClassBuilder/Building/room/";
                },
                localStorage: {
                    isStoredInLocalStorage: true,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                }
            });

            return roomCollection;

        });

define(['jquery', 'underscore', 'backbone', 'vent', 'globalConfig', 'entities/_base/when_fetch'],
        function($, _, Backbone, vent, GC) {
            //BaseCollection
            var BaseCollection = Backbone.Collection.extend({
                _xhr: [],
                initialize: function() {
                    this._xhr = [];
                    this.on('add', this.addedModel, this);
                    this.on('reset', this.collReset, this);
                },
                //localStorage
                localStorage: {
                    isStoredInLocalStorage: false,
                    maxRefresh: 300000 //5 minutes (5 * 60000)
                },
                //return key to be used for local storage
                getKey: function() {
                    return _.result(this, 'url');
                },
                fetch: function(options, params, queries, abort) {
                    // build new params
                    var newUrl = '';
                    if (params) {
                        $.each(params,
                                function(key, val) {
                                    newUrl += val + '/';
                                });
                    }

                    // newUrl = this.url() + newUrl;
                    newUrl = _.result(this, 'url') + newUrl;

                    // build new queries
                    var urlQueries = '';
                    if (queries) {
                        var i = 0;
                        $.each(queries,
                                function(key, val) {
                                    if (i == 0) {
                                        urlQueries += '?' + key + '=' + val;
                                    } else {
                                        urlQueries += '&' + key + '=' + val;
                                    }

                                    i++;
                                });
                        newUrl += urlQueries;
                    }

                    // update collection url
                    if (options) {
                        options.url = newUrl;
                    } else {
                        options = {
                            url: newUrl
                        };
                    }

                    // abort stored xhr
                    if (abort) {
                        this.abort();
                    }

                    //update getKey for local storage
                    this.getKey = options.url;

                    // store xhr to access if needed
                    var currentFetch = Backbone.Collection.prototype.fetch.call(this, options);
                    this._xhr.push(currentFetch);

                    // return xhr
                    return currentFetch;
                },
                find: function(coll, val) {

                },
                
                /******************************************************************
                *****************collection navigation functions******************
                ******************************************************************/                
                nextModel: function(model){
                    console.log('calling nextModel');
                    var i = this.indexOf(model);
                    i = i + 1;
                    if( i != this.models.length ){
                        return this.at(i);
                    }
                    return false;
                },
                
                prevModel: function(model){
                    console.log('calling prevModel');
                    var i = this.indexOf(model);
                    i = i - 1;
                    if( i > -1 ){
                        return this.at(i);
                    }
                    return false;
                },
                
                hasNext: function(model){
                    var i = this.indexOf(model);
                    i = i + 1;
                    if( i != this.models.length ){
                        return true;
                    }
                    return false;
                },
                
                hasPrev: function(model){
                    var i = this.indexOf(model);
                    i = i - 1;
                    if( i > -1 ){
                        return true;
                    }
                    return false;
                },
                /*models by params*/
                findModelByParam: function(model, param, extra){
                    _.defaults(extra, false);
                    var self = this;
                    var retModel = false;
                    var indexSeen = -1;
                    _.each(this.models, function(currModel, index){
                        if(currModel.get(param) == model.get(param)){
                            retModel = currModel;
                            indexSeen = index;
                        }
                    });
                    
                    if(!extra){
                        return retModel;
                    }
                    
                    return {model: retModel, index: indexSeen};
                },
                
                nextModelByParam: function(model, param){
                    var i = this.findModelByParam(model, param, true).index;
                    i = i + 1;
                    if( i != this.models.length ){
                        return this.at(i);
                    }
                    return false;
                },
                
                prevModelByParam: function(model, param){
                    var i = this.findModelByParam(model, param, true).index;
                    i = i - 1;
                    if( i > -1 ){
                        return this.at(i);
                    }
                    return false;
                },
                
                hasNextByParam: function(model, param){
                    var i = this.findModelByParam(model, param, true).index;
                    i = i + 1;
                    if( i != this.models.length ){
                        return true;
                    }
                    return false;
                },
                
                hasPrevByParam: function(model, param){
                    var i = this.findModelByParam(model, param, true).index;
                    i = i - 1;
                    if( i > -1 ){
                        return true;
                    }
                    return false;
                },
                /******************************************************************
                ***************END collection navigation functions*****************
                ******************************************************************/
                
                
                abort: function() {
                    if (!this._xhr > 0)
                        return;

                    this._xhr.forEach(function(element, index, array) {
                        array[index].abort();
                    });

                    this._xhr = [];
                },
                collReset: function() {
                    //loop through collection and call addedModel
                    var self = this;
                    _.each(this.models, function(model) {
                        self.addedModel(model);
                    });
                },
                addedModel: function(model) {
                    //change model url to be same as collection
                    model.url = _.result(this, 'url');
                },
                syncStart: function() {
                    vent.trigger('Components:Loader:Show');
                },
                syncStop: function() {
                    vent.trigger('Components:Loader:Hide');
                },
                /********************************************
                 SYNC override
                 Override sync function to use local storage
                 *********************************************/
                sync: function(method, model, options) {
                    _.defaults(options, {loader: true});
                    //use loader by default
                    if(options.loader){
                        this.listenToOnce(this, 'sync:start', this.syncStart, this);
                        this.listenToOnce(this, 'sync:stop', this.syncStop, this);  
                    }                    
                    
                    var key, now, timestamp, refresh;
                    if (method === 'read' && this.localStorage.isStoredInLocalStorage) {
                        // only override sync if it is a fetch('read') request
                        key = _.result(this, 'getKey');
                        if (key) {
                            now = new Date()
                                    .getTime();
                            timestamp = GC.localStorage.get(key + ":timestamp");
                            refresh = options.forceRefresh;
                            if (refresh || !timestamp || ((now - timestamp) > this.localStorage.maxRefresh)) {
                                // make a network request and store result in local storage
                                var success = options.success;
                                options.success = function(resp, status, xhr) {
                                    // check if this is an add request in which case append to local storage data instead of replace
                                    if (options.add && resp.values) {
                                        // clone the response
                                        var newData = JSON.parse(JSON.stringify(resp));
                                        // append values
                                        var prevData = GC.localStorage.get(key);
                                        newData.values = prevData.values.concat(resp.values);
                                        // store new data in local storage
                                        GC.localStorage.set(key, newData);
                                    } else {
                                        // store resp in local storage
                                        GC.localStorage.set(key, resp);
                                    }
                                    var now = new Date()
                                            .getTime();
                                    GC.localStorage.set(key + ":timestamp", now);
                                    success(resp, status, xhr);
                                };
                                // call normal backbone sync
                                Backbone.sync(method, model, options);
                            } else {
                                // provide data from local storage instead of a network call
                                var data = GC.localStorage.get(key);
                                // simulate a normal async network call
                                setTimeout(function() {
                                    options.success(data, 'success', null);
                                }, 0);
                            }
                        }
                    } else {
                        Backbone.sync(method, model, options);
                    }
                }

            });
            return BaseCollection;
        });

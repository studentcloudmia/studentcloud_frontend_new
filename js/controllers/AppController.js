// AppController.js
define([
	'marionette', 
	'app', 
	'views/security/LoginItemView', 
	'views/dashboard/DashboardLayoutManager', 
	'views/sidebar/SidebarLayoutManager'
	],
function(Marionette, app, LoginItemView, DashboardLayoutManager, SidebarLayoutManager) {
    var AppController = Marionette.Controller.extend({

        initialize: function(options) {
            
        },

        login: function() {
			//show login view
			app.sidebar.close();
			app.main.show(new LoginItemView());
        },

        dashboard: function() {
			//call DashboardLayoutView init
			DashboardLayoutManager.init();
			//call SidebarLayoutManager init
			SidebarLayoutManager.init();
        },

        catchAll: function() {
			console.log('catchAll Function');
        }

    });
    return AppController;
});
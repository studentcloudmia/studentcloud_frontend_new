#!/usr/bin/env python

import os
import sys
import commands

if len(sys.argv)!=2:
    print 'Invalid syntax: %s <type>'%sys.argv[0]
    print 'Type: '
    print '     1 - Test deployment'
    print '     2 - Dev  deployment'
    sys.exit(-1)


tag = 'TEST' if sys.argv[1]=="1" else 'DEV'

print 'Deploying %s release'%tag

git_root = '/git/innoware_ui_dev_%s'%tag
print 'Using root: %s'%git_root

os.chdir(git_root)

print 'Updating git branch'
print commands.getstatusoutput('git stash')
print commands.getstatusoutput('git stash clear')
git_sync_cmd  = 'git pull --rebase'
print commands.getstatusoutput(git_sync_cmd)


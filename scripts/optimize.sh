#!/bin/bash

if [ "$#" -ne 1 ];then
    echo "Please use TEST or DEV as an argument"
    exit;
fi

cd /git/innoware_ui_dev_$1/optimize

#get current git branch
branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
branch_name=${branch_name:-HEAD}

echo "*********************************************************"
echo "******* Currently working in branch: $branch_name ********"
echo "*********************************************************"

config_name="globalConfig_LOCAL"
rename_htaccess="No"
#determine what config to use
if [ "$branch_name" = "development" ]; then
	config_name="globalConfig_DEV"
    rename_htaccess="Yes"
elif [ "$branch_name" = "master" ]; then
	config_name="globalConfig_TEST"
    rename_htaccess="Yes"
elif [ "$branch_name" = "gold" ]; then
	config_name="globalConfig_GOLD"
    rename_htaccess="Yes"
fi
echo "Using config: $config_name"
#remove the globalConfig file
echo "Removing OLD globalConfig"
cd ../js
rm globalConfig.js
echo "Replacing globalConfig.js with $config_name"
#copy new globalConfig file
cp globalConfigs/$config_name.js globalConfig.js

if [ "$rename_htaccess" = "Yes" ]; then
    echo "*********************************************************"
    echo "**************** Renaming .htaccess file******************"
    echo "*********************************************************"
    cd ../
    cp .htaccess_copy .htaccess
    cd js
fi

echo "Start optimizing JavaScript files"
#start optimization code
cd ../optimize
node r.js -o app.build.js

echo "Start optimizing CSS files"
node r.js -o cssIn=../css/styles.css out=../css/styles-built.css
lessc ../css/mInnowareStyles.less > ../css/mInnowareStyles.min.css



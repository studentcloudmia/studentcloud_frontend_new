#!/usr/bin/env python

import sys
import commands

if len(sys.argv)!=2:
    print 'Invalid syntax: %s <type>'%sys.argv[0]
    print 'Type: '
    print '     1 - Test deployment'
    print '     2 - Dev  deployment'
    sys.exit(-1)

apache_user = 'www-data'
sudo_user   = 'ubuntu'

tag = 'TEST' if sys.argv[1]=="1" else 'DEV'

print 'Deploying %s release'%tag

#Need to add ec2test2 to your .ssh config
ssh_ec2_nodes = [
    'ec2test',
    'ec2test2',
]

# Common commands for all nodes
for ssh_ec2_node in ssh_ec2_nodes:
    print commands.getstatusoutput('ssh %s@%s /usr/bin/sync_git.py %d'%(apache_user, ssh_ec2_node, int(sys.argv[1])))

# special commands for UI
commands.getstatusoutput('ssh %s@ec2ui /usr/bin/optimize.sh %s'%(apache_user, tag))

# Common commands for all nodes
for ssh_ec2_node in ssh_ec2_nodes:
    print commands.getstatusoutput('ssh %s@%s sudo service apache2 restart'%(sudo_user, ssh_ec2_node))
    

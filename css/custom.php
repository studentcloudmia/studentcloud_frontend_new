<?php
header('Content-type: text/css');
//$data = array("name" => "Hagrid", "age" => "36");
//$data_string = json_encode($data);

//Grab the domain in the url
$url = 'localhost';//$_SERVER['HTTP_HOST'];

try {
	//This creates a new PDO object PDO also protects from sql injection
	$db = new PDO('mysql:host=configdb.c2cqinswokul.us-east-1.rds.amazonaws.com;port=3306;dbname=config', 'agonzalez', 'Baseba662$');
	//Build your sql query with parameters :field 
	$sql = "SELECT api_key FROM schools WHERE url =:field";
	//Prepare the above query
	$statement = $db->prepare($sql);
	//Bind the value received from the url or such with the parameter place holder
	$statement->bindParam(':field', $url);
	//Execute the prepared query
	$statement->execute();
	//Fetch the result
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
} catch (Exception $e) {
	//echo 'Caught exception: ',  $e->getMessage(), "\n";
	die();
}


//If nothing is found stop running anything else
if(count($result) <= 0){
	die();
}

//Get the API Key from the array
$api_key = $result[0]["api_key"];


//Make a curl request to the api server
$ch = curl_init('https://test.studentcloud.com/api/enterprisemanager/config/colors/'); 
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                               
//curl_setopt($ch,  CURLOPT_HTTPGET, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);                                                                
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//Set the timeout for 7 seconds it should not take longer than this to get the config values from the api server
curl_setopt($ch, CURLOPT_TIMEOUT, 7);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "x-api-key: $api_key",
    "Accept: application/json"));                                                                                            

//Grab config values
$result = curl_exec($ch);

// Check if any error occurred
if(curl_errno($ch))
{
    //echo 'Curl error: ' . curl_error($ch);
	die();
}

$result = json_decode($result, true);

$counter = 0;


//Dynamically create the css 
foreach ($result as $v1 => $value) {
$v1 = substr($v1, 0, -1);
echo $v1 . "{ ";
    foreach ($value as $key => $value) {
		if($key == "PropertyName"){
			echo "$value: ";
		}
		elseif($key !== "Type" and $key !== "PropertyName"){
			echo "$value; }\n";
		}
		

		
		//Need to change all of the other styles that go with 
		if($v1 == ".sidebarContent"){		
			if($key !== "Type" and $key !== "PropertyName"){
				echo "#slide { background-color: $value; }\n";
			}
		}
		
		
	}
}


//Make a curl request to the api server
$ch = curl_init('https://test.studentcloud.com/api/enterprisemanager/config/images/'); 
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                               
//curl_setopt($ch,  CURLOPT_HTTPGET, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false );
//Set the timeout for 7 seconds it should not take longer than this to get the config values from the api server
curl_setopt($ch, CURLOPT_TIMEOUT, 7);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "x-api-key: $api_key",
    "Accept: application/json"));                                                                                            

//Grab config values
$result = curl_exec($ch);

// Check if any error occurred
if(curl_errno($ch))
{
    //echo 'Curl error: ' . curl_error($ch);
	die();
}

$result = json_decode($result, true);

$counter = 0;

//var_dump($result);

//Dynamically create the css 
foreach ($result as $v1 => $value) {
$v1 = substr($v1, 0, -1);
echo $v1 . "{ ";
    foreach ($value as $key => $value) {
		
		if($key !== "Type" and $key !== "PropertyName"){
			echo "background-image: url('$value'); }\n";
			
		}
		
			
	}
		
}

?>
